<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class VerificationToken extends Model {

		/**
		 * The database table used by the model.
		 *
		 * @var string
		 */
		protected $table = 'verification_tokens';

		/**
		 * The attributes that are mass assignable.
		 *
		 * @var array
		 */
		protected $fillable = ['user_id', 'token'];

		/**
		 * The attributes excluded from the model's JSON form.
		 *
		 * @var array
		 */
		protected $hidden = [];
}
