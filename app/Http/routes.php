<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('products', 'ProductsController@home');
Route::post('products', 'ProductsController@filter');
Route::get('products/{product}', 'ProductsController@product');
Route::post('products/search', 'ProductsController@search');
Route::get('howitworks', function ()
{
  return view('howitworks');
});

// Serve static Business Landing page
Route::get('business', function() {
  return File::get(public_path() . '/business/index.html');
});


Route::get('/{token}', 'HomeController@setCookie')->where(['token' => '^[a-zA-Z0-9]{100}$']);

Route::get('/', 'HomeController@index');
Route::get('test', 'HomeController@test');
//Route::post('/newsletter/subscribe', 'HomeController@subscribe');

//Route::group(['middleware' => 'App\Http\Middleware\AlphaAccessMiddleware'], function(){
  Route::get('verify/{username}/{token}', 'Auth\AuthController@verifyWithToken');
//  Route::get('home', 'HomeController@index');
  Route::get('about', 'HomeController@about');
  Route::get('faq', 'HomeController@faq');
  Route::get('why_sell_with_us', 'HomeController@why_sell');
  Route::get('terms', 'HomeController@terms');
  Route::get('how', 'HomeController@how');
  Route::get('partners', 'HomeController@partners');

  Route::get('requests', 'RequestController@viewAll');
  Route::get('/trips', 'TripController@view');


  Route::get('contact', 'ContactController@index');
  Route::post('contact/send', 'ContactController@send');
  Route::post('support/send', 'ContactController@support');

//  Route::get('auctions', 'AuctionController@all');
//  Route::get('auctions/view/{id}', 'AuctionController@view');
//  Route::get('trips', 'WelcomeController@soon');
//  Route::get('requests', 'WelcomeController@soon');
  Route::get('watchlist', 'WelcomeController@soon');

  //Produtcs
  // Route::get('product/view/{id}', 'ProductController@view');
  // Route::get('products/{query}', 'ProductController@filter');
  // Route::get('products', 'ProductController@products');

  //Suggests
  Route::get('suggest/product', 'ProductController@suggestForm');
  Route::post('suggest/product/sent', 'ProductController@suggestSent');
  Route::get('suggest/company', 'CompanyController@suggestForm');
  Route::post('suggest/company/sent', 'CompanyController@suggestSent');
  Route::get('suggest', 'HomeController@suggest');


  //Company
  Route::get('company/view/{id}', 'CompanyController@view');
  Route::get('/partner/{name}', 'CompanyController@viewPartner');

  Route::group(['middleware' => 'auth'], function(){

    //Requests
    Route::get('myrequests', 'RequestController@myrequests');
    Route::get('request/{id}', 'RequestController@create');
    Route::post('request/{id}/create', 'RequestController@createRequest');
    Route::get('request/view/{id}', 'RequestController@view');
    Route::post('request/view/{id}/bid', 'RequestController@bid');
    Route::post('request/edit/{id}', 'RequestController@edit');
    Route::get('request/cancel/{id}', 'RequestController@cancel');
    Route::post('request/saveRequest/{id}', 'RequestController@saveRequest');
    Route::get('request/view/{req_id}/accept/{bid_id}', 'RequestController@accept');
    Route::get('bid/cancel/{id}', 'RequestController@cancelBid');

    //Deal
    Route::get('deal/{id}', 'DealController@view');
    Route::get('deal/{id}/cancel', 'DealController@cancel');
    Route::get('deal/{id}/buy', 'DealController@buy');
    Route::get('deal/{id}/deliver', 'DealController@deliver');
    Route::get('deal/{id}/recive', 'DealController@recive');
    Route::get('mydeals', 'DealController@mydeals');

    //PayPal
    Route::post('payment', 'PaypalController@payment');
    Route::get('success/{hash}', 'PaypalController@success');


    //verification
    Route::get('verify', 'ProfileController@sendVerificationEmail');

    //Bid
    Route::get('mybids', 'BidController@mybids');
    Route::get('bid/reject/{bid_id}/{reason}', 'BidController@rejectBid');


    Route::get('profile', 'ProfileController@view');
    Route::post('profile/saveProfile', 'ProfileController@saveProfile');
    Route::post('profile/saveProfileImage', 'ProfileController@saveProfileImage');
    Route::post('profile/savePayout', 'ProfileController@savePayout');
    Route::post('profile/updatePassword', 'ProfileController@updatePassword');


    Route::get('user/{id}', 'ProfileController@viewProfile');
    Route::get('user/{id}/report/{code}', 'ProfileController@reportUser');
	//Route::get('user/{id}/message', 'ProfileController@sendMessage');
	Route::get('message/{id}', 'DealController@message');

    Route::get('/trips/create', 'TripController@create');
    Route::get('/trips/createTrip', 'TripController@createTrip');
    Route::get('/trips/mytrips', 'TripController@mytrips');
    Route::get('/trips/mytrips/cancel/{id}', 'TripController@cancelTrip');
    Route::post('/trips/mytrips/edit/{id}', 'TripController@editTrip');
    Route::post('/trips/mytrips/saveTrip/{id}', 'TripController@saveTrip');

//    Route::get('auctions/create', 'AuctionController@create');
//    Route::post('auctions/save', 'AuctionController@save');
//    Route::post('auctions/bid', 'AuctionController@bid');

    //Notification System
    Route::get('notifications/read/{id}', 'NotificationsController@read');
    Route::get('notifications/remove/{id}', 'NotificationsController@remove');
    Route::get('notifications/removeAll', 'NotificationsController@removeAll');

    //Account Settings
    Route::get('profile/notifications', 'ProfileController@notifications');
    Route::get('profile/updateNotifications', 'ProfileController@updateNotifications');
  });

  Route::get('auth/social/redirect/{social_provider}', 'Auth\AuthController@social_redirect');
  Route::get('auth/social/facebook', 'Auth\AuthController@facebook');
  Route::get('auth/social/twitter', 'Auth\AuthController@twitter');

  Route::group(['middleware' => 'admin'], function(){



    //Route::get('admin/dashboard/subscribers', 'AdminController@subscribers');
    Route::get('/auctions/create', 'AuctionController@create');
    Route::get('/auctions/view/{id}', 'AuctionController@view');
    Route::get('/auctions/all', 'AuctionController@all');
    Route::post('/auctions/save', 'AuctionController@save');
    Route::post('/auctions/bid', 'AuctionController@bid');
    Route::get('mails', 'AuctionController@mail'); //dont go! email checks

    //Dashboard
    Route::get('admin', 'AdminController@dashboard');

    //Dashboard2
    Route::get('dashboard', 'AdminController@dashboard2');
    Route::get('dashboard/suggests', 'AdminController@suggests');

    Route::get('admin/test', 'AdminController@test');
    Route::post('admin/test', 'AdminController@searchTest');

    Route::get('admin/users', 'AdminController@users');
    Route::get('admin/trips', 'AdminController@trips');
    Route::get('admin/requests', 'AdminController@requests');
    Route::get('admin/companies', 'AdminController@companies');
    Route::get('admin/products', 'AdminController@products');
    Route::get('admin/stores', 'AdminController@stores');
    Route::get('admin/deal', 'AdminController@deal');
    Route::get('admin/email/users', 'AdminController@emailUsers');
    Route::get('admin/email/newsletters', 'AdminController@emailNewsletter');
    Route::post('admin/discount/add', 'DiscountController@addDiscount');
    Route::get('admin/discount/remove/{id}', 'DiscountController@removeDiscount');


    //Suggests
    Route::get('dashboard/suggest/product/f/{id}', 'SuggestController@finishedProduct');
    Route::get('dashboard/suggest/product/u/{id}', 'SuggestController@unfinishedProduct');
    Route::get('dashboard/suggest/company/f/{id}', 'SuggestController@finishedCompany');
    Route::get('dashboard/suggest/company/u/{id}', 'SuggestController@unfinishedCompany');

    //Stores
    Route::get('admin/stores/create', 'StoresController@create');
    Route::post('admin/stores/createStore', 'StoresController@createStore');
    Route::get('admin/stores/{id}/edit', 'StoresController@edit');
    Route::post('admin/stores/{id}/update', 'StoresController@update');
    Route::get('admin/stores/{id}/products', 'StoresController@products');
    Route::get('admin/stores/{id}/add/{pro_id}', 'StoresController@ProductAdd');
    Route::get('admin/stores/{id}/remove/{pro_id}', 'StoresController@ProductRemove');

    //Requests

    //Product
    Route::get('admin/product/create', 'ProductController@create');
    Route::get('admin/product/edit/{id}', 'ProductController@edit');
    Route::get('admin/product/remove/{id}', 'ProductController@remove');
    Route::post('admin/product/createProduct', 'ProductController@createProduct');
    Route::post('admin/product/update/{id}', 'ProductController@update');
    Route::get('admin/product/{id}/stores', 'ProductController@stores');
    Route::get('admin/product/{id}/add/{st_id}', 'ProductController@storeAdd');
    Route::get('admin/product/{id}/remove/{st_id}', 'ProductController@storeRemove');

    //Company
    Route::get('admin/company/create', 'CompanyController@create');
    Route::get('admin/company/edit/{id}', 'CompanyController@edit');
    Route::post('admin/company/createCompany', 'CompanyController@createCompany');
    Route::post('admin/company/update/{id}', 'CompanyController@update');

//    Route::get('profile/notifications', 'ProfileController@notifications');
  //  Route::get('profile/updateNotifications', 'ProfileController@updateNotifications');
    Route::get('admin/messages', 'ProfileController@messages');


    //Notifications
    Route::get('admin/notify', 'AdminController@notify');
    Route::get('admin/notifymsg', 'AdminController@notifymsg');
    Route::get('admin/notifybid', 'AdminController@notifybid');
    Route::get('admin/notifyverify', 'AdminController@notifyverify');
    Route::get('admin/notifyterms', 'AdminController@notifyterms');

    Route::get('ip', 'AdminController@ip'); //clean ip table

	Route::get('testing', 'AdminController@testing');
	Route::get('testing2', 'AdminController@testing2');

  });

//edited gia ta kainouria routes
  Route::get('/login', 'Auth\AuthController@getLogin');
  Route::get('/register', 'Auth\AuthController@getRegister');
  Route::get('/logout', 'Auth\AuthController@getLogout');

  Route::controllers([
  	'auth' => 'Auth\AuthController',
 	'password' => 'Auth\PasswordController',
  ]);
//});
