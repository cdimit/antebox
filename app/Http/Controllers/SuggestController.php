<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SuggestProduct;
use App\SuggestCompany;
use App\Http\Requests;

class SuggestController extends Controller
{
    public function finishedProduct($id)
    {
	$product = SuggestProduct::find($id);

	$product->finished = '1';
	$product->save();

	return redirect()->back();
    }

    public function unfinishedProduct($id)
    {
	$product = SuggestProduct::find($id);

	$product->finished = '0';
	$product->save();

	return redirect()->back();
    }

    public function finishedCompany($id)
    {
	$company = SuggestCompany::find($id);

	$company->finished = '1';
	$company->save();

	return redirect()->back();
    }

    public function unfinishedCompany($id)
    {
	$company = SuggestCompany::find($id);

	$company->finished = '0';
	$company->save();

	return redirect()->back();
    }

}
