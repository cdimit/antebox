<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Discount;
use App\Products;
use App\Http\Requests;
use Auth;

class DiscountController extends Controller
{
    public function addDiscount(Request $request)
    {
                if (Auth::check()){
	                $v = \Validator::make($request->all(), ['value' => 'required|integer']);
				}

                if ($v->fails()) {
                    return redirect()->back()->withErrors($v);
                }

		Discount::Create([
		    'product_id'	=> $request->product,
		    'style'		=> $request->style,
		    'value'		=> $request->value,
		]);

		return redirect()->back()->with('status', 'Done');
    }

    public function removeDiscount($id)
    {
	$discount = Discount::where('product_id', $id);

	$discount->delete();

	return redirect()->back();
    }

}
