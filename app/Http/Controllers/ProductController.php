<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Products;
use App\Company;
use App\IpTable;
use App\SuggestProduct;
use Mail;
use App\Stores;
use Auth;
use Input;
use Artisan;

class ProductController extends Controller
{

    private $form_rules = [
	'category'	=> 'required',
    'company_id'	=> 'required',
    'name'		=> 'required',
    'description'	=> 'required',
	'price'		=> 'required',
	'weight'	=> 'required',
	'size_h'	=> 'required',
	'size_w'	=> 'required',
	'size_d'	=> 'required',
	'pic_url'	=> '',
	'vid_url'	=> '',
	'poster'	=> '',
	'bag'		=> 'required',
    ];
	
	public function view($id)
	{
		$product = Products::findOrFail($id);
		$product->visit++;

		$ip = \Request::getClientIp();
		$rec = IpTable::where('product_id', $id)
				->where('ip_address', $ip)
				->first();

		if(!$rec){
			IpTable::create([
				'product_id' => $id,
				'ip_address' => $ip,
			]);

			$product->visit_uniq++;
		}

		$product->save();
$store=		$product->stores->first();

		return view('product.view')->with('storeF',$store)->with('product', $product);
	}
	
    public function create()
	{
		$company = Company::all();

		return view('product.create')->withCompany($company);
	}

	public function edit($productID)
	{
		$product = Products::findOrFail($productID);
		return view('product.edit')->with('product',$product);
	}

	public function remove($productID)
	{
		$product = Products::findOrFail($productID);
		Artisan::call('antebox:ipfree',[]);

		$product->delete();
		return redirect()->back();
	}

	public function createProduct(Request $request)
	{

		if (Auth::check()){
				$v = \Validator::make($request->all(), $this->form_rules);
		}

		if ($v->fails()) {
			return redirect()->back()->withErrors($v);
		}



		$image = Input::file('pic_url');
		$video = Input::file('vid_url');
		$poster = Input::file('poster');
		$vid = '';
		$logo = '';

		$destinationPath = "/var/www/antebox/public/img/products/";
		$destinationPathPoster = "/var/www/antebox/public/img/products/poster/";

                if(Products::all()->isEmpty()){
                        $temp = 1;
                }else{
                        $temp = Products::orderBy('created_at', 'desc')->first();
                        $temp= $temp->id + 1;
                }

		if (!empty($image)) {
			$ext = pathinfo($image->getClientOriginalName(),PATHINFO_EXTENSION);
			if (!$image->move($destinationPath, $temp.'.'.$ext)){
					return $this->errors(['message' => 'Error saving the logo']);
			} else {
					$logo = $temp.'.'.$ext;
			}
		}
		

		if (!empty($video)) {

			$ext = pathinfo($video->getClientOriginalName(),PATHINFO_EXTENSION);
			if (!$video->move($destinationPath, $temp.'.'.$ext)){
					return $this->errors(['message' => 'Error saving the video']);
			} else {
					$vid = ($temp).'.'.$ext;
			}
		}
		
		if (!empty($poster)) {
			if (!$poster->move($destinationPathPoster, $temp.'.jpg')){
				return $this->errors(['message' => 'Error saving the poster']);
			}
		}

		$product = Products::create([
        		'category'      => $request->category,
        		'company_id'    => $request->company_id,
       			'name'          => $request->name,
        		'description'   => $request->description,
        		'price'         => $request->price,
        		'weight'        => $request->weight,
        		'size_h'        => $request->size_h,
        		'size_w'        => $request->size_w,
        		'size_d'        => $request->size_d,
        		'pic_url'       => $logo,
        		'vid_url'       => $vid,
			'bag'		=> $request->bag,
    			]);

		//add stores
		return redirect('admin/product/'.$product->id.'/stores');

	}
	
	public function update($productID, Request $request)
	{
		if (Auth::check()){
				$v = \Validator::make($request->all(), $this->form_rules);
		}

		if ($v->fails()) {
			return redirect()->back()->withErrors($v);
		}

		$image = Input::file('pic_url');
		$video = Input::file('vid_url');
		$poster = Input::file('poster');
		$vid = '';
		$logo = '';

		$destinationPath = "/var/www/antebox/public/img/products/";
		$destinationPathPoster = "/var/www/antebox/public/img/products/poster/";
		
		$product = Products::find($productID);

		if (!empty($image)) {
			$ext = pathinfo($image->getClientOriginalName(),PATHINFO_EXTENSION);
			if (!$image->move($destinationPath, $productID.'.'.$ext)){
					return $this->errors(['message' => 'Error saving the logo']);
			} else {
					$logo = $product->company_id.'.'.$ext;
					$product->pic_url = $logo;
			}
		}

		if (!empty($video)) {

			$ext = pathinfo($video->getClientOriginalName(),PATHINFO_EXTENSION);
			if (!$video->move($destinationPath, $productID.'.'.$ext)){
					return $this->errors(['message' => 'Error saving the video']);
			} else {
					$vid = $product->company_id.'.'.$ext;
					$product->vid_url = $vid;
			}
		}
		
		if (!empty($poster)) {
			if (!$poster->move($destinationPathPoster, $productID.'.jpg')){
				return $this->errors(['message' => 'Error saving the poster']);
			}
		}

		
		$product->category = $request->category;
		$product->company_id = $request->company_id;
		$product->name = $request->name;
		$product->description = $request->description;
		$product->price = $request->price;
		$product->weight = $request->weight;
		$product->size_h = $request->size_h;
		$product->size_w = $request->size_w;
		$product->size_d = $request->size_d;
		$product->bag = $request->bag;
		
		$product->save();
				
		return redirect('product/view/'.$productID)->with('status', 'Product was successfully saved!');

	}

	//Change route to post
	public function filter($query)
	{
//		$products = (new Products)->newQuery();
//		$products = Products::Query();

		$data = explode("^", $query);

		if(count($data)<3){
		    return redirect('products');
		}

		$products = null;

                for($i=1; $i<count($data); $i += 2){
                        if($data[$i]=="name" && $i+1 <count($data)){
			    $products = Products::where('name', 'LIKE', '%'.$data[1+1].'%')->get();
			}
                }

		if($products==null){
			$products = Products::all();
		}

		$productsAll = Products::all();


                for($i=0; $i<count($productsAll); $i++){
                    $productsAll[$i] = collect($productsAll[$i])->merge(['country' => $productsAll[$i]->company->country, 'company' => $productsAll[$i]->company->name]);;
                }

		$country = null;
		$company = null;
		$category = null;
		for($i=1; $i<count($data); $i += 2){
		    if($data[$i]=='price' || $data[$i]=='weight'){
			$value = explode(",", $data[$i+1]);
			if(count($value)==2){
			    $more = Products::where($data[$i], '<=', $value[0])->get();
			    $less = Products::where($data[$i], '>=', $value[1])->get();
			    $products =  $products->diff($less)->diff($more);
			}
		    }
		    elseif($data[$i]=='country' && $i+1 <count($data)){
			if($data[$i+1]!=""){
			    $country  = $data[$i+1];
			}
		    }
		    elseif($data[$i]=='company' && $i+1 <count($data)){
			if($data[$i+1]!=""){
			    $company  = $data[$i+1];
			}
		    }
		    elseif($data[$i]=='category' && $i+1 <count($data)){
			if($data[$i+1]!=""){
			    $category = $data[$i+1];
			}
		    }
		}

                for($i=0; $i<count($products); $i++){
		    if($products[$i]->discount==null)
		    $products[$i] = collect($products[$i])->merge(['country' => $products[$i]->company->country,
								'company' => $products[$i]->company->name,
								'value' => null,
                                                                'style' => null]);
		    else{
			if($products[$i]->discount->style=='%') $discPrice = $products[$i]->price - ($products[$i]->price * ($products[$i]->discount->value / 100)); else $discPrice = $products[$i]->price - $products[$i]->discount->value;
		    $products[$i] = collect($products[$i])->merge(['country' => $products[$i]->company->country,
								'company' => $products[$i]->company->name,
								'value' => $products[$i]->discount->value,
                                                                'style' => $products[$i]->discount->style]);
			}
                }



		if($country){
		    $products = $products->where('country', $country);
		}
		if($category){
		    $products = $products->where('category', $category);
		}
		if($company){
		    $products = $products->where('company', $company);
		}

		if(count($products)<1){
//		    return redirect('products');
		}

                return view('product.products')->with('products', $products->sortBy('name'))
						->with('all', $productsAll);

	}


        public function products()
        {
                $products = Products::all()->shuffle();

		for($i=0; $i<count($products); $i++){
		    if($products[$i]->discount==null)
		    $products[$i] = collect($products[$i])->merge(['country' => $products[$i]->company->country,
								'company' => $products[$i]->company->name,
								'value' => null,
                                                                'style' => null,
								'disc' => null]);
		    else{
if($products[$i]->discount->style=='%') $discPrice = $products[$i]->price - ($products[$i]->price * ($products[$i]->discount->value / 100)); else $discPrice = $products[$i]->price - $products[$i]->discount->value;
		    $products[$i] = collect($products[$i])->merge(['country' => $products[$i]->company->country,
								'company' => $products[$i]->company->name,
								'value' => $products[$i]->discount->value,
                                                                'style' => $products[$i]->discount->style,
								'disc' => $products[$i]->price]);
			$products[$i]->price = number_format((float)$discPrice, 2, '.', '');
}
		}


                return view('product.products')->with('products', $products)
						->with('all', $products->sort());
        }


        public function stores($id)
        {
                $product = Products::findOrFail($id)->stores;
                $stores = Stores::all();
                $diff = $stores->diff($product);

                return view('product.stores')->with('stores', $product)
                                              ->with('product', $diff);
        }


	public function storeAdd($id, $st_id)
	{
		$product = Products::find($id);
		$product->stores()->attach($st_id);

		return redirect()->back();
	}

        public function storeRemove($id, $st_id)
        {
		$product = Products::find($id);
                $product->stores()->detach($st_id);

                return redirect()->back();
        }

	public function suggestForm()
	{
		return view('product.suggest');
	}

    private $form_rules_suggest = [
        'name'      => 'required|max:80',
	'category'        => 'required|max:25',
        'origin'              => 'required|max:80',
        'pic_url'       => 'mimes:png,jpeg,bmp|max:5120',
        'producer'         => 'max:80',
        'website'        => 'max:80',
        'email'        => 'max:80|email',
        'comment'        => 'max:255',
        'user_name'        => 'required|max:80',
        'user_email'        => 'required|max:80|email',
    ];

	public function suggestSent(Request $request)
	{

	    $v = \Validator::make($request->all(), $this->form_rules_suggest);
            if ($v->fails()) {
	        return redirect()->back()->withErrors($v)->withInput();
	    }




 	    $image = Input::file('pic_url');
	    $logo = null;

            $destinationPath = "/var/www/antebox/public/img/suggest/";
                $name = rand(0, 999999);
if (!empty($image)) {
			$ext = pathinfo($image->getClientOriginalName(),PATHINFO_EXTENSION);
			if (!$image->move($destinationPath, $name.'.'.$ext)){
				return $this->errors(['message' => 'Error saving the logo']);
			} else {
				$logo = '/img/suggest/'.($name).'.'.$ext;
			}
		}

            $suggest = SuggestProduct::create([
                'name'          => $request->name,
                'category'      => $request->category,
		'origin'	=> $request->origin,
		'pic_url'	=> $logo,
                'producer'      => $request->producer,
                'website'       => $request->website,
                'email'         => $request->email,
                'comment'       => $request->comment,
                'user_name'     => $request->user_name,
                'user_email'    => $request->user_email,
                'user_id'       => $request->user_id,
              ]);

	    return redirect()->back()->withStatus('done');

	}

}

