<?php namespace App\Http\Controllers;

use App\Subscriber;
use Validator;
use Request;
use Cookie;
use App;
use App\MyRequest;
use App\Products;
use App\Company;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// $this->middleware('auth');
	}

	public function setCookie($token){
		if($token == 'vTFfsPFH0Rpal1yy4uQUr6O6jzXtvoZMh8TIGEXSNJWMeYaPNex6kaPiyhFCxVWCrTjvrLxVO1ckHmKGtVAwlKTxXqzsVOcgXXpl')
		{
			$cookie = Cookie::make('alpha-user', true, 1440);
			return redirect('/')->withCookie($cookie);
		}

		App::abort(404);
	}

	public function landing()
	{
		return view('landing');
	}

	public function subscribe()
	{
		$request = Request::all();
		$validator = Validator::make($request, [
			'email' => 'required|email|unique:subscribers'
		]);

		if($validator->fails())
		{
			return response()->json([
				'success' => false,
				'message' => "The email you have provided is already subscribed!"
			], 422);
		}

		Subscriber::create(['email' => $request["email"]]);

		return response()->json([
			'success' => true,
			'message' => "We will keep you updated!"
		]);
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$request = MyRequest::Open();

		$categories = Products::all()->pluck('category')->unique()->toArray();
		$r = array_rand($categories, 3);

		// Change categories here as appropriate
		// and ammend in home_jan17.blade.php as appropriate
		$catg1 = Products::where('category', '=', $categories[$r[0]]);
		$catg2 = Products::where('category', '=', $categories[$r[1]]);
		$catg3 = Products::where('category', '=', $categories[$r[2]]);

		$featured = Products::where('category', '=', 'Featured');
		$specials = Products::where('category', '=', 'Specials');
		$mostvisited = Products::orderBy('visit', 'desc')->limit(5);

		// Select random categories for the front page
		$catg1 = $catg1->get();
		if (count($catg1) > 3) {
			$catg1 = $catg1->random(3);
		}

		$catg2 = $catg2->get();
		if (count($catg2) > 3) {
			$catg2 = $catg2->random(3);
		}

		$catg3 = $catg3->get();
		if (count($catg3) > 3) {
			$catg3 = $catg3->random(3);
		}

		$data = [	
			'catg1'					=> $catg1,
			'catg2'					=> $catg2,
			'catg3'					=> $catg3,
			'catg1_name'			=> $categories[$r[0]],
			'catg2_name'			=> $categories[$r[1]],
			'catg3_name'			=> $categories[$r[2]],
			'featured'				=> $featured->get(),
			'specials'				=> $specials->get(),
			'mostvisited'			=> $mostvisited->get(),
		];

		// return view('home_5oct')->with('request', $request);
		return view('home_jan17', $data);

	}

	public function partners()
	{
		$companies = Company::all()->shuffle();
		return view('company.partners')->with('companies', $companies);
	}

	public function products()
	{
		$products = Products::all()->shuffle();
		return view('product.products')->with('products', $products);
	}

	public function faq()
	{
		return view('shared.faq');
	}

	public function why_sell()
	{
		return view('shared.why_sell');
	}

	public function help()
	{
		return view('shared.help');
	}

	public function terms()
	{
		return view('shared.terms');
	}

	public function about()
	{
		return view('shared.about');
	}

	public function how()
	{
		return view('shared.how');
	}

        public function suggest()
        {
                return view('shared.suggest');
        }

        public function test() {
            $request = MyRequest::Open();
            return view('test')->with('request', $request);
        }

}
