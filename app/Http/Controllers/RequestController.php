<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Products;
use App\Http\Requests;
use Auth;
use Mail;
use App\MyRequest;
use App\RequestPlaces;
use App\Bid;
use App\Pay;
use App\Deal;
use App\User;
use App\Trip;
use App\Notifications;

class RequestController extends Controller
{

	private $form_rules = [
        	'quantity'	=> 'required|integer|max:255',
		'message'	=> 'string|max:1024',
		'until'		=> 'required|date',
		'place_name'    => 'required|string|max:40',
		'map_x'		=> 'required',
                'map_y'         => 'required',
                'country'       => 'required',
                'city'          => 'required',
                'address'       => 'required',
	];


	private $form_rules_bid = [
		'place_name'    => 'required|string|max:10',
		'map_x'		=> 'required_if:radioplace,other',
                'map_y'         => 'required_if:radioplace,other',
                'country'       => 'required_if:radioplace,other',
                'city'          => 'required_if:radioplace,other',
                'address'       => 'required_if:radioplace,other',
		'bid_price'       => 'required',
		'datepicker'        => 'required',
		'timepicker'        => 'required',
		'msg'         => 'string|max:4',
	];

        public function viewAll()
        {
		$requests = MyRequest::open();//->orderBy('until', 'asc')->get();

                return view('request.viewAll')->with('requests',$requests);

        }

        public function view($id)
        {

//elexos gia cancel
				$req = MyRequest::findOrFail($id);
				//$reqPlace = RequestPlaces::findOrFail($id);
				$pID = $req->product_id;
				$product = Products::find($pID);
				$bids = Bid::where('request_id', $id)->orderBy('created_at', 'asc')->paginate(10);
				$place = $req->places()->first();

		if($req->status=="Cacnelled" && Auth::user!=$req->user){
			return redirect('/request');
		}

		if($req->status=="Completed")
                return view('request.completed')->with('request',$req)->with('place', $place)->with('product',$product)->with('bids',$bids);
		else
                return view('request.view')->with('request',$req)->with('place', $place)->with('product',$product)->with('bids',$bids);

        }

	public function create($id)
	{
		$product = Products::findOrFail($id);

		return view('request.create')->with('product', $product);
	}

	public function createRequest($id, Request $request)
	{
                if (Auth::check()){
	                $v = \Validator::make($request->all(), $this->form_rules);
				}

                if ($v->fails()) {
                    return redirect()->back()->withErrors($v);
                }

                $req = MyRequest::create([
                        'user_id'       => Auth::user()->id,
                        'product_id'  	=> $id,
                        'qty'     	=> $request->quantity,
						'price'		=> $request->input_total,
                        'msg'    	=> $request->message,
                        'until'       	=> $request->until,
                ]);

		RequestPlaces::create([
                        'request_id'    => $req->id,
                        'name'	        => $request->place_name,
                        'map_x'           => $request->map_x,
                        'map_y'           => $request->map_y,
                        'country'         => $request->country,
                        'city'           => $request->city,
                        'address'         => $request->address,
		]);
		$user = Auth::user();
                $products = Products::all()->random(3);

                        Mail::send('emails.request.create', ['request' => $req, 'products' => $products], function($message) use ($user)
                        {
                          $message->from("no-reply@antebox.com", "AnteBox")
                          ->to($user->email, $user->first_name)
                          ->subject('Your Request is live!');
                        });

		$this->notifyTravellers($req);

		return redirect('request/view/'.$req->id)->with('status', 'Request was successfully saved!');
	}

	public function notifyTravellers($request)
	{
		$trips = Trip::open();

		foreach($trips as $trip){
		    foreach($request->places as $place){
			if(strcmp($trip->to_country, $place->country)==0){
			    foreach($request->product->stores as $store){
				if(strcmp($trip->from_country, $store->country)==0){
if($trip->user!=$request->user){
                $products = Products::all()->random(3);

                        Mail::send('emails.request.create_request_traveller', ['request' => $request, 'products' => $products, 'trip' => $trip], function($message) use ($trip)
                        {
                          $message->from("no-reply@antebox.com", "AnteBox")
                          ->to($trip->user->email, $trip->user->first_name)
                          ->subject('You can make this delivery!');
                        });

				    break;
}
				}
			    }
			}
		    }
		}
	}

	public function edit($id)
	{
	//	$req = MyRequest::findOrFail($id);
	//	$product = Products::findOrFail($req->product_id);
		//$place = RequestPlaces::findOrFail($id);

	//	return view('request.edit')->with('request', $req)->with('product', $product);

		return redirect()->back();
	}
	
	public function saveRequest($id, Request $request)
	{
		$req = MyRequest::findOrFail($id);
		//$place = RequestPlaces::where('request_id', $req->id);
		
		$req->qty = $request->quantity;
		$req->msg = $request->message;
		$req->until = $request->until;
		$req->save();
		
		return redirect('myrequests');
	}
	
	public function myRequests()
	{
		$reqs = MyRequest::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->paginate(10);

		return view('request.myrequests')->with('reqs', $reqs);
	}
	
	public function cancel($id)
	{
		$req = MyRequest::findOrFail($id);
                $user_id = Auth::user()->id;

		if($user_id==$req->user->id){
			$req->status = "Cancelled";
			$req->save();
		}

		foreach($req->bids as $b){
			$b->status = "Declined";
			$b->save();
	    	}

		return redirect()->back();
	}
	
	public function cancelBid($id)
	{
		$bid = Bid::findOrFail($id);
                $user_id = Auth::user()->id;

		if($user_id==$bid->user->id){
			$bid->status = "Cancelled";
			$bid->save();
		}


		return redirect()->back();
	}

	public function bid($id, Request $request){

		$req = MyRequest::find($id);

		if(!$req->status=="Active"){
		    return redirect('request');
		}

                if (Auth::check()){
	                $v = \Validator::make($request->all(), $this->form_rules_bid);
				}

                if ($v->fails()) {
                    return redirect()->back()->withErrors($v);
                }

		if ($request->date_flex == 1){
			$date_flex = 1;
			$time_flex = 1;
		} else {
			$date_flex = 0;
		}
		if ($request->time_flex == 1){
			$time_flex = 1;
		} else {
			$time_flex = 0;
		}

		if($request->radioplace=="other"){
		$place=RequestPlaces::create([
                        'request_id'    => $request->id,
                        'name'	        => $request->place_name,
                        'map_x'           => $request->map_x,
                        'map_y'           => $request->map_y,
                        'country'         => $request->country,
                        'city'           => $request->city,
                        'address'         => $request->address,
			'isClientPlace'	=> false,
		]);
		}else{
		$place = $req->places->first();
		}

		$bid = Bid::create([
					'user_id'    => $request->usr_id,
					'request_id'	        => $id,
					'request_place_id'           => $place->id,
					'price'           => $request->bid_price,
					'date'         => $request->datepicker,
					'date_flex'           => $date_flex,
					'time'         => $request->timepicker,
					'time_flex'         => $time_flex,
					'msg'         => $request->text_message,
		]);

		 Notifications::create([
                        'user_id'  => $bid->request->user->id,
                        'msg'      => $bid->user->first_name." made a bid on your request",
                        'link'     => "/request/view/".$bid->request->id,
                        'category' => "bid",
                ]);

                $products = Products::all()->random(3);

                        Mail::send('emails.request.bid', ['bid' => $bid, 'products' => $products], function($message) use ($bid)
                        {
                          $message->from("no-reply@antebox.com", "AnteBox")
                          ->to($bid->request->user->email, $bid->request->user->first_name)
                          ->subject('Your have a new delivery bid!');
                        });


		return redirect()->back();
	}

	public function accept($req_id, $bid_id){

            $req = MyRequest::findOrFail($req_id);
            $user_id = Auth::user()->id;

	    if($user_id!=$req->user->id){
		return view('errors.404');
	    }
	    if($req->status!="Open" && $req->status!="Test"){
		return view('errors.404');
	    }

	    $bid = Bid::findOrFail($bid_id);
	    if($bid->request->id!=$req->id){
		return view('errors.404');
	    }

	    $bid_price = $bid->price;
	    $traveller = $bid->user;
	    $req_price = $req->price;
	    $sum = ($bid_price + $req_price);
	    $service = $sum * 0.12;
	    $total = $sum + $service;

            $hash = bin2hex(random_bytes(32));

    	    $pay = Pay::create([
		'hash' 	=> $hash,
		'total' => $total,
		'paid'	=> false,
		'user_id' => $user_id,
	    ]);

	    $deal = Deal::create([
		'bid_id' => $bid->id,
		'client_id' => $user_id,
		'traveller_id' => $traveller->id,
		'pay_id'	=> $pay->id,
		'status'	=> "Active"
	    ]);

	    $req->status = "Completed";
	    $req->save();

	    foreach($req->bids as $b){
		$b->status = "Declined";
		$b->save();
	    }

	    $bid->status = "Accepted";
	    $bid->save();

                 Notifications::create([
                        'user_id'  => $deal->traveller->id,
                        'msg'      => $deal->client->first_name." has accepted your bid",
                        'link'     => "/deal/".$deal->id,
                        'category' => "deal",
                ]);

                Notifications::create([
                        'user_id'  => $deal->client->id,
                        'msg'      => "You now need to pay for your products",
                        'link'     => "/deal/".$deal->id,
                        'category' => "warning",
                ]);

                $products = Products::all()->random(3);

                        Mail::send('emails.request.accept_client', ['deal' => $deal, 'products' => $products], function($message) use ($deal)
                        {
                          $message->from("no-reply@antebox.com", "AnteBox")
                          ->to($deal->client->email, $deal->client->first_name)
                          ->subject('You are nearly there!');
                        });

                        Mail::send('emails.request.accept', ['deal' => $deal, 'products' => $products], function($message) use ($deal)
                        {
                          $message->from("no-reply@antebox.com", "AnteBox")
                          ->to($deal->traveller->email, $deal->traveller->first_name)
                          ->subject('New Delivery Confirmed!');
                        });


	    return redirect('deal/'.$deal->id);
	}

}
