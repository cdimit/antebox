<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Notifications;
use Auth;

class NotificationsController extends Controller
{
    public function read($id)
    {
	$user = Auth::user();
	$notif = Notifications::findOrFail($id);

	if($user->id == $notif->user_id){
		$this->remove($id);
		return redirect($notif->link);
	}


	return redirect("/");
    }

    public function remove($id)
    {
        $user = Auth::user();
        $notif = Notifications::findOrFail($id);

        if($user->id == $notif->user_id){
	    if($notif->category!="warning"){
                $notif->status = "1";
                $notif->save();
	    }
		return redirect()->back();
        }

	return redirect("/");

    }

    public function removeAll()
    {
        $user = Auth::user();
        $notif = $user->notifications()->get();

	foreach($notif as $not){
	    if($not->category!="warning"){
		$not->status = "1";
		$not->save();
	    }
	}

        return redirect()->back();

    }


}
