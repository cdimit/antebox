<?php namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\User;
use App\VerificationToken;
use App\Newsletter;
use Mail;
use Socialite;

class AuthController extends Controller {
	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

//edited
//	protected $loginPath = '/login';
	protected $redirectPath = '/';

	use AuthenticatesAndRegistersUsers;


	public function validator(array $data)
	{
		return Validator::make($data, [
			'first_name' => 'Required|Min:2|Max:25|Alpha',
			'last_name' => 'Required|Min:2|Max:25|Alpha',
			'email' => 'required|email|Between:3,64|unique:users',
			'password' => 'required|confirmed|min:6|max:16',
			'g-recaptcha-response' => 'required|captcha',
		]);
	}

	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
	 * @return void
	 */
	public function __construct(Guard $auth, Registrar $registrar)
	{
		$this->auth = $auth;
		$this->registrar = $registrar;

		$this->middleware('guest', ['except' => array('getLogout', 'verifyWithToken')]);
	}

	public function postRegister(Request $request)
	{
		$validator = $this->registrar->validator($request->all());

		if ($validator->fails())
		{
				return redirect('/register')
					->withErrors($validator)
					->withInput();
		}



// SECOND VERIFICATION STEP HERE
		$this->auth->login($this->registrar->create($request->all()));
		$this->sendVerificationEmail();

                Newsletter::create([
                    'user_id' => $this->auth->user()->id,
                ]);



		return view('/auth/done');

		return view('auth.verify.email')->with('email', $this->auth->user()->email);
	}

	public function postLogin(Request $request)
	{
		$this->validate($request, [
			'email' => 'required',
			'password' => 'required',
		]);



		$credentials = $request->only('email', 'password');



		if (Auth::attempt($credentials, $request->has('remember')))
		{
			return redirect()->intended($this->redirectPath());
		}

		return redirect()->back()->withErrors([
						'email' => 'These credentials do not match our records.',
					]);
	}

	public function social_redirect($social_provider)
	{
		return Socialite::with($social_provider)->redirect();
	}

	public function twitter()
	{
		$twitter_user = Socialite::with('twitter')->user();

		return json_encode($twitter_user);
	}

	public function facebook()
	{
		$fb_user = Socialite::with('facebook')->user();

/*
		if($fb_user->email=="eniac.blogspot@gmail.com"){

$url = 'http://graph.facebook.com/'.$fb_user->id.'/picture?type=large';
$img = '/var/www/antebox/public/img/user_avatars/chr.jpg';
file_put_contents($img, file_get_contents($url));

			return ;
		}
*/
		if($fb_user->email == null) return redirect('/register')
                                        ->withErrors('There was an error communicating with Facebook')
                                        ->withInput();

$fbname = $fb_user->user['name'];
$name = explode(' ',trim($fbname));


		$account = User::where('provider_id', $fb_user->id)->first();
		if (is_null($account))
		{
			$acc = User::where('email', $fb_user->email)->first();
			if (is_null($acc))
			{
				$user = User::create([
				'email' => $fb_user->email,
				'password' => bcrypt('NBEVD)S(qubcj7hPKxzw@hDf5*ZGMoB*'),
				'verified' => 1,
				'provider' => 'facebook',
				'provider_id' => $fb_user->id,
				'first_name' => $name[0],
				'last_name' => $name[1],
				'birthday' => '0000-00-00',
				'sex'	=> ucfirst($fb_user->user['gender'])
				]);

				$url = 'http://graph.facebook.com/'.$fb_user->id.'/picture?type=large';
				$img = '/var/www/antebox/public/img/user_avatars/'.$user->id.'.jpg';
				file_put_contents($img, file_get_contents($url));

				$user->pic_url = $user->id.'.jpg';
				$user->save();

                	        Newsletter::create([
        	                    'user_id' => $user->id,
	                        ]);


				Mail::send('emails.welcome', [], function($message) use ($user)
					{
							$message->from("no-reply@antebox.com", "AnteBox")
															->to($user->email)
															->subject('Welcome - AnteBox');
					});

				$this->auth->attempt(['email' => $user->email, 'password' => 'NBEVD)S(qubcj7hPKxzw@hDf5*ZGMoB*'], true);
				return redirect('/');
			} else {
				return redirect()->back()->withErrors('Email already in use.');
			}

		} else {
			if(($account->provider_id == $fb_user->id) && ($this->auth->attempt(['email' => $account->email, 'password' => 'NBEVD)S(qubcj7hPKxzw@hDf5*ZGMoB*'], true)))
			{
				return redirect('/');

			} else {
				return redirect()->back()->withErrors('Could not login.');
			}
		}
	}


	public function sendVerificationEmail()
	{
		$token = str_random('100');
		VerificationToken::create(array('user_id' => $this->auth->user()->id , 'token' => $token));

		Mail::send('emails.verification', ['token' => $token, 'user_id' => $this->auth->user()->id], function($message)
		{
			$message->from("no-reply@antebox.com", "AnteBox")
							->to($this->auth->user()->email, $this->auth->user()->name)
							->subject('Please confirm your email address - AnteBox');
		});
	}


	public function verifyWithToken($id, $token)
	{
		$token = VerificationToken::whereRaw("BINARY `token`= ?", array($token))->first();
		$user = User::find($id);
		if(is_null($token) || is_null($user))
		{
			return view('auth.verify.invalid');
		}

		if($token->user_id == $user->id)
		{

                 	Mail::send('emails.welcome', [], function($message) use ($user)
                        {
                                $message->from("no-reply@antebox.com", "AnteBox")
                                                                ->to($user->email)
                                                                ->subject('Welcome - AnteBox');
                        });

			$user->verified = 1;
                        $user->save();
                        $token->delete();

			return view('auth.verify.success');
		}
		return view('auth.verify.invalid');
	}
}
