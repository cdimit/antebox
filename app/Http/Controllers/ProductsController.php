<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Products;
use App\IpTable;
use App\Company;
use Illuminate\Support\Facades\Input;

class ProductsController extends Controller
{

    public function home(Request $request)
    {	
    	// Collect all products
    	$AllProducts = Products::with('company', 'discount')->get();
        $products = Products::with('company', 'discount')->paginate(25);

    	// Collect unique columns from products
    	$categories = $AllProducts->pluck('category')->unique();
    	$countries = $AllProducts->pluck('company')->pluck('country')->unique();
    	$companies = $AllProducts->pluck('company')->pluck('name')->unique();

    	// Find the lowest and highest price in products
    	$price_range = $AllProducts->pluck('price')->toArray();
    	$min_price = min($price_range);
    	$max_price = max($price_range);

    	$data = [
    		'products' 		=> $products,
    		'categories' 	=> $categories,
    		'min_price'		=> $min_price,
    		'max_price'		=> $max_price,
    		'countries'		=> $countries,
    		'companies'		=> $companies
    	];

    	return view('products.index', $data);
    	// return $request;
    }


    public function filter(Request $request)
    {
        $query = Products::query();

        if (Input::has('companies')) {
            $selected_companies = Input::get('companies');
            $query->whereHas('company', function ($q) use ($selected_companies) {
                $q->whereIn('name', $selected_companies);
            });
        } else {
            $selected_companies = [];
        }

        if (Input::has('countries')) {
            $selected_countries = Input::get('countries');
            $query->whereHas('company', function ($q) use ($selected_countries) {
                $q->whereIn('country', $selected_countries);
            });
        } else {
            $selected_countries = [];
        }


        if(Input::has('categories')) {
            $selected_categories = Input::get('categories');
            $query->whereIn('category', $selected_categories);
        } else {
            $selected_categories = [];
        }

        if (Input::has('min_price')) {
            $min_price = Input::get('min_price');
            $max_price = Input::get('max_price');
            $query->whereBetween('price', [$min_price, $max_price]);

        } else {
            $min_price = 0;
            $max_price = 100000;
        }

        $AllProducts = Products::with('company')->get();

        $products = $query->with(['company'])->get();
        $categories = $AllProducts->pluck('category')->unique();
        $countries = $AllProducts->pluck('company')->pluck('country')->unique();
        $companies = $AllProducts->pluck('company')->pluck('name')->unique();

        $data = [
                'products'              => $products,
                'categories'            => $categories,
                'companies'             => $companies,
                'countries'             => $countries,
                'min_price'             => $min_price,
                'max_price'             => $max_price,
                'selected_categories'   => $selected_categories,
                'selected_companies'    => $selected_companies,
                'selected_countries'    => $selected_countries
            ];

        return view('products.filter', $data);

    }

    public function product(Products $product)
    {
    	$categories = Products::all()->pluck('category')->unique();
        $product->visit++;
        $id = $product->id;

        $ip = \Request::getClientIp();
        $getIP = IpTable::where('product_id', $id)
                ->where('ip_address', $ip)
                ->first();

        if(!$getIP){
            IpTable::create([
                'product_id' => $id,
                'ip_address' => $ip,
            ]);

            $product->visit_uniq++;
        }

        $product->save();

    	$data = [
    		'categories'		=> $categories,
    		'product'			=> $product
    	];
    	return view('products.single', $data);
    }

    public function search(Request $search_query) 
    {
        $keywords = $search_query->search_query;
        $category = $search_query->category;
        $categories = Products::all()->pluck('category')->unique();

        if($keywords) {
            $query = Products::query();
            
            if ($category != 'All') {
                $query->where('category', '=', $category);
            }

            $query->where(function ($q) use($keywords) {
                $q->where("name", "LIKE", "%$keywords%");
            });
            $products = $query->get();

            $data = [
                'products'      => $products,
                'categories'    => $categories
            ];

            return view('products.search', $data);
        }

    }
}
