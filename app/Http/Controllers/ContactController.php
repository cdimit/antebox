<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Mail;
use Illuminate\Http\Request;

class ContactController extends Controller {


    	private $form_rules_guest = [
        	'email'                 => 'required|email',
        	'name'              => 'required|Alpha|max:60|min:2',
        	'subject'          => 'required|max:25',
        	'message'              => 'required|max:1000',
    	];

        private $form_rules_user = [
                'subject'          => 'required|max:25',
                'message'              => 'required|max:1500',
        ];


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('shared.help');
	}

	public function support(Request $request)
	{
           if (Auth::check()){
                $data = array('register' => 'True', 'name' => $request->name, 'email' => $request->email, 'subj' => "Support", 'msg' => $request['message']);
           }
           else{
                 $data = array('register' => 'False', 'name' => $request['name'], 'email' => $request['email'], 'subj' => "Support", 'msg' => $request['message']);
           }


                Mail::send('emails.contact', $data, function($message)
                {
                        $message->from("support-help@antebox.com", "AnteBox")
                                                        ->to('gehenna63@hotmail.com')
                                                        ->to('contact@antebox.com')
                                                        ->subject('Contact message from user!');
                });


            return redirect()->back();//->with('status', 'Message sent was successful!');
	}


        public function send(Request $request)
        {

	   if (Auth::check()){
	        $v = \Validator::make($request->all(), $this->form_rules_user);

	        if ($v->fails()) {
	            return redirect()->back()->withErrors($v)->withInput();
	        }

		$user = Auth::user();
		$data = array('register' => 'True', 'name' => $user->first_name, 'email' => $user->email, 'subj' => $request['subject'], 'msg' => $request['message']);
	   }
	   else{
	        $v = \Validator::make($request->all(), $this->form_rules_guest);

	        if ($v->fails()) {
	            return redirect()->back()->withErrors($v)->withInput();
	        }

		 $data = array('register' => 'False', 'name' => $request['name'], 'email' => $request['email'], 'subj' => $request['subject'], 'msg' => $request['message']);
	   }


		Mail::send('emails.contact', $data, function($message)
		{
			$message->from("contact-help@antebox.com", "AnteBox")
							->to('contact@antebox.com')
							->subject('Contact message from user!');
		});



            return redirect()->back()->with('status', 'Message sent was successful!');
        }




	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
