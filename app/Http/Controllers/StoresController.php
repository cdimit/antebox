<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Stores;
use App\Products;
use Auth;

class StoresController extends Controller
{
	protected $form_rules = [
		'name'		=> 'required',
		'phone'		=> '',
		'address'	=> 'required',
		'country'	=> 'required',
		'city'		=> 'required',
		'zip_code'	=> '',
		'map_x'		=> '',
		'map_y'		=> '',
    	];

    	public function create()
	{
		return view('stores.create');
	}

	public function createStore(Request $request)
	{
                if (Auth::check()){
	                $v = \Validator::make($request->all(), $this->form_rules);
		}

                if ($v->fails()) {
                    return redirect()->back()->withErrors($v);
                }

                Stores::create([
                        'name' 	 	=> $request->name,
                        'phone'     	=> $request->phone,
                        'address'   	=> $request->address,
                        'city'       	=> $request->city,
                        'zip_code'      => $request->zip_code,
                        'country'       => $request->country,
			'map_x'		=> $request->map_x,
			'map_y'		=> $request->map_y,
                ]);


		return redirect('admin/stores')->with('status', 'Store was successfully created!');
	}


	public function edit($id)
	{
		$store = Stores::findOrFail($id);
		return view('stores.edit')->with('store',$store);
	}


	
	public function update($id, Request $request)
	{
		if (Auth::check()){
				$v = \Validator::make($request->all(), $this->form_rules);
		}

		if ($v->fails()) {
			return redirect()->back()->withErrors($v);
		}

		$store = Stores::find($id);

		$store->name = $request->name;
		$store->phone = $request->phone;
		$store->address = $request->address;
		$store->country = $request->country;
		$store->city = $request->city;
		$store->zip_code = $request->zip_code;
	   	$store->map_x = $request->map_x;
		$store->map_y = $request->map_y;

		$store->save();

		return redirect('admin/stores')->with('status', 'Store was successfully updated!');
	}


	public function products($id)
	{
		$store = Stores::find($id)->products;
		$products = Products::all();
		$diff = $products->diff($store);

		return view('stores.products')->with('store', $store)
					      ->with('products', $diff);
	}

	public function productAdd($id, $pro_id)
	{
		$store = Stores::find($id);
		$store->products()->attach($pro_id);

		return redirect()->back();
	}

        public function productRemove($id, $pro_id)
        {
                $store = Stores::find($id);
                $store->products()->detach($pro_id);

                return redirect()->back();
        }

}
