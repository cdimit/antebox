<?php

namespace App\Http\Controllers;


use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Pay;
use App\Deal;
use App\Notifications;
use App\MyPayment;
use App\Products;
use Mail;

class PaypalController extends Controller
{
	private $_api_context;

	public function __construct()
	{
		// setup PayPal api context
		$paypal_conf = \Config::get('paypal');
		$this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
		$this->_api_context->setConfig($paypal_conf['settings']);
	}

	public function payment(Request $request)
	{

		$deal = json_decode($request->deal);

		$payer = new Payer();
		$details = new Details();
		$amount = new Amount();
		$transaction = new Transaction();
		$redirectUrls = new RedirectUrls();
		$payment = new Payment();
		$products = new Item();
		$traveller = new Item();
		$service = new Item();

		$currency = 'EUR';

		//Payer
		$payer->setPaymentMethod('paypal');

		$req = $deal->bid->request;
		//Items
		$products->setName($req->product->name)
			->setCurrency($currency)
			->setDescription($req->product->description)
			->setQuantity($req->qty)
			->setPrice($req->price / $req->qty);

		$traveller->setName('Traveller Fee')
			->setCurrency($currency)
			->setQuantity(1)
			->setPrice($deal->bid->price);

                $serv = (0.12*($deal->bid->price + $deal->bid->request->price));
		$service->setName('Service')
			->setCurrency($currency)
			->setQuantity(1)
			->setPrice($serv);

		$items[] = $products;
		$items[] = $traveller;
		$items[] = $service;

		$item_list = new ItemList();
		$item_list->setItems($items);

		//Details
		$details->setShipping('0.00')
			->setTax('0.00')
			->setSubtotal($deal->pay->total);

		//Amount
		$amount->setCurrency('EUR')
			->setTotal($deal->pay->total)
			->setDetails($details);

		//Transaction
		$transaction->setAmount($amount)
			->setItemList($item_list)
			->setDescription('Antebox');

		//Payment
		$payment->setIntent('sale')
			->setPayer($payer)
			->setTransactions([$transaction]);

		//Redirect URLs
		$redirectUrls->setReturnUrl('https://antebox.com/success/'.$deal->pay->hash)
			->setCancelUrl('https://antebox.com/deal/6');

		$payment->setRedirectUrls($redirectUrls);


		try {
		    $payment->create($this->_api_context);
		} catch (Exception $ex) {
			//error page
			exit(1);
		}

		$approvalUrl = $payment->getApprovalLink();

		\Session::put('paypal_payment_id', $payment->getId());


		return redirect($approvalUrl);


	}

	public function success($hash)
	{
		// Get the payment ID before session clear
		$payment_id = \Session::get('paypal_payment_id');

		if($payment_id==null){
			return view('/');
		}

		$pay = Pay::all()->where('hash', $hash)->first();

		if($pay==null){
			return view('errors.404');
		}

		// clear the session payment ID
		\Session::forget('paypal_payment_id');

		$payment = Payment::get($payment_id, $this->_api_context);
                $paym = json_decode($payment);

		$execution = new PaymentExecution();
		$execution->setPayerId($paym->payer->payer_info->payer_id);
		$payment->execute($execution, $this->_api_context);

		foreach($paym->transactions as $tran){
		    if($tran->amount->total!=$pay->total){
			return view('errors.404');
		    }else if($tran->amount->currency!='EUR'){
			return view('errors.404');
		    }else if($tran->payee->email!='CHRIS@ANTEBOX.COM'){
			return view('errors.404');
		    }
		}

		MyPayment::create([
			'pay_id' 	=> $pay->id,
			'transaction_id'=> $paym->id,
			'cart_id'	=> $paym->cart,
			'payment_method'=> $paym->payer->payment_method,
			'status'	=> $paym->payer->status,
			'email'		=> $paym->payer->payer_info->email,
			'first_name'	=> $paym->payer->payer_info->first_name,
			'last_name'	=> $paym->payer->payer_info->last_name,
			'payer_id'	=> $paym->payer->payer_info->payer_id,
			]);

		$pay->paid = 1;
		$pay->save();

                Notifications::create([
                        'user_id'  => $pay->deal->traveller->id,
                        'msg'      => $pay->deal->client->first_name." has paid for your deal",
                        'link'     => "/deal/".$pay->deal->id,
                        'category' => "paid",
                ]);

		$notif = $pay->deal->client->notifications()->get();

        	foreach($notif as $not){
        	    if($not->category=="warning" && $not->link="/deal/".$pay->deal->id){
        	        $not->status = "1";
        	        $not->save();
        	    }
        	}

		$deal = $pay->deal;
                $products = Products::all()->random(3);

                Mail::send('emails.request.pay_traveller', ['deal' => $deal, 'products' => $products], function($message) use ($deal)
                {
                    $message->from("no-reply@antebox.com", "AnteBox")
                    ->to($deal->traveller->email, $deal->traveller->first_name)
                    ->subject("It's Showtime!");
                });

                Mail::send('emails.request.pay_client', ['deal' => $deal, 'products' => $products], function($message) use ($deal)
                {
                    $message->from("no-reply@antebox.com", "AnteBox")
                    ->to($deal->client->email, $deal->client->first_name)
                    ->subject("One Step Closer!");
                });




		return redirect('deal/'.$pay->deal->id);

	}


}

