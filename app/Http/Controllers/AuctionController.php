<?php namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Auth;
use Cookie;
use DateTime;
use DateInterval;
use App\Auction;
use App\Bid;
use App\User;
use Mail;

class AuctionController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// $this->middleware('auth');
	}

  public function create()
  {
    return view('auctions.create');
  }

	public function all()
	{
		$auctions = Auction::paginate(20);

		return view('auctions.all')->with('auctions', $auctions);
	}

	public function bid(Request $request)
	{
		$request->merge(['user_id' => Auth::id() ]);
		$validator = Validator::make($request->all(), [
			'auction_id' => 'required',
			'user_id' => 'required',
			'amount' => 'required',
			'expires' => 'required|in:2,4,8,12,24',
			'meeting_x' => 'required',
			'meeting_y' => 'required'
 		]);

		if($validator->fails())
		{
			$errors = view('shared.errors')->withErrors($validator)->render();
			return response()->json(
					[
					'success' => false,
					'errors' => $errors]
					,422);
		}

		$expires_at = date("Y-m-d H:i:s", strtotime('+' . $request->expires .' hours'));
		$request->merge(['expires_at' => $expires_at ]);
		Bid::create($request->all());

		return response()->json(
				['success' => true,
				'data' => json_encode($request->all())]);
	}

	public function view($id)
	{
		$auction = Auction::find($id);
		$dateTimeNow = new DateTime("now");
		if($auction->deadline > $dateTimeNow)
		{
			$interval = $auction->deadline->diff($dateTimeNow);
			$auction->ends_days = $interval->format('%d');
			$auction->ends_hours = $interval->format('%H');
			$auction->ends_minutes = $interval->format('%i');
			$auction->ended = false;
		}else {
			$auction->ended = true;
		}

		$bids = Bid::where('auction_id', $id)->get();
		$auction->bid_count= $bids->count();

		return view('auctions.view')->with('auction', $auction)
																->with('bids', $bids);
	}

	public function save(Request $request)
	{
		$this->validate($request, [
			'user_id' => 'required',
			'item_name' => 'required',
			'description' => 'required',
			'parcel_size' => 'required',
			'size_h' => 'required_if:parcel_size,Custom',
			'size_w' => 'required_if:parcel_size,Custom',
			'size_d' => 'required_if:parcel_size,Custom',
			'origin' => 'required',
			'destination' => 'required',
			'deadline' => 'required|date|after:now',
		]);

		$auction = Auction::create($request->all());

		return redirect('/auctions/view/' . $auction->id);
	}

	public function mail()
	{

		$users = User::all();

		$emails = ['gehenna63@hotmail.com'];

//		foreach($users as $user){

			Mail::send('emails.news_apr', [], function($message) use ($user)
			{
			  $message->from("newsletter@antebox.com", "AnteBox")
        		  ->to($user->email, $user->first_name)
//			  ->to('christoforos.konstantinidis.1@gmail.com', 'Chris')
//			  ->to('gehenna63@hotmail.com', 'chris')
        		  ->subject('Share your unique story with AnteBox');
			});
//		}
	}
}
