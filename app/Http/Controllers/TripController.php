<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Trip;
use App\User;
use Auth;
use DateTimeZone;
use DateTime;
use Mail;

class TripController extends Controller
{


	private $form_rules = [
		'from_country'	=> 'required',
		'to_country'	=> 'required',
		'from_city'	=> 'sometimes|different:to_city',
		'departure'           => 'required',
	];
	
	private $form_rules2 = [
		'from_country'	=> 'required',
//		'return_from_country'	=> 'required',
		'to_country'	=> 'required',
//		'return_to_country'	=> 'required',
//		'from_city'	=> 'sometimes|different:to_city',
		'departure'           => 'required',
		'return_departure'		=> 'required',
	];

	public function view()
	{

                $trips =  Trip::all();

                $tz = new DateTimeZone('Europe/Athens');
                $nowf = new DateTime('now', $tz);
		$nowt = $nowf->format('Y-m-d H:i:s');
                $now = strtotime($nowf->format('Y-m-d H:i:s'));

                foreach($trips as $trip){
                    $time = strtotime($trip->dep);
                    if($now > $time ){
                        if($trip->status!="Cancelled"){
                            $trip->status="Expired";
                            $trip->save();
                        }
                    }

                }

		$trips = Trip::where('dep', '>', $nowt)
				->where('status', '<>', 'Cancelled')
				->orderBy('dep', 'asc')
				->paginate(10);


		return view('trip.view')->with('trips', $trips);
	}

	public function create()
	{
		return view('trip.create');
	}

	public function createTrip(Request $request)
	{
		
		if ($request->optradio == 1){
			if (Auth::check()){
				$v = \Validator::make($request->all(), $this->form_rules);
			}
			
			if ($v->fails()) {
				return redirect()->back()->withErrors($v);
			}

			Trip::create([
				'user_id'       => Auth::user()->id,
				'from_country'  => $request->from_country,
				'from_city'     => $request->from_city,
				'to_country'    => $request->to_country,
				'to_city'       => $request->to_city,
				'dep'           => $request->departure." 23:59",
				'arr'           => $request->departure." 23:59",
				'status'	=> "Open",
			]);
		} else {
			if (Auth::check()){
				$v = \Validator::make($request->all(), $this->form_rules2);
			}
			
			if ($v->fails()) {
				return redirect()->back()->withErrors($v);
			}

			Trip::create([
				'user_id'       => Auth::user()->id,
				'from_country'  => $request->from_country,
				'from_city'     => $request->from_city,
				'to_country'    => $request->to_country,
				'to_city'       => $request->to_city,
				'dep'           => $request->departure." 23:59",
				'arr'           => $request->departure." 23:59",
				'status'	=> "Open",
			]);
			
			Trip::create([
				'user_id'       => Auth::user()->id,
				'from_country'  => $request->to_country,
				'from_city'     => $request->to_city,
				'to_country'    => $request->from_country,
				'to_city'       => $request->from_city,
				'dep'           => $request->return_departure." 23:59",
				'arr'           => $request->return_arrival." 23:59",
				'status'	=> "Open",
			]);
		}
				
		return redirect('trips/mytrips')->with('status', 'Trip was successfully saved!');
	}

	public function mytrips()
        {

                $user_id = Auth::user()->id;

                //Expired Check
                $trips = User::find($user_id)->trips;

		$tz = new DateTimeZone('Europe/Athens');
		$nowf = new DateTime('now', $tz);
		$now = strtotime($nowf->format('Y-m-d H:i:s'));

		foreach($trips as $trip){
		    $time = strtotime($trip->dep);
		    if($now > $time ){
                        if($trip->status!='Cancelled'){
                            $trip->status='Expired';
                            $trip->save();
                        }
		    }
                }

                $trips = Trip::where('user_id', $user_id)->orderBy('dep', 'desc')->paginate(10);

                return view('trip.mytrips')->with('trips', $trips);
        }




	public function cancelTrip($trip_id)
        {

                $user_id = Auth::user()->id;

                $trip = Trip::find($trip_id);

		//Expired Check
                $tz = new DateTimeZone('Europe/Athens');
                $nowf = new DateTime('now', $tz);
                $now = strtotime($nowf->format('Y-m-d H:i:s'));
		$time = strtotime($trip->dep);
                if(($now > $time)){
                    return redirect('trips/mytrips');
                }

                if($user_id==$trip->user_id){
                        $trip->status='Cancelled';
                        $trip->save();
                }

                return redirect('trips/mytrips');
        }

	public function editTrip($trip_id){
		$user_id = Auth::user()->id;

		$trip = Trip::find($trip_id);
		
		return view('trip.edit')->with('trip', $trip);
		
		
	}
	
	public function saveTrip(Request $request, $trip_id)
	{
		$user_id = Auth::user()->id;
		$trip = Trip::find($trip_id);
		
		$trip->from_country = $request->from_country;
		$trip->to_country = $request->to_country;
		$trip->dep = $request->departure." 23:59";
		$trip->arr = $request->departure." 23:59";
		
		$trip->save();
		
		return redirect('trips/mytrips');
	}


}
