<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Deal;
use App\Notifications;
use App\Products;
use Mail;
use App\Messages;
use Auth;

class DealController extends Controller
{
        public function view($id)
        {
            $deal = Deal::findOrFail($id);
            $user = Auth::user();
            $request = $deal->bid->request;

            if($user==$deal->client){
                return view('request.deal_client')->with('deal', $deal)->with('request', $request);
            }
            else if($user==$deal->traveller){
                return view('request.deal_traveller')->with('deal', $deal)->with('request', $request);
            }
            else{
                return view('errors.404');
            }
        }

	public function cancel($id)
	{
        	$deal = Deal::findOrFail($id);
                $user_id = Auth::user()->id;

		if($deal->status!="Active"){
                    return view('errors.404');
		}

                if($user_id==$deal->client->id){
		    if($deal->isBuy){
                        return view('errors.404');
		    }else{
                        $deal->status = "Cancelled";
                        $deal->save();
		    }
                }

		if($user_id==$deal->traveller->id){
                    $deal->status = "Cancelled";
                    $deal->save();
		}


                return redirect()->back();
	}
	
	public function message($id)
	{
		$message = $_GET['text_message'];
		$isClientMsg = $_GET['isClientMsg'];
		
		$msg = Messages::create([
					'deal_id' 	=> $id,
					'message' 	=> $message,
					'isClientMsg' 	=> $isClientMsg,
		]);


		$deal = Deal::find($id);
		$auth = Auth::user();
		if($isClientMsg){
			$user=$deal->traveller;
		}else{
			$user=$deal->client;
		}

                Notifications::create([
                        'user_id'  => $user->id,
                        'msg'      => "You have a new message from ".$auth->first_name,
                        'link'     => "deal/".$id,
                        'category' => "msg",
                ]);


                $products = Products::all()->random(3);

                        Mail::send('emails.request.message', ['sender' => $auth, 'receiver' => $user, 'msg' => $msg, 'products' => $products], function($message) use ($user, $auth)
                        {
                          $message->from("no-reply@antebox.com", "AnteBox")
                          ->to($user->email, $user->first_name)
                          ->subject('Your have a new message from '.$auth->first_name.'!');
                        });


	}

	public function buy($id)
	{

	        $deal = Deal::find($id);
                $auth = Auth::user();
		$traveller = $deal->traveller;

		if($auth!=$traveller){
                        return view('errors.404');
		}
		if(!$deal->pay->paid){
                        return view('errors.404');
		}
		if($deal->isBuy){
                        return view('errors.404');
		}

		$deal->isBuy = '1';
		$deal->save();

                Notifications::create([
                        'user_id'  => $deal->client->id,
                        'msg'      => $auth->first_name." bought the products you requested",
                        'link'     => "deal/".$id,
                        'category' => "shop",
                ]);

                $products = Products::all()->random(3);

                        Mail::send('emails.request.buy_client', ['deal' => $deal, 'products' => $products], function($message) use ($deal)
                        {
                          $message->from("no-reply@antebox.com", "AnteBox")
                          ->to($deal->client->email, $deal->client->first_name)
                          ->subject('Your order is on its way!');
                        });

                        Mail::send('emails.request.buy_traveller', ['deal' => $deal, 'products' => $products], function($message) use ($deal)
                        {
                          $message->from("no-reply@antebox.com", "AnteBox")
                          ->to($deal->traveller->email, $deal->traveller->first_name)
                          ->subject('You are ready!');
                        });


		return redirect()->back();
	}

	public function mydeals()
	{
		$user = Auth::user();

		$dealc = $user->clientDeals;
		$dealt = $user->travellerDeals;

		return view('request.mydeals')->with('dealc', $dealc)->with('dealt', $dealt);
	}

	public function deliver($id)
	{
                $deal = Deal::find($id);
                $auth = Auth::user();
                $traveller = $deal->traveller;

                if($auth!=$traveller){
                        return view('errors.404');
                }
                if(!$deal->pay->paid){
                        return view('errors.404');
                }
                if(!$deal->isBuy){
                        return view('errors.404');
                }

                $deal->isDeliverd = '1';
                $deal->save();

                Notifications::create([
                        'user_id'  => $deal->client->id,
                        'msg'      => $auth->first_name." delivered the products",
                        'link'     => "deal/".$id,
                        'category' => "antebox",
                ]);

		if($deal->isReceived){
			$deal->status="Completed";
			$deal->save();
		}

                return redirect()->back();

	}


        public function recive($id)
        {
                $deal = Deal::find($id);
                $auth = Auth::user();
                $client = $deal->client;

                if($auth!=$client){
                        return view('errors.404');
                }
                if(!$deal->pay->paid){
                        return view('errors.404');
                }
                if(!$deal->isBuy){
                        return view('errors.404');
                }

                $deal->isReceived = '1';
                $deal->save();

                Notifications::create([
                        'user_id'  => $deal->traveller->id,
                        'msg'      => $auth->first_name." received the products",
                        'link'     => "deal/".$id,
                        'category' => "antebox",
                ]);

                if($deal->isDeliverd){
                        $deal->status="Completed";
                        $deal->save();
                }

                return redirect()->back();

        }

}
