<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use App\RejectBid;
use App\Bid;

class BidController extends Controller
{

        public function mybids()
        {
        	$user = Auth::user();
		$cancel = $user->bids->where('status', 'Cancelled');
		$bids = $user->bids->diff($cancel);

                return view('request.mybids')->with('bids', $bids);
        }

	public function reject($bid)
	{
		$bid->status = "Rejected";
		$bid->save();

	}

	public function rejectBid($bid_id, $reason)
	{
                $user = Auth::user();
                $bid = Bid::findOrFail($bid_id);
                if($user->id === $bid->request->user->id){
		    $str = 'Just Reject';
		    switch($reason) {
			case 1:
    			    $str = "Too Expensive";
        		    break;
    			case 2:
    			    $str = "Can't this date/time";
        		    break;
    			case 3:
			    $str = "Less review/profile details";
        		    break;
                        case 4:
			    $str = "Just Reject";
                            break;
		    }

		    RejectBid::create(['bid_id' => $bid_id,
					'reason' => $str,
					]);

		    $this->reject($bid);
                }
                else{
                    return view('errors.404');
                }

                return redirect()->back()->with('status', "Bid Rejected Successfuly!");

	}
}
