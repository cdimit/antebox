<?php namespace App\Http\Controllers;

use App\Subscriber;
use App\Trip;
use App\Notifications;
use App\Products;
use App\SuggestProduct;
use App\SuggestCompany;
use App\User;
use App\Company;
use App\Stores;
use App\Bid;
use Mail;
use App\Deal;
use App\IpTable;
use App\ReportUser;
use App\MyRequest;
use App\Newsletter;
use Auth;
use Validator;
use Cookie;

use PayPal\Api\Payment;

use Illuminate\Http\Request;
use App\Http\Requests;

class AdminController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// $this->middleware('auth');
	}

	public function subscribers()
	{
		$subscribers = Subscriber::paginate(20);
		$subscribers_count = $subscribers->count();

		return view('admin.dashboard.subscribers')
					->with('subscribers', $subscribers)
					->with('subscribers_count', $subscribers_count);
	}

	public function dashboard()
	{
		$users = User::all();
		$trips = Trip::open();
		$report = ReportUser::all();
		$suggest = SuggestProduct::count() + SuggestCompany::count();

		return view('admin.dashboard')->with('users', $users)
					      ->with('trips', $trips)
					      ->with('suggest', $suggest)
					      ->with('report', $report);
	}

	public function dashboard2()
	{
		$users = User::all();
		$trips = Trip::open();
		$report = ReportUser::all();
		$suggest = SuggestProduct::open()->count() + SuggestCompany::open()->count();
		$products = Products::count();
		$company = Company::count();
		$requests = MyRequest::open()->count();
		$deals = Deal::active()->count();

		return view('admin.dashboard2')->with('users', $users)
					      ->with('trips', $trips)
					      ->with('requests', $requests)
					      ->with('suggest', $suggest)
					      ->with('products', $products)
					      ->with('deals', $deals)
					      ->with('company', $company)
					      ->with('report', $report);
	}

	public function users()
	{
		$users = User::all();

		return view('admin.users')->with('users', $users);

	}

	public function emailUsers()
	{
		$users = User::all();

		foreach($users as $user){
		    echo $user->email;
		    echo "<br>";
		}
	}

	public function emailNewsletter()
	{
		$users = User::all();

		foreach($users as $user){
		  if($user->isNewsletter()){
		    echo $user->email;
		    echo "<br>";
		  }
		}
	}

	public function searchTest(Request $request)
	{
        $keyword=$request->keyword;
        $pro= Products::where('name','LIKE','%'.$keyword.'%')->orderBy('name','asc')->get();
        return view('admin.test')->withProducts($pro);
	}

	public function test()
	{
//		$deal = Deal::find(8);
//		$products = Products::all()->random(3);

/*
		        Mail::send('emails.request.buy_client', ['deal' => $deal, 'products' => $products], function($message)
                        {
                          $message->from("test@antebox.com", "AnteBox")
//                          ->to('christoforos.konstantinidis.1@gmail.com', 'christos')
                          ->to('gehenna63@hotmail.com', 'christos')
                          ->subject('test');
                        });
*/


		return view('home_5oct');//->withProducts(Products::all());
	}

	public  function trips()
	{
		$trips = Trip::all();
		$cancel = $trips->where('status', 'Cancelled');
		$expired = $trips->where('status', 'Expired');
                $full = $trips->where('status', 'Full');
                $almost = $trips->where('status', 'Almost Full');
		$complete = $trips->where('status', 'Completed');
		$open = Trip::open();


		$from = $trips->pluck('from_country')->unique();
		$to = $trips->pluck('to_country')->unique();

		return view('admin.trips')->with('trips', $trips)
					  ->with('open', $open)
					  ->with('cancel', $cancel)
                                          ->with('full', $full)
                                          ->with('almost', $almost)
                                          ->with('complete', $complete)
                                          ->with('expired', $expired)
					  ->with('from',$from)
					  ->with('to', $to);
	}

	public function products()
	{
                $products = Products::all();

                return view('admin.products')->with('products', $products);
	}

        public function companies()
        {
                $company = Company::all();

                return view('admin.companies')->with('companies', $company);
        }

        public function requests()
        {
                $requests = MyRequest::all();

                return view('admin.requests')->with('requests', $requests);
        }

	public function deal()
	{
		$deals = Deal::all();

		return view('admin.deal')->with('deals', $deals);
	}


	public function stores()
        {
                $stores = Stores::all();

                return view('admin.stores')->with('stores', $stores);
        }

	public function suggests()
        {
                $suggest_company = SuggestCompany::all();
                $suggest_product = SuggestProduct::all();

                return view('admin.suggests')->with('company', $suggest_company)
						->with('product', $suggest_product);
        }


	public function ip()
	{
		$ip = IpTable::all();

		foreach ($ip as $i){
		    $i->delete();
		}
	}

	public function notify()
	{
		$user = Auth::user();

		Notifications::create([
			'user_id'  => $user->id,
			'msg'	   => "Add a Profile Picture",
			'link'	   => "/profile",
		]);

		return redirect()->back();

	}

        public function notifymsg()
        {
                $user = Auth::user();

                Notifications::create([
                        'user_id'  => $user->id,
                        'msg'      => "You have a new message",
                        'link'     => "admin/messages",
			'category' => "msg",
                ]);

                return redirect()->back();

        }

        public function notifybid()
        {
                $user = Auth::user();

                Notifications::create([
                        'user_id'  => $user->id,
                        'msg'      => $user->first_name." made a bid on your request",
                        'link'     => "/",
                        'category' => "bid",
                ]);

                return redirect()->back();

        }

	//Send Notification to all unverified users
        public function notifyverify()
        {
		$users = User::all()->where('verified', '0');

		foreach($users as $user){
	                Notifications::create([
        	                'user_id'  => $user->id,
                	        'msg'      => "Verify your email",
                        	'link'     => "/profile",
                        	'category' => "warning",
                	]);
		}

                return redirect()->back();
        }

	//Send notification to all users for update terms
        public function notifyterms()
        {
		//safety
		return redirect()->back();

		$users = User::all();

		foreach($users as $user){
                	Notifications::create([
                        	'user_id'  => $user->id,
                        	'msg'      => "We are updating our Terms and Conditions",
                        	'link'     => "/terms",
                        	'category' => "terms",
                	]);

                        Mail::send('emails.terms', ['first_name' => $user->first_name], function($message) use ($user)
                        {
                          $message->from("no-reply@antebox.com", "AnteBox")
                          ->to($user->email, $user->first_name)
                          ->subject('New features, new terms!');
                        });

		}

                return redirect()->back();

        }

		public function testing(){
			
			$request = MyRequest::Open();

			return view('home_5oct')->with('request', $request);
		
		}
		
		public function testing2(){
			
			return view('admin.testing2');
		
		}
}


