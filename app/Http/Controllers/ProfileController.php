<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Address;
use Input;
use App\ReportUser;
use App\Trip;
use App\VerificationToken;
use Hash;
use DateTime;
use DateInterval;
use App\ReportUserOther;
use App\Notifications;
use App\Messages;
use App\User;
use App\PayoutMethod;
use App\Newsletter;
use Mail;
use Schema;

class ProfileController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/


	private $form_rules = [
        	'first_name'		=> 'required|min:2|max:25|Alpha',
		'last_name'		=> 'required|min:2|max:25|Alpha',
		'email'                 => 'required|email|max:80|unique:users,email,',
		'mobile_phone'		=> 'numeric|digits_between:6,20',
		'description'		=> 'max:255',
		'file'			=> 'mimes:png,jpeg,bmp|max:5120',
	];

        private $form_rules_image = [
                'file'                  => 'mimes:png,jpeg,bmp|max:5120',
        ];

        private $form_rules_payout = [
                'paypal'                  => 'email|max:80',
                'iban'                  => 'max:34|min:15',
        ];

        private $form_rules_password = [
                'old_password'		=> 'required',
                'new_password'		=> 'required|confirmed|min:6|max:16',
        ];

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// $this->middleware('auth');
		$user = \Auth::user();

        	if ($user) {
        	    $this->form_rules['email'] .= $user->id;
		}

	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function view()
	{
		return view('profile.view');
	}
	
	public function messages()
	{
		return view('profile.messages');
	}

	public function updatePassword(Request $request)
	{
            $v = \Validator::make($request->all(), $this->form_rules_password);

            if ($v->fails()) {
               return redirect()->back()->withErrors($v)->withInput();
            }

	    $user = Auth::user();

	    if(Hash::check($request->old_password, $user->password)){
		$user->password = bcrypt($request->new_password);
		$user->save();
	    }
	    else{
	        return redirect()->back()->with('error', 'Wrong Password!!');
	    }

      	    return redirect()->back()->with('status', 'Password was successfully updated!');

	}


	public function sendVerificationEmail()
	{
		$user = Auth::user();
		if($user->isVerified()){
		    return view('errors.404');
		}


       		Mail::send('emails.verification', ['token' => $user->verificationToken->token, 'user_id' => $user->id], function($message) use ($user)
               	{
                        $message->from("no-reply@antebox.com", "AnteBox")
                                                        ->to($user->email, $user->name)
                                                        ->subject('Please confirm your email address - AnteBox');
                });

		return redirect()->back();

	}

	public function saveProfile(Request $request)
	{

	    $v = \Validator::make($request->all(), $this->form_rules);

	    if ($v->fails()) {
	       return redirect()->back()->withErrors($v)->withInput();
	    }

	    $id = Auth::user()->id;
	    $user = Auth::user()->find($id);
	    $user->first_name = $request['first_name'];
	    $user->last_name = $request['last_name'];

    	    if($user->email != $request->email){
	        $user->email = $request['email'];
	        $user->verified = 0;
		if($user->verificationToken){
	  	    $user->verificationToken->delete();
		}
                        Notifications::create([
                                'user_id'  => $user->id,
                                'msg'      => "Verify your email",
                                'link'     => "/profile",
                                'category' => "warning",
                        ]);

                    	$token = str_random('100');
                    	VerificationToken::create(array('user_id' => $user->id , 'token' => $token));

                        Mail::send('emails.verification', ['token' => $token, 'user_id' => $user->id], function($message) use ($user)
                        {
                                $message->from("no-reply@antebox.com", "AnteBox")
                                                                ->to($user->email, $user->name)
                                                                ->subject('Please confirm your email address - AnteBox');
                        });

	    }
	    if(($request['mobile_phone'] != '') || ($user->mobile_phone != NULL))
	    	$user->mobile_phone = $request['mobile_phone'];
            if(($request['sex'] != '') || ($user->sex != NULL))
		$user->sex = $request['sex'];
            $user->birthday = $request['year'] . '-' . $request['month'] . '-' . $request['day'];
            if(($request['description'] != '') || ($user->description != NULL))
	    	$user->description = $request['description'];

	    $image = Input::file('file');

	    $destinationPath = "/var/www/antebox/public/img/user_avatars/";

	    if (!empty($image)) {

		if (!Input::file('file')->isValid()) {
	                return redirect()->back()->withErrors('The picture must be less than 5Mb');
                }

		$ext = pathinfo($image->getClientOriginalName(),PATHINFO_EXTENSION);
		if (!$image->move($destinationPath, $id.'.'.$ext)){
		    return $this->errors(['message' => 'Error saving the image']);
		} else {
		    $user->pic_url = $id.'.'.$ext;
		}
	    }

	    $user->save();

	    return redirect()->back()->with('status', 'Profile was successfully saved!');
	}

	public function saveProfileImage(Request $request)
	{

            $v = \Validator::make($request->all(), $this->form_rules_image);

            if ($v->fails()) {
               return redirect()->back()->withErrors($v)->withInput();
            }

            $id = Auth::user()->id;
            $user = Auth::user()->find($id);

            $image = Input::file('file');

            $destinationPath = "/var/www/antebox/public/img/user_avatars/";

           // if (!empty($image)) {

                if (!Input::file('file')->isValid()) {
                        return redirect()->back()->withErrors('The picture must be less than 5Mb');
                }

                $ext = pathinfo($image->getClientOriginalName(),PATHINFO_EXTENSION);
                if (!$image->move($destinationPath, $id.'.'.$ext)){
                    return $this->errors(['message' => 'Error saving the image']);
                } else {
                    $user->pic_url = $id.'.'.$ext;
                }
            //}

            $user->save();

            return redirect()->back()->with('status', 'Profile picture was successfully saved!');

	}

	public function viewProfile($userID){

	    $user = Auth::user()->findOrFail($userID);

	    return view('profile.user')->with('user',$user);

	}

	public function sendMessage($id){

	    $from = Auth::user();
		$to = User::find($id);
		
		$subject = $_GET['text_subject'];
		$message = $_GET['text_message'];
		
		Messages::create([
					'user_id_to' 	=> $to->id,
					'user_id_from' 	=> $from->id,
					'subject'		=> $subject,
					'message'	 	=> $message,
		]);
		
	    //return view('profile.user')->with('user',$user);
		return view('errors.404');
	}
	
	public function reportUser($id, $code){

		$comment = $_GET['comment'];

		$user_from = Auth::user()->id;

                $to = User::find($id);
                $raw = $to->isReportedFrom($user_from);

		//if you want undo and has a record
		if($code == '5'){
			if($raw){
				$raw->report_code = '5';
				$raw->save();
				return view('errors.404');
			}
		}

		//if you want report and you have undo record
		if($raw && $raw->report_code == '5'){
			$raw->report_code = $code;
			$raw->save();

			if($code=='4'){
				$other = $raw->reportUserOther()->first();
				if($other){
					$other->comment = $comment;
					$other->save();
				}else{
					 ReportUserOther::create([
                                	'report_user_id' => $raw->id,
                                	'comment' => $comment,
	                        	]);
				}

			}
			return view('errors.404');
		}

		//if you have a record and you want another one(do nothing)
		if($raw)
			return view('errors.404');

		//create report
		$report = ReportUser::create([
			'user_id_from' => $user_from,
			'user_id_to' => $id,
			'report_code' => $code,
		]);

		//create report other
		if($code == '4'){
			ReportUserOther::create([
				'report_user_id' => $report->id,
				'comment' => $comment,
			]);
		}

		return view('errors.404');
	}


        public function notifications()
        {
                return view('profile.notifications');
        }

	public function updateNotifications(Request $request)
	{

		$user = Auth::user();
		$raw = $user->isNewsletter();

		if($request->newsletter==1 && !$raw){
			 Newsletter::create([
                   	     'user_id' => $user->id,
			]);
		}
		else if($request->newsletter!=1 && $raw){
			$news = Newsletter::all();
			foreach($news as $n){
				if($n->user_id==$user->id)
				    $n->delete();
			}
		}

		return redirect()->back();
	}


	public function savePayout(Request $request)
	{
		$user = Auth::user();

            $v = \Validator::make($request->all(), $this->form_rules_payout);

            if ($v->fails()) {
               return redirect()->back()->withErrors($v)->withInput();
            }

	    $methods = $user->payoutMethods;
	    $paypal = $methods->where('method', 'paypal')->first();
	    $iban = $methods->where('method', 'iban')->first();

	    if(!$paypal){
	        if($request->paypal!=""){
		    PayoutMethod::create([
			'method' => 'paypal',
			'data'	=> $request->paypal,
			'user_id' => $user->id,
		    ]);
		}
	    }else{
                if($request->paypal!=""){
		    $paypal->data = $request->paypal;
		    $paypal->save();
                }else if($request->paypal==""){
		    $paypal->delete();
		}
	    }

            if(!$iban){
                if($request->iban!=""){
                    PayoutMethod::create([
                        'method' => 'iban',
                        'data'  => $request->iban,
                        'user_id' => $user->id,
                    ]);
                }
            }else{
                if($request->iban!=""){
                    $iban->data = $request->iban;
                    $iban->save();
                }else if($request->iban==""){
                    $iban->delete();
                }
            }


		return redirect()->back();
	}

/*
	public function saveAddress(Request $request)
	{
		$request["user_id"] = Auth::id();
		$this->validate($request, [
			'user_id' => 'required',
			'address_line_1' => 'required',
			'address_line_2' => 'required',
			'city' => 'required',
			'county' => 'required',
			'postcode' => 'required',
			'country' => 'required'
		]);

		Address::create($request->all());

		return view('auth.done');
	}


	public function address()
	{
		return view('address.create');
	}
*/
}
