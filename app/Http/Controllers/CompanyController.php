<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SuggestCompany;
use App\Http\Requests;
use App\Company;
use Auth;
use Input;

class CompanyController extends Controller
{

	protected $form_rules = [
		'email'		=> 'required|email',
		'name'		=> 'required',
		'site'		=> 'url',
		'nickname'	=> 'required|alpha_dash',
		'pic_url'	=> '',
		'vid_url'	=> '',
		'poster'	=> '',
		'phone'		=> '',
		'address'	=> '',
		'country'	=> 'required',
		'city'		=> '',
		'zip_code'	=> 'numeric',
		'twitter'	=> 'url',
		'facebook'	=> 'url',
		'instagram'	=> 'url',
		'youtube'	=> 'url',
		'about'		=> 'required',
		'map_x'		=> '',
		'map_y'		=> '',
		'quote'		=> 'required',
		'quote_author' 	=> 'required',
    	];

	public function view($id)
	{
		$company = Company::findOrFail($id);

		return view('company.view')->with('company', $company);
	}

	public function viewPartner($name)
	{
		$nick = strtolower($name);
		$company = Company::all()->where('nickname', $nick)->first();

		return view('company.view')->with('company', $company);
	}

	public function create()
	{
		return view('company.create');
	}
	
	public function edit($companyID)
	{
		$company = Company::findOrFail($companyID);
		return view('company.edit')->with('company',$company);
	}

	public function createCompany(Request $request)
	{
		if (Auth::check()){
			$v = \Validator::make($request->all(), $this->form_rules);
		}

		if ($v->fails()) {
			return redirect()->back()->withErrors($v);
		}

		$image = Input::file('pic_url');
		$video = Input::file('vid_url');
		$poster = Input::file('poster');
		$vid = '';
		$logo = '';
		
		$destinationPath = "/var/www/antebox/public/img/company/";
		$destinationPathPoster = "/var/www/antebox/public/img/company/poster/";

		if(Company::all()->isEmpty()){
			$temp = 1;
		}else{
			$temp = Company::orderBy('created_at', 'desc')->first();
			$temp= $temp->id + 1;
		}

		if (!empty($image)) {
			$ext = pathinfo($image->getClientOriginalName(),PATHINFO_EXTENSION);
			if (!$image->move($destinationPath, $temp.'.'.$ext)){
				return $this->errors(['message' => 'Error saving the logo']);
			} else {
				$logo = ($temp).'.'.$ext;
			}
		}
					
	 	if (!empty($video)) {

			$ext = pathinfo($video->getClientOriginalName(),PATHINFO_EXTENSION);
			if (!$video->move($destinationPath, $temp.'.'.$ext)){
				return $this->errors(['message' => 'Error saving the video']);
			} else {
				$vid = ($temp).'.'.$ext;
		    }
		}
		
		if (!empty($poster)) {
			if (!$poster->move($destinationPathPoster, $temp.'.jpg')){
				return $this->errors(['message' => 'Error saving the poster']);
			}
		}

		$company = Company::create([
                    'email'         => $request->email,
                    'name'          => $request->name,
                    'site'          => $request->site,
		    'nickname'	    => $request->nickname,
                    'pic_url'       => $logo,
                    'vid_url'       => $vid,
                    'phone'         => $request->phone,
                    'address'       => $request->address,
                    'country'       => $request->country,
                    'city'          => $request->city,
                    'zip_code'      => $request->zip_code,
                    'twitter'       => $request->twitter,
                    'facebook'      => $request->facebook,
                    'instagram'     => $request->instagram,
					'youtube'	    => $request->youtube,
                    'about'         => $request->about,
                    'map_x'         => $request->map_x,
                    'map_y'         => $request->map_y,
					'quote'	    	=> $request->quote,
					'quote_author'  => $request->quote_author,
        	]);

		return redirect('company/view/'.$company->id);
	}
	
	public function update($companyID, Request $request)
	{
		if (Auth::check()){
				$v = \Validator::make($request->all(), $this->form_rules);
		}

		if ($v->fails()) {
			return redirect()->back()->withErrors($v);
		}

		$image = Input::file('pic_url');
		$video = Input::file('vid_url');
		$poster = Input::file('poster');
		$vid = '';
		$logo = '';
		
		$destinationPath = "/var/www/antebox/public/img/company/";
		$destinationPathPoster = "/var/www/antebox/public/img/company/poster/";
		
		$company = Company::find($companyID);
		
		if (!empty($image)) {
			$ext = pathinfo($image->getClientOriginalName(),PATHINFO_EXTENSION);
			if (!$image->move($destinationPath, $companyID.'.'.$ext)){
				return $this->errors(['message' => 'Error saving the image']);
			} else {
				$logo = $companyID.'.'.$ext;
				$company->pic_url = $logo;
			}
		}
					
	 	if (!empty($video)) {

			$ext = pathinfo($video->getClientOriginalName(),PATHINFO_EXTENSION);
			if (!$video->move($destinationPath, $companyID.'.'.$ext)){
				return $this->errors(['message' => 'Error saving the video']);
			} else {
				$vid = $companyID.'.'.$ext;
				$company->vid_url = $vid;
		    }
		}
		
		if (!empty($poster)) {
			if (!$poster->move($destinationPathPoster, $companyID.'.jpg')){
				return $this->errors(['message' => 'Error saving the poster']);
			}
		}
		
		$company->name = $request->name;
		$company->email = $request->email;
		$company->site = $request->site;
	        $company->nickname = $request->nickname;
		$company->phone = $request->phone;
		$company->address = $request->address;
		$company->country = $request->country;
		$company->city = $request->city;
		$company->zip_code = $request->zip_code;
		$company->twitter = $request->twitter;
		$company->facebook = $request->facebook;
		$company->instagram = $request->instagram;
		$company->about = $request->about;
	        $company->map_x = $request->map_x;
		$company->map_y = $request->map_y;
		$company->quote = $request->quote;
		$company->quote_author = $request->quote_author;
		
		$company->save();        	

		return redirect('company/view/'.$companyID)->with('status', 'Company was successfully saved!');
	}

	public function suggestForm()
	{
		return view('company.suggest');
	}

    private $form_rules_suggest = [
        'company'      => 'required|max:80',
        'country'        => 'required|max:80',
        'address'              => 'max:127',
        'products'       => 'required|max:1024',
        'unique'         => 'required|max:512',
        'website'        => 'max:80',
        'email'        => 'required|max:80|email',
        'comment'        => 'max:255',
        'name'        => 'required|max:80',
        'phone'        => 'max:80',
    ];

	public function suggestSent(Request $request)
        {

            $v = \Validator::make($request->all(), $this->form_rules_suggest);

            if ($v->fails()) {
                return redirect()->back()->withErrors($v)->withInput();
            }

	    SuggestCompany::create($request->all());

	    return redirect()->back()->withStatus('done');
	}


}
