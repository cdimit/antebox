<?php namespace App\Http\Middleware;

use Closure;
use Auth;
use App;

class AdminMiddleware {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if (is_null($request->user()) || !$request->user()->isAdmin())
    {
        App::abort(404);
    }

		return $next($request);
	}

}
