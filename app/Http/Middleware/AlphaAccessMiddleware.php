<?php namespace App\Http\Middleware;

use Closure;
use App;

class AlphaAccessMiddleware {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if($request->cookie('alpha-user') || App::environment() == 'dev')
		{
			return $next($request);
		}

    return redirect('/');
	}
}
