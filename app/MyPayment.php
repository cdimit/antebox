<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MyPayment extends Model
{
     /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'payment';

        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
                        'pay_id',
                        'transaction_id',
                        'cart_id',
			'payment_method',
			'status',
			'email',
			'first_name',
			'last_name',
			'payer_id',
	];
}
