<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportUserOther extends Model
{
        /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'report_user_other';

        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
                        'report_user_id',
                        'comment',
                        ];



}
