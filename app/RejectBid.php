<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RejectBid extends Model
{
     /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'reject_bid';

        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
                        'bid_id',
                        'reason',
	];
}
