<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Messages extends Model
{
         /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'messages';

        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
                        'deal_id',
                        'message',
                        'isClientMsg',
        ];

    public function deal()
    {
        return $this->belongsTo('App\Deal', 'deal_id');
    }


}
