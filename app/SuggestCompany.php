<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuggestCompany extends Model
{
     /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'suggestCompany';

        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
                        'company',
                        'country',
                        'address',
                        'website',
                        'products',
                        'unique',
                        'comment',
                        'name',
                        'email',
                        'phone',
                        'user_id',
                        'finished',
                        'assign',
        ];

    public function setAddressAttribute($address)
    {
        $this->attributes['address'] = trim($address) !== '' ? $address : null;
    }

    public function setWebsiteAttribute($website)
    {
        $this->attributes['website'] = trim($website) !== '' ? $website : null;
    }

    public function setPhoneAttribute($phone)
    {
        $this->attributes['phone'] = trim($phone) !== '' ? $phone : null;
    }

    public function setCommentAttribute($comment)
    {
        $this->attributes['comment'] = trim($comment) !== '' ? $comment : null;
    }

    public function scopeOpen($query)
    {
        return $query->where('finished', '0')->get();
    }
}
