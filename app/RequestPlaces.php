<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestPlaces extends Model
{
        /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'request_places';

        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
                        'request_id',
                        'name',
                        'map_x',
                        'map_y',
                        'country',
                        'city',
			'address',
			'isClientPlace',
        ];

}
