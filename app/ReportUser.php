<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportUser extends Model
{
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'report_user';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
			'user_id_from',
			'user_id_to',
			'report_code',
			'solution',
			];


    public function reportUserOther()
    {
        return $this->hasOne('App\ReportUserOther');
    }


}
