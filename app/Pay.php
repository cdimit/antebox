<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pay extends Model
{
     /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'pay';

        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
                        'user_id',
                        'hash',
                        'total',
                        'paid',
	];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function isPaid()
    {
        return $this->attributes['paid'] == true;
    }

    public function deal()
    {
	return $this->hasOne('App\Deal');
    }

}
