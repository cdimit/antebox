<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
     /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'trip';

        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
                        'user_id',
                        'from_country',
			'from_city',
                        'to_country',
                        'to_city',
                        'dep',
			'arr',
			'status',
	];
	

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function scopeOpen($query)
    {
	return $query->where('status', 'Open')->get();
    }

}
