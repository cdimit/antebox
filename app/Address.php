<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model {

		/**
		 * The database table used by the model.
		 *
		 * @var string
		 */

		/**
		 * The attributes that are mass assignable.
		 *
		 * @var array
		 */
		protected $fillable = ['user_id', 'address_line_1', 'address_line_2', 'city', 'county', 'postcode', 'country'];

		/**
		 * The attributes excluded from the model's JSON form.
		 *
		 * @var array
		 */
		protected $hidden = [];
}
