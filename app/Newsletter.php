<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model
{
     /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'newsletter';

        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
                        'user_id',
        ];

}
