<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IpTable extends Model
{
     /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'ip_table';

        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
                        'product_id',
                        'ip_address',
	];

}
