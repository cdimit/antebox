<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bid extends Model
{
     /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'bid';

        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
                        'user_id',
                        'request_id',
			'request_place_id',
			'price',
			'date',
			'date_flex',
			'time',
			'time_flex',
			'msg',
			'status',
	];


    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function deal()
    {
        return $this->hasOne('App\Deal');
    }

    public function reject()
    {
        return $this->hasOne('App\RejectBid', 'bid_id');
    }


    public function request()
    {
        return $this->belongsTo('App\MyRequest', 'request_id');
    }

    public function requestPlace()
    {
        return $this->belongsTo('App\RequestPlaces', 'request_place_id');
    }

    public function scopeOpen($query)
    {
	return $query->where('status', 'Open')->get();
    }

}
