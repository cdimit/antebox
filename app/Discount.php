<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
     /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'discount';

        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
                        'product_id',
                        'value',
                        'style',
	];

}
