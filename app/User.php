<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Auth;
use Cache;
use App\ReportUser;
use App\Newsletter;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['email',
			'password',
			'verified',
			'first_name',
			'last_name',
			'pic_url',
			'mobile_phone',
			'description',
			'sex',
			'role',
			'birthday',
			'provider',
			'provider_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];


	public function isAdmin()
	{
        	return $this->attributes['role'] == 'admin';
    	}

   	public function isVerified()
   	{
       		return $this->attributes['verified'] == '1';
   	}

	public function isReportedFrom($from)
	{
		$res = ReportUser::where('user_id_to', $this->id)
					->where('user_id_from', $from)
					->get();

		if($res == NULL) {
			return false;
		}

		foreach($res as $raw) {
			if(($raw->report_code != 5) && ($raw->solution == NULL) ){
				return $raw;
			}
		}

		return false;
	}


	public function trips()
	{
		return $this->hasMany('App\Trip');
	}

        public function payoutMethods()
        {
                return $this->hasMany('App\PayoutMethod');
        }

	public function isNewsletter()
	{
                $res = Newsletter::where('user_id', $this->id)
                                        ->first();

                if($res == NULL) {
                        return false;
                }

		return $res;

	}

	public function notifications()
	{
		return $this->hasMany('App\Notifications');
	}

        public function bids()
        {
                return $this->hasMany('App\Bid', 'user_id');
        }

        public function requests()
        {
                return $this->hasMany('App\MyRequest');
        }

        public function clientDeals()
        {
                return $this->hasMany('App\Deal', 'client_id');
        }

        public function travellerDeals()
        {
                return $this->hasMany('App\Deal', 'traveller_id');
        }

        public function pays()
        {
                return $this->hasMany('App\Pay');
        }

   	public function verificationToken()
   	{
   		return $this->hasOne('App\VerificationToken');
   	}

	public function isOnline()
	{
	    return Cache::has('user-is-online-' . $this->id);
	}
}
