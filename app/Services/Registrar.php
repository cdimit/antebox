<?php namespace App\Services;

use App\User;
use Validator;
use Illuminate\Contracts\Auth\Registrar as RegistrarContract;

class Registrar implements RegistrarContract {

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	public function validator(array $data)
	{
		return Validator::make($data, [
			'first_name' => 'Required|Min:2|Max:25|Alpha',
			'last_name' => 'Required|Min:2|Max:25|Alpha',
			'email' => 'required|email|Between:3,64|unique:users',
			'password' => 'required|confirmed|min:6|max:16',
			'g-recaptcha-response' => 'required|captcha',
		]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return User
	 */
	public function create(array $data)
	{
		return User::create([
			'email' => $data['email'],
			'password' => bcrypt($data['password']),
			'first_name' => $data['first_name'],
			'last_name' => $data['last_name'],
		]);
	}

}
