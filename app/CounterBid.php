<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CounterBid extends Model
{
     /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'counterBid';

        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
                        'bid_id',
                        'date',
			'time',
	];

}
