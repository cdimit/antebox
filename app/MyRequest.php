<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MyRequest extends Model
{
    /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'request';

        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
                        'user_id',
                        'product_id',
                        'qty',
			'price',
                        'msg',
                        'until',
                        'status',
        ];

    public function product()
    {
        return $this->belongsTo('App\Products', 'product_id');
    }

    public function places()
    {
	return $this->hasMany('App\RequestPlaces', 'request_id');
    }

    public function bids()
    {
        return $this->hasMany('App\Bid', 'request_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function scopeOpen($query)
    {
	return $query->where('status', 'Open')->orderBy('until','asc')->get();
    }
}
