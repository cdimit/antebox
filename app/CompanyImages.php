<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyImages extends Model
{
         /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'companyImages';

        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
                        'company_id',
                        'pic_url',
        ];

}
