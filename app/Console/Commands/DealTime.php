<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Deal;

class DealTime extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'antebox:dealtime';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '(Antebox) check if a deal unfinished';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $deals = Deal::active();

        foreach ($deals as $deal){
                $date = strtotime($deal->bid->date);
                $remaining = $date + 86400 - 10600 - time();
                if($remaining<=0){
                    $deal->status = "Unfinished";
                    $deal->save();
		}
        }

    }
}
