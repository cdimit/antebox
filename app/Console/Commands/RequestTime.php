<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\MyRequest;

class RequestTime extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'antebox:requesttime';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '(Antebox) check if a request expired';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $request = MyRequest::open();

        foreach ($request as $req){
		$date = strtotime($req->until);
                $remaining = $date + 86400 - 10600 - time();
		if($remaining<=0){
		    $req->status = "Expired";
		    $req->save();

		    foreach($req->bids as $bid){
			$bid->status = "Declined";
			$bid->save();
		    }
		}
       }

    }

}
