<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Notifications;

class NotificationsClean extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'antebox:notificationsclean';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '(Antebox) remove the read notifications from database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $notifications = Notifications::all();

	foreach($notifications as $not){
	    if($not->status==1){
		$not->delete();
	    }
	}
    }
}
