<?php namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {

	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		'App\Console\Commands\Inspire',
		'App\Console\Commands\IpFree',
		'App\Console\Commands\RequestTime',
		'App\Console\Commands\NotificationsClean',
		'App\Console\Commands\DealTime',
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	 * @return void
	 */
	protected function schedule(Schedule $schedule)
	{
		//$schedule->command('inspire')->hourly();
		$schedule->command('antebox:ipfree')->everyThirtyMinutes();
		$schedule->command('antebox:requesttime')->daily();
		$schedule->command('antebox:notificationsclean')->weekly();
		$schedule->command('antebox:dealtime')->daily();
	}

}
