<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deal extends Model
{
     /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'deal';

        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
                        'bid_id',
                        'client_id',
                        'traveller_id',
                        'pay_id',
                        'status',
                        'isBuy',
                        'isReceived',
                        'isDeliverd',
	];

    public function client()
    {
        return $this->belongsTo('App\User', 'client_id');
    }

    public function bid()
    {
        return $this->belongsTo('App\Bid', 'bid_id');
    }

    public function traveller()
    {
        return $this->belongsTo('App\User', 'traveller_id');
    }

    public function pay()
    {
        return $this->belongsTo('App\Pay', 'pay_id');
    }

    public function messages()
    {
        return $this->hasMany('App\Messages');//->orderBy('id', 'DESC');
    }

    public function scopeActive($query)
    {
        return $query->where('status', 'Active')->get();
    }


}
