<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    /**
        * The database table used by the model.
        *
        * @var string
    */
    protected $table = 'company';

    /**
       * The attributes that are mass assignable.
       *
       * @var array
    */
    protected $fillable = [
	'email',
	'name',
	'site',
	'pic_url',
	'vid_url',
	'phone',
	'address',
	'country',
	'city',
	'zip_code',
	'twitter',
	'facebook',
	'instagram',
	'youtube',
	'about',
	'map_x',
	'map_y',
	'quote',
	'quote_author',
	'nickname',
    ];


    public function products()
    {
	return $this->hasMany('App\Products');
    }

    public function images()
    {
	return $this->hasMany('App\CompanyImages');
    }




}
