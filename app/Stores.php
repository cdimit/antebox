<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stores extends Model
{
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'stores';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
			'name',
			'phone',
			'address',
                        'zip_code',
                        'country',
                        'city',
                        'map_x',
			'map_y',
			];


    public function products()
    {
	return $this->belongsToMany('App\Products' , 'product_stores', 'store_id', 'product_id');
    }


}
