<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuggestProduct extends Model
{
     /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'suggestProduct';

        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
                        'name',
                        'category',
                        'origin',
                        'pic_url',
                        'producer',
                        'website',
                        'email',
                        'comment',
                        'user_name',
                        'user_email',
                        'user_id',
                        'finished',
                        'assign',
	];

    public function setProducerAttribute($producer)
    {
        $this->attributes['producer'] = trim($producer) !== '' ? $producer : null;
    }

    public function setWebsiteAttribute($website)
    {
        $this->attributes['website'] = trim($website) !== '' ? $website : null;
    }

    public function setEmailAttribute($email)
    {
        $this->attributes['email'] = trim($email) !== '' ? $email : null;
    }

    public function setCommentAttribute($comment)
    {
        $this->attributes['comment'] = trim($comment) !== '' ? $comment : null;
    }

    public function scopeOpen($query)
    {
        return $query->where('finished', '0')->get();
    }

}
