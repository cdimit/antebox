<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Auction extends Model {

	//
	protected $fillable = ['user_id', 'item_name', 'item_quantity', 'description', 'origin', 'destination', 'meeting_x', 'meeting_y', 'deadline'];

	protected $dates = ['deadline'];
}
