<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Products extends Model
{
    /**
      * The database table used by the model.
      *
      * @var string
    */
    protected $table = 'products';

    /**
      * The attributes that are mass assignable.
      *
      * @var array
    */
    protected $fillable = [
	'category',
        'company_id',
        'name',
        'description',
	'price',
	'weight',
	'size_h',
	'size_w',
	'size_d',
	'pic_url',
	'vid_url',
	'visit',
	'visit_uniq',
	'bag',
    ];

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function images()
    {
        return $this->hasMany('App\ProductImages');
    }

    public function stores()
    {
	return $this->BelongsToMany('App\Stores', 'product_stores', 'product_id', 'store_id');
    }

    public function discount()
    {
	return $this->hasOne('App\Discount', 'product_id');
    }
}
