<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\UserRole;

class UserRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_roles')->truncate();

        UserRole::create(['user_id' => 1, 'role_id' => 1]);
        UserRole::create(['user_id' => 2, 'role_id' => 1]);
        UserRole::create(['user_id' => 3, 'role_id' => 1]);
    }
}
