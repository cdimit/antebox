<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->string('name');
            $table->string('site');
            $table->string('pic_url')->default("company.png");
	    $table->string('vid_url')->nullable();
            $table->string('phone')->nullable();
            $table->string('address')->nullable();
	    $table->string('country');
            $table->string('city')->nullable();
            $table->string('zip_code')->nullable();
            $table->string('twitter')->nullable();
            $table->string('facebook')->nullable();
            $table->string('instagram')->nullable();
            $table->string('youtube')->nullable();
            $table->longText('about');
	    $table->string('quote');
	    $table->string('quote_author');
            $table->string('map_x')->nullable();
            $table->string('map_y')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('company');
    }
}
