<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestPlacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_places', function (Blueprint $table) {
            $table->increments('id');
	    $table->integer('request_id')->unsigned();
	    $table->string('name');
            $table->string('map_x');
            $table->string('map_y');
	    $table->string('country');
            $table->string('city');
            $table->string('address');
            $table->boolean('isClientPlace')->default(true);
	    $table->timestamps();

            $table->foreign('request_id')->references('id')->on('request');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('request_places');
    }
}
