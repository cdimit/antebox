<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDealTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deal', function (Blueprint $table) {
            $table->increments('id');
	    $table->integer('bid_id')->unsigned();
	    $table->integer('client_id')->unsigned();
	    $table->integer('traveller_id')->unsigned();
	    $table->integer('pay_id')->unsigned();
            $table->string('status');
            $table->boolean('isBuy')->default(false);
            $table->boolean('isReceived')->default(false);
            $table->boolean('isDeliverd')->default(false);
	    $table->timestamps();

            $table->foreign('bid_id')->references('id')->on('bid');
            $table->foreign('client_id')->references('id')->on('users');
            $table->foreign('traveller_id')->references('id')->on('users');
            $table->foreign('pay_id')->references('id')->on('pay');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('deal');
    }
}
