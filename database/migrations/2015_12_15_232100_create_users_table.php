<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('email')->unique();
			$table->string('password', 60);
			$table->rememberToken();
			$table->boolean('verified')->default(0);
			$table->string('provider')->nullable();
			$table->string('provider_id')->nullable();
			$table->timestamps();
			$table->string('first_name');
			$table->string('last_name');
			$table->string('pic_url')->default('profile.png');
			$table->string('mobile_phone')->nullable();
			$table->string('description')->nullable();
			$table->string('sex')->nullable();
			$table->date('birthday')->default('0000-00-00');
			$table->enum('role',array_keys(trans('globals.roles')))->default('user');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
