<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuggestCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('suggestCompany', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company', 80);
            $table->string('country', 80);
            $table->string('address', 127)->nullable();
            $table->string('website', 80)->nullable();
            $table->string('products', 1024);
            $table->string('unique', 512);
            $table->string('comment', 255)->nullable();
            $table->string('name', 80);
            $table->string('email', 80);
            $table->string('phone', 80)->nullable();
            $table->integer('user_id')->unsigned()->nullable();

            $table->boolean('finished')->default(false);
            $table->integer('assign')->unsigned()->nullable();

            $table->timestamps();

            $table->foreign('assign')->references('id')->on('users');
            $table->foreign('user_id')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('suggestCompany');
    }
}
