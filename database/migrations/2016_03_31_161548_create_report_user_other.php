<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportUserOther extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_user_other', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('report_user_id')->unsigned();
            $table->string('comment'); //255char
            $table->timestamps();
            $table->foreign('report_user_id')->references('id')->on('report_user');
          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('report_user_other');
    }
}
