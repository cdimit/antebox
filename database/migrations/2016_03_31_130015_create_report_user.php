<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id_from')->unsigned();
            $table->integer('user_id_to')->unsigned();
            $table->tinyinteger('report_code');
            $table->string('solution')->nullable(); //255char
            $table->timestamps();
            $table->foreign('user_id_to')->references('id')->on('users');
            $table->foreign('user_id_from')->references('id')->on('users');
          });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('report_user');
    }
}
