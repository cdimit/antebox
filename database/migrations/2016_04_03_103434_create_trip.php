<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrip extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	Schema::create('trip', function (Blueprint $table) {
        	$table->increments('id');
        	$table->integer('user_id')->unsigned();
        	$table->string('from_country');
		$table->string('from_city')->nullable();
		$table->string('to_country'); //255char
		$table->string('to_city')->nullable();
		$table->dateTime('dep');
		$table->dateTime('arr');
		$table->string('status');
        	$table->timestamps();
        	$table->foreign('user_id')->references('id')->on('users');
	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('trip');
    }
}
