<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRejectBidTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	Schema::create('reject_bid', function (Blueprint $table) {
	    $table->increments('id');
	    $table->integer('bid_id')->unsigned();
	    $table->string('reason');
	    $table->timestamps();

            $table->foreign('bid_id')->references('id')->on('bid');

	});    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	Schema::drop('reject_bid');
    }
}
