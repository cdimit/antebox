<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuggestProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
 public function up()
    {
       Schema::create('suggestProduct', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 80);
            $table->string('category', 25);
            $table->string('origin', 80);
            $table->string('pic_url')->nullable();
            $table->string('producer', 80)->nullable();
            $table->string('website', 80)->nullable();
            $table->string('email', 80)->nullable();
            $table->string('comment', 255)->nullable();
            $table->string('user_name', 80);
            $table->string('user_email', 80);
            $table->integer('user_id')->unsigned()->nullable();

            $table->boolean('finished')->default(false);
            $table->integer('assign')->unsigned()->nullable();

            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('assign')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('suggestProduct');
    }
}
