<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ip_table', function(Blueprint $table)
	{
		$table->increments('id');
		$table->integer('product_id')->unsigned();
		$table->string('ip_address', 45);
		$table->timestamps();

		$table->foreign('product_id')->references('id')->on('products');
	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ipTable');
    }
}
