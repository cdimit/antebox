<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('payment', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pay_id')->unsigned();
            $table->string('transaction_id', 255);
            $table->string('cart_id', 255);
	    $table->string('payment_method');
	    $table->string('status');
	    $table->string('email');
	    $table->string('first_name');
	    $table->string('last_name');
            $table->string('payer_id', 255);
            $table->timestamps();

            $table->foreign('pay_id')->references('id')->on('pay');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payment');
    }
}
