<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBidTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bid', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('request_id')->unsigned();
            $table->double('price', 10, 2);
            $table->string('msg')->nullable();
            $table->date('date');
	    $table->boolean('date_flex')->default(0);
	    $table->time('time');
            $table->boolean('time_flex')->default(0);
	    $table->integer('request_place_id')->unsigned();
            $table->string('status')->default('Open');
            $table->timestamps();

            $table->foreign('request_id')->references('id')->on('request');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('request_place_id')->references('id')->on('request_places');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bid');
    }
}
