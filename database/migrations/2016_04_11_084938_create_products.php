<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('category', 50);
            $table->integer('company_id')->unsigned();
            $table->string('name', 100);
            $table->string('description', 500);
            $table->double('price', 10, 2);
	    $table->integer('weight')->unsigned();
	    $table->integer('size_h')->unsigned();
            $table->integer('size_w')->unsigned();
            $table->integer('size_d')->unsigned();
	    $table->string('pic_url')->default("product.png");
	    $table->string('vid_url')->nullable();
	    $table->integer('visit')->default(0);
	    $table->integer('visit_uniq')->default(0);

	    $table->foreign('company_id')->references('id')->on('company');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
