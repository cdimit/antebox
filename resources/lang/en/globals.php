<?php 

    return [
	'roles' => [
	    'user'	=> 'User',
	    'admin'	=> 'Admin',
	],

    	'report_code' => [
        	'1' => 'profile',
        	'2'  => 'share',
        	'3' => 'spam',
        	'4'  => 'other',
        	'5'  => 'undo',
		'6' => 'error',
    	],

    ];
?>
