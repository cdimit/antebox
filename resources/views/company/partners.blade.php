@include('partials.header')

<div>
	<div class="container companies">
		<h1>Companies</h1>
		
		<div class="row">
			@foreach($companies as $company)
			<a href="/company/view/{{$company->id}}">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-3">
						<img src="{{ URL::asset('img/company/'.$company->pic_url) }}" style="max-width: 225px; max-height:225px; width:100%; height:auto;">
					</div>
					<div class="col-md-9">
						<h1>
							{{ $company->name }}	
						</h1>
						<blockquote>
							<p>
								{{$company->quote}}
							</p>
							<footer>
								{{$company->quote_author}}
							</footer>
						</blockquote>
					</div>
				</div>
			</div>
			</a>
			@endforeach
		</div>
	</div>
	
</div>

@include('partials.footer')