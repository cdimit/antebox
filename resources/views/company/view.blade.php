@extends('app')
@section('open-graph')
<meta property="og:title" content="{{ $company->name }} - Antebox" />
<meta property="og:image" content="{{ URL::asset('img/company/'.$company->pic_url) }}" />
<meta property="og:image:secure_url" content="{{ URL::asset('img/company/'.$company->pic_url) }}" />
<meta property="og:url" content="https://www.antebox.com/company/view/{{$company->id}}" />
<meta property="og:description" content="{{ str_limit($company->about, $limit = 150, $end = '...') }}" />
<meta name="twitter:card" content="summary">
<meta name="twitter:site" content="@anteboxapp">
<meta name="twitter:creator" content="@anteboxapp">
<meta name="twitter:title" content="{{$company->name}} - Antebox">
<meta name="twitter:description" content="{{ str_limit($company->about, $limit = 150, $end = '...') }}">
<meta name="twitter:image" content="{{ URL::asset('img/company/'.$company->pic_url) }}">
@endsection

@section('content')

<style>
  html, body {
	height: 100%;
	margin: 0;
	padding: 0;
  }
  #map {
	height: 100%;
  }
</style>
<style>
  .carousel-inner > .item > img,
  .carousel-inner > .item > a > img {
      width: 70%;
      margin: auto;
  }
</style>

  <?php
        function getCountryCode($tmp) {
		$country_arr = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Angola", "Anguilla", "Antartica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Ashmore and Cartier Island", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burma", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Clipperton Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo, Democratic Republic of the", "Congo, Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Europa Island", "Falkland Islands (Islas Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "French Guiana", "French Polynesia", "French Southern and Antarctic Lands", "Gabon", "Gambia", "Gaza Strip", "Georgia", "Germany", "Ghana", "Gibraltar", "Glorioso Islands", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard Island and McDonald Islands", "Holy See (Vatican City)", "Honduras", "Hong Kong", "Howland Island", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Ireland, Northern", "Israel", "Italy", "Jamaica", "Jan Mayen", "Japan", "Jarvis Island", "Jersey", "Johnston Atoll", "Jordan", "Juan de Nova Island", "Kazakhstan", "Kenya", "Kiribati", "Korea, North", "Korea, South", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Man, Isle of", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Midway Islands", "Moldova", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn Islands", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russia", "Rwanda", "Saint Helena", "Saint Kitts and Nevis", "Saint Lucia", "Saint Pierre and Miquelon", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Scotland", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and South Sandwich Islands", "Spain", "Sri Lanka", "Sudan", "Suriname", "Svalbard", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Tobago", "Toga", "Tokelau", "Tonga", "Trinidad", "Tunisia", "Turkey", "Turkmenistan", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "Uruguay", "USA", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands", "Wales", "Wallis and Futuna", "West Bank", "Western Sahara", "Yemen", "Zambia", "Zimbabwe");
		$countryVal_arr = array("af","al","dz","as","ao","ai","aq","ag","ar","am","aw","au","au","at","az","bs","bh","bd","bb","by","be","bz","bj","bm","bt","bo","ba","bw","br","vg","bn","bg","bf","mm","bi","kh","cm","ca","cv","ky","cf","td","cl","cn","cx","fr","cc","co","km","cd","cg","ck","cr","ci","hr","cu","cy","cz","dk","dj","dm","do","ec","eg","sv","gq","er","ee","et","tf","fk","fo","fj","fi","fr","gf","pf","tf","ga","gm","ps","ge","de","gh","gi","tf","gr","gl","gd","gp","gu","gt","gg","gn","gw","gy","ht","hm","va","hn","hk","um","hu","is","in","id","ir","iq","ie","_Northern Ireland","il","it","jm","sj","jp","um","je","um","jo","tf","kz","ke","ki","kp","kr","kw","kg","la","lv","lb","ls","lr","ly","li","lt","lu","mo","mk","mg","mw","my","mv","ml","mt","im","mh","mq","mr","mu","yt","mx","fm","um","md","mc","mn","ms","ma","mz","na","nr","np","nl","an","nc","nz","ni","ne","ng","nu","nf","mp","no","om","pk","pw","pa","pg","py","pe","ph","pn","pl","pt","pr","qa","fr","ro","ru","rw","sh","kn","lc","pm","vc","ws","sm","st","sa","_Scotland","sn","rs","sc","sl","sg","sk","si","sb","so","za","gs","es","lk","sd","sr","sj","sz","se","ch","sy","tw","tj","tz","th","tt","tg","tk","to","tt","tn","tr","tm","tv","ug","ua","ae","gb","uy","us","uz","vu","ve","vn","vi","_Wales","wf","ps","eh","ye","zm","zw");

            $key = array_search($tmp, $country_arr);
                return $countryVal_arr[$key];
        }
  ?>

	<div class="container" style="max-width: 960px;">
	
		<div class="row" style="margin-bottom: 40px;">
			<div class="col-xs-4">
			  <img src="{{ URL::asset('img/company/'.$company->pic_url) }}" 
					style="max-width: 225px; max-height:225px; width:100%; height:auto;">
			</div>
			<div class="col-sm-8">
				<h1><span class="text-brand-brown">{{ $company->name }}</span></h1>
				<blockquote class="blockquote-reverse">
					<p>{{ $company->quote }}</p>
					<footer style="margin-top: 0px;">{{$company->quote_author}}</footer>
				</blockquote>
			</div>
		</div>
		
		
		<?php if (!($company->vid_url == '')){ 
			$img = 'img/company/poster/' . $company->id . '.jpg';
		?>
		<div class="row embed-responsive embed-responsive-16by9" style="margin-bottom: 40px;">
			<video id="company_video" class="embed-responsive-item" autoplay poster="{{ URL::asset($img) }}" controls muted>
				<source src="{{ URL::asset('img/company/'.$company->vid_url) }}" type="video/mp4">
			</video>
		</div><?php } ?>

		<?php $from_country_url = 'img/flags/flags_iso/32/' . getCountryCode($company->country) . '.png'; ?>

		<div class="row">
			<div class="col-md-6">
				
				<div class="row">
					<div class="panel panel-default">
						<div class="panel-heading"><h3 class="panel-title">About {{ $company->name }}</h3></div>
						<div class="panel-body">
							<div style="white-space:pre-wrap">{{ $company->about }}</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="panel panel-default">
						<div class="panel-heading"><h3 class="panel-title">Contact</h3></div>
						<div class="panel-body">
							<p><span style="font-size: 16px;">
								<strong>Website: </strong>
								<a href="{{$company->site}}">{{ $company->site }}</a></span>
							</p>
							<p><span style="font-size: 16px;"><strong>Email:</strong> {{$company->email}}</span></p>
							<p><span style="font-size: 16px;"><strong>Telephone:</strong> {{$company->phone}}</span></p>
							 
					<?php if (!empty($company->facebook)) echo '<a href="'.$company->facebook.'" class="btn btn-primary"><i class="fa fa-facebook" aria-hidden="true"> facebook</i></a>'; ?>
					<?php if (!empty($company->twitter)) echo '<a href="'.$company->twitter.'" class="btn btn-info"><i class="fa fa-twitter" aria-hidden="true"> twitter</i></a>'; ?>
					<?php if (!empty($company->instagram)) echo '<a href="'.$company->instagram.'" class="btn btn-warning"><i class="fa fa-instagram" aria-hidden="true"> instagram</i></a>'; ?>
					<?php if (!empty($company->youtube)) echo '<a href="'.$company->youtube.'" class="btn btn-danger"><i class="fa fa-youtube" aria-hidden="true"> youtube</i></a>'; ?>
						</div>
					</div>
				</div>
					
				<div class="row">
					<div class="panel panel-default">
						<div class="panel-heading"><h3 class="panel-title">Address</h3></div>
						<div class="panel-body">
							<address>
								<p><strong>{{ $company->name }}</strong><p>
								<p><img src="{{ url(asset($from_country_url)) }}"> {{ $company->country }}, {{ $company->city }}</p>
								<p>{{ $company->address }}</p>
								<p>{{ $company->zip_code }}</p>
							</address>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				
				<div class="row">
					<div class="panel panel-default">
						<div class="panel-heading"><h3 class="panel-title">Products</h3></div>
						<div class="panel-body" style="padding: 0 0 0 0;">
							<!-- CAROUSEL -->
							<div id="productCarousel" class="carousel slide" data-ride="carousel">
							  <!-- Indicators -->
							  <ol class="carousel-indicators">
								<?php $counter =0; ?>
								@foreach($company->products as $prod)
								@if($counter==0)
								<li data-target="#productCarousel" data-slide-to="0" class="active"></li>
								@else
								<li data-target="#productCarousel" data-slide-to=$counter></li>
								@endif
								<?php $counter++;?>
								@endforeach
							  </ol>

							  <!-- Wrapper for slides -->
							  <div class="carousel-inner" role="listbox">
							     <?php $coun = 0; ?>
							      @foreach($company->products as $prod)

								@if($coun ==0)
								<div  class="item active" >
								  <a href="/products/{{$prod->id}}">
								  <img src="{{ URL::asset('img/products/'.$prod->pic_url) }}" alt="Product" />
								  </a>
								</div>
								@else
                                                                <div class="item">
                                                                  <a href="/products/{{$prod->id}}">
                                                                  <img src="{{ URL::asset('img/products/'.$prod->pic_url) }}" alt="Product" >
								  </a>
                                                                </div>
								@endif
								<?php $coun++;?>
							      @endforeach
							  </div>

							  @if($coun!=0)
							  <!-- Left and right controls -->
							  <a class="left carousel-control" href="#productCarousel" role="button" data-slide="prev">
								<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
								<span class="sr-only">Previous</span>
							  </a>
							  <a class="right carousel-control" href="#productCarousel" role="button" data-slide="next">
								<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
								<span class="sr-only">Next</span>
							  </a>
							  @endif
							</div>
							<!-- END CAROUSEL -->
						</div>
					</div>				
				</div>
	@if( $company->map_x != null)			
				<div class="row">
					<div class="panel panel-default">
						<div class="panel-heading"><h3 class="panel-title">Map</h3></div>
						<div class="panel-body" style="padding: 0 0 0 0;">
							<div id="googleMap" style="width:auto;height:380px;"></div>
						</div>
					</div>
				</div>
	@endif
			</div>
		</div>
	
	</div>
<script
src="https://maps.google.com/maps/api/js?v=3.5&sensor=true">
</script>


<script>
var myCenter=new google.maps.LatLng({{ $company->map_x}},{{$company->map_y}});

function initialize()
{
var mapProp = {
  center:myCenter,
  zoom:14,
  mapTypeId:google.maps.MapTypeId.ROADMAP
  };

var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

var marker=new google.maps.Marker({
  position:myCenter,
  });

marker.setMap(map);
}

google.maps.event.addDomListener(window, 'load', initialize);
</script>

<script>
	$('document').ready(function(){
	
		var flag = false;
		var playing = true;
		$('#company_video').click(function(){
			if (flag) {
				if ( playing == true ) {
					$('#company_video').trigger('pause');
					playing = false;
				} else {
					$('#company_video').trigger('play');
					playing = true;
				}
			} else {
				$('#company_video').prop('muted', false);
				flag = true;
			}
		});
	});
</script>

@endsection
