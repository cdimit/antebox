@extends('app')

@section('content')
  <div class="container">
    <div class="col-md-12">

	  <div class="row">
        <div class="col-md-10 col-md-offset-2">
          <h1><span class="text-brand-brown">Create a Company!</span></h1>
        </div>
      </div>

 	@if (count($errors) > 0)
	  <div class="alert alert-danger">
  		<strong>Whoops!</strong> There were some problems with your input.<br><br>
  		<ul>
  			@foreach ($errors->all() as $error)
  				<li>{{ $error }}</li>
  			@endforeach
  		</ul>
  	  </div>
    	@endif

    </div>


	<form action="/admin/company/update/{{ $company->id }}" method="post" enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		
		<div class="row" style="margin-bottom: 20px;">
			<div class="form-group">
				<label style="text-align: right; font-weight: bold; padding-top: 5px;" class="col-sm-4 control-label text-left">Name (*)</label>
				<div class="col-sm-6">
					<input type="text" class="form-control" name="name" value="{{ $company->name }}">
				</div>
			</div>
		</div>
		
		<div class="row" style="margin-bottom: 20px;">
			<div class="form-group">
				<label style="text-align: right; font-weight: bold; padding-top: 5px;" class="col-sm-4 control-label text-left">Email (*)</label>
				<div class="col-sm-6">
					<input type="text" class="form-control" name="email" value="{{ $company->email }}">
				</div>
			</div>
		</div>
		
		<div class="row" style="margin-bottom: 20px;">
			<div class="form-group">
				<label style="text-align: right; font-weight: bold; padding-top: 5px;" class="col-sm-4 control-label text-left">Website (*)</label>
				<div class="col-sm-6">
					<input type="text" class="form-control" name="site" value="{{ $company->site }}">
				</div>
			</div>
		</div>
		
                <div class="row" style="margin-bottom: 20px;">
                        <div class="form-group">
                                <label style="text-align: right; font-weight: bold; padding-top: 5px;" class="col-sm-4 control-label text-left">Nickname (*)</label>
                                <div class="col-sm-6">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="basic-addon3">https://antebox.com/partner/</span>
                                        <input type="text" class="form-control" name="nickname" value="{{$company->nickname}}">
                                    </div>
                                </div>
                        </div>
                </div>

		<div class="row" style="margin-bottom: 20px;">
			<div class="form-group">
				<label style="text-align: right; font-weight: bold; padding-top: 5px;" class="col-sm-4 control-label text-left">Telephone</label>
				<div class="col-sm-6">
					<input type="text" class="form-control" name="phone" value="{{ $company->phone }}">
				</div>
			</div>
		</div>
		
		<div class="row" style="margin-bottom: 20px;">
			<div class="form-group">
				<label style="text-align: right; font-weight: bold; padding-top: 5px;" class="col-sm-4 control-label text-left">Address</label>
				<div class="col-sm-6">
					<input type="text" class="form-control" name="address" value="{{ $company->address }}">
				</div>
			</div>
		</div>
		
		<div class="row" style="margin-bottom: 20px;">
			<div class="form-group">
				<label style="text-align: right; font-weight: bold; padding-top: 5px;" class="col-sm-4 control-label text-left">Country (*)</label>
				<div class="col-sm-6">
					<input type="text" class="form-control" name="country" value="{{ $company->country }}">
				</div>
			</div>
		</div>
		
		<div class="row" style="margin-bottom: 20px;">
			<div class="form-group">
				<label style="text-align: right; font-weight: bold; padding-top: 5px;" class="col-sm-4 control-label text-left">City</label>
				<div class="col-sm-6">
					<input type="text" class="form-control" name="city" value="{{ $company->city }}">
				</div>
			</div>
		</div>
		
		<div class="row" style="margin-bottom: 20px;">
			<div class="form-group">
				<label style="text-align: right; font-weight: bold; padding-top: 5px;" class="col-sm-4 control-label text-left">ZIP Code</label>
				<div class="col-sm-6">
					<input type="text" class="form-control" name="zip_code" value="{{ $company->zip_code }}">
				</div>
			</div>
		</div>
		
		<div class="row" style="margin-bottom: 20px;">
			<div class="form-group">
				<label style="text-align: right; font-weight: bold; padding-top: 5px;" class="col-sm-4 control-label text-left">Twitter</label>
				<div class="col-sm-6">
					<input type="text" class="form-control" name="twitter" value="{{ $company->twitter }}">
				</div>
			</div>
		</div>
		
		<div class="row" style="margin-bottom: 20px;">
			<div class="form-group">
				<label style="text-align: right; font-weight: bold; padding-top: 5px;" class="col-sm-4 control-label text-left">Facebook</label>
				<div class="col-sm-6">
					<input type="text" class="form-control" name="facebook" value="{{ $company->facebook }}">
				</div>
			</div>
		</div>
		
		<div class="row" style="margin-bottom: 20px;">
			<div class="form-group">
				<label style="text-align: right; font-weight: bold; padding-top: 5px;" class="col-sm-4 control-label text-left">Instagram</label>
				<div class="col-sm-6">
					<input type="text" class="form-control" name="instagram" value="{{ $company->instagram }}">
				</div>
			</div>
		</div>
		
		<div class="row" style="margin-bottom: 20px;">
			<div class="form-group">
				<label style="text-align: right; font-weight: bold; padding-top: 5px;" class="col-sm-4 control-label text-left">Map Coordinate X</label>
				<div class="col-sm-6">
					<input type="text" class="form-control" name="map_x" value="{{ $company->map_x }}">
				</div>
			</div>
		</div>
		
		<div class="row" style="margin-bottom: 20px;">
			<div class="form-group">
				<label style="text-align: right; font-weight: bold; padding-top: 5px;" class="col-sm-4 control-label text-left">Map Coordinate Y</label>
				<div class="col-sm-6">
					<input type="text" class="form-control" name="map_y" value="{{ $company->map_y }}">
				</div>
			</div>
		</div>
		
		<div class="row" style="margin-bottom: 20px;">
			<div class="form-group">
				<label style="text-align: right; font-weight: bold; padding-top: 5px;" class="col-sm-4 control-label text-left">About (*)</label>
				<div class="col-sm-6">
					<textarea class="form-control" rows="4" style="resize: none;" name="about" value="">{{ $company->about }}</textarea>
				</div>
			</div>
		</div>
		
		<div class="row" style="margin-bottom: 20px;">
			<div class="form-group">
				<label style="text-align: right; font-weight: bold; padding-top: 5px;" class="col-sm-4 control-label text-left">Quote Author (*)</label>
				<div class="col-sm-6">
					<input type="text" class="form-control" name="quote_author" value="{{ $company->quote_author }}">
				</div>
			</div>
		</div>

		<div class="row" style="margin-bottom: 20px;">
			<div class="form-group">
				<label style="text-align: right; font-weight: bold; padding-top: 5px;" class="col-sm-4 control-label text-left">Quote (*)</label>
				<div class="col-sm-6">
					<textarea class="form-control" rows="4" style="resize: none;" name="quote" value="">{{ $company->quote }}</textarea>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="form-group">
				<label style="text-align: right; font-weight: bold; padding-top: 5px;" class="col-sm-4 control-label text-left">Picture URL (*)</label>
				<div class="col-sm-6">
					<input type="file" accept="image/*" id="pic_url" name="pic_url" class="hidden">
					<label class="btn btn-default form-control" for="pic_url">Choose Picture</label>
					<label id="label_picture" style="margin-top:10px;">{{ $company->pic_url }}</label>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="form-group">
				<label style="text-align: right; font-weight: bold; padding-top: 5px;" class="col-sm-4 control-label text-left">Video URL</label>
				<div class="col-sm-6">
					<input type="file" accept="video/*" id="vid_url" name="vid_url" class="hidden">
					<label class="btn btn-default form-control" for="vid_url">Choose Video</label>
					<label id="label_video" style="margin-top:10px;">{{ $company->vid_url }}</label>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="form-group">
				<label style="text-align: right; font-weight: bold; padding-top: 5px;" class="col-sm-4 control-label text-left">Video Poster</label>
				<div class="col-sm-6">
					<input type="file" accept="image/jpg" id="poster" name="poster" class="hidden">
					<label class="btn btn-default form-control" for="poster">Choose Poster</label>
					<label id="label_poster" style="margin-top:10px;">{{ $company->id }}.jpg</label>
				</div>
			</div>
		</div>
		
		<div class="row" style="margin-bottom: 20px;">
			<div class="form-group">
				<div class="col-sm-2 col-sm-offset-8">
					<button type="submit" class="btn btn-primary form-control" style="width:100%;">Update</button>
				</div>
			</div>
		</div>

	</form>
  </div>

<!-- script for showing path of image file -->
<script>
    $(function() {
		$("input:file").change(function(){
			var filePath = $(this).val();
			var filename = filePath.substring(filePath.lastIndexOf("\\") + 1);
			if ($(this).attr('name') == "pic_url"){
				$('#label_picture').html(filename);
			} else if ($(this).attr('name') == "vid_url"){
				$('#label_video').html(filename);
			} else {
				$('#label_poster').html(filename);
			}
			
		});
    });

</script>  
  
  
@endsection
