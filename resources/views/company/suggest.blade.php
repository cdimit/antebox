@extends('app')

@section('content')
  <div class="container">
    <div class="col-md-12">

    @if (count($errors) > 0)
	<div class="alert alert-danger">
  		<strong>Whoops!</strong> There were some problems with your input.<br><br>
  		<ul>
  			@foreach ($errors->all() as $error)
  				<li>{{ $error }}</li>
  			@endforeach
  		</ul>
  	</div>
    @endif

  @if (session('status'))
    <div class="alert alert-success">
	    Thank you for your application!<br>
            We will now look into this and get back to you within 48hrs.<br>
            If you have any questions feel free to contact us at contact@antebox.com
    </div>
  @endif

  <form class="form-horizontal" role="form" method="POST" action="{{ url('/suggest/company/sent')}}">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="row">
        <div class="form-group">
            <label style="text-align: center; font-weight: bold;" class="col-md-12 control-label text-left">
            <h3><strong>Apply to Join</strong></h3>
 A growing network, with users from around the world</labe>
        </div>
    </div>
    <br>



<div class="col-md-offset-1">
      <div class="col-md-10">

 <div class="panel panel-default">
   <div class="panel-heading"><h3 class="panel-title">First, a few things about your company.</h3></div>
      <div class="panel-body">
    <div class="row">
  <div class="form-group">
      <label style="text-align: right; font-weight: bold;" class="col-md-4 control-label text-left">Name (*)</label>
      <div class="col-md-6">
    <input type="text" class="form-control" name="company" value="{{ old('company') }}">
      </div>
  </div>
    </div>

    <div class="row">
  <div class="form-group">
      <label style="text-align: right; font-weight: bold;" class="col-md-4 control-label text-left">Country (*)</label>
      <div class="col-md-6">
    <input type="text" class="form-control" name="country" value="{{ old('country') }}">
      </div>
  </div>
    </div>

    <div class="row">
	<div class="form-group">
	    <label style="text-align: right; font-weight: bold;" class="col-md-4 control-label text-left">Address</label>
	    <div class="col-md-6">
		<input type="text" class="form-control"  name="address" value="{{ old('address') }}" >
	    </div>
	</div>
    </div>

        <div class="row">
  <div class="form-group">
      <label style="text-align: right; font-weight: bold;" class="col-md-4 control-label text-left">Website</label>
      <div class="col-md-6">
    <input type="text" class="form-control"  name="website" value="{{ old('website') }}" >
      </div>
  </div>
    </div>

</div>
</div>
</div>

<br>

      <div class="col-md-10">

 <div class="panel panel-default">
   <div class="panel-heading"><h3 class="panel-title">Secondly, a few things about your products.</h3></div>
      <div class="panel-body">
 <div class="row">
  <div class="form-group">
      <label style="text-align: right; font-weight: bold;" class="col-md-4 control-label text-left">What products do you have? (*)</label>
      <div class="col-md-6">
    <textarea class="form-control" rows="4" name="products" style="resize:none;">{{ old('products') }}</textarea>
      </div>
  </div>
    </div>

    <div class="row">
  <div class="form-group">
      <label style="text-align: right; font-weight: bold;" class="col-md-4 control-label text-left">Why are they unique? (*)</label>
      <div class="col-md-6">
    <textarea class="form-control" rows="4" name="unique" style="resize:none;">{{ old('unique') }}</textarea>
      </div>
  </div>
    </div>

    <div class="row">
  <div class="form-group">
      <label style="text-align: right; font-weight: bold;" class="col-md-4 control-label text-left">Anything else?</label>
      <div class="col-md-6">
    <textarea class="form-control" rows="4" name="comment" style="resize:none;">{{ old('comment') }}</textarea>
      </div>
  </div>
    </div>

    </div></div></div>

<br>
      <div class="col-md-10">

 <div class="panel panel-default">
   <div class="panel-heading"><h3 class="panel-title">Who should we contact?</h3></div>
      <div class="panel-body">

<div class="row">
  <div class="form-group">
      <label style="text-align: right; font-weight: bold;" class="col-md-4 control-label text-left">Name (*)</label>
      <div class="col-md-6">
    <input type="text" class="form-control" name="name" value="{{ old('name') }}">
      </div>
  </div>
    </div>

    <div class="row">
  <div class="form-group">
      <label style="text-align: right; font-weight: bold;" class="col-md-4 control-label text-left">Email (*)</label>
      <div class="col-md-6">
    <input type="text" class="form-control" name="email" value="{{ old('email') }}">
      </div>
  </div>
    </div>

    <div class="row">
  <div class="form-group">
      <label style="text-align: right; font-weight: bold;" class="col-md-4 control-label text-left">Phone</label>
      <div class="col-md-6">
    <input type="text" class="form-control"  name="phone" value="{{ old('phone') }}" >
      </div>
  </div>
    </div>

@if(!Auth::guest())
<input type="hidden" name="user_id" value="{{Auth::user()->id}}">
@endif


    </div></div></div>
    <br>

    <div class="row">
        <div class="form-group">
            <label style="text-align: left; font-weight: bold;" class="col-md-12 control-label text-left">
            (*): Required</labe>
        </div>
    </div>

</div>


    <div class="row">
	<div class="form-group">
	    <div class="col-md-2 col-md-offset-8">
		<button style="width:100%" class="btn btn-success">
		    Send
		</button>
	    </div>
	</div>
    </div>
    <br>

</form>
</div>
</div>
@endsection

