@include('partials.header')
<link rel="stylesheet" href="{{ URL::asset('assets/css/styles.css') }}">

<section id="about">
    <div class="container">

        <div class="section-heading  sp-effect3">
            <h1>What is AnteBox?</h1>
            <div class="divider"></div>
        </div>

        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-6">
                <div class="about-item  sp-effect2">
                    <a href="/products"><i class="fa fa-globe fa-2x"></i></a>
                    <h3>Local products</h3>
                    <p>Find unique and traditional products directly from local producers from around the world.</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6" >
                <div class="about-item  sp-effect5">
                    <a href="/suggest/product"><i class="fa fa-shopping-cart fa-2x"></i></a>
                    <h3>Request</h3>
                    <p>Whether you are a foody, you miss your country's products or you want to surprise your guests, we have the right products for you.</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6" >
                <div class="about-item  sp-effect5">
                    <i class="fa fa-plane fa-2x"></i>
                    <h3>Deliver</h3>
                    <p>Shipping is not only complex and expensive, it takes too long! That is why we use travellers, like you, to deliver the requests.</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6" >
                <div class="about-item  sp-effect1">
                    <i class="fa fa-users fa-2x"></i>
                    <h3>Network</h3>
                    <p>Our network is growing and involves producers, retailers, restaurants and hotels.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="features">
    <div class="container">
        <div class="section-heading  sp-effect3">
            <h1>Why Join Us?</h1>
            <div class="divider"></div>

        </div>
        <div class="row">
            <div class="col-md-4 col-sm-4  sp-effect1">
                <div class="media text-center feature">
                    <div class="media-body">
                        <h1 class="media-heading">Clients</h1>
                    </div>
                </div>

              <div class="media text-right feature">
                  <span class="pull-right">
                      <i class="fa fa-globe fa-2x"></i>
                  </span>
                  <div class="media-body">
                      <h3 class="media-heading">No Limits</h3>
                       Travelers, like ants, can go anywhere
                  </div>
              </div>
              <br>
                <div class="media text-right feature">
                    <span class="pull-right">
                        <i class="fa fa-thumbs-up fa-2x"></i>
                    </span>
                    <div class="media-body">
                        <h3 class="media-heading">Personalised Delivery</h3>
                        Receive delivery bids from travellers and select the one you prefer. Contact your traveller through the in-house chat system
                    </div>
                </div>
                <div class="media feature text-right">
                    <span class="pull-right" >
                        <i class="fa fa-clock-o fa-2x"></i>
                    </span>
                    <div class="media-body">
                        <h3 class="media-heading">Quick delivery.</h3>
                        Your deliveries are made by travellers like yourself, and could reach you even in the same day.
                    </div>
                </div>
                <div class="media text-right feature">
                    <span class="pull-right" >
                        <i class="fa fa-building fa-2x"></i>
                    </span>
                    <div class="media-body">
                        <h3 class="media-heading">Local Products</h3>
                        Local businesses make their products in small batches and take care of every detail
                    </div>
                </div>
                <div class="media text-right feature">
                    <span class="pull-right" >
                        <i class="fa fa-home fa-2x"></i>
                    </span>
                    <div class="media-body">
                        <h3 class="media-heading">Home is where you are</h3>
                        Never miss products from your country again!
                    </div>
                </div>

            </div>
            <div class="col-md-4 col-sm-4">
                <img src="assets/img/handshake-128fff.png" class="img-responsive  sp-effect5" alt="">
            </div>
            <div class="col-md-4 col-sm-4  sp-effect2">
                <div class="media text-center feature">
                    <div class="media-body">
                        <h1 class="media-heading">Travellers</h1>
                    </div>
                </div>
                <div class="media feature">
                    <span class="pull-left">
                        <i class="fa fa-heart fa-2x"></i>
                    </span>
                    <div class="media-body">
                        <h3 class="media-heading">Visit local shops</h3>
                        Our trips will be so much better if we were able to experience authentic and traditional goods from every country we visit
                    </div>
                </div>
                <div class="media feature">
                    <span class="pull-left">
                        <i class="fa fa-percent fa-2x"></i>
                    </span>
                    <div class="media-body">
                        <h3 class="media-heading">Get discounts and offers</h3>
                        Our partners are happy to offer you better prices and additional products when you show them you are delivering via AnteBox
                    </div>
                </div>
                <div class="media feature">
                    <span class="pull-left" >
                        <i class="fa fa-money fa-2x"></i>
                    </span>
                    <div class="media-body">
                        <h3 class="media-heading">Get paid</h3>
                        You have the ability to make delivery bids on requests and get paid when you deliver the products
                    </div>
                </div>
                <div class="media feature">
                    <span class="pull-left">
                        <i class="fa fa-arrow-up fa-2x"></i>
                    </span>
                    <div class="media-body">
                        <h3 class="media-heading">Support local businesses</h3>
                      These businesses do not have the staff, time or expertise to ship their products abroad.
                    </div>
                </div>
                <div class="media feature">
	    <span class="pull-left">
                        <i class="fa fa-smile-o fa-2x"></i>
                    </span>
                    <div class="media-body">
                        <h3 class="media-heading">Great hospitality.</h3>
                      Our local partners are happy to welcome you with offers, free tasting and tours of their areas.
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>


@include('partials.footer')