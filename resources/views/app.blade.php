<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=0.6">
	<meta name="description" content="A new innovative consultancy company that helps globalize businesses that work in a geographically limited area">
	<meta name="keywords" content="antebox,antebox revolution,local businesses,local business,travel,travellers,unique,haloumi,halloumi,food,unique food,unique haloumi,unique halloumi,unique fashion,shop,online,startup">

	@yield('open-graph')
	<meta property="fb:admins" content="605108675" />
	<meta property="fb:app_id" content="510079345866294" />

	<title>AnteBox - A network for local businesses powered by you</title>
	<link rel="shortcut icon" href="{{ asset('img/app/favicon.ico') }}">

	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/media.css') }}" rel="stylesheet">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

	<!-- SweetAlert -->
	<script src="{{ asset('/js/sweetalert.min.js') }}"></script>
	<link href="{{ asset('/css/sweetalert.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/themify-icons.css') }}" rel="stylesheet">

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.5.8/slick.css"/>
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.5.8/slick-theme.css"/>

	@yield('plugins')
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-71587454-1', 'auto');
          ga('send', 'pageview');
        </script>

		<!-- Facebook Pixel Code -->
		<script>
		!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
		n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
		document,'script','https://connect.facebook.net/en_US/fbevents.js');

		fbq('init', '238526746496667');
		fbq('track', "PageView");</script>
		<noscript><img height="1" width="1" style="display:none"
		src="https://www.facebook.com/tr?id=238526746496667&ev=PageView&noscript=1"
		/></noscript>
		<!-- End Facebook Pixel Code -->

	<style>
	    footer {
		margin-top: 200px;
	    }
	    @media (min-width: 768px) {
			ul.nav li.dropdown:hover > ul.dropdown-menu {
				display: block;
			}
	    }
		.badge-notify{
		   background:red;
		   position:relative;
		}
	</style>
	

</head>
<body>
	@include('cookieConsent::index')
	@yield('slideBanner')

<nav class="navbar navbar-default" style="width:100%">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#Navbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{ url('/') }}">
		<img alt="Antebox" style="max-height:100%; width:auto; margin-right:15px;" src="{{ URL::asset('img/app/logo.png')}}"/></a>
    </div>
    <div class="collapse navbar-collapse" id="Navbar">
      <ul class="nav navbar-nav">
		<li><a href="{{ url('/') }}"><i style="font-size:24px;color:#fff" class="fa fa-home"></i></a></li>
		<li><a href="{{ url('/requests') }}">Requests</a></li>
		<li><a href="{{ url('/trips') }}">Travelers</a></li>
		<li><a href="{{ url('/products') }}">Products</a></li>
		<li><a href="{{ url('/partners') }}">Partners</a></li>
	  </ul>
	  <ul class="nav navbar-nav navbar-right">
			<li><a href="/how">How it works</a></li>
		@if (Auth::guest())
			<li><a href="{{ url('/login') }}">Login</a></li>
			<li><a href="{{ url('/register') }}">Register</a></li>
		@else

		<?php $notify = Auth::user()->notifications()->get()->where('status', '0')->sortByDesc('id');?>
			<li class="dropdown" >
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
					<i class="fa fa-bell" aria-hidden="true"></i>
					@if($notify->count()!=0)
					<span class="badge badge-notify">{{ $notify->count()}}</span>
					@endif
					<span class="caret"></span>
				</a>
				@if($notify->count()!=0)
				<ul class="dropdown-menu" aria-labelledby="dropdownMenu1" style="background: #f5f5f5; padding: 0;">
					<table class="table" style="padding: 0; margin-bottom: 0px;">
						@foreach($notify as $not)
						<li>
							<tr class="active">
								<td><a href="/notifications/read/{{$not->id}}">
								@if($not->category=="antebox")
								<img src="{{ URL::asset("img/app/notify.png") }}" width="30" height="30" />
								@elseif($not->category=="msg")
								<img src="{{ URL::asset("img/app/notify-msg.png") }}" width="30" height="30" />
								@elseif($not->category=="paid")
								<img src="{{ URL::asset("img/notifications/paid.png") }}" width="30" height="30" />
								@elseif($not->category=="bid")
								<img src="{{ URL::asset("img/app/notify-bid.png") }}" width="30" height="30" />
								@elseif($not->category=="warning")
								<img src="{{ URL::asset("img/notifications/warning.png") }}" width="30" height="30" />
								@elseif($not->category=="shop")
								<img src="{{ URL::asset("img/notifications/shop.png") }}" width="30" height="30" />
								@elseif($not->category=="deal")
								<img src="{{ URL::asset("img/notifications/deal.png") }}" width="30" height="30" />
								@elseif($not->category=="terms")
								<img src="{{ URL::asset("img/notifications/terms.png") }}" width="30" height="30" />
								@endif
								</a></td>
								<td><a href="/notifications/read/{{$not->id}}"><button type="button" class="btn btn-default btn-block">{{$not->msg}}</button></a></td>
								<td><a href="/notifications/remove/{{$not->id}}"><img src="{{ URL::asset("img/app/notify-remove.png") }}" width="30" height="30" /></a></td>
							</tr>  
						</li>

						@endforeach
						@if($notify->count()>1)
						<li>
							<tr class="active">
								<td></td>
								<td>
									<a href="/notifications/removeAll">
										<button type="button" class="btn btn-default btn-block">Clear All</button>
									</a>
								</td>
								<td></td>
							</tr>
						</li>
						@endif
					</table>
				</ul>
				@endif
			</li>

			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
					<!-- Welcome, {{ Auth::user()->first_name }} -->
					<span class="fa fa-user"></span>
					<span class="caret"></span>
				</a>
				<ul class="dropdown-menu" role="menu">
					<li><a href="/user/{{ Auth::user()->id }}">Profile</a></li>
					<li><a href="/mydeals">My Deals</a></li>
                		        <li><a href="{{ url('/myrequests') }}">My Requests</a></li>
					<li><a href="/mybids">My Bids</a></li>
		                        <li><a href="{{ url('/trips/mytrips') }}">My Trips</a></li>
					<li><a href="{{ url('/logout') }}">Logout</a></li>
				</ul>
			</li>
		@endif
	  </ul>
      <!-- <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
      </ul>
	  -->
    </div>
  </div>
</nav>

	@yield('content')
	@include('footer')
	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.5.8/slick.min.js"></script>
	
	@yield('scripts')
</body>
</html>
