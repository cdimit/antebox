@extends('app')

@section('content')
  <div class="container">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-12">
          <span class="text-brand-brown" style="font-size: 78px;">Join the Market!</span>
        </div>
      </div>
      <div class="row">
        <hr>
      </div>
      <div style="border: 3px solid;">
        <div class="row">
          <div class="col-sm-9 col-sm-offset-1" style="padding-top: 30px;">
            <div class="row">
              <div class="col-sm-10">
                <span class="text-brand-brown" style="font-size: 30px; font-weight:bolder;">You are ready!</span>
              </div>
              <div class="col-sm-2">
                <i style="font-size:40px;" class="text-brand-blue fa fa-user"></i>
              </div>
            </div>
            <div class="row" style="padding-top: 40px;padding-bottom: 40px;">
              <div class="col-sm-10 col-sm-offset-1" style="font-weight:bold;">
                <p class="text-brand-blue">You should have received an email by now, at the email address you have specified. Just click on the link to verify it and you are ready to start your journey on AnteBox!</p>
                <p class="text-brand-blue">Don't forget to order the product you couldn't get to for so long, or make some money on your travels by becoming a courier.</p>
	      </div>
	      <div class="col-md-4 col-md-offset-4">
		<a href="/profile">
                  <button type="submit" class="btn btn-primary form-control">
                    Update your profile now!
                  </button>
                </a>
	      </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row" style="padding: 20px;">
      </div>
    </div>
  </div>
@endsection

@section('plugins')
  <link rel="stylesheet" href="{{ URL::asset('css/bzwizard.css') }}"/>
@endsection

@section('scripts')

@endsection
