@extends('app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-default">
          <div class="panel-heading">Verify Email Address</div>

          <div class="panel-body">
            <div>
              <p>An email with a link to verify your email address has been sent to {{ $email }}</p>
              <p>You need to complete this step before you can start using Antebox.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
