@extends('app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-default">
          <div class="panel-heading">Invalid Token</div>

          <div class="panel-body">
            <div>
              <p>The token you are trying to use is invalid.</p>
              <p>Please make sure you are using the link provided in the email.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
