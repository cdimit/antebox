@extends('app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-default">
          <div class="panel-heading">Your Email Is Verified!</div>

          <div class="panel-body">
            <div>
              <p>Your account has been verified and is now ready for use.</p>
              <p>Please login <a href="/login">here</a> to start using AnteBox.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
