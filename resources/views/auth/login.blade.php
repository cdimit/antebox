@extends('app')

@section('content')
<div class="container" style="padding-top:30px;">
    <div class="col-md-12">
	<div class="row" style="padding-bottom: 50px;">
		<div class="col-md-8 col-md-offset-2 brand-border"style="padding-bottom: 50px;">
			<div class="row" style="padding-bottom: 20px;">
				<div class="col-md-12 text-center">
					<span class="text-brand-blue" style="font-size: 50px;">Login</span>
				</div>
			</div>
			@if (count($errors) > 0)
				<div class="alert alert-danger">
					<strong>Whoops!</strong> There were some problems with your input.<br><br>
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif

			<form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/login') }}">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">

				<div class="form-group">
				    <div class="col-md-6 col-md-offset-4" style="">
					<a href="/auth/social/redirect/facebook" class="btn btn-primary" style="width: 100%;">
					    <i class="fa fa-facebook"></i> Sign-In with Facebook
					</a>
				    </div>
				</div>
<!--
				<div class="form-group">
				    <div class="col-md-6 col-md-offset-4">
					<a href="" class="btn btn-info" style="width: 100%;">
					    <i class="fa fa-twitter"></i> Sign-In with Twitter
					</a>
				    </div>
				</div>
-->
				<div class="form-group">
					<label class="col-md-4 control-label">Email</label>
					<div class="col-md-6">
						<input type="text" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label">Password</label>
					<div class="col-md-6">
						<input type="password" class="form-control" name="password" required>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-6 col-md-offset-4">
						<div class="checkbox">
							<label>
								<input type="checkbox" name="remember"> Remember Me
							</label>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-3 col-md-offset-4" >
						<button type="submit" class="btn btn-primary form-control">
						  	<span class="fa fa-sign-in"></span>
							&nbsp; LOGIN
						</button>
					</div>
					<div class="col-md-3">
						<a href="/register" class="btn btn-info form-control">
						 	<span class="fa fa-user-plus"></span>
							&nbsp; REGISTER
						</a>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-6 col-md-offset-4">
						<a class="btn btn-link" href="{{ url('/password/email') }}">Forgot Your Password?</a>
					</div>
				</div>
			</form>
		</div>
	</div>
    </div>
</div>
@endsection
