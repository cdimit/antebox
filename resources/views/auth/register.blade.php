@extends('app')

@section('content')
  <div class="container">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-12">
          <span class="text-brand-brown huge-title">Join the Market!</span>
        </div>
      </div>
      <div class="row">
        <div class="col-md-8">
          <span style="font-size:16px;">Create an account today to start finding your favourite items. It's quick, secure and simple to get started</span>
        </div>
      </div>
      <div class="row">
        <hr>
      </div>
      <div class="brand-border">
        <div class="row">
          <div class="col-sm-12" style="padding-top: 30px;">
            <span class="text-brand-brown" style="font-size: 30px; font-weight:bolder;">Let's create your account first!</span>
          </div>
        </div>

		<br><br>
        <div class="row">
			<div id="email-registration" class="col-md-6 col-md-offset-3">
        			    	@if (count($errors) > 0)
  						<div class="alert alert-danger">
  							<strong>Whoops!</strong> There were some problems with your input.<br><br>
  							<ul>
  								@foreach ($errors->all() as $error)
  									<li>{{ $error }}</li>
  								@endforeach
  							</ul>
  						</div>
  					@endif
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/register')}}">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">

	      <div class="form-group">

                <label style="text-align:left; font-weight: bold;" class="col-md-4 control-label">Sign-Up with</label>

                <div class="col-md-8">
		  <a href="/auth/social/redirect/facebook" class="btn btn-primary" style="width: 100%;">
		    <i class="fa fa-facebook"></i> Facebook
		  </a>
		</div>
<!--		<div class="col-md-4">
		  <a href="" class="btn btn-info" style="width: 100%;">
		    <i class="fa fa-twitter"></i> Twitter
		  </a>
		</div>
-->
              </div>
             <div class="form-group">
                <label style="text-align:left; font-weight: bold;" class="col-md-4 control-label">First Name</label>
                <div class="col-md-8  {{$errors->has('first_name') ? 'has-error' : ''}}">
                  <input class="form-control" name="first_name" value="{{ old('first_name') }}">
                </div>
              </div>

             <div class="form-group">
                <label style="text-align:left; font-weight: bold;" class="col-md-4 control-label" >Last Name</label>
                <div class="col-md-8  {{$errors->has('last_name') ? 'has-error' : ''}}">
                  <input class="form-control" name="last_name" value="{{ old('last_name') }}" >
                </div>
              </div>


	      <div class="form-group">
                <label style="text-align:left; font-weight: bold;" class="col-md-4 control-label text-left">E-mail Address</label>
                <div class="col-md-8  {{$errors->has('email') ? 'has-error' : ''}}">
                  <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                </div>
              </div>

              <div class="form-group">
                <label style="text-align:left; font-weight: bold;" class="col-md-4 control-label">Password</label>
                <div class="col-md-8  {{$errors->has('password') ? 'has-error' : ''}}">
                  <input type="password" class="form-control" name="password">
                </div>
              </div>

              <div class="form-group">
                <label style="text-align:left; font-weight: bold;" class="col-md-4 control-label">Confirm Password</label>
                <div class="col-md-8   {{$errors->has('password') ? 'has-error' : ''}}">
                  <input type="password" class="form-control" name="password_confirmation">
                </div>
              </div>


	<div class="form-group">
             <label style="text-align:left; font-weight: bold;" class="col-md-4 control-label">Are you human?</label>
              <div class="row">
                <div class="col-md-8">
			{!! app('captcha')->display(); !!}
                </div>
              </div>
	</div>


              <div class="col-12 text-center" style="padding-top: 50px;">
                <strong>By creating an account, you are agreeing to <a href="terms">our Terms and Conditions</a></strong>
              </div>
  	      <div class="form-group" style="padding-top: 30px; padding-bottom: 10px">
			<div class="col-md-5 col-md-offset-2" >
				  <a href="/login" class="btn btn-primary form-control" >
					<span class="fa fa-sign-in"></span>
					&nbsp; LOGIN
				  </a>
			</div>
		<div class="col-md-5">
		 <button type="submit" class="btn btn-info active form-control">
		     <span class="fa fa-user-plus">
		     &nbsp; REGISTER
		  </button>
		</div>
              </div>

            </form>
          </div>
        </div>
      </div>
      <div class="row" style="padding: 20px;">
      </div>
    </div>
  </div>
@endsection

@section('plugins')
  <link rel="stylesheet" href="{{ URL::asset('css/bzwizard.css') }}"/>
@endsection

@section('scripts')
<!--
  <script type="text/javascript">
    $(document).ready(function() {
      $('.registration-toggle').click(function(){
        $('#registration-email-toggle').toggleClass('btn-option');
        $('#registration-email-toggle').toggleClass('btn-option-active');

        $('#registration-social-toggle').toggleClass('btn-option-active');
        $('#registration-social-toggle').toggleClass('btn-option');

        $('#email-registration').slideToggle(1000);
        $('#social-registration').slideToggle(1000);
      });
    });
-->
  </script>
@endsection

