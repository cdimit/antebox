@extends('app')


@section('content')

<div id="fb-root"></div>
<script>
	window.fbAsyncInit = function() {
		FB.init({appId: '510079345866294', status: true, cookie: true,
		xfbml: true});
	};
	(function() {
		var e = document.createElement('script'); e.async = true;
		e.src = document.location.protocol +
		'//connect.facebook.net/en_US/all.js';
		document.getElementById('fb-root').appendChild(e);
	}());
</script>

<script type="text/javascript">
$(document).ready(function(){
	$('.share_button').click(function(e){
		e.preventDefault();
		FB.ui(
		{
			method: 'feed',
			name: '{{ Auth::user()->first_name }} is requesting ' + $(this).data("options").qty + ' ' + $(this).data("options").name + '!',
			link: 'https://www.antebox.com',
			picture: $(this).data("options").pic_url,
			caption: 'AnteBox: Visit local businesses and experience unique products.',
			description: "Through AnteBox you can find and request unique local food products from around the world!",
			message: ""
		});
	});
});
</script>

<?php
	function getCountryCode($tmp) {
		$country_arr = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Angola", "Anguilla", "Antartica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Ashmore and Cartier Island", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burma", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Clipperton Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo, Democratic Republic of the", "Congo, Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Europa Island", "Falkland Islands (Islas Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "French Guiana", "French Polynesia", "French Southern and Antarctic Lands", "Gabon", "Gambia", "Gaza Strip", "Georgia", "Germany", "Ghana", "Gibraltar", "Glorioso Islands", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard Island and McDonald Islands", "Holy See (Vatican City)", "Honduras", "Hong Kong", "Howland Island", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Ireland, Northern", "Israel", "Italy", "Jamaica", "Jan Mayen", "Japan", "Jarvis Island", "Jersey", "Johnston Atoll", "Jordan", "Juan de Nova Island", "Kazakhstan", "Kenya", "Kiribati", "Korea, North", "Korea, South", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Man, Isle of", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Midway Islands", "Moldova", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn Islands", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russia", "Rwanda", "Saint Helena", "Saint Kitts and Nevis", "Saint Lucia", "Saint Pierre and Miquelon", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Scotland", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and South Sandwich Islands", "Spain", "Sri Lanka", "Sudan", "Suriname", "Svalbard", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Tobago", "Toga", "Tokelau", "Tonga", "Trinidad", "Tunisia", "Turkey", "Turkmenistan", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "Uruguay", "USA", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands", "Wales", "Wallis and Futuna", "West Bank", "Western Sahara", "Yemen", "Zambia", "Zimbabwe");
		$countryVal_arr = array("af","al","dz","as","ao","ai","aq","ag","ar","am","aw","au","au","at","az","bs","bh","bd","bb","by","be","bz","bj","bm","bt","bo","ba","bw","br","vg","bn","bg","bf","mm","bi","kh","cm","ca","cv","ky","cf","td","cl","cn","cx","fr","cc","co","km","cd","cg","ck","cr","ci","hr","cu","cy","cz","dk","dj","dm","do","ec","eg","sv","gq","er","ee","et","tf","fk","fo","fj","fi","fr","gf","pf","tf","ga","gm","ps","ge","de","gh","gi","tf","gr","gl","gd","gp","gu","gt","gg","gn","gw","gy","ht","hm","va","hn","hk","um","hu","is","in","id","ir","iq","ie","_Northern Ireland","il","it","jm","sj","jp","um","je","um","jo","tf","kz","ke","ki","kp","kr","kw","kg","la","lv","lb","ls","lr","ly","li","lt","lu","mo","mk","mg","mw","my","mv","ml","mt","im","mh","mq","mr","mu","yt","mx","fm","um","md","mc","mn","ms","ma","mz","na","nr","np","nl","an","nc","nz","ni","ne","ng","nu","nf","mp","no","om","pk","pw","pa","pg","py","pe","ph","pn","pl","pt","pr","qa","fr","ro","ru","rw","sh","kn","lc","pm","vc","ws","sm","st","sa","_Scotland","sn","rs","sc","sl","sg","sk","si","sb","so","za","gs","es","lk","sd","sr","sj","sz","se","ch","sy","tw","tj","tz","th","tt","tg","tk","to","tt","tn","tr","tm","tv","ug","ua","ae","gb","uy","us","uz","vu","ve","vn","vi","_Wales","wf","ps","eh","ye","zm","zw");

	    $key = array_search($tmp, $country_arr);
		return $countryVal_arr[$key];
	}
  ?>

	<div class="container">
			
		<table class="table table-hover">
			<thead>
				<tr>
					<th></th>
					<th>Request</th>
					<th>From</th>
					<th>To</th>
					<th>Bids</th>
					<th>Status</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
			  @foreach($reqs as $request)
		                @if($request->status=="Open")
             			<tr  class="success" >

             			@elseif($request->status=="Cancelled")
             			<tr  class="danger" >

             			@elseif($request->status=="Expired")
             			<tr  class="warning" >

             			@elseif($request->status=="Completed")
             			<tr  class="info" >
				@else
				<tr>
				@endif
             			  <!-- onclick="document.location = '/request/view/{{$request->id}}';" style="cursor: pointer;"> -->

				<td>
					<?php $image = 'img/products/' . $request->product->pic_url  ?>
					<a href="/product/view/{{$request->product->id}}" title="{{ $request->product->name }}" data-toggle="popover" data-trigger="hover">
					<img src="{{ URL::asset($image) }}" width="50" height="50" /></a>
				</td>
				<td style="max-width: 280px; min-width: 240px;"><span><strong>{{$request->product->name}}</strong></span><br>		
					<p style="line-height: 1.5em; height: 3em; overflow: hidden; white-space: nowrap; text-overflow: ellipsis; width: 100%;">	
						{{ $request->msg }}
					</p>
				</td>
				<td><?php $stores =  $request->product->stores->pluck('country')->unique() ?>
					@foreach($stores as $st)
                                	<?php $store_country_url = 'img/flags/flags_iso/32/' . getCountryCode($st) . '.png'; ?>
                                	<img src="{{ url(asset($store_country_url)) }}">
                                	<span> {{ $st }}</span>
					<br>
                                	@endforeach
				</td>
				<td>
					@foreach($request->places as $place)
					@if($place->isClientPlace)
                                        <?php $place_country_url = 'img/flags/flags_iso/32/' . getCountryCode($place->country) . '.png'; ?>
                                        <img src="{{ url(asset($place_country_url)) }}">
                                        <span> {{ $place->country }}</span>
                                        <br>
					@endif
                                        @endforeach
				 </td>
				<?php $cancel = $request->bids->where('status', "Cancelled"); ?>
				<td>{{$request->bids->diff($cancel)->count()}}</td>
				<td>
			<?php   $date = strtotime($request->until);
                                $remaining = $date + 86400 - 10600 - time(); 
				$days = floor($remaining / 86400);
				$hours = floor(($remaining % 86400) / 3600);
			?>
				{{$request->status}} 
 @if($request->status=="Open")
<br>
				{{ $days }} Days, {{$hours}} Hours
@endif
				</td>
				<td>
					<style>
						.button-container form,
						.button-container form div {
							display: inline;
						}

						.button-container button {
							display: inline;
							vertical-align: middle;
						}
					</style>
					<div class="button-container">
						<?php $image = 'img/products/'.$request->product->pic_url; ?>
						<button type="button" style="min-width: 38px;" class="btn btn-primary share_button"
							data-options='{"name":"{{ $request->product->name }}",
											"pic_url":"{{ url(asset($image)) }}",
											"qty":"{{ $request->qty }}"}'>
							<i class="fa fa-facebook"></i> <span class="hidden-xs hidden-sm"> Share</span>
						</button>
<!--
						<form action="request/edit/{{ $request->id }}" method="post">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<button type="submit" style="min-width: 38px;" class="btn btn-warning">
								<i class="fa fa-edit"></i> <span class="hidden-xs hidden-sm">Edit</span>
							</button>
						</form>
-->

						<a href="/request/view/{{$request->id}}" style="min-width: 38px;" class="btn btn-danger">
							<i class="fa fa-eye"></i> <span class="hidden-xs hidden-sm"> View</span>
						</a>
					
	@if($request->status=="Open")
						<form action="request/cancel/{{$request->id}}" method="get">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<button type="submit" style="min-width: 38px;" class="btn btn-danger">
								<i class="fa fa-times"></i> <span class="hidden-xs hidden-sm"> Cancel</span>
							</button>
						</form>
@endif
        @if($request->status=="Expired")
<!--
                                                <form action="#" method="get">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        <button type="submit" style="min-width: 38px;" class="btn btn-danger">
                                                                <i class="fa fa-times"></i> <span class="hidden-xs hidden-sm">Refund</span>
                                                        </button>
                                                </form>
-->
        @endif

					</div>
				</td>
			  @endforeach
			</tbody>
		</table>
	</div>


@endsection
