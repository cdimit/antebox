@extends('app')

@section('content')
	<style>
		.table > tbody > tr > td {
			 vertical-align: middle;
		}
		.vertical-alignment-helper {
			display:table;
			height: 100%;
			width: 100%;
			pointer-events:none;
		}
		.vertical-align-center {
			/* To center vertically */
			display: table-cell;
			vertical-align: middle;
			pointer-events:none;
		}
		.modal-content {
			/* Bootstrap sets the size of the modal in the modal-dialog class, we need to inherit it */
			width:inherit;
			height:inherit;
			/* To center horizontally */
			margin: 0 auto;
			pointer-events:all;
		}
	</style>


	<?php
		use App\User;

		function getCountryCode($tmp) {
			$country_arr = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Angola", "Anguilla", "Antartica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Ashmore and Cartier Island", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burma", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Clipperton Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo, Democratic Republic of the", "Congo, Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Europa Island", "Falkland Islands (Islas Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "French Guiana", "French Polynesia", "French Southern and Antarctic Lands", "Gabon", "Gambia", "Gaza Strip", "Georgia", "Germany", "Ghana", "Gibraltar", "Glorioso Islands", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard Island and McDonald Islands", "Holy See (Vatican City)", "Honduras", "Hong Kong", "Howland Island", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Ireland, Northern", "Israel", "Italy", "Jamaica", "Jan Mayen", "Japan", "Jarvis Island", "Jersey", "Johnston Atoll", "Jordan", "Juan de Nova Island", "Kazakhstan", "Kenya", "Kiribati", "Korea, North", "Korea, South", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Man, Isle of", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Midway Islands", "Moldova", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn Islands", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russia", "Rwanda", "Saint Helena", "Saint Kitts and Nevis", "Saint Lucia", "Saint Pierre and Miquelon", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Scotland", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and South Sandwich Islands", "Spain", "Sri Lanka", "Sudan", "Suriname", "Svalbard", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Tobago", "Toga", "Tokelau", "Tonga", "Trinidad", "Tunisia", "Turkey", "Turkmenistan", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "Uruguay", "USA", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands", "Wales", "Wallis and Futuna", "West Bank", "Western Sahara", "Yemen", "Zambia", "Zimbabwe");
			$countryVal_arr = array("af","al","dz","as","ao","ai","aq","ag","ar","am","aw","au","au","at","az","bs","bh","bd","bb","by","be","bz","bj","bm","bt","bo","ba","bw","br","vg","bn","bg","bf","mm","bi","kh","cm","ca","cv","ky","cf","td","cl","cn","cx","fr","cc","co","km","cd","cg","ck","cr","ci","hr","cu","cy","cz","dk","dj","dm","do","ec","eg","sv","gq","er","ee","et","tf","fk","fo","fj","fi","fr","gf","pf","tf","ga","gm","ps","ge","de","gh","gi","tf","gr","gl","gd","gp","gu","gt","gg","gn","gw","gy","ht","hm","va","hn","hk","um","hu","is","in","id","ir","iq","ie","_Northern Ireland","il","it","jm","sj","jp","um","je","um","jo","tf","kz","ke","ki","kp","kr","kw","kg","la","lv","lb","ls","lr","ly","li","lt","lu","mo","mk","mg","mw","my","mv","ml","mt","im","mh","mq","mr","mu","yt","mx","fm","um","md","mc","mn","ms","ma","mz","na","nr","np","nl","an","nc","nz","ni","ne","ng","nu","nf","mp","no","om","pk","pw","pa","pg","py","pe","ph","pn","pl","pt","pr","qa","fr","ro","ru","rw","sh","kn","lc","pm","vc","ws","sm","st","sa","_Scotland","sn","rs","sc","sl","sg","sk","si","sb","so","za","gs","es","lk","sd","sr","sj","sz","se","ch","sy","tw","tj","tz","th","tt","tg","tk","to","tt","tn","tr","tm","tv","ug","ua","ae","gb","uy","us","uz","vu","ve","vn","vi","_Wales","wf","ps","eh","ye","zm","zw");

			$key = array_search($tmp, $country_arr);
			return $countryVal_arr[$key];
		}
	?>

	<?php $from_country_url = 'img/flags/flags_iso/32/' . getCountryCode($product->company->country) . '.png'; ?>


	<div class="container" style="max-width: 780px;">

@if (count($errors) > 0)
	  <div class="alert alert-danger">
  		<strong>Whoops!</strong> There were some problems with your input.<br><br>
  		<ul>
  			@foreach ($errors->all() as $error)
  				<li>{{ $error }}</li>
  			@endforeach
  		</ul>
  	  </div>
    	@endif

	 @if (session('status'))
                  <div class="alert alert-success">
                   <strong>{{ session('status') }}</strong>
                   </div>
        @endif

					<div class="row" style="margin-top: 15px; margin-left: 10px;">
		<h3  class="text-info">{{ $request->status }}</h3>
</div>

			<div class="row" style="margin-bottom: 40px; margin-top: 20px;">
				<div class="col-xs-6">
				  <img src="{{ URL::asset('img/products/'.$product->pic_url) }}" 
						style="max-width: 350px; max-height: 350px; width:100%; height:auto;">
				</div>

				<div class="col-xs-6">
					<div class="row">
						<h1><span class="text-brand-brown">{{ $product->name }} </span>
						<img src="{{ url(asset($from_country_url)) }}"></h1>
					</div>
					
					<div class="row" style="margin-top: 20px;">
						<table style="margin-left: 10px;">
							<tr>
								<td style="width: 80px;"><span>Quantity</span></td>
								<td><span>{{ $request->qty }}</span></td>
							</tr>
							<tr>
								<td><span>Price</span></td>
								<td><span class="fa fa-eur">&nbsp;</span> <span>{{ $request->price }}</span></td>
							</tr>
                                                        <tr>
                                                                <td><span>Weight</span></td>
                                                                <td><span>{{ $request->product->weight * $request->qty }}</span><span class="fa">gr</span></td>
                                                        </tr>

							@if($request->msg!=null)
							<tr>
								<td colspan="2">
									<div class="panel panel-default" style="margin-top: 20px;">
										<div class="panel-heading"><h3 class="panel-title"><span>User Message:</span></h3></div>
										<div class="panel-body">
											<span><div style="white-space:pre-wrap">{{ $request->msg }}</div></span>
										</div>
									</div>
								</td>
							</tr>
							@endif
						</table>
					</div>
					

				</div>
			</div>
			
			
		
			<div class="row">
			<div class="col-sm-6"  style="margin-left: 20px; margin-top: 20px;">
				<div class="panel panel-default">
				<div class="panel-heading"><h3 class="panel-title">Dimensions</h3></div>
				<div class="panel-body">
					<div class="row">
						<label style="width: 60px;"><strong>Width:</strong></label>
						<span>{{ $product->size_w }}cm</span>
					</div>
					<div class="row">
						<label style="width: 60px;"><strong>Height</strong>:</label>
						<span>{{ $product->size_h }}cm</span>
					</div>
					<div class="row">
						<label style="width: 60px;"><strong>Depth:</strong></label>
						<span>{{ $product->size_d }}cm</span>
					</div>
				</div>
				</div>
			</div>
			</div>

			
			<div class="row" style="margin-left: 20px; margin-top: 20px;">
			<div class="col-sm-6">
			@foreach($product->stores as $store)
				<?php $store_country_url = 'img/flags/flags_iso/32/' . getCountryCode($store->country) . '.png'; ?>
				<div class="row">
					<div class="panel panel-default">
						<div class="panel-heading"><h3 class="panel-title">From: {{$store->name}} <img src="{{ url(asset($store_country_url)) }}"></h3></div>
						<div class="panel-body">
							<address>
								<div class="row"><label style="width: 60px;"><strong>Address:</strong></label></div>
								<div class="row"><label style="width: 60px;">{{ $store->country }}, {{ $store->city }}</label></div>
								<div class="row"><label style="width: 60px;">{{ $store->address }}</label></div>
								<div class="row"><label style="width: 60px;">{{ $store->zip_code }}</label></div>
							</address>
<p><strong>Telephone:</strong> {{$store->phone}}</span></p>
<?php $link = 'https://maps.google.com/maps?&z=10&q='.$store->map_x.'+'.$store->map_y.'&ll='.$store->map_x.'+'.$store->map_y ?>
<p><a href="{{$link}}">Map</a></p>
						</div>
					</div>
				</div>
					
		@endforeach
			
			</div>
			</div>
			<div class="row" style="margin-left: 20px; margin-top: 20px;">
<div class="col-sm-6">
				<div class="row">
					<div class="panel panel-default">
						<div class="panel-heading"><h3 class="panel-title">To: {{$place->name}}</h3></div>
						<div class="panel-body" style="padding: 0 0 0 0;">
							<div id="clientGoogleMap" style="width:auto;height:380px;"></div>
						</div>
<?php $place_country_url = 'img/flags/flags_iso/32/' . getCountryCode($place->country) . '.png'; ?>
<div class="row"><label style="width: 60px;"><strong>Country: </strong></label>{{$place->country}} <img src="{{ url(asset($place_country_url)) }}"></div>
<div class="row"><label style="width: 60px;"><strong>City: </strong></label>{{$place->city}}</div>
<div class="row"><label style="width: 60px;"><strong>Address: </strong></label>{{$place->address}}</div>
					</div>
				</div>
</div>


			</div>

			<h4 style="margin-left: 20px; margin-top: 40px;">Travelers Bids</h4>
			
			<div class="row" style="margin-left: 20px; margin-top: 20px;">
				<table class="table">
					<thead>
						<tr>
							<th></th>
							<th>Name</th>
							<th>Message</th>
							<th>Bid</th>
						</tr>
					</thead>
					<tbody>
						@foreach($bids as $bid)
						@if($bid->status=="Accepted")
						<tr class="success" >
							<td>
								<?php
									$user = User::findOrFail($bid->user_id);
									$image = 'img/user_avatars/' . $user->pic_url;  ?>
									<a href="/user/{{$user->id}}" title="{{ $user->first_name }} {{ $user->last_name }}" data-toggle="popover" 
										data-trigger="hover"><img src="{{ URL::asset($image) }}" width="80" height="80" /></a>
							</td>
							<td><p>{{ $user->first_name }} {{ $user->last_name }}</p></td>
							<td style="max-width: 280px; min-width: 240px;">
								<p style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis; width: 100%;">	
									{{ $bid->msg }}
								</p>
							</td>
							
							<td>
								<p><span class="fa fa-euro"></span>{{ $bid->price }}</p>
							</td>
							<td>
								<p>{{ $bid->date }}</p>
								<p>{{ $bid->time }}</p>
								<?php $link = 'https://maps.google.com/maps?&z=10&q='.$bid->requestPlace->map_x.'+'.$bid->requestPlace->map_y.'&ll='.$bid->requestPlace->map_x.'+'.$bid->requestPlace->map_y ?>
								<p><a href="{{$link}}">{{ $bid->requestPlace->name}}
								@if($bid->requestPlace->isClientPlace)<br>(Client Place)@endif
								</a></p>
							</td>

							<td>Accepted</td>
						</tr>
@endif
						@endforeach
					</tbody>
				</table>
			</div>
	</div>

    </div>
</div>

<script>

	$(function () {
		$('#datepicker').datetimepicker({
			format: "YYYY/MM/DD",
		});
		
		$('#timepicker').datetimepicker({
			format: "HH:mm",
		});
	});
</script>

<script
src="https://maps.googleapis.com/maps/api/js">
</script>

<script>

$(document).ready(function () {
   $("#userradio").click(function(){
		document.getElementById('mapdiv').style.display = 'none';
   });

   $("#otherradio").click(function(){
		document.getElementById('mapdiv').style.display = 'block';
		initialize();
   });
   
    $("#date_flex").click(function(){
		if ($("#date_flex").is(':checked')){
			$('#time_flex').prop('checked', true);
		} else {}
		 
	});
   
});
</script>

<script>

function initialize()
{

  var mapProp = {
    center: new google.maps.LatLng({{$place->map_x}},{{$place->map_y}}),
    zoom:12,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };

var  map = new google.maps.Map(document.getElementById("clientGoogleMap"),mapProp);


var marker=new google.maps.Marker({
  position: new google.maps.LatLng({{$place->map_x}},{{$place->map_y}}),
  });

marker.setMap(map);
}


google.maps.event.addDomListener(window, 'load', initialize);
</script>


@endsection

@section('plugins')
  <link rel="stylesheet" href="{{ URL::asset('css/bootstrap-datetimepicker.min.css') }}"/> 
@endsection

@section('scripts')
  <script src="{{ URL::asset('/js/moment.min.js') }}"></script>
  <script type="text/javascript" charset="UTF-8" src="{{ URL::asset('js/bootstrap-datetimepicker.js') }}"></script>
@endsection
