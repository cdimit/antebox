@extends('app')

@section('content')
	
	<style>
		.spinner {
		  width: 100px;
		}
		.spinner input {
		  text-align: right;
		}
		.input-group-btn-vertical {
		  position: relative;
		  white-space: nowrap;
		  width: 1%;
		  vertical-align: middle;
		  display: table-cell;
		}
		.input-group-btn-vertical > .btn {
		  display: block;
		  float: none;
		  width: 100%;
		  max-width: 100%;
		  padding: 8px;
		  margin-left: -1px;
		  position: relative;
		  border-radius: 0;
		}
		.input-group-btn-vertical > .btn:first-child {
		  border-top-right-radius: 4px;
		}
		.input-group-btn-vertical > .btn:last-child {
		  margin-top: -2px;
		  border-bottom-right-radius: 4px;
		}
		.input-group-btn-vertical i{
		  position: absolute;
		  top: 0;
		  left: 4px;
		}
	</style>


	<?php
		function getCountryCode($tmp) {
			$country_arr = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Angola", "Anguilla", "Antartica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Ashmore and Cartier Island", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burma", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Clipperton Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo, Democratic Republic of the", "Congo, Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Europa Island", "Falkland Islands (Islas Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "French Guiana", "French Polynesia", "French Southern and Antarctic Lands", "Gabon", "Gambia", "Gaza Strip", "Georgia", "Germany", "Ghana", "Gibraltar", "Glorioso Islands", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard Island and McDonald Islands", "Holy See (Vatican City)", "Honduras", "Hong Kong", "Howland Island", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Ireland, Northern", "Israel", "Italy", "Jamaica", "Jan Mayen", "Japan", "Jarvis Island", "Jersey", "Johnston Atoll", "Jordan", "Juan de Nova Island", "Kazakhstan", "Kenya", "Kiribati", "Korea, North", "Korea, South", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Man, Isle of", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Midway Islands", "Moldova", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn Islands", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russia", "Rwanda", "Saint Helena", "Saint Kitts and Nevis", "Saint Lucia", "Saint Pierre and Miquelon", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Scotland", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and South Sandwich Islands", "Spain", "Sri Lanka", "Sudan", "Suriname", "Svalbard", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Tobago", "Toga", "Tokelau", "Tonga", "Trinidad", "Tunisia", "Turkey", "Turkmenistan", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "Uruguay", "USA", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands", "Wales", "Wallis and Futuna", "West Bank", "Western Sahara", "Yemen", "Zambia", "Zimbabwe");
			$countryVal_arr = array("af","al","dz","as","ao","ai","aq","ag","ar","am","aw","au","au","at","az","bs","bh","bd","bb","by","be","bz","bj","bm","bt","bo","ba","bw","br","vg","bn","bg","bf","mm","bi","kh","cm","ca","cv","ky","cf","td","cl","cn","cx","fr","cc","co","km","cd","cg","ck","cr","ci","hr","cu","cy","cz","dk","dj","dm","do","ec","eg","sv","gq","er","ee","et","tf","fk","fo","fj","fi","fr","gf","pf","tf","ga","gm","ps","ge","de","gh","gi","tf","gr","gl","gd","gp","gu","gt","gg","gn","gw","gy","ht","hm","va","hn","hk","um","hu","is","in","id","ir","iq","ie","_Northern Ireland","il","it","jm","sj","jp","um","je","um","jo","tf","kz","ke","ki","kp","kr","kw","kg","la","lv","lb","ls","lr","ly","li","lt","lu","mo","mk","mg","mw","my","mv","ml","mt","im","mh","mq","mr","mu","yt","mx","fm","um","md","mc","mn","ms","ma","mz","na","nr","np","nl","an","nc","nz","ni","ne","ng","nu","nf","mp","no","om","pk","pw","pa","pg","py","pe","ph","pn","pl","pt","pr","qa","fr","ro","ru","rw","sh","kn","lc","pm","vc","ws","sm","st","sa","_Scotland","sn","rs","sc","sl","sg","sk","si","sb","so","za","gs","es","lk","sd","sr","sj","sz","se","ch","sy","tw","tj","tz","th","tt","tg","tk","to","tt","tn","tr","tm","tv","ug","ua","ae","gb","uy","us","uz","vu","ve","vn","vi","_Wales","wf","ps","eh","ye","zm","zw");

			$key = array_search($tmp, $country_arr);
			return $countryVal_arr[$key];
		}
	?>

	<?php $from_country_url = 'img/flags/flags_iso/32/' . getCountryCode($product->company->country) . '.png'; ?>


	<div class="container" style="max-width: 780px;">

 	@if (count($errors) > 0)
	  <div class="alert alert-danger">
  		<strong>Whoops!</strong> There were some problems with your input.<br><br>
  		<ul>
  			@foreach ($errors->all() as $error)
  				<li>{{ $error }}</li>
  			@endforeach
  		</ul>
  	  </div>
    	@endif

			<form action="/request/saveRequest/{{$request->id}}" method="post">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">

			<div class="row" style="margin-bottom: 40px; margin-top: 20px;">
				<div class="col-xs-6">
				  <img src="{{ URL::asset('img/products/'.$product->pic_url) }}" 
						style="max-width: 350px; max-height: 350px; width:100%; height:auto;">
				</div>
				<div class="col-xs-6">
					<div class="row">
						<h1><span class="text-brand-brown">{{ $product->name }} </span>
						<img src="{{ url(asset($from_country_url)) }}"></h1>
					</div>
					
					<div class="row" style="margin-top: 20px;">
						<table style="margin-left: 10px;">
							<tr>
								<td  style="width: 80px;"><span>Quantity</span></td>
								<td>
									<div class="input-group spinner">
										<input name="quantity" id="quantity" type="text" class="form-control" value="{{ $request->qty }}">
										<div class="input-group-btn-vertical">
											<button class="btn btn-default" id="btn_inc" type="button"><i class="fa fa-caret-up"></i></button>
											<button class="btn btn-default" id="btn_dec" type="button"><i class="fa fa-caret-down"></i></button>
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td><span>Price</span></td>
								<td><span class="fa fa-times"></span> {{ $product->price }}</td>
							</tr>
							<tr>
								<td><span>Total</span></td>
								<td><span class="fa fa-eur">&nbsp;</span> <label id="label_total"></label></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			
			<div class="row" style="margin-left: 20px; margin-top: 40px;">
				<label style="width: 40px;"><u>From</u> </label>
				@foreach($product->stores as $st)
				<br>
			        <?php $store_country_url = 'img/flags/flags_iso/32/' . getCountryCode($st->country) . '.png'; ?>
				<img src="{{ url(asset($store_country_url)) }}">
				<span> {{ $st->country }}</span>
				@endforeach
			</div>
			
			<div class="row" style="margin-left: 20px; margin-top: 20px;">
              <input type="hidden" name="map_x">
              <input type="hidden" name="map_y">
				<!--<label>Add a Meeting Place</label><br>
				
				<input type="text" id="find" value=""><button onclick="fun()" type="button">Find</button><br>-->
				<div style="width: 500px;">
					<div class="input-group">
						<input type="text" id="find" class="form-control" placeholder="Add a Meeting Place">
						<span class="input-group-btn">
							<button class="btn btn-secondary" onclick="fun()" type="button">Find</button>
						</span>
					</div>
				</div>
				
				
				<div id="googleMap" style="width:500px;height:380px;"></div>
				
				<table style="margin-top: 10px; width: 450px;">
					<tr>
						<td><label style="width: 100px;">Place Name</label></td>
						<td><input type="text" class="form-control" style="width:400px;" name="place_name" id="place_name" value="Place 1" ></td>
					</tr>
					<tr>
						<td><label style="width: 100px;">Country</label></td>
						<td><input type="text" class="form-control" style="width:400px;" id="country" name="country" value="" readonly></td>
					</tr>
					<tr>
						<td><label style="width: 100px;">City</label></td>
						<td><input type="text" class="form-control" style="width:400px;" id="city" value="" name="city" readonly></td>
					</tr>
					<tr>
						<td><label style="width: 100px;">Address</label></td>
						<td><input type="text" class="form-control" style="width:400px;" id="address" value="" name="address" size="60" readonly></td>
					</tr>
					<tr>
						<td>
							<button class="btn btn-primary form-control" onclick="editfun()" id="edit" type="button" >Edit</button>
							<button class="btn btn-primary form-control" onclick="savefun()" type="button" id="save" style="display:none">Save</button>
						</td><td></td>
					</tr>
				</table>
			</div>
			
			<div class="row" style="margin-left: 20px; margin-top: 40px;">
				<span>I want bids until</span>
			</div>
			
			<div class="row" style="margin-left: 20px;">
				<div style="width: 500px;">
					<div class="form-group">
						<div class='input-group date' id='datetimepicker'>
							<input type='text' name="until" id="until" value="{{ $request->until }}" class="form-control" />
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row" style="margin-left: 20px; margin-top: 20px;">
				<span>Message</span>
			</div>
			
			<div class="row" style="margin-left: 20px; margin-top: 10px;">
				<div style="width: 500px;">
					<textarea class="form-control" rows="4" name="message" style="text-align: left; resize:none;">
					{{ $request->msg }}
					</textarea>
				</div>
			</div>
			
			<div class="row" style="margin-left: 20px; margin-top: 20px;">
				<button type="submit" style="width: 100px;" class="btn btn-primary">Save</button>
			</div>
		
		</form>
	</div>
	

<script type="text/javascript">
$(document).ready(function() {
		var num = {{ $product->price }} * $('#quantity').val();
		var total = num.toFixed(2);
		$('#label_total').html(total);
		
		$('#quantity').change(function(){
			num = {{ $product->price }} * $('#quantity').val();
			total = num.toFixed(2);
			$('#label_total').html(total);
		});
		$('#btn_inc').click(function(){
			var qty = parseInt($('#quantity').val()) + 1;
			num = {{ $product->price }} * qty;
			total = num.toFixed(2);
			$('#label_total').html(total);
		});
		$('#btn_dec').click(function(){
			var qty = parseInt($('#quantity').val()) - 1;
			if ( qty < 1 ){
				$('#quantity').val('2');
				qty = 1;
			} else {
				num = {{ $product->price }} * qty;
				total = num.toFixed(2);
				$('#label_total').html(total);
			}
			
		});
		
		var date = new Date();
		date.setDate(date.getDate());
		var maxMonth = new Date();
		maxMonth.setDate(date.getDate());
		maxMonth.setMonth(date.getMonth() + 1);
		
		$('#datetimepicker').datetimepicker({
			format: "YYYY/MM/DD",
		});
		
		$('#datetimepicker').data("DateTimePicker").minDate(date);
		$('#datetimepicker').data("DateTimePicker").maxDate(maxMonth);
		
		
		(function ($) {
			$('.spinner .btn:first-of-type').on('click', function() {
				$('.spinner input').val( parseInt($('.spinner input').val(), 10) + 1);
			});
			$('.spinner .btn:last-of-type').on('click', function() {
				$('.spinner input').val( parseInt($('.spinner input').val(), 10) - 1);
			});
		})(jQuery);
	});

</script>

<script
src="https://maps.googleapis.com/maps/api/js">
</script>

<script>
var map;
var myCenter=new google.maps.LatLng(47.978509, 24.162087);

   geocoder = new google.maps.Geocoder();

function codeLatLng(lat, lng) {

    var city="antebox";
    var latlng = new google.maps.LatLng(lat, lng);
    geocoder.geocode({'latLng': latlng}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
      console.log(results)
        if (results[1]) {
         //formatted address
        var values = results[0].formatted_address.split(',');
        if(isNaN(values[values.length-1]))
        document.getElementById("country").value =values[values.length-1].split(' ').join('');
        else
        document.getElementById("country").value =values[values.length-2].split(' ').join('');
        document.getElementById("address").value =values;

        //find country name
             for (var i=0; i<results[0].address_components.length; i++) {
            for (var b=0;b<results[0].address_components[i].types.length;b++) {

            //there are different types that might hold a city admin_area_lvl_1 usually does in come cases looking for sublocality type will be more appropriate
                if (results[0].address_components[i].types[b] == "administrative_area_level_1") {
                    //this is the object you are looking for
                    city= results[0].address_components[i];
                    break;
                }
            }
        }

        //city data
        if(values[values.length-1]!=' UK')
                document.getElementById("city").value =city.long_name;
        else
                document.getElementById("city").value =values[values.length-3];

          $("input[name='map_x']").val(lat);
          $("input[name='map_y']").val(lng);

 } else {
          alert("No results found");
        }
      } else {
        alert("Geocoder failed due to: " + status);
        document.getElementById("city").value = "-";
        document.getElementById("country").value = "-";
        document.getElementById("address").value = "-";
      }
    });
  }


function initialize()
{
var mapProp = {
  center:myCenter,
  zoom:3,
  mapTypeId:google.maps.MapTypeId.ROADMAP
  };

  map = new google.maps.Map(document.getElementById("googleMap"),mapProp);

  google.maps.event.addListener(map, 'click', function(event) {
    placeMarker(event.latLng);
  });
}

var marker;

function placeMarker(location) {

 if ( marker ) {
    marker.setPosition(location);
  } else {
  marker = new google.maps.Marker({
      position: location,
      map: map
    });
  }

map.panTo(marker.getPosition());


codeLatLng(location.lat(),location.lng());
}

function fun(){
 find = document.getElementById('find');
            var geocoder =  new google.maps.Geocoder();
    geocoder.geocode( { 'address': find.value}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            var latlng = new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng());
 	    placeMarker(latlng);
          } else {
            alert("Something got wrong " + status);
          }
        });
}

function editfun(){
 $("#city").prop('readonly',false);
 $("#address").prop('readonly',false);
 document.getElementById('edit').style.display = 'none';
 document.getElementById('save').style.display = 'block';
}

function savefun(){
 $("#city").prop('readonly',true);
 $("#address").prop('readonly',true);
 document.getElementById('edit').style.display = 'block';
 document.getElementById('save').style.display = 'none';
}


google.maps.event.addDomListener(window, 'load', initialize);
</script>


@endsection

@section('plugins')
  <link rel="stylesheet" href="{{ URL::asset('css/bootstrap-datetimepicker.min.css') }}"/> 
@endsection

@section('scripts')
  <script src="{{ URL::asset('/js/moment.min.js') }}"></script>
  <script type="text/javascript" charset="UTF-8" src="{{ URL::asset('js/bootstrap-datetimepicker.js') }}"></script>
@endsection
