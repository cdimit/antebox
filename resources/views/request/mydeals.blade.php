@extends('app')


@section('content')

	<div class="container">
	    <h3>As Client</h3>
		<table class="table table-hover">
			<thead>
				<tr>
					<th></th>
					<th>Traveller</th>
					<th>Price</th>
					<th>Weight</th>
					<th>Date</th>
					<th>Time</th>
					<th>Status</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
			  @foreach($dealc as $c)
		                @if($c->status=="Active")
             			<tr  class="success" >

             			@elseif($c->status=="Cancelled")
             			<tr  class="danger" >

             			@elseif($c->status=="Unfinished")
             			<tr  class="warning" >

             			@elseif($c->status=="Completed")
             			<tr  class="info" >
				@else
				<tr>
				@endif
             			 <!-- onclick="document.location = '/request/view/{{$c->id}}';" style="cursor: pointer;"> -->

				<td>
					<?php $image = 'img/products/' . $c->bid->request->product->pic_url  ?>
					<img src="{{ URL::asset($image) }}" width="50" height="50" />
				</td>
				<td><span><strong>{{$c->traveller->first_name}} {{$c->traveller->last_name}}</strong></span></td>

				<td><span><strong><span class="fa fa-eur">&nbsp;</span>{{$c->pay->total}}</strong></span><br></td>
				<td><span><strong>{{ $c->bid->request->product->weight * $c->bid->request->qty }}<span class="fa">gr</span></strong></span><br></td>

				<td><span><strong>{{$c->bid->date}}</strong></span><br>
				 </td>
				<td><span><strong>{{$c->bid->time}}</strong></span><br>
				</td>
				<td><span><strong>{{$c->status}}</strong></span><br>
				</td>
				<td>
						<a href="/deal/{{$c->id}}" style="min-width: 38px;" class="btn btn-danger">
							<i class="fa fa-eye"></i> <span class="hidden-xs hidden-sm"> View</span>
						</a>
				</td>
				</tr>
			  @endforeach
			</tbody>
		</table>
<br>
	    <h3>As Traveller</h3>
		<table class="table table-hover">
			<thead>
				<tr>
					<th></th>
					<th>Client</th>
					<th>Price</th>
					<th>Weight</th>
					<th>Date</th>
					<th>Time</th>
					<th>Status</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
			  @foreach($dealt as $t)
		                @if($t->status=="Active")
             			<tr  class="success" >

             			@elseif($t->status=="Cancelled")
             			<tr  class="danger" >

             			@elseif($t->status=="Completed")
             			<tr  class="info" >
				@else
				<tr>
				@endif
             			 <!-- onclick="document.location = '/request/view/{{$t->id}}';" style="cursor: pointer;"> -->

				<td>
					<?php $image = 'img/products/' . $t->bid->request->product->pic_url  ?>
					<img src="{{ URL::asset($image) }}" width="50" height="50" />
				</td>
				<td><span><strong>{{$t->client->first_name}} {{$t->client->last_name}}</strong></span></td>

				<td><span><strong><span class="fa fa-eur">&nbsp;</span>{{$t->pay->total}}</strong></span></td>
				<td><span><strong>{{ $t->bid->request->product->weight * $t->bid->request->qty }}<span class="fa">gr</span></strong></span><br></td>

				<td><span><strong>{{$t->bid->date}}</strong></span><br>
				 </td>
				<td><span><strong>{{$t->bid->time}}</strong></span><br>
				</td>
				<td><span><strong>{{$t->status}}</strong></span><br>
				</td>
				<td>
						<a href="/deal/{{$t->id}}" style="min-width: 38px;" class="btn btn-danger">
							<i class="fa fa-eye"></i> <span class="hidden-xs hidden-sm"> View</span>
						</a>
				</td>
				</tr>
			  @endforeach
			</tbody>
		</table>

	</div>


@endsection
