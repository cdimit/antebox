@extends('app')

@section('content')
	<style>
		.table > tbody > tr > td {
			 vertical-align: middle;
		}
		.vertical-alignment-helper {
			display:table;
			height: 100%;
			width: 100%;
			pointer-events:none;
		}
		.vertical-align-center {
			/* To center vertically */
			display: table-cell;
			vertical-align: middle;
			pointer-events:none;
		}
		.modal-content {
			/* Bootstrap sets the size of the modal in the modal-dialog class, we need to inherit it */
			width:inherit;
			height:inherit;
			/* To center horizontally */
			margin: 0 auto;
			pointer-events:all;
		}
	</style>


	<?php
		use App\User;

		function getCountryCode($tmp) {
			$country_arr = array("Unkown","Afghanistan", "Albania", "Algeria", "American Samoa", "Angola", "Anguilla", "Antartica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Ashmore and Cartier Island", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burma", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Clipperton Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo, Democratic Republic of the", "Congo, Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Europa Island", "Falkland Islands (Islas Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "French Guiana", "French Polynesia", "French Southern and Antarctic Lands", "Gabon", "Gambia", "Gaza Strip", "Georgia", "Germany", "Ghana", "Gibraltar", "Glorioso Islands", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard Island and McDonald Islands", "Holy See (Vatican City)", "Honduras", "Hong Kong", "Howland Island", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Ireland, Northern", "Israel", "Italy", "Jamaica", "Jan Mayen", "Japan", "Jarvis Island", "Jersey", "Johnston Atoll", "Jordan", "Juan de Nova Island", "Kazakhstan", "Kenya", "Kiribati", "Korea, North", "Korea, South", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Man, Isle of", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Midway Islands", "Moldova", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn Islands", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russia", "Rwanda", "Saint Helena", "Saint Kitts and Nevis", "Saint Lucia", "Saint Pierre and Miquelon", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Scotland", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and South Sandwich Islands", "Spain", "Sri Lanka", "Sudan", "Suriname", "Svalbard", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Tobago", "Toga", "Tokelau", "Tonga", "Trinidad", "Tunisia", "Turkey", "Turkmenistan", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "Uruguay", "USA", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands", "Wales", "Wallis and Futuna", "West Bank", "Western Sahara", "Yemen", "Zambia", "Zimbabwe");
			$countryVal_arr = array("un","af","al","dz","as","ao","ai","aq","ag","ar","am","aw","au","au","at","az","bs","bh","bd","bb","by","be","bz","bj","bm","bt","bo","ba","bw","br","vg","bn","bg","bf","mm","bi","kh","cm","ca","cv","ky","cf","td","cl","cn","cx","fr","cc","co","km","cd","cg","ck","cr","ci","hr","cu","cy","cz","dk","dj","dm","do","ec","eg","sv","gq","er","ee","et","tf","fk","fo","fj","fi","fr","gf","pf","tf","ga","gm","ps","ge","de","gh","gi","tf","gr","gl","gd","gp","gu","gt","gg","gn","gw","gy","ht","hm","va","hn","hk","um","hu","is","in","id","ir","iq","ie","_Northern Ireland","il","it","jm","sj","jp","um","je","um","jo","tf","kz","ke","ki","kp","kr","kw","kg","la","lv","lb","ls","lr","ly","li","lt","lu","mo","mk","mg","mw","my","mv","ml","mt","im","mh","mq","mr","mu","yt","mx","fm","um","md","mc","mn","ms","ma","mz","na","nr","np","nl","an","nc","nz","ni","ne","ng","nu","nf","mp","no","om","pk","pw","pa","pg","py","pe","ph","pn","pl","pt","pr","qa","fr","ro","ru","rw","sh","kn","lc","pm","vc","ws","sm","st","sa","_Scotland","sn","rs","sc","sl","sg","sk","si","sb","so","za","gs","es","lk","sd","sr","sj","sz","se","ch","sy","tw","tj","tz","th","tt","tg","tk","to","tt","tn","tr","tm","tv","ug","ua","ae","gb","uy","us","uz","vu","ve","vn","vi","_Wales","wf","ps","eh","ye","zm","zw");

			$key = array_search($tmp, $country_arr);
			return $countryVal_arr[$key];
		}
	?>

	<?php $from_country_url = 'img/flags/flags_iso/32/' . getCountryCode($product->company->country) . '.png'; ?>


	<div class="container" style="max-width: 780px;">

@if (count($errors) > 0)
	  <div class="alert alert-danger">
  		<strong>Whoops!</strong> There were some problems with your input.<br><br>
  		<ul>
  			@foreach ($errors->all() as $error)
  				<li>{{ $error }}</li>
  			@endforeach
  		</ul>
  	  </div>
    	@endif

	 @if (session('status'))
                  <div class="alert alert-success">
                   <strong>{{ session('status') }}</strong>
                   </div>
        @endif

					<div class="row" style="margin-top: 15px; margin-left: 10px;">
@if($request->status=="Open")
						<h3  class="text-success">{{ $request->status }}</h3>
                        <?php   $date = strtotime($request->until);
                                $remaining = $date + 86400 - 10600 - time(); 
                                $days = floor($remaining / 86400);
                                $hours = floor(($remaining % 86400) / 3600);
                        ?>
                                <h4  class="text-info">
                                {{ $days }} Days, {{$hours}} Hours </h4>

@else
<h3  class="text-info">{{ $request->status }}</h3>
@endif
</div>

			<div class="row" style="margin-bottom: 40px; margin-top: 20px;">
				<div class="col-xs-6">
<a href="/product/view/{{$product->id}}">
				  <img src="{{ URL::asset('img/products/'.$product->pic_url) }}" 
						style="max-width: 350px; max-height: 350px; width:100%; height:auto;">
</a>
				</div>

				<div class="col-xs-6">
					<div class="row">
						<h1><a href="/product/view/{{$product->id}}">
<span class="text-brand-brown">{{ $product->name }} </span></a>
						<img src="{{ url(asset($from_country_url)) }}"></h1>
					</div>
					
					<div class="row" style="margin-top: 20px;">
						<table style="margin-left: 10px;">
							<tr>
								<td style="width: 80px;"><span>Quantity</span></td>
								<td><span>{{ $request->qty }}</span></td>
							</tr>
							<tr>
								<td><span>Price</span></td>
								<td><span class="fa fa-eur">&nbsp;</span> <span>{{ $request->price }}</span></td>
							</tr>
                                                        <tr>
                                                                <td><span>Weight</span></td>
                                                                <td><span>{{ $request->product->weight * $request->qty }}</span><span class="fa">gr</span></td>
                                                        </tr>

<tr><td><br></td></tr>
							<tr>
							<td>
						<a href="/user/{{$request->user->id}}">
						<img src="/img/user_avatars/{{ $request->user->pic_url }}" style="width:50px; height:50px; float:left; border-radius:50%; margin-right:25px;"></a>
							</td>
							<td>
						<a href="/user/{{$request->user->id}}">
						<strong>{{$request->user->first_name}} {{$request->user->last_name}}</strong></a>
							</td>
							</tr>
							@if($request->msg!=null)
							<tr>
								<td colspan="2">
									<div class="panel panel-default" style="margin-top: 20px;">
										<div class="panel-heading"><h3 class="panel-title"><span>{{$request->user->first_name}} Message:</span></h3></div>
										<div class="panel-body">
											<span><div style="white-space:pre-wrap">{{ $request->msg }}</div></span>
										</div>
									</div>
								</td>
							</tr>
							@endif
						</table>
					</div>
					

				</div>
			</div>
			
			
		
			<div class="row">
			<div class="col-sm-6"  style="margin-left: 20px; margin-top: 20px;">
				<div class="panel panel-default">
				<div class="panel-heading"><h3 class="panel-title">Dimensions</h3></div>
				<div class="panel-body">
					<div class="row">
						<label style="width: 60px;"><strong>Width:</strong></label>
						<span>{{ $product->size_w }}cm</span>
					</div>
					<div class="row">
						<label style="width: 60px;"><strong>Height</strong>:</label>
						<span>{{ $product->size_h }}cm</span>
					</div>
					<div class="row">
						<label style="width: 60px;"><strong>Depth:</strong></label>
						<span>{{ $product->size_d }}cm</span>
					</div>
				</div>
				</div>
			</div>
			</div>

			
			<div class="row" style="margin-left: 20px; margin-top: 20px;">
			<div class="col-sm-6">
			@foreach($product->stores as $store)
				<?php $store_country_url = 'img/flags/flags_iso/32/' . getCountryCode($store->country) . '.png'; ?>
				<div class="row">
					<div class="panel panel-default">
						<div class="panel-heading"><h3 class="panel-title">From: {{$store->name}} <img src="{{ url(asset($store_country_url)) }}"></h3></div>
						<div class="panel-body">
							<address>
								<div class="row"><label style="width: 60px;"><strong>Address:</strong></label></div>
								<div class="row"><label style="width: 100%;">{{ $store->country }}, {{ $store->city }}</label></div>
								<div class="row"><label style="width: 100%;">{{ $store->address }}</label></div>
								<div class="row"><label style="width: 100%;">{{ $store->zip_code }}</label></div>
							</address>
<p><strong>Telephone:</strong> {{$store->phone}}</span></p>
<?php $link = 'https://maps.google.com/maps?&z=10&q='.$store->map_x.'+'.$store->map_y.'&ll='.$store->map_x.'+'.$store->map_y ?>
<p><a href="{{$link}}">Map</a></p>
						</div>
					</div>
				</div>
					
		@endforeach
			
			</div>
			</div>
			<div class="row" style="margin-left: 20px; margin-top: 20px;">
<div class="col-sm-6">
				<div class="row">
					<div class="panel panel-default">
						<div class="panel-heading"><h3 class="panel-title">To: {{$place->name}}</h3></div>
						<div class="panel-body" style="padding: 0 0 0 0;">
							<div id="clientGoogleMap" style="width:auto;height:380px;"></div>
						</div>
<?php $place_country_url = 'img/flags/flags_iso/32/' . getCountryCode($place->country) . '.png'; ?>
<div class="row"><label style="width: 60px;"><strong>Country: </strong></label>{{$place->country}} <img src="{{ url(asset($place_country_url)) }}"></div>
<div class="row"><label style="width: 60px;"><strong>City: </strong></label>{{$place->city}}</div>
<div class="row"><label style="width: 60px;"><strong>Address: </strong></label>{{$place->address}}</div>
					</div>
				</div>
</div>


			</div>
			
			@if(!($request->user_id == Auth::user()->id) && $request->status=="Open")
			<div class="row" style="margin-left: 20px; margin-top: 40px;">
				<p  data-toggle="modal"
					data-target="#modal-bid"
					id="bid_btn"
					class="btn btn-primary form-control" style="max-width: 125px; margin-bottom: 8px;" >
					Bid
				</p>
			</div>
			@endif

			<h4 style="margin-left: 20px; margin-top: 40px;">Travelers Bids</h4>
			
			<div class="row" style="margin-left: 20px; margin-top: 20px;">
				<table class="table">
					<thead>
						<tr>
							<th></th>
							<th>Name</th>
							<th>Message</th>
							<th>Bid</th>
						</tr>
					</thead>
					<tbody>
						@foreach($bids as $bid)
@if($bid->status=="Open")
						<tr>
							<td>
								<?php
									$user = User::findOrFail($bid->user_id);
									$image = 'img/user_avatars/' . $user->pic_url;  ?>
							<a href="/user/{{$user->id}}" title="{{ $user->first_name }} {{ $user->last_name }}" data-toggle="popover" 
								data-trigger="hover"><img src="{{ URL::asset($image) }}" width="80" height="80" /></a>
							</td>
							<td><p>{{ $user->first_name }} {{ $user->last_name }}</p></td>
							<td style="max-width: 280px; min-width: 240px;">
								<p class="p_message" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis; width: 100%;">	
									{{ $bid->msg }}
								</p>
							</td>
							
							<td>
								<p><span class="fa fa-euro"></span>{{ $bid->price }}</p>
							</td>
							<td>
								<p>{{ $bid->date }} @if($bid->date_flex)
								<span title="Flexible" class="text-success glyphicon glyphicon-exclamation-sign"></span>
								@endif</p>
								<p>{{ $bid->time }} @if($bid->time_flex)
								<span title="Flexible" class="text-success glyphicon glyphicon-exclamation-sign"></span>
								@endif</p>
								<?php $link = 'https://maps.google.com/maps?&z=10&q='.$bid->requestPlace->map_x.'+'.$bid->requestPlace->map_y.'&ll='.$bid->requestPlace->map_x.'+'.$bid->requestPlace->map_y ?>
								<p><a href="{{$link}}">{{ $bid->requestPlace->name}}
								@if($bid->requestPlace->isClientPlace)<br>({{$request->user->first_name}} Place)@endif
								</a></p>
							</td>

							@if ($request->user_id == Auth::user()->id)
<!--
			   				    @if($bid->date_flex)
								<td><a href="#" class="btn btn-warning"><span class="fa fa-question"></span> Ask for better Date</a></td>
							    @elseif($bid->time_flex)
								<td><a href="#" class="btn btn-warning"><span class="fa fa-question"></span> Ask for better Time</a></td>
							    @endif 
-->
								<td><a href="/request/view/{{$request->id}}/accept/{{$bid->id}}" class="btn btn-success"><span class="fa fa-check"></span> Accept</a></td>
<!-- Trigger the modal with a button -->
  <td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal"><span class="fa fa-close"></span> Reject</button></td>

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Why do you want to reject this bid?</h4>
        </div>
        <div class="modal-body">
          <a href="/bid/reject/{{$bid->id}}/1" class="btn btn-default btn-block">The delivery is too expensive</a>
          <a href="/bid/reject/{{$bid->id}}/2" class="btn btn-default btn-block">The delivery time is too long</a>
          <a href="/bid/reject/{{$bid->id}}/3" class="btn btn-default btn-block">Please suggest other meeting time and date</a>
          <a href="/bid/reject/{{$bid->id}}/4" class="btn btn-default btn-block">Just Reject</a>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
							@endif

							@if ($bid->user_id == Auth::user()->id)
								<td><a href="/bid/cancel/{{$bid->id}}" class="btn btn-danger"><span class="fa fa-close"></span> Cancel</a></td>
							@endif
						</tr>
@elseif($bid->status=="Rejected")
<tr class="danger">
                                                       <td>
                                                                <?php
                                                                        $user = User::findOrFail($bid->user_id);
                                                                        $image = 'img/user_avatars/' . $user->pic_url;  ?>
                                                        <a href="/user/{{$user->id}}" title="{{ $user->first_name }} {{ $user->last_name }}" data-toggle="popover" 
                                                                data-trigger="hover"><img src="{{ URL::asset($image) }}" width="80" height="80" /></a>
                                                        </td>
                                                        <td><p>{{ $user->first_name }} {{ $user->last_name }}</p></td>
                                                        <td style="max-width: 280px; min-width: 240px;">
                                                                <p class="p_message" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis; width: 100%;">
                                                                        {{ $bid->msg }}
                                                                </p>
                                                        </td>

                                                        <td>
                                                                <p><span class="fa fa-euro"></span>{{ $bid->price }}</p>
                                                        </td>
                                                        <td>
                                                                <p>{{ $bid->date }} @if($bid->date_flex)
                                                                <span title="Flexible" class="text-success glyphicon glyphicon-exclamation-sign"></span>
                                                                @endif</p>
                                                                <p>{{ $bid->time }} @if($bid->time_flex)
                                                                <span title="Flexible" class="text-success glyphicon glyphicon-exclamation-sign"></span>
                                                                @endif</p>
                                                                <?php $link = 'https://maps.google.com/maps?&z=10&q='.$bid->requestPlace->map_x.'+'.$bid->requestPlace->map_y.'&ll='.$bid->requestPlace->map_x.'+'.$bid->requestPlace->map_y ?>
                                                                <p><a href="{{$link}}">{{ $bid->requestPlace->name}}
                                                                @if($bid->requestPlace->isClientPlace)<br>({{$request->user->first_name}} Place)@endif
                                                                </a></p>
                                                        </td>
<td>
	<strong>Rejected</strong><br>
	{{$bid->reject['reason']}}
</td>
@endif
						@endforeach
					</tbody>
				</table>
			</div>
	</div>
	
<!-- Modal Message -->
<div class="modal" id="modal-bid" tabindex="-3" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="vertical-alignment-helper">
	<div class="modal-dialog vertical-align-center">
	    <div class="modal-content">
		<div class="modal-header" style="background-color: #eeeeee; color: #565a5c; font-size: 15px;">
			<button type="button" class="close" data-dismiss="modal" title="Close">
				<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
			</button>
		    <h4 class="modal-title" id="myModalLabel">Bid</h4>
		</div>
		<div class="modal-body">
			<form action="{{ $request->id }}/bid" method="post">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="usr_id" value="{{ Auth::user()->id }}">
			<div class="row space-2" style="margin-bottom: 20px;">
				<table>
					<tr>
						<td style="width: 70px; padding-left: 20px;"><label>Price</label></td>
						<td style="width: 120px;">
							<div class="input-group">
								<input type="text" name="bid_price" class="form-control" aria-describedby="basic-addon2">
								<span class="input-group-addon" id="basic-addon2"><span class="fa fa-eur"></span></span>
							</div>
						</td>
						<td style="padding-left: 20px; padding-right: 20px;"><span class="fa fa-plus"></span></td>
						<td>{{ $request->price }} <span class="fa fa-eur"></span></td>
					</tr>
				</table>
			</div>
			
<div class="row space-2" style="margin-bottom: 20px;">
				<table>
					<tr>
						<td style="width: 120px; padding-left: 20px;"><label>Meeting Place</label></td>
						<td style="width: 120px;">
							<div class="input-group">
								<input type="radio" id="userradio" name="radioplace" value="user" checked> {{$place->name}} (Client)<br>
								<input type="radio" id="otherradio" name="radioplace" value="other" > Other<br>
							</div>
						</td>
					</tr>
				</table>
			</div>

<div style="display:none" id="mapdiv">

<div class="row" style="margin-left: 20px; margin-top: 20px;">
              <input type="hidden" name="map_x">
              <input type="hidden" name="map_y">
				<!--<label>Add a Meeting Place</label><br>
				
				<input type="text" id="find" value=""><button onclick="fun()" type="button">Find</button><br>-->
				<div style="width: 500px;">
					<div class="input-group">
						<input type="text" id="find" class="form-control" placeholder="Add a Meeting Place">
						<span class="input-group-btn">
							<button class="btn btn-secondary" onclick="fun()" type="button">Find</button>
						</span>
					</div>
				</div>
				
				
				<div id="googleMap" style="width:500px;height:380px;"></div>
				
				<table style="margin-top: 10px; width: 450px;">
					<tr>
						<td><label style="width: 100px;">Place Name</label></td>
						<td><input type="text" class="form-control" style="width:400px;" name="place_name" id="place_name" value="Place 1" ></td>
					</tr>
					<tr>
						<td><label style="width: 100px;">Country</label></td>
						<td><input type="text" class="form-control" style="width:400px;" id="country" name="country" value="" readonly></td>
					</tr>
					<tr>
						<td><label style="width: 100px;">City</label></td>
						<td><input type="text" class="form-control" style="width:400px;" id="city" value="" name="city" readonly></td>
					</tr>
					<tr>
						<td><label style="width: 100px;">Address</label></td>
						<td><input type="text" class="form-control" style="width:400px;" id="address" value="" name="address" size="60" readonly></td>
					</tr>
					<tr>
						<td>
							<button class="btn btn-primary form-control" onclick="editfun()" id="edit" type="button" >Edit</button>
							<button class="btn btn-primary form-control" onclick="savefun()" type="button" id="save" style="display:none">Save</button>
						</td><td></td>
					</tr>
				</table>
			</div>
</div>
			
			<div class="row space-2" style="margin-bottom: 20px;">
				<table>
					<tr>
						<td style="width: 70px; padding-left: 20px;"><label>Date</label></td>
						<td>
							<div class='input-group date' id='datepicker'>
								<input type='text' name="datepicker" id="datepicker" class="form-control" />
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</td>
						<td style="padding-left: 20px;">
							<div class="checkbox">
								<label><input type="checkbox" name="date_flex" id="date_flex" value="1">Flexible</label>
							</div>
						</td>
					</tr>
				</table>
			</div>
			
			<div class="row space-2" style="margin-bottom: 20px;">
				<table>
					<tr>
						<td style="width: 70px; padding-left: 20px;"><label>Time</label></td>
						<td>
							<div class='input-group date' id='timepicker'>
								<input type='text' name="timepicker" id="timepicker" class="form-control" />
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</td>
						<td style="padding-left: 20px;">
							<div class="checkbox">
								<label><input type="checkbox" name="time_flex" id="time_flex" value="1">Flexible</label>
							</div>
						</td>
					</tr>
				</table>
			</div>
			
			<div class="row space-2" style="margin-bottom: 20px;">
				<div class="col-xs-12 col-middle-alt">
					<textarea name="text_message" id="text_message" class="form-control" rows="5" style="resize:none;" placeholder="Message"></textarea>
				</div>
			</div>
			
		    <div class="row space-2" style="margin-bottom: 20px;">
				<div class="col-xs-3 col-xs-offset-3 col-middle-alt">
					<button class="btn btn-primary form-control" type="button" class="close" data-dismiss="modal" title="Close">
					Close
					</button>
				</div>
				<div class="col-xs-3 col-middle-alt">
					<button id="btn_send" type="submit" class="btn btn-primary form-control">
						Bid
					</button>
				</div>
		    </div>
		</form>
		</div>
	    </div>
	</div>
    </div>
</div>

<script>

	$(function () {
		$('#datepicker').datetimepicker({
			format: "YYYY/MM/DD",
		});
		
		$('#timepicker').datetimepicker({
			format: "HH:mm",
		});
	});
</script>

<script
src="https://maps.googleapis.com/maps/api/js">
</script>

<script>

$(document).ready(function () {
   $("#userradio").click(function(){
		document.getElementById('mapdiv').style.display = 'none';
   });

   $("#otherradio").click(function(){
		document.getElementById('mapdiv').style.display = 'block';
		initialize();
   });
   
    $("#date_flex").click(function(){
		if ($("#date_flex").is(':checked')){
			$('#time_flex').prop('checked', true);
		} else {}
		 
	});
   
	$('.p_message').click(function(){
		$($(this)).removeAttr('style');
	});
});
</script>

<script>
var map;
var myCenter=new google.maps.LatLng(47.978509, 24.162087);

   geocoder = new google.maps.Geocoder();

function codeLatLng(lat, lng) {

    var city="antebox";
    var latlng = new google.maps.LatLng(lat, lng);
    geocoder.geocode({'latLng': latlng}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
      console.log(results)
        if (results[1]) {
         //formatted address
        var values = results[0].formatted_address.split(',');
        if(isNaN(values[values.length-1]))
        document.getElementById("country").value =values[values.length-1].split(' ').join('');
        else
        document.getElementById("country").value =values[values.length-2].split(' ').join('');
        document.getElementById("address").value =values;

        //find country name
             for (var i=0; i<results[0].address_components.length; i++) {
            for (var b=0;b<results[0].address_components[i].types.length;b++) {

            //there are different types that might hold a city admin_area_lvl_1 usually does in come cases looking for sublocality type will be more appropriate
                if (results[0].address_components[i].types[b] == "administrative_area_level_1") {
                    //this is the object you are looking for
                    city= results[0].address_components[i];
                    break;
                }
            }
        }

        //city data
        if(values[values.length-1]!=' UK')
                document.getElementById("city").value =city.long_name;
        else
                document.getElementById("city").value =values[values.length-3];

          $("input[name='map_x']").val(lat);
          $("input[name='map_y']").val(lng);

 } else {
          alert("No results found");
        }
      } else {
        alert("Geocoder failed due to: " + status);
        document.getElementById("city").value = "-";
        document.getElementById("country").value = "-";
        document.getElementById("address").value = "-";
      }
    });
  }


function initialize()
{

  var mapProp2 = {
    center: new google.maps.LatLng({{$place->map_x}},{{$place->map_y}}),
    zoom:12,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };

var mapProp = {
  center:myCenter,
  zoom:3,
  mapTypeId:google.maps.MapTypeId.ROADMAP
  };

  map = new google.maps.Map(document.getElementById("googleMap"),mapProp);
  map2 = new google.maps.Map(document.getElementById("clientGoogleMap"),mapProp2);


var marker=new google.maps.Marker({
  position: new google.maps.LatLng({{$place->map_x}},{{$place->map_y}}),
  });

marker.setMap(map2);



  google.maps.event.addListener(map, 'click', function(event) {
    placeMarker(event.latLng);
  });


}

var marker;

function placeMarker(location) {

 if ( marker ) {
    marker.setPosition(location);
  } else {
  marker = new google.maps.Marker({
      position: location,
      map: map
    });
  }

map.panTo(marker.getPosition());


codeLatLng(location.lat(),location.lng());
}

function fun(){
 find = document.getElementById('find');
            var geocoder =  new google.maps.Geocoder();
    geocoder.geocode( { 'address': find.value}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            var latlng = new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng());
 	    placeMarker(latlng);
          } else {
            alert("Something got wrong " + status);
          }
        });
}

function editfun(){
 $("#city").prop('readonly',false);
 $("#address").prop('readonly',false);
 document.getElementById('edit').style.display = 'none';
 document.getElementById('save').style.display = 'block';
}

function savefun(){
 $("#city").prop('readonly',true);
 $("#address").prop('readonly',true);
 document.getElementById('edit').style.display = 'block';
 document.getElementById('save').style.display = 'none';
}


google.maps.event.addDomListener(window, 'load', initialize);
</script>


@endsection

@section('plugins')
  <link rel="stylesheet" href="{{ URL::asset('css/bootstrap-datetimepicker.min.css') }}"/> 
@endsection

@section('scripts')
  <script src="{{ URL::asset('/js/moment.min.js') }}"></script>
  <script type="text/javascript" charset="UTF-8" src="{{ URL::asset('js/bootstrap-datetimepicker.js') }}"></script>
@endsection
