@extends('app')
	
	<style>
		.table > tbody > tr > td {
			 vertical-align: middle;
		}
		
		.chatbox{
			margin-bottom: -200px;
			padding-top: 60px;
			padding-bottom: 60px;
			background: #ebeef0;
		}
		.panel {
			box-shadow: 0 2px 0 rgba(0,0,0,0.075);
			border-radius: 0;
			border: 0;
			margin-bottom: 24px;
		}
		.panel .panel-heading, .panel>:first-child {
			border-top-left-radius: 0;
			border-top-right-radius: 0;
		}
		.panel-heading {
			position: relative;
			height: 50px;
			padding: 0;
			border-bottom:1px solid #eee;
		}
		.panel-control {
			height: 100%;
			position: relative;
			float: right;
			padding: 0 15px;
		}
		.panel-title {
			font-weight: normal;
			padding: 0 20px 0 20px;
			line-height: 34px;
			white-space: nowrap;
			overflow: hidden;
			text-overflow: ellipsis;
		}
		.panel-control>.btn:last-child, .panel-control>.btn-group:last-child>.btn:first-child {
			border-bottom-right-radius: 0;
		}
		.panel-control .btn, .panel-control .dropdown-toggle.btn {
			border: 0;
		}
		.nano {
			position: relative;
			width: 100%;
			height: 100%;
			overflow: hidden;
		}
		.nano>.nano-content {
			position: absolute;
			overflow: scroll;
			overflow-x: hidden;
			top: 0;
			right: 0;
			bottom: 0;
			left: 0;
		}
		.pad-all {
			padding: 15px;
		}
		.mar-btm {
			margin-bottom: 15px;
		}
		.media-block .media-left {
			display: block;
			float: left;
		}
		.img-sm {
			width: 46px;
			height: 46px;
		}
		.media-block .media-body {
			display: block;
			overflow: hidden;
			width: auto;
		}
		.pad-hor {
			padding-left: 15px;
			padding-right: 15px;
		}
		.speech {
			position: relative;
			background: #ffda87;
			color: #a07617;
			display: inline-block;
			border-radius: 0;
			padding: 12px 20px;
		}
		.speech:before {
			content: "";
			display: block;
			position: absolute;
			width: 0;
			height: 0;
			left: 0;
			top: 0;
			border-top: 7px solid transparent;
			border-bottom: 7px solid transparent;
			border-right: 7px solid #ffda87;
			margin: 15px 0 0 -6px;
		}
		.speech-right>.speech:before {
			left: auto;
			right: 0;
			border-top: 7px solid transparent;
			border-bottom: 7px solid transparent;
			border-left: 7px solid #b7dcfe;
			border-right: 0;
			margin: 15px -6px 0 0;
		}
		.speech .media-heading {
			font-size: 1.2em;
			color: #a07617;
			display: block;
			border-bottom: 1px solid rgba(0,0,0,0.1);
			margin-bottom: 10px;
			padding-bottom: 5px;
			font-weight: 300;
		}
		.speech-time {
			margin-top: 20px;
			margin-bottom: 0;
			font-size: .8em;
			font-weight: 300;
		}
		.media-block .media-right {
			float: right;
		}
		.speech-right {
			text-align: right;
		}
		.pad-hor {
			padding-left: 15px;
			padding-right: 15px;
		}
		.speech-right>.speech {
			background: #b7dcfe;
			color: #317787;
			text-align: right;
		}
		.speech-right>.speech .media-heading {
			color: #317787;
		}
		.btn-primary, .btn-primary:focus, .btn-hover-primary:hover, .btn-hover-primary:active, .btn-hover-primary.active, .btn.btn-active-primary:active, .btn.btn-active-primary.active, .dropdown.open>.btn.btn-active-primary, .btn-group.open .dropdown-toggle.btn.btn-active-primary {
			background-color: #579ddb;
			border-color: #5fa2dd;
			color: #fff !important;
		}
		.btn {
			cursor: pointer;
			/* background-color: transparent; */
			color: inherit;
			padding: 6px 12px;
			border-radius: 0;
			border: 1px solid 0;
			font-size: 11px;
			line-height: 1.42857;
			vertical-align: middle;
			-webkit-transition: all .25s;
			transition: all .25s;
		}
		.form-control {
			font-size: 11px;
			height: 100%;
			border-radius: 0;
			box-shadow: none;
			border: 1px solid #e9e9e9;
			transition-duration: .5s;
		}
		.nano>.nano-pane {
			background-color: rgba(0,0,0,0.1);
			position: absolute;
			width: 5px;
			right: 0;
			top: 0;
			bottom: 0;
			opacity: 0;
			-webkit-transition: all .7s;
			transition: all .7s;
		}
	</style>

@section('content')
<section style="background-color: #ebeef0; margin-top:-19px;">

<?php
	function getCountryCode($tmp) {
		$country_arr = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Angola", "Anguilla", "Antartica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Ashmore and Cartier Island", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burma", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Clipperton Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo, Democratic Republic of the", "Congo, Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Europa Island", "Falkland Islands (Islas Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "French Guiana", "French Polynesia", "French Southern and Antarctic Lands", "Gabon", "Gambia", "Gaza Strip", "Georgia", "Germany", "Ghana", "Gibraltar", "Glorioso Islands", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard Island and McDonald Islands", "Holy See (Vatican City)", "Honduras", "Hong Kong", "Howland Island", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Ireland, Northern", "Israel", "Italy", "Jamaica", "Jan Mayen", "Japan", "Jarvis Island", "Jersey", "Johnston Atoll", "Jordan", "Juan de Nova Island", "Kazakhstan", "Kenya", "Kiribati", "Korea, North", "Korea, South", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Man, Isle of", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Midway Islands", "Moldova", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn Islands", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russia", "Rwanda", "Saint Helena", "Saint Kitts and Nevis", "Saint Lucia", "Saint Pierre and Miquelon", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Scotland", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and South Sandwich Islands", "Spain", "Sri Lanka", "Sudan", "Suriname", "Svalbard", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Tobago", "Toga", "Tokelau", "Tonga", "Trinidad", "Tunisia", "Turkey", "Turkmenistan", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "Uruguay", "USA", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands", "Wales", "Wallis and Futuna", "West Bank", "Western Sahara", "Yemen", "Zambia", "Zimbabwe");
		$countryVal_arr = array("af","al","dz","as","ao","ai","aq","ag","ar","am","aw","au","au","at","az","bs","bh","bd","bb","by","be","bz","bj","bm","bt","bo","ba","bw","br","vg","bn","bg","bf","mm","bi","kh","cm","ca","cv","ky","cf","td","cl","cn","cx","fr","cc","co","km","cd","cg","ck","cr","ci","hr","cu","cy","cz","dk","dj","dm","do","ec","eg","sv","gq","er","ee","et","tf","fk","fo","fj","fi","fr","gf","pf","tf","ga","gm","ps","ge","de","gh","gi","tf","gr","gl","gd","gp","gu","gt","gg","gn","gw","gy","ht","hm","va","hn","hk","um","hu","is","in","id","ir","iq","ie","_Northern Ireland","il","it","jm","sj","jp","um","je","um","jo","tf","kz","ke","ki","kp","kr","kw","kg","la","lv","lb","ls","lr","ly","li","lt","lu","mo","mk","mg","mw","my","mv","ml","mt","im","mh","mq","mr","mu","yt","mx","fm","um","md","mc","mn","ms","ma","mz","na","nr","np","nl","an","nc","nz","ni","ne","ng","nu","nf","mp","no","om","pk","pw","pa","pg","py","pe","ph","pn","pl","pt","pr","qa","fr","ro","ru","rw","sh","kn","lc","pm","vc","ws","sm","st","sa","_Scotland","sn","rs","sc","sl","sg","sk","si","sb","so","za","gs","es","lk","sd","sr","sj","sz","se","ch","sy","tw","tj","tz","th","tt","tg","tk","to","tt","tn","tr","tm","tv","ug","ua","ae","gb","uy","us","uz","vu","ve","vn","vi","_Wales","wf","ps","eh","ye","zm","zw");

	    $key = array_search($tmp, $country_arr);
		return $countryVal_arr[$key];
	}
  ?>

	<div class="container" style="padding-top: 60px; padding-bottom: 60px; border-bottom: 1px solid black;">

	<div class="row">
		@if($deal->status=="Active")
			<div class="panel panel-success">
			<div class="panel-heading"><h3 class="panel-title">Deal is Active</h3></div>
		@elseif($deal->status=="Cancelled")
			<div class="panel panel-danger">
			<div class="panel-heading"><h3 class="panel-title">Deal is Cancelled</h3></div>
		@elseif($deal->status=="Unfinished")
			<div class="panel panel-warning">
			<div class="panel-heading"><h3 class="panel-title">Deal is Unfinished</h3></div>
		@elseif($deal->status=="Completed")
			<div class="panel panel-info">
			<div class="panel-heading"><h3 class="panel-title">Deal is Completed</h3></div>
		@else
			<div class="panel panel-default">
			<div class="panel-heading"><h3 class="panel-title">Deal</h3></div>
		@endif
				<div class="panel-body">
					<table style="width:100%">
					    <tr>
						<td><a href="/product/view/{{$request->product->id}}">{{ $request->product->name }}</a> <span class="fa fa-close"> {{$request->qty}} </td>
						<td><span class="fa fa-euro">{{ number_format((float)$request->price, 2, '.', '') }}</td>
						<td>Traveller:</td>
						<td><a href="/user/{{$deal->bid->user->id}}">{{ $deal->bid->user->first_name}} {{$deal->bid->user->last_name}}</a></td>
					    </tr>
					    <tr>
						<td>Traveller Fee</td>
						<td><span class="fa fa-euro">{{ $deal->bid->price }}</td>
						<td>Date:</td>
						<td>{{$deal->bid->date}}</td>
					    </tr>
					    <tr>
						<td  style="border-bottom: solid 1px black;">Services (12%)</td>
					<?php $serv = (0.12*($deal->bid->price + $request->price)) ?>
						<td  style="border-bottom: solid 1px black;"><span class="fa fa-euro">{{ number_format((float)$serv, 2, '.', '') }}</td>
						<td>Time:</td>
						<td>{{ $deal->bid->time }}
					    </tr>
                                            <tr>
                                                <td><strong>Total</strong></td>
                                                <td><span class="fa fa-euro"><strong>{{ $deal->pay->total }}</strong></td>
                                            </tr>
					</table>
					<hr>
<div class="col-md-6">
					<table style="width:100%">
					    <tr>
						<td><u>Meeting Point:</u></td>
					    </tr>
					    <tr>
						<td>Place:</td>
						<td>{{ $deal->bid->requestPlace->name }}</td>
					    </tr>
					    <tr>
						<td>Country:</td>
						<td>{{ $deal->bid->requestPlace->country }}</td>
					    </tr>
					    <tr>
						<td>City:</td>
						<td>{{ $deal->bid->requestPlace->city }}</td>
					    </tr>
					    <tr>
						<td>Addresss:</td>
						<td>{{ $deal->bid->requestPlace->address }}</td>
					    </tr>
					</table>
				     </div>
				<div class="col-md-6">
					<div id="googleMap" style="width:auto;height:190px;"></div>
				</div>
		    </div>
		</div>
	</div>

        <div class="row">
<div class="col-md-7 col-md-offset-1">
@if($deal->status=="Active!")
           <a href="/deal/{{$deal->id}}/cancel" class="btn btn-danger btn-block"><span class="fa fa-close"></span> Cancel the Deal</a>
@endif
	   <br>
<table style="width:100%" cellpadding="20">
<tr>
<td style="width:10%">
<strong>Step 1</strong>
</td>
<td style="width:15%">
@if(!$deal->pay->paid)
	<img src="{{ URL::asset('img/steps/pay.gif')}}" style="width: 100; height: 100"/>
@else
	<img src="{{ URL::asset('img/steps/pay2.png')}}" style="width: 103; height: 100"/>
@endif
</td>
<td style="width:75%; padding-left: 40px;" align="center">
    @if($deal->pay->paid)
	Paid
    @else
    <form action = "/payment" method = "post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

	<input type="hidden" name="deal" value="{{$deal}}">


	<button class="btn btn-success btn-block" type="submit"> <span class="glyphicon glyphicon-credit-card"></span> Pay safely via Paypal</button>
    </form>
<small>Your money will be safely held by AnteBox until the delivery is made!</small>
    @endif
</td>
</tr>
<tr>
<td>
</td>
<td>
<br>
</td>
</tr>
<tr>
<td>
<strong>Step 2</strong>
</td>
<td>
@if(!$deal->pay->paid)
	<img src="{{ URL::asset('img/steps/buy1.png')}}" style="width: 100; height: 100"/>
@elseif(!$deal->isBuy)
	<img src="{{ URL::asset('img/steps/buy.gif')}}" style="width: 100; height: 100"/>
@else
	<img src="{{ URL::asset('img/steps/buy2.png')}}" style="width: 100; height: 100"/>
@endif
</td>
<td style="width:75%; padding-left: 40px;" align="center">
  @if($deal->isBuy)
        Traveller Bought the products
    @elseif(!$deal->pay->paid)
        The Traveller is waiting for your payment!
    @else
	Traveller didn't buy the products yet!
    @endif
</td>
</tr>
<tr>
<td>
</td>
<td>
<br>
</td>
</tr>
<td>
</td>
<tr>
<td>
<strong>Step 3</strong>
</td>
<td>
@if(!$deal->isBuy)
	<img src="{{ URL::asset('img/steps/meet1.png')}}" style="width: 100; height: 100"/>
@elseif(!$deal->isReceived)
	<img src="{{ URL::asset('img/steps/meet.gif')}}" style="width: 100; height: 100"/>
@else
	<img src="{{ URL::asset('img/steps/meet2.png')}}" style="width: 100; height: 100"/>
@endif
</td>
<td style="width:75%; padding-left: 40px;" align="center">
    @if($deal->isReceived)
       Enjoy your Products!
    @elseif($deal->isBuy)
         <a href="/deal/{{$deal->id}}/recive" class="btn btn-primary btn-block">I Received the Products</a>
    @else
         <a href="#" class="btn btn-primary btn-block disabled">I Received the Products</a>
    @endif
</td>
</tr>
</table>
</div>
	</div>
</div>

</section>

	
<div class="chatbox">
	<div class="container">
	<div class="col-lg-10 col-lg-offset-1">
		<div class="panel panel-default">
        	<!--Heading-->
    		<div class="panel-heading">
    			<h3 class="panel-title" style="font-size: 20px;">Chat with {{ $deal->traveller->first_name}} {{ $deal->traveller->last_name}}</h3>
    		</div>
			<hr style="margin-top: 2px; margin-bottom: 2px;">
    		<!--Widget body-->
    		<div id="chat-body" class="collapse in">
    			<div class="nano has-scrollbar" style="height:380px">
    				<div class="nano-content pad-all" tabindex="0" id="scrollableDiv">
    					<ul class="list-unstyled media-block">
						@foreach($deal->messages as $msg)
							@if ( $msg->isClientMsg )
								<li class="mar-btm">
									<div class="media-right">
										<?php $image = 'img/user_avatars/' . $deal->client->pic_url  ?>
										<a href="/user/{{$deal->client->id}}" title="{{ $deal->client->first_name }} {{ $deal->client->last_name }}" data-toggle="popover" data-trigger="hover">
											<img src="{{ URL::asset($image) }}" class="img-circle img-sm" alt="Profile Picture"/>
										</a>
									</div>
									<div class="media-body pad-hor speech-right">
										<div class="speech">
											<a href="#" class="media-heading">{{ $deal->client->first_name }} {{ $deal->client->last_name }}</a>
											<p>{{ $msg->message }}</p>
											<p class="speech-time">
												<i class="fa fa-clock-o fa-fw"></i> {{ $msg->created_at }}
											</p>
										</div>
									</div>
								</li>
							@else
								<li class="mar-btm">
									<div class="media-left">
										<?php $image = 'img/user_avatars/' . $deal->traveller->pic_url  ?>
										<a href="/user/{{$deal->traveller->id}}" title="{{ $deal->traveller->first_name }} {{ $deal->traveller->last_name }}" data-toggle="popover" data-trigger="hover">
											<img src="{{ URL::asset($image) }}" class="img-circle img-sm" alt="Profile Picture"/>
										</a>
									</div>
									<div class="media-body pad-hor">
										<div class="speech">
											<a href="#" class="media-heading">{{ $deal->traveller->first_name }} {{ $deal->traveller->last_name }}</a>
											<p>{{ $msg->message }}</p>
											<p class="speech-time">
											<i class="fa fa-clock-o fa-fw"></i> {{ $msg->created_at }}
											</p>
										</div>
									</div>
								</li>
							@endif
						@endforeach
    					</ul>
    				</div>
    			<div class="nano-pane"><div class="nano-slider" style="height: 141px; transform: translate(0px, 0px);"></div></div></div>
    
    			<!--Widget footer-->
    			<div class="panel-footer">
    				<div class="row">
    					<div class="col-xs-9">
    						<input type="text" placeholder="Type your message..." id="text_message" class="form-control chat-input">
    					</div>
    					<div class="col-xs-3">
    						<button class="btn btn-primary btn-block" id="btn_send" type="submit">Send</button>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
	</div>
	</div>
</div>

<?php 
	if ($deal->client->id == Auth::user()->id){
		$isClient = 1; 
	} else {
		$isClient = 0;
	}
?>
<script
src="https://maps.google.com/maps/api/js?v=3.5&sensor=true">
</script>


<script>
var myCenter=new google.maps.LatLng({{ $deal->bid->requestPLace->map_x}},{{$deal->bid->requestPlace->map_y}});

function initialize()
{
var mapProp = {
  center:myCenter,
  zoom:14,
  mapTypeId:google.maps.MapTypeId.ROADMAP
  };

var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

var marker=new google.maps.Marker({
  position:myCenter,
  });

marker.setMap(map);
}

google.maps.event.addDomListener(window, 'load', initialize);
</script>
<script>
	$(document).ready(function(){
		var scrollBottom = $('#scrollableDiv');
		scrollBottom.scrollTop(scrollBottom.prop("scrollHeight"));
		$("#text_message").val('');
		//$("#text_message").focus();

		$("#btn_send").click(function(e){
			e.preventDefault();
			if ( $.trim($("#text_message").val()).length > 0) {
				$("#btn_send").prop('disabled', true);
				$.ajax({type:"get",
					url: "/message/{{ $deal->id }}",
					data: { text_message: $("#text_message").val(), isClientMsg: {{ $isClient }} },
					success: function(result){
						$("#btn_send").prop('disabled', false);
						window.location.reload();
					}
				});
			}
		});
		
		$("input").keypress(function(event) {
			if (event.which == 13) {
				event.preventDefault();
				$("#btn_send").click();
			}
		});
	});
</script>
@endsection
