<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=0.6">
    <meta name="description"
          content="A new innovative consultancy company that helps globalize businesses that work in a geographically limited area">
    <meta name="keywords"
          content="antebox,antebox revolution,local businesses,local business,travel,travellers,unique,haloumi,halloumi,food,unique food,unique haloumi,unique halloumi,unique fashion,shop,online,startup">

    <meta property="og:title" content="AnteBox - A network for local businesses powered by you"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="https://www.antebox.com"/>
    <meta property="og:description"
          content="A new innovative consultancy company that helps globalize businesses that work in a geographically limited area."/>
    <meta property="og:image" content="{{ url(asset('img/app/favicon.jpg')) }}"/>
    <meta property="og:image:secure_url" content="{{ url(asset('img/app/favicon.jpg')) }}"/>
    <meta property="og:image:type" content="image/png"/>
    <meta property="fb:admins" content="605108675"/>
    <meta property="fb:app_id" content="510079345866294"/>

    <title>AnteBox - A network for local businesses powered by you</title>
    <link rel="shortcut icon" href="{{ asset('img/app/favicon.ico') }}">

    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/landing.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/media.css') }}" rel="stylesheet">

    <!-- Fonts -->
    <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

    <!-- SweetAlert -->
    <script src="{{ asset('/js/sweetalert.min.js') }}"></script>
    <link href="{{ asset('/css/sweetalert.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/themify-icons.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.5.8/slick.css"/>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.5.8/slick-theme.css"/>

@yield('plugins')
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                        (i[r].q = i[r].q || []).push(arguments)
                    }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-71587454-1', 'auto');
        ga('send', 'pageview');
    </script>

    <!-- Facebook Pixel Code -->
    <script>
        !function (f, b, e, v, n, t, s) {
            if (f.fbq)return;
            n = f.fbq = function () {
                n.callMethod ?
                        n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq)f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window,
                document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');

        fbq('init', '238526746496667');
        fbq('track', "PageView");</script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=238526746496667&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->

    <style>
        footer {
            margin-top: 200px;
        }

        @media (min-width: 768px) {
            ul.nav li.dropdown:hover > ul.dropdown-menu {
                display: block;
            }
        }

        .badge-notify {
            background: red;
            position: relative;
        }
    </style>


</head>

<body>
<body>
@include('cookieConsent::index')
@yield('slideBanner')

<nav class="navbar navbar-default" style="width:100%">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#Navbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url('/') }}">
                <img alt="Antebox" style="max-height:100%; width:auto; margin-right:15px;"
                     src="{{ URL::asset('img/app/logo.png')}}"/></a>
        </div>
        <div class="collapse navbar-collapse" id="Navbar">
            <ul class="nav navbar-nav">
                <li><a href="{{ url('/') }}"><i style="font-size:24px;color:#fff" class="fa fa-home"></i></a></li>
                <li><a href="{{ url('/requests') }}">Requests</a></li>
                <li><a href="{{ url('/trips') }}">Travelers</a></li>
                <li><a href="{{ url('/products') }}">Products</a></li>
                <li><a href="{{ url('/partners') }}">Partners</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/how">How it works</a></li>
                @if (Auth::guest())
                    <li><a href="{{ url('/login') }}">Login</a></li>
                    <li><a href="{{ url('/register') }}">Register</a></li>
                @else

                    <?php $notify = Auth::user()->notifications()->get()->where('status', '0')->sortByDesc('id');?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <i class="fa fa-bell" aria-hidden="true"></i>
                            @if($notify->count()!=0)
                                <span class="badge badge-notify">{{ $notify->count()}}</span>
                            @endif
                            <span class="caret"></span>
                        </a>
                        @if($notify->count()!=0)
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1"
                                style="background: #f5f5f5; padding: 0;">
                                <table class="table" style="padding: 0; margin-bottom: 0px;">
                                    @foreach($notify as $not)
                                        <li>
                                            <tr class="active">
                                                <td><a href="/notifications/read/{{$not->id}}">
                                                        @if($not->category=="antebox")
                                                            <img src="{{ URL::asset("img/app/notify.png") }}" width="30"
                                                                 height="30"/>
                                                        @elseif($not->category=="msg")
                                                            <img src="{{ URL::asset("img/app/notify-msg.png") }}"
                                                                 width="30" height="30"/>
                                                        @elseif($not->category=="paid")
                                                            <img src="{{ URL::asset("img/notifications/paid.png") }}"
                                                                 width="30" height="30"/>
                                                        @elseif($not->category=="bid")
                                                            <img src="{{ URL::asset("img/app/notify-bid.png") }}"
                                                                 width="30" height="30"/>
                                                        @elseif($not->category=="warning")
                                                            <img src="{{ URL::asset("img/notifications/warning.png") }}"
                                                                 width="30" height="30"/>
                                                        @elseif($not->category=="shop")
                                                            <img src="{{ URL::asset("img/notifications/shop.png") }}"
                                                                 width="30" height="30"/>
                                                        @elseif($not->category=="deal")
                                                            <img src="{{ URL::asset("img/notifications/deal.png") }}"
                                                                 width="30" height="30"/>
                                                        @elseif($not->category=="terms")
                                                            <img src="{{ URL::asset("img/notifications/terms.png") }}"
                                                                 width="30" height="30"/>
                                                        @endif
                                                    </a></td>
                                                <td><a href="/notifications/read/{{$not->id}}">
                                                        <button type="button"
                                                                class="btn btn-default btn-block">{{$not->msg}}</button>
                                                    </a></td>
                                                <td><a href="/notifications/remove/{{$not->id}}"><img
                                                                src="{{ URL::asset("img/app/notify-remove.png") }}"
                                                                width="30" height="30"/></a></td>
                                            </tr>
                                        </li>

                                    @endforeach
                                    @if($notify->count()>1)
                                        <li>
                                            <tr class="active">
                                                <td></td>
                                                <td>
                                                    <a href="/notifications/removeAll">
                                                        <button type="button" class="btn btn-default btn-block">Clear
                                                            All
                                                        </button>
                                                    </a>
                                                </td>
                                                <td></td>
                                            </tr>
                                        </li>
                                    @endif
                                </table>
                            </ul>
                        @endif
                    </li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        <!-- Welcome, {{ Auth::user()->first_name }} -->
                            <span class="fa fa-user"></span>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            @if(Auth::user()->isAdmin())
                                <li><a href="/dashboard">Dashboard</a></li>
                            @endif
                            <li><a href="/user/{{ Auth::user()->id }}">Profile</a></li>
                            <li><a href="/mydeals">My Deals</a></li>
                            <li><a href="{{ url('/myrequests') }}">My Requests</a></li>
                            <li><a href="/mybids">My Bids</a></li>
                            <li><a href="{{ url('/trips/mytrips') }}">My Trips</a></li>
                            <li><a href="{{ url('/logout') }}">Logout</a></li>
                        </ul>
                    </li>
                @endif
            </ul>
            <!-- <ul class="nav navbar-nav navbar-right">
              <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
              <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
            </ul>
            -->
        </div>
    </div>
</nav>

<div class="container-fluid">
    {{--Requests--}}
    <div class="row height-60">
        <div class="col-md-12">
            <h1>Unique products<br>
                <small>Request the one you want</small>
            </h1>
        </div>
        <div class="col-xs-6 col-md-2 min-250">
            <a href="/product/view/50">
                <img class="product-img img-responsive" src="{{ URL::asset('/img/products/50.jpg') }}" alt="">
            </a>
            <h4>Panettone with pistachio</h4>
        </div>
        <div class="col-xs-6 col-md-2 min-250">
            <a href="/product/view/12">
                <img class="product-img img-responsive" src="{{ URL::asset('/img/products/12.jpg') }}" alt="">
            </a>
            <h4>Halloumi Cheese</h4>
        </div>
        <div class="col-xs-6 col-md-2 min-250">
            <a href="/product/view/1">
                <img class="product-img img-responsive" src="{{ URL::asset('/img/products/1.jpg') }}" alt="">
            </a>
            <h4>Sousoukos</h4>
        </div>
        <div class="col-xs-6 col-md-2 min-250">
            <a href="/product/view/15">
                <img class="product-img img-responsive" src="{{ URL::asset('/img/products/15.jpg') }}" alt="">
            </a>
            <h4>Spreadable Beer Jam (Gipsy)</h4>
        </div>
        <div class="col-xs-12 col-md-3 col-md-offset-1">
            <a class="landing-register-link" href="/register">
                <div class="landing-register-btn text-center">
                    <h2>Become</h2>
                    <h2>AnteBox user</h2>
                    <br>
                    <br>
                    <h2>Sign up <i class="glyphicon glyphicon-arrow-right"></i></h2>
                </div>
            </a>
        </div>
        <div class="col-xs-12 col-md-7 spacer">
            <div class="input-group">
                <span class="input-group-btn">
                <button class="btn btn-default" id="btnSearch" type="button"><i class="glyphicon glyphicon-search"></i></button>
                </span>
                    <input type="text" class="form-control" id="inputSearch" placeholder="Search for product...">
            </div>
        </div>
    </div>
    {{--How it works--}}
    <div class="row height-60 items-background">
        <div class="col-md-12">
            <h1>How does it work?</h1>
        </div>
        <div class="row">
            <div class="spacer">
                <div class="col-md-3 spacer">
                    <div class="green-circle center-block">
                        <span>
                            <h3>Browse through products</h3><br>
                            <img class="landing-icon" src="{{ URL::asset('/assets/img/landing/search.png') }}" alt="">
                        </span>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="green-circle center-block spacer">
                        <span>
                            <h3>Request the one you want</h3><br>
                            <img class="landing-icon" src="{{ URL::asset('/assets/img/landing/bid.png') }}" alt="">
                        </span>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="green-circle center-block spacer">
                        <span>
                            <h3>Receive delivery bids by the local business</h3><br>
                            <img class="landing-icon" src="{{ URL::asset('/assets/img/landing/shop.png') }}" alt="">
                        </span>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="green-circle center-block spacer">
                        <span>
                            <h3>And by travelers! <a href="https://www.antebox.com/#features">Why deliver?</a></h3><br>
                            <img class="landing-icon" src="{{ URL::asset('/assets/img/landing/plane.png') }}" alt="">

                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--Products--}}
    <div class="row height-60 local-business-background spacer">
        <div class="col-md-4">
            <h1>Local products taste better</h1>
            <h3>These products do not only have a
                story and are naturally made, they
                are also cheaper. Sourced directly
                from local producers, we make sure
                you get the best products for the
                lowest price. </h3>
            <a href="https://www.antebox.com/#features" class="btn btn-default about-btn landing-register-btn spacer">
                <h3>Why order via AnteBox?</h3>
            </a>
        </div>
    </div>
    {{--Add product--}}
    <div class="row height-60 items-background">
        <div class="col-md-12 text-center">
            <h1>Suggest your favorite product</h1>
            <a href="/suggest/product">
                <div class="col-md-4 col-md-offset-4 suggest-product-btn about-btn spacer">
                    <h1><i class="glyphicon glyphicon-heart"></i> Suggest a product</h1>
                </div>
            </a>
        </div>
    </div>
    {{--Delivery--}}
    <div class="row height-60 plane-trans-background">
        <div class="col-md-12 text-right">
            <h1>Personalised delivery</h1>
            <div class="col-md-4 col-md-offset-8">
                <h2 class="spacer">Chat with the business sending your product or the traveller delivering it</h2>
            </div>
        </div>
    </div>
    {{--Bid--}}
    <div class="row height-60 blue-triangle-background">
        <div class="col-md-12 text-center">
            <div class="section-heading inverse scrollpoint sp-effect3">
                <h1>Recent Requests</h1>
                <div class="divider"></div>
                <p>Find a request that you can fulfill</p>
            </div>
            <div class="row">
                <div class="col-md-10 col-md-push-1 scrollpoint sp-effect3">
                    <div class="review-filtering">
                        @foreach($request as $req)
                            <div class="review rollitin">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="review-person">
                                            <img src="/img/user_avatars/{{ $req->user->pic_url }}" alt=""
                                                 height="108" class="img-circle">
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="review-comment">
                                            <div class="row">
                                                <div class="col-md-9">
                                                    <h3>“{{$req->msg}}”</h3>
                                                    <p>
                                                        - {{$req->user->first_name}} {{$req->user->last_name}}
                                                        <br><br>
                                                        <strong><a href="/product/view/{{$req->product->id}}">{{$req->product->name}}</a></strong>
                                                        <br>
                                                        <?php $stores = $req->product->stores->pluck('country')->unique() ?>
                                                        @foreach($stores as $st)
                                                            <span>{{ $st }}</span>
                                                        @endforeach
                                                        <i class="fa fa-plane fa-1x"></i>
                                                        @foreach($req->places as $place)
                                                            @if($place->isClientPlace)
                                                                {{ $place->country }}
                                                            @endif
                                                        @endforeach
                                                        <span></span><a href="/request/view/{{$req->id}}"
                                                                        class="btn btn-success">Make a bid</a>
                                                    </p>
                                                </div>
                                                <div class="col-md-3">
                                                    <img src="">
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--Partners--}}
    <div class="row">
        <div class="col-md-12 bottom-spacer text-center">
            <h1>As seen at:</h1>
            <div class="col-md-3 partner text-center">
                <img src="{{ URL::asset('/assets/img/landing/slush.png') }}" alt="">
            </div>

            <div class="col-md-3 partner text-center">
                <img src="{{ URL::asset('/assets/img/landing/accelerate.png') }}" alt="">
            </div>
            <div class="col-md-3 partner text-center">
                <img src="{{ URL::asset('/assets/img/landing/web-summit.png') }}" alt="">
            </div>
            <div class="col-md-3 partner text-center">
                <img src="{{ URL::asset('/assets/img/landing/startups4peace.jpg') }}" alt="">
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $(function () {
            if ($(window).width() <= 620) {
                <?php $image_path = URL::asset("assets/img/freeze/logo.png"); ?>
                $('.navbar-brand').html('<img src="{{ $image_path }}" width="0px" alt="" class="logo">');
            }
        });
        $(window).resize(function () {
            if ($(window).width() <= 620) {
                <?php $image_path = URL::asset("assets/img/freeze/logo.png"); ?>
                $('.navbar-brand').html('<img src="{{ $image_path }}" width="0px" alt="" class="logo">');
            } else {
                <?php $image_path = URL::asset("assets/img/logo-128fff.png"); ?>
                $('.navbar-brand').html('<img src="{{ $image_path }}" width="0px" alt="" class="logo">');
            }
        });
    });

</script>

<script src="{{ URL::asset('assets/js/jquery-1.11.1.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/slick.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/placeholdem.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/rs-plugin/js/jquery.themepunch.plugins.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/waypoints.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/scripts.js') }}"></script>
<script>
    $(document).ready(function () {
        appMaster.preLoader();
        $('#btnSearch').click(function () {
            var search = $("#inputSearch").val();
            window.location = "/products/%5Ename%5E" + search +"%5Ecategory%5E";
        });
    });
</script>

@include('footer')

</body>

</html>