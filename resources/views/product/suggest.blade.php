@extends('app')

@section('content')
  <div class="container">
    <div class="col-md-12">

    @if (count($errors) > 0)
	<div class="alert alert-danger">
  		<strong>Whoops!</strong> There were some problems with your input.<br><br>
  		<ul>
  			@foreach ($errors->all() as $error)
  				<li>{{ $error }}</li>
  			@endforeach
  		</ul>
  	</div>
    @endif

  @if (session('status'))
    <div class="alert alert-success">
    Thank you for submitting a suggestion!<br>
            Our Team will now look into this.<br>
            Feel free to drop us a line at contact@antebox.com
    </div>
  @endif

  <form class="form-horizontal" role="form" method="POST" action="{{ url('/suggest/product/sent')}}" enctype = "multipart/form-data">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="row">
        <div class="form-group">
            <label style="text-align: center; font-weight: bold;" class="col-md-12 control-label text-left">
            <h3><strong>Suggest a Product</strong></h3>
            Do you want to order something that we do not currently offer?</labe>
        </div>
    </div>
    <br>


<div class="col-md-offset-1">
      <div class="col-md-10">

 <div class="panel panel-default">
   <div class="panel-heading"><h3 class="panel-title">Product</h3></div>
      <div class="panel-body">
    <div class="row">
  <div class="form-group">
      <label style="text-align: right; font-weight: bold;" class="col-md-4 control-label text-left">Name (*)</label>
      <div class="col-md-6">
    <input type="text" class="form-control" name="name" value="{{ old('name') }}">
      </div>
  </div>
    </div>

    <div class="row">
  <div class="form-group">
      <label style="text-align: right; font-weight: bold;" class="col-md-4 control-label text-left">Category (*)</label>
      <div class="col-md-6">
    <input type="text" class="form-control" name="category" value="{{ old('category') }}" placeholder="Food, Jewellery, Shoes, ...">
      </div>
  </div>
    </div>

    <div class="row">
	<div class="form-group">
	    <label style="text-align: right; font-weight: bold;" class="col-md-4 control-label text-left">Origin (*)</label>
	    <div class="col-md-6">
		<input type="text" class="form-control"  name="origin" value="{{ old('origin') }}" placeholder="Europe, Greece, Athens, Creta, ...">
	    </div>
	</div>
    </div>

<div class="row">
      <div class="form-group">
        <label style="text-align: right; font-weight: bold; padding-top: 5px;" class="col-sm-4 control-label text-left">Picture</label>
        <div class="col-sm-6">
          <input type="file" accept="image/*" id="pic_url" name="pic_url" class="hidden">
          <label class="btn btn-default form-control" for="pic_url">Choose Picture</label>
          <label id="label_picture" style="margin-top:10px;"></label>
        </div>
      </div>
    </div>
</div>
</div>
</div>

<br>

      <div class="col-md-10">

 <div class="panel panel-default">
   <div class="panel-heading"><h3 class="panel-title">Producer</h3></div>
      <div class="panel-body">
 <div class="row">
  <div class="form-group">
      <label style="text-align: right; font-weight: bold;" class="col-md-4 control-label text-left">Name</label>
      <div class="col-md-6">
    <input type="text" class="form-control" name="producer" value="{{ old('producer') }}">
      </div>
  </div>
    </div>

    <div class="row">
  <div class="form-group">
      <label style="text-align: right; font-weight: bold;" class="col-md-4 control-label text-left">Website</label>
      <div class="col-md-6">
    <input type="text" class="form-control" name="website" value="{{ old('website') }}">
      </div>
  </div>
    </div>

    <div class="row">
  <div class="form-group">
      <label style="text-align: right; font-weight: bold;" class="col-md-4 control-label text-left">Email</label>
      <div class="col-md-6">
    <input type="text" class="form-control"  name="email" value="{{ old('email') }}">
      </div>
  </div>
    </div>
    </div></div></div>

<br>
      <div class="col-md-10">

 <div class="panel panel-default">
   <div class="panel-heading"><h3 class="panel-title">More</h3></div>
      <div class="panel-body">

  <div class="row">
  <div class="form-group">
      <label style="text-align: right; font-weight: bold;" class="col-md-4 control-label text-left">Anything else?</label>
      <div class="col-md-6">
    <textarea class="form-control" rows="4" name="comment" style="resize:none;">{{ old('comment') }}</textarea>
      </div>
  </div>
    </div>

@if(Auth::guest())
    <div class="row">
  <div class="form-group">
      <label style="text-align: right; font-weight: bold;" class="col-md-4 control-label text-left">Your Name (*)</label>
      <div class="col-md-6">
    <input type="text" class="form-control"  name="user_name" value="{{ old('user_name') }}">
      </div>
  </div>
    </div>

    <div class="row">
  <div class="form-group">
      <label style="text-align: right; font-weight: bold;" class="col-md-4 control-label text-left">Your Email (*)</label>
      <div class="col-md-6">
    <input type="text" class="form-control"  name="user_email" value="{{ old('user_email') }}">
      </div>
  </div>
    </div>
    <br>
@else
<input type="hidden" name="user_name" value="{{Auth::user()->first_name}}">
<input type="hidden" name="user_email" value="{{Auth::user()->email}}">
<input type="hidden" name="user_id" value="{{Auth::user()->id}}">
@endif
    </div></div></div>
    <br>

    <div class="row">
        <div class="form-group">
            <label style="text-align: left; font-weight: bold;" class="col-md-12 control-label text-left">
            (*): Required</labe>
        </div>
    </div>

</div>

    <div class="row">
	<div class="form-group">
	    <div class="col-md-2 col-md-offset-8">
		<button style="width:100%" class="btn btn-success">
		    Send
		</button>
	    </div>
	</div>
    </div>
    <br>

</form>
</div>
</div>
@endsection

