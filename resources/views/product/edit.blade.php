@extends('app')

@section('content')
  <div class="container">
    <div class="col-md-12">

	  <div class="row">
        <div class="col-md-10 col-md-offset-2">
          <h1><span class="text-brand-brown">Create a Product!</span></h1>
        </div>
      </div>

 	@if (count($errors) > 0)
	  <div class="alert alert-danger">
  		<strong>Whoops!</strong> There were some problems with your input.<br><br>
  		<ul>
  			@foreach ($errors->all() as $error)
  				<li>{{ $error }}</li>
  			@endforeach
  		</ul>
  	  </div>
    	@endif

    </div>


	<form action="/admin/product/update/{{ $product->id }}" method="post" enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		
		<div class="row" style="margin-bottom: 20px;">
			<div class="form-group">
				<label style="text-align: right; font-weight: bold; padding-top: 5px;" class="col-sm-4 control-label text-left">Category</label>
				<div class="col-sm-6">
					<input type="text" class="form-control" name="category" value="{{ $product->category }}">
				</div>
			</div>
		</div>
		
		<div class="row" style="margin-bottom: 20px;">
			<div class="form-group">
				<label style="text-align: right; font-weight: bold; padding-top: 5px;" class="col-sm-4 control-label text-left">Company ID</label>
				<div class="col-sm-6">
					<input type="text" class="form-control" name="company_id" value="{{ $product->company_id }}">
				</div>
			</div>
		</div>
		
		<div class="row" style="margin-bottom: 20px;">
			<div class="form-group">
				<label style="text-align: right; font-weight: bold; padding-top: 5px;" class="col-sm-4 control-label text-left">Name</label>
				<div class="col-sm-6">
					<input type="text" class="form-control" name="name" value="{{ $product->name }}">
				</div>
			</div>
		</div>
		
		<div class="row" style="margin-bottom: 20px;">
			<div class="form-group">
				<label style="text-align: right; font-weight: bold; padding-top: 5px;" class="col-sm-4 control-label text-left">Description</label>
				<div class="col-sm-6">
					<input type="text" class="form-control" name="description" value="{{ $product->description }}">
				</div>
			</div>
		</div>
		
		<div class="row" style="margin-bottom: 20px;">
			<div class="form-group">
				<label style="text-align: right; font-weight: bold; padding-top: 5px;" class="col-sm-4 control-label text-left">Price</label>
				<div class="col-sm-6">
					<input type="text" class="form-control" name="price" value="{{ $product->price }}">
				</div>
			</div>
		</div>
		
                <div class="row" style="margin-bottom: 20px;">
                        <div class="form-group">
                                <label style="text-align: right; font-weight: bold; padding-top: 5px;" class="col-sm-4 control-label text-left">Bag (*)</label>
                                <div class="col-sm-6">
                                    <select class="form-control" name="bag">
                                        <option>cabin</option>
                                        <option @if($product->bag=="checkin") selected @endif>checkin</option>
                                    </select>
                                </div>
                        </div>
                </div>

		<div class="row" style="margin-bottom: 20px;">
			<div class="form-group">
				<label style="text-align: right; font-weight: bold; padding-top: 5px;" class="col-sm-4 control-label text-left">Weight</label>
				<div class="col-sm-6">
					<input type="text" class="form-control" name="weight" value="{{ $product->weight }}">
				</div>
			</div>
		</div>
		
		<div class="row" style="margin-bottom: 20px;">
			<div class="form-group">
				<label style="text-align: right; font-weight: bold; padding-top: 5px;" class="col-sm-4 control-label text-left">Height</label>
				<div class="col-sm-6">
					<input type="text" class="form-control" name="size_h" value="{{ $product->size_h }}">
				</div>
			</div>
		</div>
		
		<div class="row" style="margin-bottom: 20px;">
			<div class="form-group">
				<label style="text-align: right; font-weight: bold; padding-top: 5px;" class="col-sm-4 control-label text-left">Width</label>
				<div class="col-sm-6">
					<input type="text" class="form-control" name="size_w" value="{{ $product->size_w }}">
				</div>
			</div>
		</div>
		
		<div class="row" style="margin-bottom: 20px;">
			<div class="form-group">
				<label style="text-align: right; font-weight: bold; padding-top: 5px;" class="col-sm-4 control-label text-left">Depth</label>
				<div class="col-sm-6">
					<input type="text" class="form-control" name="size_d" value="{{ $product->size_d }}">
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="form-group">
				<label style="text-align: right; font-weight: bold; padding-top: 5px;" class="col-sm-4 control-label text-left">Picture URL</label>
				<div class="col-sm-6">
					<input type="file" accept="image/*" id="pic_url" name="pic_url" class="hidden">
					<label class="btn btn-default form-control" for="pic_url">Choose Picture</label>
					<label id="label_picture" style="margin-top:10px;">{{ $product->pic_url }}</label>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="form-group">
				<label style="text-align: right; font-weight: bold; padding-top: 5px;" class="col-sm-4 control-label text-left">Video URL</label>
				<div class="col-sm-6">
					<input type="file" accept="video/*" id="vid_url" name="vid_url" class="hidden">
					<label class="btn btn-default form-control" for="vid_url">Choose Video</label>
					<label id="label_video" style="margin-top:10px;">{{ $product->vid_url }}</label>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="form-group">
				<label style="text-align: right; font-weight: bold; padding-top: 5px;" class="col-sm-4 control-label text-left">Video Poster</label>
				<div class="col-sm-6">
					<input type="file" accept="image/jpg" id="poster" name="poster" class="hidden">
					<label class="btn btn-default form-control" for="poster">Choose Poster</label>
					<label id="label_poster" style="margin-top:10px;">{{ $product->id }}.jpg</label>
				</div>
			</div>
		</div>
		
		<div class="row" style="margin-bottom: 20px;">
			<div class="form-group">
				<div class="col-sm-2 col-sm-offset-8">
					<button type="submit" class="btn btn-primary form-control" style="width:100%;">Update</button>
				</div>
			</div>
		</div>

	</form>
  </div>

<!-- script for showing path of image file -->
<script>
    $(function() {
		$("input:file").change(function(){
			var filePath = $(this).val();
			var filename = filePath.substring(filePath.lastIndexOf("\\") + 1);
			if ($(this).attr('name') == "pic_url"){
				$('#label_picture').html(filename);
			} else if ($(this).attr('name') == "vid_url"){
				$('#label_video').html(filename);
			} else {
				$('#label_poster').html(filename);
			}
			
		});
    });

</script>  
  
  
@endsection
