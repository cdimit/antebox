@extends('app')

@section('content')


<?php $category = $products->pluck('category')->unique()->sort(); ?>
<?php $categoryAll = $all->pluck('category')->unique()->sort(); ?>
<?php $country = $products->pluck('country')->unique()->sort(); ?>
<?php $countryAll = $all->pluck('country')->unique()->sort();?>
<?php $company = $products->pluck('company')->unique()->sort(); ?>
<?php $companyAll = $all->pluck('company')->unique()->sort(); ?>
<?php $price = $products->pluck('price')->unique();?>
<?php $priceAll = $all->pluck('price')->unique();?>
<?php $weight = $products->pluck('weight')->unique(); ?>
<?php $weightAll = $all->pluck('weight')->unique(); ?>

<?php
        function getCountryCode($tmp) {
		$country_arr = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Angola", "Anguilla", "Antartica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Ashmore and Cartier Island", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burma", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Clipperton Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo, Democratic Republic of the", "Congo, Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Europa Island", "Falkland Islands (Islas Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "French Guiana", "French Polynesia", "French Southern and Antarctic Lands", "Gabon", "Gambia", "Gaza Strip", "Georgia", "Germany", "Ghana", "Gibraltar", "Glorioso Islands", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard Island and McDonald Islands", "Holy See (Vatican City)", "Honduras", "Hong Kong", "Howland Island", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Ireland, Northern", "Israel", "Italy", "Jamaica", "Jan Mayen", "Japan", "Jarvis Island", "Jersey", "Johnston Atoll", "Jordan", "Juan de Nova Island", "Kazakhstan", "Kenya", "Kiribati", "Korea, North", "Korea, South", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Man, Isle of", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Midway Islands", "Moldova", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn Islands", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russia", "Rwanda", "Saint Helena", "Saint Kitts and Nevis", "Saint Lucia", "Saint Pierre and Miquelon", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Scotland", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and South Sandwich Islands", "Spain", "Sri Lanka", "Sudan", "Suriname", "Svalbard", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Tobago", "Toga", "Tokelau", "Tonga", "Trinidad", "Tunisia", "Turkey", "Turkmenistan", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "Uruguay", "USA", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands", "Wales", "Wallis and Futuna", "West Bank", "Western Sahara", "Yemen", "Zambia", "Zimbabwe");
		$countryVal_arr = array("af","al","dz","as","ao","ai","aq","ag","ar","am","aw","au","au","at","az","bs","bh","bd","bb","by","be","bz","bj","bm","bt","bo","ba","bw","br","vg","bn","bg","bf","mm","bi","kh","cm","ca","cv","ky","cf","td","cl","cn","cx","fr","cc","co","km","cd","cg","ck","cr","ci","hr","cu","cy","cz","dk","dj","dm","do","ec","eg","sv","gq","er","ee","et","tf","fk","fo","fj","fi","fr","gf","pf","tf","ga","gm","ps","ge","de","gh","gi","tf","gr","gl","gd","gp","gu","gt","gg","gn","gw","gy","ht","hm","va","hn","hk","um","hu","is","in","id","ir","iq","ie","_Northern Ireland","il","it","jm","sj","jp","um","je","um","jo","tf","kz","ke","ki","kp","kr","kw","kg","la","lv","lb","ls","lr","ly","li","lt","lu","mo","mk","mg","mw","my","mv","ml","mt","im","mh","mq","mr","mu","yt","mx","fm","um","md","mc","mn","ms","ma","mz","na","nr","np","nl","an","nc","nz","ni","ne","ng","nu","nf","mp","no","om","pk","pw","pa","pg","py","pe","ph","pn","pl","pt","pr","qa","fr","ro","ru","rw","sh","kn","lc","pm","vc","ws","sm","st","sa","_Scotland","sn","rs","sc","sl","sg","sk","si","sb","so","za","gs","es","lk","sd","sr","sj","sz","se","ch","sy","tw","tj","tz","th","tt","tg","tk","to","tt","tn","tr","tm","tv","ug","ua","ae","gb","uy","us","uz","vu","ve","vn","vi","_Wales","wf","ps","eh","ye","zm","zw");

            $key = array_search($tmp, $country_arr);
                return $countryVal_arr[$key];
        }
  ?>

<?php $link = $_SERVER['REQUEST_URI'];
if(strlen($link)==9){ $link = $link.'/';} ?>


<?php
	               $data = explode("^", rawurldecode($link));
$chosenName = null;
$linkCateg = null;
$chosenCategory = null;
$linkCompany = null;
$chosenCompany = null;
$linkCountry = null;
$chosenCountry = null;
$linkPrice = null;
$chosenPrice = null;
$linkWeight = null;
$chosenWeight = null;
$maxp = null;
$minp = null;
$maxw = null;
$minw = null;
		for($i=1; $i<count($data); $i+=2){
		    if($data[$i]=="category"){
			$chosenCategory = $data[$i+1];
			$linkCateg = "/products/";
			for($j=1; $j<count($data); $j++){
			    if($j!=$i && $j!=$i+1){
				$linkCateg = $linkCateg."^".$data[$j];
			    }
			}
		    }
		    if($data[$i]=="company"){
			$chosenCompany = $data[$i+1];
			$linkCompany = "/products/";
			for($j=1; $j<count($data); $j++){
			    if($j!=$i && $j!=$i+1){
				$linkCompany = $linkCompany."^".$data[$j];
			    }
			}
		    }
		    if($data[$i]=="country"){
			$chosenCountry = $data[$i+1];
			$linkCountry = "/products/";
			for($j=1; $j<count($data); $j++){
			    if($j!=$i && $j!=$i+1){
				$linkCountry = $linkCountry."^".$data[$j];
			    }
			}
		    }
		    if($data[$i]=="price"){
			$chosenPrice = $data[$i+1];
                        $value = explode(",", $data[$i+1]);
                        if(count($value)==2){
			    $maxp = $value[1];
			    $minp = $value[0];
			}
			$linkPrice = "/products/";
			for($j=1; $j<count($data); $j++){
			    if($j!=$i && $j!=$i+1){
				$linkPrice = $linkPrice."^".$data[$j];
			    }
			}
		    }
		    if($data[$i]=="weight"){
			$chosenWeight = $data[$i+1];
                        $value = explode(",", $data[$i+1]);
                        if(count($value)==2){
			    $maxw = $value[1];
			    $minw = $value[0];
			}
			$linkWeight = "/products/";
			for($j=1; $j<count($data); $j++){
			    if($j!=$i && $j!=$i+1){
				$linkWeight = $linkWeight."^".$data[$j];
			    }
			}
		    }
		    if($data[$i]=="name"){
			$chosenName = $data[$i+1];
		    }
		}
if($linkCateg==null){$linkCateg = $link;}
if($linkCompany==null){$linkCompany = $link;}
if($linkCountry==null){$linkCountry = $link;}
if($linkPrice==null){$linkPrice = $link;}
if($linkWeight==null){$linkWeight = $link;}
if($maxp==null){$maxp = $priceAll->max();}
if($minp==null){$minp = $priceAll->min();}
if($maxw==null){$maxw = $weightAll->max();}
if($minw==null){$minw = $weightAll->min();}
?>


	<div class="container" style="max-width: 1280px;">
	
		<div class="row">
			<div class="col-xs-7">
				<input type="text" class="form-control" name="name" id="search" value="{{$chosenName}}"  placeholder="Search..."/>
			</div>
			<div class="col-xs-3">
				<select id="category" class="form-control">
					<option value="">All Categories</option>
					<option class="select-dash" disabled="disabled">--------------</option>
					@foreach($categoryAll as $c)
					@if(rawurlencode($c)==$chosenCategory)
							<option value="{{$c}}" selected>{{$c}}</option>
					@else
							<option value="{{$c}}">{{$c}}</option>
					@endif
					@endforeach
				</select>
			</div>
			<div class="col-xs-2">
				<a class="btn btn-primary" class="form-control" id="btnSearch" href="/products/^name^">Search</a>
			</div>
		</div>
	
		<div class="row" style="margin-top: 40px;">

			<div class="col-xs-3">
<h3 style="display:inline;"><u>Filter</u></h3>
				@if($products->count()!=$all->count())
					<a href="/products">Clear all</a>
				@endif
<br><br>
				<p><strong>Category</strong></p>

				@foreach($categoryAll as $c)
					@if(rawurldecode($c)==$chosenCategory)
						<div class="radio">
							<a href="{{$linkCateg}}" style="color:black"><img src="/img/app/checked.png"> {{$c}} <span class="glyphicon glyphicon-remove"></a>
						</div>
					@else
						<div class="radio">
							<label><input type="radio" value="{{$linkCateg}}^category^{{$c}}" name="radio_category">{{$c}}</label>
						</div>
					@endif
				@endforeach


				<p style="margin-top: 36px;"><strong>Price (<i class="fa fa-euro"></i>)</strong></p>
				<div id="amount"></div>
				<div id="slider-range" style="width: 160px; margin-top: 8px;"></div>
					
					
				<p style="margin-top: 36px;"><strong>Country</strong></p>

					@foreach($countryAll as $c)
						@if($country->contains($c) || $chosenCountry!=null)
						    @if(rawurldecode($c)==$chosenCountry)
							<div class="radio">
								 <a href="{{$linkCountry}}" style="color:black"><img src="/img/app/checked.png"> {{$c}} <span class="glyphicon glyphicon-remove"></span></a>
							</div>
						    @else
							<div class="radio">
								<label><input type="radio" value="{{$link}}^country^{{$c}}" name="radio_country">{{$c}}</label>
							</div>
						    @endif
						@elseif(count($products)==0)
							<div class="radio">
								<label><input type="radio" value="{{$link}}^country^{{$c}}" name="radio_country">{{$c}}</label>
							</div>
						@else
							<div class="radio">
								<label><input disabled type="radio" value="{{$link}}^country^{{$c}}" name="radio_country">{{$c}}</label>
							</div>
						@endif
					@endforeach
					
				<p style="margin-top: 36px;"><strong>Company</strong></p>

					@foreach($companyAll as $c)
						@if($company->contains($c) || $chosenCompany!=null)
							@if(rawurldecode($c)==$chosenCompany)
								<div class="radio">
									<a href="{{$linkCompany}}" style="color:black"><img src="/img/app/checked.png"> {{$c}} <span class="glyphicon glyphicon-remove"></a>
								</div>
							@else
								<div class="radio">
									<label><input type="radio" value="{{$link}}^company^{{$c}}" name="radio_country">{{$c}}</label>
								</div>
							@endif
						@elseif(count($products)==0)
							<div class="radio">
								<label><input type="radio" value="{{$link}}^company^{{$c}}" name="radio_country">{{$c}}</label>
							</div>
						@else
							<div class="radio">
								<label><input type="radio" disabled value="{{$link}}^company^{{$c}}" name="radio_country">{{$c}}</label>
							</div>
						@endif
					@endforeach

					
				<p style="margin-top: 36px;"><strong>Weight (gr)</strong></p>
				<div id="amount2"></div>
				<div id="slider-range2" style="width: 160px; margin-top: 8px;"></div>


			</div>
		<div class="col-sm-9">
	<table>
<strong>{{$products->count()}} Products</strong>
<hr>
@foreach($products as $product)
<tr>
<td onclick="document.location = '/product/view/{{$product->get('id')}}';" style="cursor: pointer;">
                <div class="row" style="margin-bottom: 40px;" >
                        <div class="col-xs-4">
                          <img src="{{ URL::asset('img/products/'.$product->get('pic_url')) }}" 
                                        style="max-width: 225px; max-height:225px; width:100%; height:auto;">
                        </div>
                        <div class="col-sm-8">
<?php $from_country_url = 'img/flags/flags_iso/32/' . getCountryCode($product->get('country')) . '.png'; ?>
                                <h3><span class="text-brand-brown">{{ $product->get('name') }}</span></h3>
                                <p><img src="{{ url(asset($from_country_url)) }}" title="{{$product->get('country')}}"> {{ $product->get('country') }}</p>
                                <p>{{ $product->get('category') }}</p>
				@if($product->get('style')==null)
                                <p><i class="fa fa-euro"></i>{{ $product->get('price') }}</p>
				@else
                                <p><small><i class="fa fa-euro"></i><del>{{ $product->get('price') }}</del></small>
@if($product->get('style')=='%')
<strong style="color:red;font-size:20px;">{{$product->get('value')}}{{$product->get('style')}}</strong>
@else
<strong style="color:red;font-size:20px;">{{$product->get('style')}}{{$product->get('value')}}&#8364;</strong>
@endif
</p>
<?php if($product->get('style')=='%') $discPrice = $product->get('price') - ($product->get('price') * ($product->get('value') / 100)); else $discPrice = $product->get('price') - $product->get('value'); ?>
                                <p><i class="fa fa-euro"></i>{{ number_format((float)$discPrice, 2, '.', '') }}</p>
				@endif
                                <p>{{ $product->get('weight') }}gr</p>
                        </div>
                </div>
<hr>
</td>
</tr>

@endforeach

</table>

				</div>
		</div>


<br>
	</div>
	


<script>
	$(document).ready(function(){
		$('#btnSearch').click(function (){
			$('#btnSearch').attr('href', $('#btnSearch').attr('href') + $('#search').val() + '^category^' + $('#category').val());
		});
		
		$(function() {
			$( "#slider-range" ).slider({
			  range: true,
			  min: {{ $priceAll->min() }},
			  max: {{ $priceAll->max() }},
			  values: [ {{ $minp }}, {{ $maxp }} ],
			  slide: function( event, ui ) {
				$( "#amount" ).html( ui.values[ 0 ] + " - " + ui.values[ 1 ] );
			  }
			});
			$( "#amount" ).html( $( "#slider-range" ).slider( "values", 0 ) + " - " + $( "#slider-range" ).slider( "values", 1 ) );
		});
		
		$(function() {
			$( "#slider-range2" ).slider({
			  range: true,
			  min: {{ $weightAll->min() }},
			  max: {{ $weightAll->max() }},
			  values: [ {{ $minw }}, {{ $maxw }} ],
			  slide: function( event, ui ) {
				$( "#amount2" ).html( ui.values[ 0 ] + " - " + ui.values[ 1 ] );
			  }
			});
			$( "#amount2" ).html( $( "#slider-range2" ).slider( "values", 0 ) + " - " + $( "#slider-range2" ).slider( "values", 1 ) );
		});
		
		$( "#slider-range" ).mouseup(function() {
			window.location.href = "{{$linkPrice}}^price^" + $( "#slider-range" ).slider("option", "values");
		});
		
		$( "#slider-range2" ).mouseup(function() {
			window.location.href = "{{$linkWeight}}^weight^" + $( "#slider-range2" ).slider("option", "values");
		});

		
		
		$(function(){
			$('input:radio').change(
				function(){
					window.location.href = $(this).val();
				}
			);         
		});

	});
	
	
	
</script>
	
@endsection


@section('plugins')
  <link rel="stylesheet" href="{{ URL::asset('css/jquery-ui.css') }}"/> 
@endsection

@section('scripts')
  <script src="{{ URL::asset('/js/jquery-ui.js') }}"></script>
@endsection
