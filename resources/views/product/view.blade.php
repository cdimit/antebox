﻿@extends('app')

@section('content')

<style>
video { 
	-webkit-background-size:cover; 
	-moz-background-size:cover; 
	-o-background-size:cover; 
	background-size:cover; 

}
</style>

	<?php
		function getCountryCode($tmp) {
		$country_arr = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Angola", "Anguilla", "Antartica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Ashmore and Cartier Island", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burma", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Clipperton Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo, Democratic Republic of the", "Congo, Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Europa Island", "Falkland Islands (Islas Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "French Guiana", "French Polynesia", "French Southern and Antarctic Lands", "Gabon", "Gambia", "Gaza Strip", "Georgia", "Germany", "Ghana", "Gibraltar", "Glorioso Islands", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard Island and McDonald Islands", "Holy See (Vatican City)", "Honduras", "Hong Kong", "Howland Island", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Ireland, Northern", "Israel", "Italy", "Jamaica", "Jan Mayen", "Japan", "Jarvis Island", "Jersey", "Johnston Atoll", "Jordan", "Juan de Nova Island", "Kazakhstan", "Kenya", "Kiribati", "Korea, North", "Korea, South", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Man, Isle of", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Midway Islands", "Moldova", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn Islands", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russia", "Rwanda", "Saint Helena", "Saint Kitts and Nevis", "Saint Lucia", "Saint Pierre and Miquelon", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Scotland", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and South Sandwich Islands", "Spain", "Sri Lanka", "Sudan", "Suriname", "Svalbard", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Tobago", "Toga", "Tokelau", "Tonga", "Trinidad", "Tunisia", "Turkey", "Turkmenistan", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "Uruguay", "USA", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands", "Wales", "Wallis and Futuna", "West Bank", "Western Sahara", "Yemen", "Zambia", "Zimbabwe");
		$countryVal_arr = array("af","al","dz","as","ao","ai","aq","ag","ar","am","aw","au","au","at","az","bs","bh","bd","bb","by","be","bz","bj","bm","bt","bo","ba","bw","br","vg","bn","bg","bf","mm","bi","kh","cm","ca","cv","ky","cf","td","cl","cn","cx","fr","cc","co","km","cd","cg","ck","cr","ci","hr","cu","cy","cz","dk","dj","dm","do","ec","eg","sv","gq","er","ee","et","tf","fk","fo","fj","fi","fr","gf","pf","tf","ga","gm","ps","ge","de","gh","gi","tf","gr","gl","gd","gp","gu","gt","gg","gn","gw","gy","ht","hm","va","hn","hk","um","hu","is","in","id","ir","iq","ie","_Northern Ireland","il","it","jm","sj","jp","um","je","um","jo","tf","kz","ke","ki","kp","kr","kw","kg","la","lv","lb","ls","lr","ly","li","lt","lu","mo","mk","mg","mw","my","mv","ml","mt","im","mh","mq","mr","mu","yt","mx","fm","um","md","mc","mn","ms","ma","mz","na","nr","np","nl","an","nc","nz","ni","ne","ng","nu","nf","mp","no","om","pk","pw","pa","pg","py","pe","ph","pn","pl","pt","pr","qa","fr","ro","ru","rw","sh","kn","lc","pm","vc","ws","sm","st","sa","_Scotland","sn","rs","sc","sl","sg","sk","si","sb","so","za","gs","es","lk","sd","sr","sj","sz","se","ch","sy","tw","tj","tz","th","tt","tg","tk","to","tt","tn","tr","tm","tv","ug","ua","ae","gb","uy","us","uz","vu","ve","vn","vi","_Wales","wf","ps","eh","ye","zm","zw");

			$key = array_search($tmp, $country_arr);
				return $countryVal_arr[$key];
		}
	?>

	<?php $from_country_url = 'img/flags/flags_iso/32/' . getCountryCode($product->company->country) . '.png'; ?>

	
	<div class="container" style="max-width: 780px;">
		
		<div class="row" style="margin-bottom: 40px;">
			<div class="col-xs-6">
			  <img src="{{ URL::asset('img/products/'.$product->pic_url) }}" 
					style="max-width: 350px; max-height: 350px; width:100%; height:auto;">
			</div>
			<div class="col-xs-6">
				<div class="row">
					<h1><span class="text-brand-brown">{{ $product->name }} </span><img src="{{ url(asset($from_country_url)) }}"></h1>
				</div>
				<div class="row">
					<!-- <label style="width: 80px;">Category</label> --><span class="text-brand-brown" style="font-size: 20px;">{{ $product->category }}</span>
				</div>
				<div class="row">
				    @if($product->discount==null)
					<span class="text-brand-brown" style="font-size: 20px;">&#8364; {{ $product->price }}</span>
				    @else
	<span class="text-brand-gray" style="font-size: 18px;"><del>&#8364; {{ $product->price }}</del>
@if($product->discount->style=='%')
<strong style="color:red;font-size:20px;">{{$product->discount->value}}{{$product->discount->style}}</strong>
@else
<strong style="color:red;font-size:20px;">{{$product->discount->style}}{{$product->discount->value}}&#8364;</strong>
@endif
</span><br>
<?php if($product->discount->style=='%') $discPrice = $product->price - ($product->price * ($product->discount->value / 100)); else $discPrice = $product->price - $product->discount->value; ?>
	<span class="text-brand-brown" style="font-size: 20px;">&#8364; {{  number_format((float)$discPrice, 2, '.', '')  }} </span>
				    @endif
				</div>
				<div class="row">
					<!-- <label style="width: 80px;">Weight:</label>--> <span class="text-brand-brown" style="font-size: 20px;">{{ $product->weight }}gr</span>
				</div>
				<div class="row">
				&nbsp;
				</div>
				<div class="row">
					@if(Auth::check())
					<a class="btn btn-primary" href="/request/{{ $product->id }}">Request Product</a>
					@else
					<a class="btn btn-primary" href="/login">Request Product</a>
					@endif
				</div>

			</div>
		</div>
		
		<ul class="nav nav-tabs" style="margin-bottom: 20px;">
			<li class="active"><a data-toggle="tab" href="#details">DETAILS</a></li>
			<li><a data-toggle="tab" href="#dimensions">DIMENSIONS</a></li>
			<li class="disabled"><a data-toggle="tab" href="#">REVIEWS</a></li>
			<li><a data-toggle="tab" href="#stores">STORES</a></li>
			<li><a data-toggle="tab" href="#company">COMPANY</a></li>
		</ul>
		
		<div class="tab-content" style="margin-bottom: 80px;">
			<div id="details" class="tab-pane fade-in active">
				<div class="row" style="margin-bottom: 40px;">
					<h4>{{ $product->description }}</h4>
				</div>
				<div class="row">
					<?php if (!($product->vid_url == '')){ 
						$img = 'img/products/poster/' . $product->id . '.jpg';
					?>
					<div class="row embed-responsive embed-responsive-16by9" style="margin-bottom: 40px;">
					    
						<video id="product_video" class="embed-responsive-item" autoplay poster="{{ URL::asset($img) }}" controls muted>
							<source src="{{ URL::asset('img/products/'.$product->vid_url) }}" type="video/mp4">
						</video>
					</div><?php } ?>

				</div>
			</div>

			<div id="dimensions" class="tab-pane fade">

				<div class="row">
					<label style="width: 60px;"><strong>Weight:</strong></label>
					<span>{{ $product->weight }}gr</span>
				</div>
				<div class="row">
					<label style="width: 60px;"><strong>Width:</strong></label>
					<span>{{ $product->size_w }}cm</span>
				</div>
				<div class="row">
					<label style="width: 60px;"><strong>Height</strong>:</label>
					<span>{{ $product->size_h }}cm</span>
				</div>
				<div class="row">
					<label style="width: 60px;"><strong>Depth:</strong></label>
					<span>{{ $product->size_d }}cm</span>
				</div>

			</div>

			<div id="reviews" class="tab-pane fade">
				<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
</div>
			<div id="stores" class="tab-pane fade">
		@foreach($product->stores as $store)
	<?php $store_country_url = 'img/flags/flags_iso/32/' . getCountryCode($store->country) . '.png'; ?>
<div class="col-md-6">
				<div class="row">
					<div class="panel panel-default">
						<div class="panel-heading"><h3 class="panel-title">{{$store->name}} <img src="{{ url(asset($store_country_url)) }}"></h3></div>
						<div class="panel-body">
							<address>
								<p><strong>Address:</strong><p>
								<p> {{ $store->country }}, {{ $store->city }}</p>
								<p>{{ $store->address }}</p>
								<p>{{ $store->zip_code }}</p>
							</address>
<p><strong>Telephone:</strong> {{$store->phone}}</span></p>
<?php $link = 'https://maps.google.com/maps?&z=10&q='.$store->map_x.'+'.$store->map_y.'&ll='.$store->map_x.'+'.$store->map_y ?>
<p><a href="{{$link}}">Map</a></p>
						</div>
					</div>
				</div>
						
			</div>
		@endforeach

			</div>

			<div id="company" class="tab-pane fade">
				<div class="row">
					<h3 style="margin-top: 0px;">{{ $product->company->name }}</h3>
				</div>
				<div class="row">
					<div style="white-space:pre-wrap">{{ $product->company->about }}</div>
					<a href="{{ URL::asset('/company/view/'.$product->company->id) }}" class="btn btn-primary pull-right" style="width: 100px;">More</a>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
    $('[data-toggle="popover"]').popover();   
	
	var flag = false;
		var playing = true;
		$('#product_video').click(function(){
			if (flag) {
				if ( playing == true ) {
					$('#product_video').trigger('pause');
					playing = false;
				} else {
					$('#product_video').trigger('play');
					playing = true;
				}
			} else {
				$('#product_video').prop('muted', false);
				flag = true;
			}
		});
});
</script>




@endsection

