@extends('app')


@section('content')
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">

<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>


  <div class="container">

<div class="row-fluid">
    <div class="col-xs-12">

<h3>Add Stores to Product</h3><br>

 <table class="table table-striped">
          <thead>
            <tr>
                <th>Name</th>
                <th>Country</th>
                <th>City</th>
                <th>Options</th>
             </tr>
          </thead>
         <tbody>
         @foreach($stores as $st)
             <tr>
                <td>{{ $st->name }}</td>
                <td>{{ $st->country }}</td>
                <td>{{ $st->city }}</td>
                <td><a href="remove/{{$st->id}}" class="btn btn-danger">Remove</a>
</td>
             </tr>
         @endforeach
         </tbody>
      </table>

<br>
<hr>
        <table class="table table-striped" id="example">
          <thead>
            <tr>
                <th>Name</th>
                <th>Country</th>
                <th>City</th>
		<th>Options</th>
             </tr>
          </thead>
         <tbody>
         @foreach($product as $st)
             <tr>
                <td>{{ $st->name }}</td>
                <td>{{ $st->country }}</td>
		<td>{{ $st->city }}</td>
		<td><a href="add/{{$st->id}}" class="btn btn-info">Add</a>
</td>
             </tr>
         @endforeach
         </tbody>
    </table>

      </div>
    </div>

  </div>

<script>
$("#example").dataTable();
</script>



@endsection
