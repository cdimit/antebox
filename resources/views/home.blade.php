@extends('app')
<style>
	.jumbotron{
		position: relative;
		overflow: hidden;
		height: 600px;
	}
	.video-container {
		position: relative;
		color: #ffffff;
		z-index: 2;
	}

	.video-header {
		position: absolute;
		height: auto;
		width: auto;
		min-height: 100%;
		min-width: 100%;
		left: 50%;
		top: 50%;
		-webkit-transform: translate3d(-50%, -50%, 0);
		transform: translate3d(-50%, -50%, 0);
		z-index: 1;
	}

	.card-container {
		display: table;
		background-position: center;
		margin-top: 24px;
		height: 350px;
		width: 100%;
		background-size: cover;
	}
	.text-container {
		display: table-cell;
		vertical-align: middle;
		color: white;
		text-align: center;
		top: 50%;
	}
</style>
@section('content')

<?php $category = $products->pluck('category')->unique()->sort(); ?>

<section style="background-color: #163850; margin-top:-20px;">

	<div class="jumbotron">
		<video id="home_video" muted loop autoplay preload="none" class="video-header" poster="{{ URL::asset('img/posters/home.jpg') }}">
			<source src="{{ URL::asset('vid/comp.mp4') }}" type="video/mp4">
		</video>
		<div class="video-container">
			<div class="container">

				<div class="row text-center" style="padding-bottom: 20px;">
					<h1 class="" style="color: #fff">The global market comes to your neighbourhood</h1>
					<span style="color: #fff; font-size: 25px;">Every business has its own story and we are here to share it<span>
				</div>
				<!--
				<div class="row text-center">
					<img id="play-btn" src="{{ URL::asset('img/app/play-btn.png') }}" />
				</div>
				-->
				<div class="row" style="position: relative; top: 220px;">
					<div class="col-xs-7">
						<input type="text" class="form-control" name="name" id="search" value=""  placeholder="Search for products..."/>
					</div>
					<div class="col-xs-3">
						<select id="category" class="form-control">
							<option value="">All Categories</option>
							<option class="select-dash" disabled="disabled">--------------</option>
		                                        @foreach($category as $c)
                                                        <option value="{{$c}}">{{$c}}</option>
		                                        @endforeach
						</select>
					</div>
					<div class="col-xs-2">
						<a class="btn btn-info" class="form-control" id="btnSearch" href="/products/^name^">Search</a>
					</div>
				</div>


				<!--
				@if (Auth::guest())
				<div class="row text-center" style="padding-bottom: 20px;">
					<div class="col-md-2 col-md-offset-4" style="padding-top: 20px;">
						<a href="/login" class="btn btn-how" style="width: 140px;">LOGIN</a>
					</div>
					<div class="col-md-2" style="padding-top: 20px;">
						<a href="/register" class="btn btn-how" style="width: 140px;">REGISTER</a>
					</div>
				</div>
				@endif
				-->
			</div>
		</div>
	</div>

</section>


<section style="background-color: #d5dbe1; padding-bottom: 50px; padding-top: 0px; margin-top: -30px;">
	<div class="container">
		<div class="row" style="margin-top: 40px;">
			<div class="col-lg-8 col-md-12">
				<a href="/products/%5Ecountry%5ECyprus" style="text-decoration: none;">
					<div class="card-container" style="background-image: url(' {{ URL::asset('img/posters/1.png') }} ');">
						<div class="text-container">
							<h2>Experience Cyprus</h2>
						</div>
					</div>
				</a>
			</div>

			<div class="col-lg-4 col-md-6">
				<a href="/trips" style="text-decoration: none;">
					<div class="card-container" style="background-image: url('{{ URL::asset('img/posters/trip.jpg') }} ');">
						<div class="text-container">
							<h2>Are you travelling?</h2>
						</div>
					</div>
				</a>
			</div>

			<div class="col-lg-4 col-md-6">
				<a href="/product/view/11" style="text-decoration: none;">
					<div class="card-container" style="background-image: url(' {{ URL::asset('img/products/11.jpg') }} ');">
						<div class="text-container">
							<h2>Breaded Baby Mozzarella</h2>
							<h4 style="position: absolute; top: 20px; left: 20px;"><img src="{{ url::asset('img/flags/flags_iso/32/it.png') }}"> Italy</h4>
							<div style="position: absolute; bottom: 20px; width: 80px; height: 50px; background: grey;">
								<h4>€ 14.63</h4>
								<h5 style="color: red; margin-top: -8px;"><del>€ 19.50</del></h5>
							</div>
							<div style="position: absolute; top: 32px; right: 15px; width: 60px; height: 50px; background: red;">
								<h4><strong>25% OFF</strong></h4>
							</div>
						</div>
					</div>
				</a>
			</div>

			<div class="col-lg-4 col-md-6">
				<a href="/product/view/12" style="text-decoration: none;">
					<div class="card-container" style="background-image: url(' {{ URL::asset('img/products/12.jpg') }} ');">
						<div class="text-container">
							<h2>Halloumi Cheese</h2>
							<h4 style="position: absolute; top: 20px; left: 20px;"><img src="{{ url::asset('img/flags/flags_iso/32/cy.png') }}"> Cyprus</h4>
							<div style="position: absolute; bottom: 20px; width: 80px; height: 40px; background: grey;">
								<h4>€ 12.50</h4>
							</div>
						</div>
					</div>
				</a>
			</div>

			<div class="col-lg-4 col-md-6">
				<a href="/product/view/1" style="text-decoration: none;">
					<div class="card-container" style="background-image: url(' {{ URL::asset('img/products/1.jpg') }} ');">
						<div class="text-container">
							<h2>Sousoukos</h2>
							<h4 style="position: absolute; top: 20px; left: 20px;"><img src="{{ url::asset('img/flags/flags_iso/32/cy.png') }}"> Cyprus</h4>
							<div style="position: absolute; bottom: 20px; width: 80px; height: 40px; background: grey;">
								<h4>€ 7.00</h4>
							</div>
						</div>
					</div>
				</a>
			</div>

			<div class="col-lg-4 col-md-6">
				<a href="/products/%5Ecountry%5EItaly" style="text-decoration: none;">
					<div class="card-container" style="background-image: url('{{ URL::asset('img/posters/italy.jpg') }} ');">
						<div class="text-container">
							<h2>Taste Italy</h2>
						</div>
					</div>
				</a>
			</div>

			<div class="col-lg-8 col-md-6">
				<a href="/requests" style="text-decoration: none;">
					<div class="card-container" style="background-image: url('{{ URL::asset('img/posters/delivery.jpg') }} ');">
						<div class="text-container">
							<h2>Deliver something now</h2>
							<h4>View current requests</h4>
						</div>
					</div>
				</a>
			</div>
		</div>
	</div>

<!--
	<?php
	function getCountryCode($tmp) {
		$country_arr = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Angola", "Anguilla", "Antartica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Ashmore and Cartier Island", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burma", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Clipperton Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo, Democratic Republic of the", "Congo, Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Europa Island", "Falkland Islands (Islas Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "French Guiana", "French Polynesia", "French Southern and Antarctic Lands", "Gabon", "Gambia", "Gaza Strip", "Georgia", "Germany", "Ghana", "Gibraltar", "Glorioso Islands", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard Island and McDonald Islands", "Holy See (Vatican City)", "Honduras", "Hong Kong", "Howland Island", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Ireland, Northern", "Israel", "Italy", "Jamaica", "Jan Mayen", "Japan", "Jarvis Island", "Jersey", "Johnston Atoll", "Jordan", "Juan de Nova Island", "Kazakhstan", "Kenya", "Kiribati", "Korea, North", "Korea, South", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Man, Isle of", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Midway Islands", "Moldova", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn Islands", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russia", "Rwanda", "Saint Helena", "Saint Kitts and Nevis", "Saint Lucia", "Saint Pierre and Miquelon", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Scotland", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and South Sandwich Islands", "Spain", "Sri Lanka", "Sudan", "Suriname", "Svalbard", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Tobago", "Toga", "Tokelau", "Tonga", "Trinidad", "Tunisia", "Turkey", "Turkmenistan", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "Uruguay", "USA", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands", "Wales", "Wallis and Futuna", "West Bank", "Western Sahara", "Yemen", "Zambia", "Zimbabwe");
		$countryVal_arr = array("af","al","dz","as","ao","ai","aq","ag","ar","am","aw","au","au","at","az","bs","bh","bd","bb","by","be","bz","bj","bm","bt","bo","ba","bw","br","vg","bn","bg","bf","mm","bi","kh","cm","ca","cv","ky","cf","td","cl","cn","cx","fr","cc","co","km","cd","cg","ck","cr","ci","hr","cu","cy","cz","dk","dj","dm","do","ec","eg","sv","gq","er","ee","et","tf","fk","fo","fj","fi","fr","gf","pf","tf","ga","gm","ps","ge","de","gh","gi","tf","gr","gl","gd","gp","gu","gt","gg","gn","gw","gy","ht","hm","va","hn","hk","um","hu","is","in","id","ir","iq","ie","_Northern Ireland","il","it","jm","sj","jp","um","je","um","jo","tf","kz","ke","ki","kp","kr","kw","kg","la","lv","lb","ls","lr","ly","li","lt","lu","mo","mk","mg","mw","my","mv","ml","mt","im","mh","mq","mr","mu","yt","mx","fm","um","md","mc","mn","ms","ma","mz","na","nr","np","nl","an","nc","nz","ni","ne","ng","nu","nf","mp","no","om","pk","pw","pa","pg","py","pe","ph","pn","pl","pt","pr","qa","fr","ro","ru","rw","sh","kn","lc","pm","vc","ws","sm","st","sa","_Scotland","sn","rs","sc","sl","sg","sk","si","sb","so","za","gs","es","lk","sd","sr","sj","sz","se","ch","sy","tw","tj","tz","th","tt","tg","tk","to","tt","tn","tr","tm","tv","ug","ua","ae","gb","uy","us","uz","vu","ve","vn","vi","_Wales","wf","ps","eh","ye","zm","zw");

		$key = array_search($tmp, $country_arr);
		return $countryVal_arr[$key];
	}
	?>

	<div class="row">
		<div class="text-center">
			<h3>Products</h3>
		</div>
	</div>

	<div class="row text-center">
		<div class="auctions-carousel">
			@foreach($products as $prod)
				<div class="slick-slide" style="margin: 10px; height: auto;">
					<div class="thumbnail" style="margin:0; width: auto; height: auto;">
						<img style="height: 200px; width: auto;" src="{{ asset('/img/products/'.$prod->pic_url)}}" alt="{{$prod->name}}" title="{{$prod->name}}" >
						<div class="caption" style="margin: 0">
							<p>

					<?php $country_url = 'img/flags/flags_iso/32/' . getCountryCode($prod->company->country) . '.png'; ?>
								<img style="display: inline;" alt"Cyprus" title="Cyprus" src="{{ asset($country_url) }}">
								{{$prod->name}}
							</p>
							<p style="margin:0;padding:0;">
								<a href="{{ asset('/product/view/'.$prod->id)}}" class="btn btn-info" style="width: 100px;" role="button">More</a>
								<a href="{{ asset('/request/'.$prod->id)}}" class="btn btn-primary" style="width: 100px;" role="button">Request</a>
							</p>
						</div>
					</div>
				</div>
			@endforeach

		</div>
	</div>

-->
</section>

<section style="background-color: #efefef;margin-bottom: -200px;">
	<div class="container">
		<div class="row text-center" style="padding-top: 40px; padding-bottom: 40px;">
			<div class="col-md-3">
				<img src="{{ asset('img/achievements/cityspark.png') }}" style="width: auto; height: 40px;">
			</div>
			<div class="col-md-3">
				<img src="{{ asset('img/achievements/citystarters.png') }}" style="width: auto; height: 40px;">
			</div>
			<div class="col-md-3">
				<img src="{{ asset('img/achievements/madeatcity.png') }}" style="width: auto; height: 40px;">
			</div>
			<div class="col-md-3">
				<img src="{{ asset('img/achievements/thehangout.png') }}" style="width: auto; height: 40px;">
			</div>
		</div>
	</div>
</section>


<script type="text/javascript">
	$('document').ready(function(){
		// $('#become-a-courier').slideDown(1500);
		// $('#closeCourierBanner').click(function(){
		// 	$('#become-a-courier').slideUp(1000);
		// });

		$('#play-btn').show();

		var autoplay = false;

		/*
		$('#home_video').on('play', function() {
            autoplay = true;
			$('#play-btn').hide();
        });
		*/
		var autoplay = $('#home_video').paused;
		$('.jumbotron').click(function(){
			if ( autoplay == true ) {
				$('#home_video').trigger('pause');
				autoplay = false;
//				$('#play-btn').toggle();
			} else {
				$('#home_video').trigger('play');
				autoplay = true;
//				$('#play-btn').toggle();
			}
		});


		/*
		$('.auctions-carousel').slick({
			autoplay: false,
			arrows:false,
			autoplaySpeed: 1500,
			dots: true,
		  infinite: false,
		  speed: 300,
		  slidesToShow: 4,
		  slidesToScroll: 1,
		  responsive: [
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 3,
		        slidesToScroll: 1,
					 	autoplay: false,
		        infinite: true,
		        dots: true
		      }
		    },
		    {
		      breakpoint: 768,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 1
		      }
		    },
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }
		    // You can unslick at a given breakpoint now by adding:
		    // settings: "unslick"
		    // instead of a settings object
		  ]
		});

		*/

                $('#btnSearch').click(function (){
                        $('#btnSearch').attr('href', $('#btnSearch').attr('href') + $('#search').val() + '^category^' + $('#category').val());
                });
	});


</script>
@endsection
