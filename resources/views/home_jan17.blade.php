@include('partials.header')

<div class="container oobe">
	<a href="/howitworks">
	<div class="row">
			
		
		<div class="col-xs-12 col-md-11">
			<h1 >
				AnteBox is not a normal e-commerce platform...
			</h1>	
			<h2>
				Find out how it works
			</h2>
		</div>
		<div class="col-xs-12 col-md-1">
			<h1 style="font-size: 5.5em; margin-bottom: 0;">
				<i class="fa fa-arrow-right"></i>

			</h1>
		</div>
	</div>
	</a>
</div>

<!-- Slider AREA -->
<div class="slider-area">
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-sm-9">
				<!-- Main Slider -->
				<div class="main-slider">
					<div class="slider">
						<div id="mainSlider-2" class="nivoSlider slider-image">
							<img src="/img/frontpage_jan17/slider/main-slider-1.jpg" alt="main slider" title="#htmlcaption1"/>
							<img src="/img/frontpage_jan17/slider/main-slider-2.jpg" alt="main slider" title="#htmlcaption2"/>
							<img src="/img/frontpage_jan17/slider/main-slider-3.jpg" alt="main slider" title="#htmlcaption3"/>
						</div>
						
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-3">
				<div class="right-banner">
					<div class="right-banner-img single-add">
						<a href="#"><img src="/img/frontpage_jan17/slider/terre-di-carmen.jpg" alt="slider"></a>
					</div>
					<div class="right-banner-img single-add">
						<a href="#"><img src="/img/frontpage_jan17/slider/slider-2.jpg" alt="slider"></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- offer AREA -->
<div class="offer-area">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-4 col-xs-12">
				<div class="single-offer">
					<div class="sigle-offer-icon">
						<p><i class="fa fa-child"></i></p>
					</div>
					<div class="sigle-offer-content">
						<h2>Multi-delivery</h2>
						<p>Select the bid you prefer</p>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-12">
				<div class="single-offer">
					<div class="sigle-offer-icon">
						<p><i class="fa fa-gift"></i></p>
					</div>
					<div class="sigle-offer-content">
						<h2>Unique products</h2>
						<p>Difficult to find online</p>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-12">
				<div class="single-offer">
					<div class="sigle-offer-icon">
						<p><i class="fa fa-plane"></i></p>
					</div>
					<div class="sigle-offer-content">
						<h2>International shipping</h2>
						<p>Covering all the world</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Featured Product AREA -->
<div class="featured-product-area">
	<div class="container">
		<div class="feature-product-header">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#feature-product" data-toggle="tab">Featured</a></li>
				<li><a href="#best-sellers" data-toggle="tab">Best Sellers</a></li>
				<li><a href="#specials" data-toggle="tab">Specials</a></li>
			</ul>
		</div>
		<div class="feature-product-body">
			<div class="tab-content">
				<div class="tab-pane active" id="feature-product">
					<div class="row">
						<div id="owl-feature" class="owl-carousel">
							@foreach($featured as $product)
							<div class="col-md-3 product-col">
								<div class="single-featured-product">
									<div class="fiture-product-img">
										<a href="products/{{$product->id}}">
											<img class="primary-img" src="{{ URL::asset('img/products/'.$product->pic_url) }}" alt="product">
											<img class="secondary-img" src="{{ URL::asset('img/products/'.$product->pic_url) }}" alt="product">
										</a>
									</div>
									<div class="fiture-product-content">
										<a href="products/{{$product->id}}"><h2> {{$product->name}} </h2></a>
										@if(isset($product->discount))
										<?php 
											$value = $product->discount->value;
											$style = $product->discount->style;

											if ($style == "%") {
												$price = round((1 - ($value/100)) * $product->price, 2); 
											}
											
										?>
										<h3>
										€ <strike class="price-strike">{{$product->price}}</strike> &nbsp; {{$price}} &nbsp;
										</h3>
										<span class="discount">
										&nbsp;
										{{$product->discount->value}} {{$product->discount->style}} OFF
										&nbsp;
										</span>
										@else
										<h3>
										€ {{$product->price}}
										</h3>
										@endif
									</div>
									<div class="new-sell">
										<p class="new">Featured</p>
									</div>
									<div class="add-to-chart">
										<ul class="list-inline">
											<li><a href="products/{{$product->id}}" class="add-chart">Request product</a></li>
										</ul>
									</div>
								</div>
							</div>
							@endforeach
							
						</div>
					</div>
				</div>
				<div class="tab-pane" id="best-sellers">
					<div class="row">
						<div id="owl-spacial" class="owl-carousel">
							@foreach($mostvisited as $product)
							<div class="col-md-3 product-col">
								<div class="single-featured-product">
									<div class="fiture-product-img">
										<a href="products/{{$product->id}}">
											<img class="primary-img" src="{{ URL::asset('img/products/'.$product->pic_url) }}" alt="product">
											<img class="secondary-img" src="{{ URL::asset('img/products/'.$product->pic_url) }}" alt="product">
										</a>
									</div>
									<div class="fiture-product-content">
										<a href="products/{{$product->id}}"><h2> {{$product->name}} </h2></a>
										@if(isset($product->discount))
										<?php 
											$value = $product->discount->value;
											$style = $product->discount->style;

											if ($style == "%") {
												$price = round((1 - ($value/100)) * $product->price, 2); 
											}
											
										?>
										<h3>
										€ <strike class="price-strike">{{$product->price}}</strike> &nbsp; {{$price}} &nbsp;
										</h3>
										<span class="discount">
										&nbsp;
										{{$product->discount->value}} {{$product->discount->style}} OFF
										&nbsp;
										</span>
										@else
										<h3>
										€ {{$product->price}}
										</h3>
										@endif
									</div>
									<div class="new-sell">
										<p class="new">Best seller</p>
									</div>
									<div class="add-to-chart">
										<ul class="list-inline">
											<li><a href="products/{{$product->id}}" class="add-chart">Request product</a></li>
										</ul>
									</div>
								</div>
							</div>
							@endforeach
						</div>
					</div>
				</div>
				<div class="tab-pane" id="specials">
					<div class="row">
						<div id="owl-best-sell" class="owl-carousel">
							@foreach($specials as $product)
							<div class="col-md-3 product-col">
								<div class="single-featured-product">
									<div class="fiture-product-img">
										<a href="products/{{$product->id}}">
											<img class="primary-img" src="{{ URL::asset('img/products/'.$product->pic_url) }}" alt="product">
											<img class="secondary-img" src="{{ URL::asset('img/products/'.$product->pic_url) }}" alt="product">
										</a>
									</div>
									<div class="fiture-product-content">
										<a href="products/{{$product->id}}"><h2> {{$product->name}} </h2></a>
										@if(isset($product->discount))
										<?php 
											$value = $product->discount->value;
											$style = $product->discount->style;

											if ($style == "%") {
												$price = round((1 - ($value/100)) * $product->price, 2); 
											}
											
										?>
										<h3>
										€ <strike class="price-strike">{{$product->price}}</strike> &nbsp; {{$price}} &nbsp;
										</h3>
										<span class="discount">
										&nbsp;
										{{$product->discount->value}} {{$product->discount->style}} OFF
										&nbsp;
										</span>
										@else
										<h3>
										€ {{$product->price}}
										</h3>
										@endif
									</div>
									<div class="new-sell">
										<p class="new">Specials</p>
									</div>
									<div class="add-to-chart">
										<ul class="list-inline">
											<li><a href="products/{{$product->id}}" class="add-chart">Request product</a></li>
										</ul>
									</div>
								</div>
							</div>
							@endforeach
						</div>
					</div>
				</div>	
			</div>
		</div>
	</div>
</div>

<!-- Add Banner AREA -->
<div class="add-banner-area">
	<div class="container">
		<div class="add-banner">
			<div class="row">
				<div class="col-md-8 col-sm-8">
					<div class="add-banner-left">
						<div class="row">
							<div class="col-md-12 col-sm-12">
								<div class="row">
									<div class="col-md-6 col-sm-6">
										<div class="add-banner-img single-add">
											<a href="#"><img src="/img/frontpage_jan17/banner/banner-1.jpg" alt="add"></a>
										</div>
									</div>
									<div class="col-md-6 col-sm-6">
										<div class="add-banner-img single-add">
											<a href="#"><img src="/img/frontpage_jan17/banner/banner-2.jpg" alt="add"></a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 col-sm-12 add-banner-bottom">
								<div class="add-banner-img single-add">
									<a href="#"><img src="/img/frontpage_jan17/banner/banner-wide.jpg" alt="add"></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-4">
					<div class="add-banner-right">
						<div class="add-banner-img single-add">
							<a href="#"><img src="/img/frontpage_jan17/banner/banner-tall.jpg" alt="add"></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- Single Producnt Slider AREA -->
<div class="single-product-slider">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-4">
				<div class="single-product-heading">
					<h2><span> {{$catg1_name}} </span></h2>
				</div>
				<div id="bag-men-carousel">
					<div class="row">
						@foreach($catg1 as $product)
						<div class="col-md-12 col-sm-12">
							<div class="row frontpage-single">
								<div class="col-md-4 col-xs-3">
									<a href="products/{{$product->id}}">
										<img class="frontpage-img" src="{{ URL::asset('img/products/'.$product->pic_url) }}" alt="item">
									</a>
								</div>
								<div class="col-md-8 col-xs-9">
									<a href="products/{{$product->id}}"><h2> {{$product->name}} </h2></a>
									@if(isset($product->discount))
									<?php 
										$value = $product->discount->value;
										$style = $product->discount->style;

										if ($style == "%") {
											$price = round((1 - ($value/100)) * $product->price, 2); 
										}
										
									?>
									€ <strike class="price-strike">{{$product->price}}</strike> &nbsp; {{$price}} &nbsp;
									<span class="discount">
									&nbsp;
									{{$product->discount->value}} {{$product->discount->style}} OFF
									&nbsp;
									</span>
									@else
									<h3>
									€ {{$product->price}}
										
									</h3>
									@endif
									
								</div>
							</div>
							
						</div>
						@endforeach
					</div>
					
				</div>
			</div>
			<div class="col-md-4 col-sm-4">
				<div class="single-product-heading">
					<h2><span> {{$catg2_name}} </span></h2>
				</div>
				<div id="bag-men-carousel">
					<div class="row">
						@foreach($catg2 as $product)
						<div class="col-md-12 col-sm-12">
							<div class="row frontpage-single">
								<div class="col-md-4 col-xs-3">
									<a href="products/{{$product->id}}">
										<img class="frontpage-img" src="{{ URL::asset('img/products/'.$product->pic_url) }}" alt="item">
									</a>
								</div>
								<div class="col-md-8 col-xs-9">
									<a href="products/{{$product->id}}"><h2> {{$product->name}} </h2></a>
									@if(isset($product->discount))
									<?php 
										$value = $product->discount->value;
										$style = $product->discount->style;

										if ($style == "%") {
											$price = round((1 - ($value/100)) * $product->price, 2); 
										}
										
									?>
									€ <strike class="price-strike">{{$product->price}}</strike> &nbsp; {{$price}} &nbsp;
									<span class="discount">
									&nbsp;
									{{$product->discount->value}} {{$product->discount->style}} OFF
									&nbsp;
									</span>
									@else
									<h3>
									€ {{$product->price}}
										
									</h3>
									@endif
									
								</div>
							</div>
							
						</div>
						@endforeach
					</div>
					
				</div>
			</div>
			<div class="col-md-4 col-sm-4">
				<div class="single-product-heading">
					<h2><span> {{$catg3_name}} </span></h2>
				</div>
				<div id="bag-men-carousel">
					<div class="row">
						@foreach($catg3 as $product)
						<div class="col-md-12 col-sm-12">
							<div class="row frontpage-single">
								<div class="col-md-4 col-xs-3">
									<a href="products/{{$product->id}}">
										<img class="frontpage-img" src="{{ URL::asset('img/products/'.$product->pic_url) }}" alt="item">
									</a>
								</div>
								<div class="col-md-8 col-xs-9">
									<a href="products/{{$product->id}}"><h2> {{$product->name}} </h2></a>
									@if(isset($product->discount))
									<?php 
										$value = $product->discount->value;
										$style = $product->discount->style;

										if ($style == "%") {
											$price = round((1 - ($value/100)) * $product->price, 2); 
										}
										
									?>
									€ <strike class="price-strike">{{$product->price}}</strike> &nbsp; {{$price}} &nbsp;
									<span class="discount">
									&nbsp;
									{{$product->discount->value}} {{$product->discount->style}} OFF
									&nbsp;
									</span>
									@else
									<h3>
									€ {{$product->price}}
										
									</h3>
									@endif
									
								</div>
							</div>
							
						</div>
						@endforeach
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Blog AREA -->
<div class="blog-area">
	<div class="container">
		<div class="blog-heading">
			<h2><span></span></h2>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="single-blog">
					<div class="blog-img single-add">
						<a href="#"><img src="/img/frontpage_jan17/about/suggest.jpg" alt="blog"></a>
					</div>
					<div class="blog-content">
						<h2><a href="#">Suggest a product</a></h2>
						<p>
							Couldn't find what you need on AnteBox? Suggest your product to us, and we'll work to get it for you on our platform!
						</p>
						<a class="btn btn-primary" href="/suggest/product">Suggest a product</a>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="single-blog">
					<div class="blog-img single-add">
						<a href="#"><img src="/img/frontpage_jan17/about/apply.jpg" alt="blog"></a>
					</div>
					<div class="blog-content">
						<h2><a href="#">Apply to join</a></h2>
						<p>
							Interested in working with us? Apply to join to become a partner with Antebox!
						</p>
						<a class="btn btn-primary" href="/business">Learn more</a>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="single-blog">
					<div class="blog-img single-add">
						<a href="#"><img src="/img/frontpage_jan17/about/howitworks.jpg" alt="blog"></a>
					</div>
					<div class="blog-content">
						<h2><a href="#">How does Antebox work?</a></h2>
						<p>
							Request the product you want instead of buying it immediately, enabling you to receive multiple delivery bids, even by travellers just like you.
						</p>
						<a class="btn btn-primary" href="/howitworks">Learn about how it works</a>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</div>

<!-- Claint AREA -->
<div class="claint-area">
	<div class="container">
	<h2>As seen at</h2>
		<div class="row">
			<div id="owl-claint" class="owl-carousel">
			<div class="col-md-2">
				<div class="claint-img">
					<a href="#"><img src="img/frontpage_jan17/logos/bdl-logo.png" alt="claint"></a>
				</div>
			</div>
			<div class="col-md-2">
				<div class="claint-img">
					<a href="#"><img src="img/frontpage_jan17/logos/ALPHA Badge.png" alt="claint"></a>
				</div>
			</div>
			<div class="col-md-2">
				<div class="claint-img">
					<a href="#"><img src="img/frontpage_jan17/logos/cyec_web.png" alt="claint"></a>
				</div>
			</div>
			<div class="col-md-2">
				<div class="claint-img">
					<a href="#"><img src="/img/achievements/cityspark.png" alt="claint"></a>
				</div>
			</div>
			<div class="col-md-2">
				<div class="claint-img">
					<a href="#"><img src="/img/achievements/citystarters.png" alt="claint"></a>
				</div>
			</div>
			<div class="col-md-2">
				<div class="claint-img">
					<a href="#"><img src="/img/achievements/madeatcity.png" alt="claint"></a>
				</div>
			</div>
			<div class="col-md-2">
				<div class="claint-img">
					<a href="#"><img src="/img/achievements/thehangout.png" alt="claint"></a>
				</div>
			</div>
			<div class="col-md-2">
				<div class="claint-img">
					<a href="#"><img src="/img/frontpage_jan17/logos/slush-logo.png" alt="claint"></a>
				</div>
			</div>
			<div class="col-md-2">
				<div class="claint-img">
					<a href="#"><img src="/img/frontpage_jan17/logos/startups4peace.jpg" alt="claint"></a>
				</div>
			</div>
			</div>

		</div>
	</div>
</div>

@include('partials.footer')