@include('partials.header')

<!-- Breadcurb AREA -->
<div class="breadcurb-area">
	<div class="container">
		<ul class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li><a href="/products">All products</a></li>
		</ul>
	</div>
</div>

<!-- Product Item AREA -->
<div class="product-item-area">
	<div class="container">
		<div class="row">

			<div class="col-md-9 col-sm-8">
				<div class="product-item-list">
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="product-item-heading">
								<div class="item-heading-title">
									<h2>Products</h2>
								</div>
								<div class="result-short-view">
									<div class="result-short">
										<p>Showing {{sizeof($products)}} results </p>
										
									</div>
									
								</div>
							</div>
						</div>
					</div>
					
					<div class="row">
						@for($i = 0; $i < sizeof($products); $i++)
						{{-- Product i --}}
						<div class="col-md-4 col-sm-6">
							<div class="single-item-area">
								<div class="single-item">
									<div class="product-item-img">
										<a href="products/{{$products[$i]->id}}">
											<img class="primary-img" src="{{ URL::asset('img/products/'.$products[$i]->pic_url) }}" alt="item">
										</a>
										<div class="product-item-action">
											<a href="products/{{$products[$i]->id}}"><i class="fa fa-external-link"></i></a>
										</div>
									</div>
									<div class="single-item-content">
										<h2><a href="products/{{$products[$i]->id}}">
											{{$products[$i]->name}}
										</a></h2>
										
										<h3>€ {{$products[$i]->price}}</h3>
									</div>
								</div>
								<div class="item-action-button fix">
									@if(Auth::check())
									<a href="/request/{{ $products[$i]->id }}">Request Product</a>
									@else
									<a href="/login">Request Product</a>
									@endif
								</div>
							</div>
						</div>
						@endfor
					</div>
			
				</div>	
			</div>

			{{-- Include sidebar for filtering products --}}
			@include('partials.sidebar_filter')
		</div>
	</div>
</div>


@include('partials.footer')
<script type="text/javascript">
	/*---------------------
	 Price Filter
	--------------------- */
	$( "#slider-range" ).slider({
	  range: true,
	  min: {{ $min_price}},
	  max: {{ $max_price}},
	  values: [ {{ $min_price}}, {{ $max_price}} ],
	  slide: function( event, ui ) {
		$( "#amount" ).val( "€" + ui.values[ 0 ] + " - €" + ui.values[ 1 ] );
		$("#min_price").val($("#slider-range").slider("values", 0));
		$("#max_price").val($("#slider-range").slider("values", 1));
	  }
	});

	$( "#amount" ).val( "€" + $( "#slider-range" ).slider( "values", 0 ) +
	  " - €" + $( "#slider-range" ).slider( "values", 1 ) );   
	$("#min_price").val($("#slider-range").slider("values", 0));
	$("#max_price").val($("#slider-range").slider("values", 1));
</script>
