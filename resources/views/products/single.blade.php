@include('partials.header')

<!-- Breadcumbs -->
<div class="breadcurb-area">
	<div class="container">
		<ul class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li><a href="/products">All products</a></li>
		</ul>
	</div>
</div>



<!-- Product Item AREA -->
<div class="product-item-area">
	<div class="container">
		<div class="row">

			<div class="col-md-12 col-sm-12">
				<div class="row">
					<div class="col-md-5 col-sm-5">
						<div class="product-item-tab">
							<!-- Product images - large -->
							<div class="single-tab-content">
								<div class="tab-content">
									<div role="tabpanel" class="tab-pane active" id="img-one"><img src="{{ URL::asset('img/products/'.$product->pic_url) }}" alt="tab-img"></div>
									
								</div>
							</div>
							<!-- Nav tabs -->
							<div class="single-tab-img">
								
							</div>
						</div>
					</div>
					<div class="col-md-7 col-sm-7">
						<div class="product-tab-content">
							<div class="product-tab-header">
								<h1>{{$product->name}}</h1>
								<h3>
									<?php $product->discount ?>
									@if(isset($product->discount))
									<?php 
										$value = $product->discount->value;
										$style = $product->discount->style;

										if ($style == "%") {
											$price = round((1 - ($value/100)) * $product->price, 2); 
										}
										
									?>
									€ <strike class="price-strike">{{$product->price}}</strike> &nbsp; {{$price}} &nbsp;
									<span class="discount">
									&nbsp;
									{{$product->discount->value}} {{$product->discount->style}} OFF
									&nbsp;
									</span>
									@else
									€ {{$product->price}}
									@endif
								</h3>
								<h4>{{ $product->weight }} g</h4>
								<h4> {{$product->company->country}} </h4>
								@if(Auth::check())
								<a class="btn btn-primary" href="/request/{{ $product->id }}">Request Product</a>
								@else
								<a class="btn btn-primary" href="/login">Request Product</a>
								@endif
							</div>
							
							<div class="product-item-details">
								<p> {{$product->description}} </p>
							</div>
							
							
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="description-tab">
						<!-- Nav tabs -->
							<ul class="nav nav-tabs" role="tablist">
								<li role="presentation" class="active"><a href="#description" role="tab" data-toggle="tab">Description</a></li>
								<li role="presentation"><a href="#information" role="tab" data-toggle="tab">Product details</a></li>
								<li role="presentation"><a href="#stores" role="tab" data-toggle="tab">Stores</a></li>
								<li role="presentation"><a href="#company" role="tab" data-toggle="tab">About the company</a></li>

							</ul>

							  <!-- Tab panes -->
							<div class="tab-content">
								<div role="tabpanel" class="tab-pane active" id="description">
									<p>
										{{ $product->description }}
										@if ($product->vid_url)
											<?php $img = 'img/products/poster/' . $product->id . '.jpg'; ?>
											<video class="product-video"  poster="{{ URL::asset($img) }}" controls muted>
												<source src="{{ URL::asset('img/products/'.$product->vid_url) }}" type="video/mp4">
											</video>
										
										@endif
									</p>
								</div>
								<div role="tabpanel" class="tab-pane" id="information">
									<h3>Product dimensions</h3>
									<li>
										Width: {{$product->size_w}} cm
									</li>
									<li>
										Height: {{$product->size_h}} cm
									</li>
									<li>
										Depth: {{$product->size_d}} cm
									</li>
									<li>
										Weight: {{$product->weight}} g
									</li>
								</div>
								<div role="tabpanel" class="tab-pane" id="stores">
									<div class="row">
										@foreach($product->stores as $store)
										<div class="col-md-6">
											<div class="store panel panel-default">
												<div class="store-heading panel-heading">
													<h3>
													{{$store->name }}
													</h3>
												</div>
												<div class="panel-body">
													<h4>Address:</h4>
													<p>
													{{ $store->address }}
													{{ $store->city }}
													{{ $store->country }}	
													</p>
													@if(isset($store->phone))
													<h4>Telephone:</h4>
													{{ $store->phone }}
													@endif
												</div>
											</div>
										</div>
										@endforeach
									</div>
								</div>
								<div role="tabpanel" class="tab-pane" id="company">
									<h2>{{$product->company->name}}</h2>
									<p>
										{{$product->company->about}}
									</p>
									<a class="btn btn-primary" href="/company/view/{{ $product->company->id }}">Learn more</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>


@include('partials.footer')
