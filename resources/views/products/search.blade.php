@include('partials.header')

<!-- Breadcumbs -->
<div class="breadcurb-area">
	<div class="container">
		<ul class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li><a href="/products">All products</a></li>
		</ul>
	</div>
</div>

<!-- Product Item AREA -->
<div class="product-item-area">
	<div class="container">
		<div class="row">
			{{-- Include sidebar for filtering products --}}
			{{-- @include('partials.sidebar_filter') --}}

			<div class="col-md-12 col-sm-12">
				<div class="product-item-list">
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="product-item-heading">
								<div class="item-heading-title">
									<h2>Products</h2>
								</div>
								<div class="result-short-view">
									<div class="result-short">
										<p>Showing {{sizeof($products)}} results </p>
										
									</div>
									
								</div>
							</div>
						</div>
					</div>
					<div class="row">

						@foreach($products as $product)
						<div class="col-md-3 col-sm-6 col-xs-12">
							<div class="single-item-area">
								<div class="single-item">
									<div class="product-item-img">
										<a href="/products/{{$product->id}}">
											<img class="primary-img" src="{{ URL::asset('img/products/'.$product->pic_url) }}" alt="item">
										</a>
										<div class="product-item-action">
											<a href="/products/{{$product->id}}"><i class="fa fa-external-link"></i></a>
										</div>
									</div>
									<div class="single-item-content">
										<h2><a href="/products/{{$product->id}}">
											{{$product->name}}
											{{$product->country}}
										</a></h2>
										
										<h3>
											@if(isset($product->discount))
											<?php 
												$value = $product->discount->value;
												$style = $product->discount->style;

												if ($style == "%") {
													$price = round((1 - ($value/100)) * $product->price, 2); 
												}
												
											?>
											€ <strike class="price-strike">{{$product->price}}</strike> &nbsp; {{$price}} &nbsp;
											<span class="discount">
											&nbsp;
											{{$product->discount->value}} {{$product->discount->style}} OFF
											&nbsp;
											</span>
											@else
											€ {{$product->price}}
											@endif
										</h3>
									</div>
								</div>
								<div class="item-action-button fix">
									@if(Auth::check())
									<a href="/request/{{ $product->id }}">Request Product</a>
									@else
									<a href="/login">Request Product</a>
									@endif
								</div>
							</div>
						</div>
						@endforeach
						
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>


@include('partials.footer')
