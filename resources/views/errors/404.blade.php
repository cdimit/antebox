@extends('app')

@section('content')
<body style="background-color: #163850">
	<div class="bg">
		  <div class="container content">
				<div class="row text-center" style="padding-top: 80px;">
					<div class="col-xs-8 col-xs-offset-2">
						<img style="max-width:100%;height:auto;" src="{{ URL::asset('img/app/logo.png') }}"/>
					</div>
				</div>
				<div class="row text-center">
					<div class="col-xs-12" style="padding-top:50px;color: #fff">
						<h2>We couldn't find the page you are looking for!</h2>
					</div>
				</div>
			</div>
	 </div>
 </div>
</body>
@endsection
