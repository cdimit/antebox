@extends('app')

@section('content')
  <style>
	.table > tbody > tr > td {
		 vertical-align: middle;
	}
  </style>

  <?php
	use App\User;
	
	function getCountryCode($tmp) {
		$country_arr = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Angola", "Anguilla", "Antartica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Ashmore and Cartier Island", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burma", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Clipperton Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo, Democratic Republic of the", "Congo, Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Europa Island", "Falkland Islands (Islas Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "French Guiana", "French Polynesia", "French Southern and Antarctic Lands", "Gabon", "Gambia", "Gaza Strip", "Georgia", "Germany", "Ghana", "Gibraltar", "Glorioso Islands", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard Island and McDonald Islands", "Holy See (Vatican City)", "Honduras", "Hong Kong", "Howland Island", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Ireland, Northern", "Israel", "Italy", "Jamaica", "Jan Mayen", "Japan", "Jarvis Island", "Jersey", "Johnston Atoll", "Jordan", "Juan de Nova Island", "Kazakhstan", "Kenya", "Kiribati", "Korea, North", "Korea, South", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Man, Isle of", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Midway Islands", "Moldova", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn Islands", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russia", "Rwanda", "Saint Helena", "Saint Kitts and Nevis", "Saint Lucia", "Saint Pierre and Miquelon", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Scotland", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and South Sandwich Islands", "Spain", "Sri Lanka", "Sudan", "Suriname", "Svalbard", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Tobago", "Toga", "Tokelau", "Tonga", "Trinidad", "Tunisia", "Turkey", "Turkmenistan", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "Uruguay", "USA", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands", "Wales", "Wallis and Futuna", "West Bank", "Western Sahara", "Yemen", "Zambia", "Zimbabwe");
		$countryVal_arr = array("af","al","dz","as","ao","ai","aq","ag","ar","am","aw","au","au","at","az","bs","bh","bd","bb","by","be","bz","bj","bm","bt","bo","ba","bw","br","vg","bn","bg","bf","mm","bi","kh","cm","ca","cv","ky","cf","td","cl","cn","cx","fr","cc","co","km","cd","cg","ck","cr","ci","hr","cu","cy","cz","dk","dj","dm","do","ec","eg","sv","gq","er","ee","et","tf","fk","fo","fj","fi","fr","gf","pf","tf","ga","gm","ps","ge","de","gh","gi","tf","gr","gl","gd","gp","gu","gt","gg","gn","gw","gy","ht","hm","va","hn","hk","um","hu","is","in","id","ir","iq","ie","_Northern Ireland","il","it","jm","sj","jp","um","je","um","jo","tf","kz","ke","ki","kp","kr","kw","kg","la","lv","lb","ls","lr","ly","li","lt","lu","mo","mk","mg","mw","my","mv","ml","mt","im","mh","mq","mr","mu","yt","mx","fm","um","md","mc","mn","ms","ma","mz","na","nr","np","nl","an","nc","nz","ni","ne","ng","nu","nf","mp","no","om","pk","pw","pa","pg","py","pe","ph","pn","pl","pt","pr","qa","fr","ro","ru","rw","sh","kn","lc","pm","vc","ws","sm","st","sa","_Scotland","sn","rs","sc","sl","sg","sk","si","sb","so","za","gs","es","lk","sd","sr","sj","sz","se","ch","sy","tw","tj","tz","th","tt","tg","tk","to","tt","tn","tr","tm","tv","ug","ua","ae","gb","uy","us","uz","vu","ve","vn","vi","_Wales","wf","ps","eh","ye","zm","zw");

	    $key = array_search($tmp, $country_arr);
		return $countryVal_arr[$key];
	}
  ?>
  
  <div class="container">
    <div class="row">
		<div class="pull-left">
			<h3>ALL TRIPS HERE</h3>
		</div>
		<div style="width: 150px; margin-top: 15px;" class="pull-right">
			<a href="trips/create" style="width: 150px" class="btn btn-primary">Add Trip</a>
		</div>
	</div>

    <div class="row">
        <table class="table">
	    <thead>
	        <tr>
		    <th></th>
		    <th>From</th>
		    <th>To</th>
		    <th>Date of Departure</th>
		    <th>Status</th>
		</tr>
	    </thead>
	    <tbody>
	        @foreach($trips as $trip)
		    <tr>
		        <td>
			    <?php $image = 'img/user_avatars/' . $trip->user->pic_url  ?>
			    <a href="/user/{{$trip->user->id}}" title="{{ $trip->user->first_name }} {{ $trip->user->last_name }}" data-toggle="popover" data-trigger="hover"><img src="{{ URL::asset($image) }}" width="50" height="50" /></a>
			</td>
			<td>
				<?php $from_country_url = 'img/flags/flags_iso/32/' . getCountryCode($trip->from_country) . '.png'; ?>
				<?php $to_country_url = 'img/flags/flags_iso/32/' . getCountryCode($trip->to_country) . '.png'; ?>
			    <img src="{{ url(asset($from_country_url)) }}">
			    {{ $trip->from_country }} {{ $trip->from_city }}
			</td>
			<td>
				<img src="{{ url(asset($to_country_url)) }}">
			    {{ $trip->to_country }} {{ $trip->to_city }}
			</td>
			<td>
				<?php  $trip_pieces = explode(" ","$trip->dep "); 
					echo $trip_pieces[0];
				?>
			</td>

	                @if($trip->status=="Open")
        	            <td class="text-success"><strong>{{ $trip->status }}</strong></td>
                	@elseif($trip->status=="Almost Full")
                	    <td class="text-warning"><strong>{{ $trip->status }}</strong></td>
                	@elseif($trip->status=="Full")
                	    <td class="text-info"><strong>{{ $trip->status }}</strong></td>
			@endif
			<td></td><!-- <a href="" class="btn btn-info">Request Delivery</a> -->
		    </tr>
		@endforeach

	    </tbody>
        </table>
      </div>
	  <div class="row text-center">{!! $trips->render() !!}</div>
   </div>

@endsection

