@extends('app')

@section('content')
  <div class="container">
    <div class="col-md-12" style="margin-bottom: 50px;">

<div class="row">
        <div class="col-md-12">
          <span class="text-brand-brown huge-title">Add a Trip!</span>
        </div>
      </div>
      <div class="row">
        <div class="col-md-8">
          <span style="font-size:16px;">Keep track of your trips, receive unique offers for the countries you are visiting and earn some extra cash by delivering a unique product!</span>
        </div>
      </div>

 	@if (count($errors) > 0)
	  <div class="alert alert-danger">
  		<strong>Whoops!</strong> There were some problems with your input.<br><br>
  		<ul>
  			@foreach ($errors->all() as $error)
  				<li>{{ $error }}</li>
  			@endforeach
  		</ul>
  	  </div>
    	@endif

    </div>

	<form action="/trips/createTrip" method="get">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		<div class="row" style="margin-bottom: 10px;">

			<div class="col-sm-4 col-sm-offset-1" style="margin-bottom: 0px;">

				<div class="row text-center">
					<div class="form-group">
						<h3>From</h3>
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						<!-- onchange="print_state('from_city',this.selectedIndex);"  -->
						<select class="form-control" id="from_country" name="from_country">
						</select>
					</div>
				</div>
				<!--
				<div class="row">
					<div class="form-group">
						<select class="form-control" style="width:100%;" name="from_city" id ="from_city"></select>
					</div>
				</div>
				-->
				
				<div class="row">
					<div class="form-group">
						<div class='input-group date' id='datetimepicker1'>
							<input type='text' name="departure" id="departure" class="form-control" />
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</div>
				</div>
				
			</div>
			
			<div class="col-sm-4" style="margin-bottom: 0px;">
				<div class="row text-center">
					<div class="form-group">
						<h3>To</h3>
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						 <!-- onchange="print_state('to_city',this.selectedIndex);"-->
						<select class="form-control" id="to_country" name="to_country">
						</select>
					</div>
				</div>
				<div class="row">
					<div class="form-group return_date">
						<div class='input-group date' id='datetimepicker2'>
							<input type='text' name="return_departure" id="return_departure" class="form-control" />
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</div>
				</div>
				<!--
				<div class="row">
					<div class="form-group">
						<select class="form-control" style="width:100%;" name="to_city" id ="to_city"></select>
					</div>
				</div>
				-->

			</div>
		</div>
		
		<div class="row" style="margin-bottom: 10px;">
			<div class="col-sm-4 col-sm-offset-1" style="margin-bottom: 0px;">
				<div class="row">
					<div class="form-group" style="margin-left: 10px; margin-top: -10px;">
						<div class="radio">
							<label><input type="radio" id="radio1" value="1" name="optradio" checked>One Way</label>
							<label style="margin-left: 20px;"><input type="radio" id="radio2" value="2" name="optradio">Return</label>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="form-group">
				<div class="col-sm-4 col-sm-offset-3">
					<button type="submit" class="btn btn-primary form-control" style="width:100%;">Create</button>
				</div>
			</div>
		</div>

	</form>
  </div>

	<script type="text/javascript">
		$(function () {
			$(".return_date").hide();
			var date = new Date();
			date.setDate(date.getDate());

			$('#datetimepicker1').datetimepicker({
				format: "YYYY/MM/DD"
			});
			$('#datetimepicker2').datetimepicker({
				format: "YYYY/MM/DD",
				useCurrent: false
			});

			$('#datetimepicker1').data("DateTimePicker").minDate(date);

			$("#datetimepicker1").on("dp.change", function (e) {
			    $('#datetimepicker2').data("DateTimePicker").minDate(e.date);
			});
			$("#datetimepicker2").on("dp.change", function (e) {
			    $('#datetimepicker1').data("DateTimePicker").maxDate(e.date);
			});
			
			$('input[type=radio][name=optradio]').change(function(){
				if (this.value == "2"){
					$(".return_date").show();
				} else {
					$(".return_date").hide();
				}
			});
			
			
		});
	</script>

<!--
<div class="row text-center">
	<label class="control-label text-center text-brand-blue bold">Required By Time</label>
	<input type="hidden" class="form-control" placeholder="Time" name="dep" value="{{ old('dep') }}">
	<div id="deadline-timepicker"></div>
</div>
-->

<script type="text/javascript" src="{{ URL::asset('js/countries.js') }}"></script>
<script language="javascript">print_country("from_country");</script>
<script language="javascript">print_country("to_country");</script>

@endsection

@section('plugins')
  <link rel="stylesheet" href="{{ URL::asset('css/bootstrap-datetimepicker.min.css') }}"/> 
@endsection

@section('scripts')
  <script src="{{ URL::asset('/js/moment.min.js') }}"></script>
  <script type="text/javascript" charset="UTF-8" src="{{ URL::asset('js/bootstrap-datetimepicker.js') }}"></script>
@endsection

