@extends('app')


@section('content')

<style>
.table > tbody > tr > td {
	 vertical-align: middle;
}

</style>

<div id="fb-root"></div>
<script>
	window.fbAsyncInit = function() {
		FB.init({appId: '510079345866294', status: true, cookie: true,
		xfbml: true});
	};
	(function() {
		var e = document.createElement('script'); e.async = true;
		e.src = document.location.protocol +
		'//connect.facebook.net/en_US/all.js';
		document.getElementById('fb-root').appendChild(e);
	}());
</script>

<script type="text/javascript">
$(document).ready(function(){
	$('.share_button').click(function(e){
		e.preventDefault();
		FB.ui(
		{
			method: 'feed',
			name: '{{ Auth::user()->first_name }} is travelling to ' + $(this).data("options").to + '!',
			link: 'https://www.antebox.com',
			picture: '{{ url(asset("img/social/plane.jpg")) }}',
			caption: 'AnteBox: Visit local businesses and experience unique products.',
			description: "Keep track of your trips, receive unique offers for the countries you are visiting and earn some extra cash by delivering a unique product!",
			message: ""
		});
	});
});
</script>

  <?php
	function getCountryCode($tmp) {
		$country_arr = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Angola", "Anguilla", "Antartica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Ashmore and Cartier Island", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burma", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Clipperton Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo, Democratic Republic of the", "Congo, Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Europa Island", "Falkland Islands (Islas Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "French Guiana", "French Polynesia", "French Southern and Antarctic Lands", "Gabon", "Gambia", "Gaza Strip", "Georgia", "Germany", "Ghana", "Gibraltar", "Glorioso Islands", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard Island and McDonald Islands", "Holy See (Vatican City)", "Honduras", "Hong Kong", "Howland Island", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Ireland, Northern", "Israel", "Italy", "Jamaica", "Jan Mayen", "Japan", "Jarvis Island", "Jersey", "Johnston Atoll", "Jordan", "Juan de Nova Island", "Kazakhstan", "Kenya", "Kiribati", "Korea, North", "Korea, South", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Man, Isle of", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Midway Islands", "Moldova", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn Islands", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russia", "Rwanda", "Saint Helena", "Saint Kitts and Nevis", "Saint Lucia", "Saint Pierre and Miquelon", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Scotland", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and South Sandwich Islands", "Spain", "Sri Lanka", "Sudan", "Suriname", "Svalbard", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Tobago", "Toga", "Tokelau", "Tonga", "Trinidad", "Tunisia", "Turkey", "Turkmenistan", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "Uruguay", "USA", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands", "Wales", "Wallis and Futuna", "West Bank", "Western Sahara", "Yemen", "Zambia", "Zimbabwe");
		$countryVal_arr = array("af","al","dz","as","ao","ai","aq","ag","ar","am","aw","au","au","at","az","bs","bh","bd","bb","by","be","bz","bj","bm","bt","bo","ba","bw","br","vg","bn","bg","bf","mm","bi","kh","cm","ca","cv","ky","cf","td","cl","cn","cx","fr","cc","co","km","cd","cg","ck","cr","ci","hr","cu","cy","cz","dk","dj","dm","do","ec","eg","sv","gq","er","ee","et","tf","fk","fo","fj","fi","fr","gf","pf","tf","ga","gm","ps","ge","de","gh","gi","tf","gr","gl","gd","gp","gu","gt","gg","gn","gw","gy","ht","hm","va","hn","hk","um","hu","is","in","id","ir","iq","ie","_Northern Ireland","il","it","jm","sj","jp","um","je","um","jo","tf","kz","ke","ki","kp","kr","kw","kg","la","lv","lb","ls","lr","ly","li","lt","lu","mo","mk","mg","mw","my","mv","ml","mt","im","mh","mq","mr","mu","yt","mx","fm","um","md","mc","mn","ms","ma","mz","na","nr","np","nl","an","nc","nz","ni","ne","ng","nu","nf","mp","no","om","pk","pw","pa","pg","py","pe","ph","pn","pl","pt","pr","qa","fr","ro","ru","rw","sh","kn","lc","pm","vc","ws","sm","st","sa","_Scotland","sn","rs","sc","sl","sg","sk","si","sb","so","za","gs","es","lk","sd","sr","sj","sz","se","ch","sy","tw","tj","tz","th","tt","tg","tk","to","tt","tn","tr","tm","tv","ug","ua","ae","gb","uy","us","uz","vu","ve","vn","vi","_Wales","wf","ps","eh","ye","zm","zw");

	    $key = array_search($tmp, $country_arr);
		return $countryVal_arr[$key];
	}
  ?>

  <div class="container">

	<div class="row">

	  <div class="col-12">

		<h1>&nbsp;My Trips</h1>
                @if (session('status'))
                  <div class="alert alert-success">
                   <strong>{{ session('status') }}</strong>
                   </div>
                @endif
	  </div>
	</div>
	<div class="row">
	    <a href="/trips/create" class="btn btn-primary pull-right">Add Trip</a>
	</div>

    <br>


<div class="row-fluid">
    <div class="col-xs-12">
        <table class="table table-hover">
	  <thead>
            <tr>
                <th>From</th>
                <th>To</th>
                <th>Departure Date</th>
                <th>Status</th>
                <th></th>
             </tr>
	  </thead>
         <tbody>
         @foreach($trips as $trip)
	     @if($trip->status=="Open" || $trip->status=="Full" || $trip->status=="Almost Full")
	     <tr  class="success">
             @elseif($trip->status=="Cancelled")
             <tr  class="danger">
             @elseif($trip->status=="Expired")
             <tr  class="warning">
             @elseif($trip->status=="Completed")
             <tr  class="info">

	     @endif
		<td>
		<?php $from_country_url = 'img/flags/flags_iso/32/' . getCountryCode($trip->from_country) . '.png'; ?>
		<?php $to_country_url = 'img/flags/flags_iso/32/' . getCountryCode($trip->to_country) . '.png'; ?>
			<img src="{{ url(asset($from_country_url)) }}">
			{{ $trip->from_country }} {{ $trip->from_city }}
		</td>
		<td>
			<img src="{{ url(asset($to_country_url)) }}">
			{{ $trip->to_country }} {{ $trip->to_city }}
		</div>
		</td>
        <td>
			<?php  $trip_pieces = explode(" ","$trip->dep "); 
				echo $trip_pieces[0];
			?>
		</td>

		@if($trip->status=="Open")
		<td class="text-success"><strong>{{ $trip->status }}</strong></td>
		@elseif($trip->status=="Almost Full")
                <td class="text-warning"><strong>{{ $trip->status }}</strong></td>
		@elseif($trip->status=="Full")
                <td class="text-info"><strong>{{ $trip->status }}</strong></td>
		@elseif($trip->status=="Cancelled")
                <td class="text-danger"><strong>{{ $trip->status }}</strong></td>
		@else
                <td class="text-muted"><strong>{{ $trip->status }}</strong></td>
		@endif
		@if($trip->status=="Open")
		
		<td>
			<style>
				.button-container form,
				.button-container form div {
					display: inline;
				}

				.button-container button {
					display: inline;
					vertical-align: middle;
				}
			</style>
			<div class="button-container">
				<button type="button" style="min-width: 38px;" class="btn btn-primary share_button"
					data-options='{"to":"{{ $trip->to_country }}"}'>
					<i class="fa fa-facebook"></i> <span class="hidden-xs hidden-sm">Share</span>
				</button>
				<form action="/trips/mytrips/edit/{{ $trip->id }}" method="post">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<button type="submit" style="min-width: 38px;" class="btn btn-warning">
						<i class="fa fa-edit"></i> <span class="hidden-xs hidden-sm">Edit</span>
					</button>
				</form>
				<form action="/trips/mytrips/cancel/{{ $trip->id }}" method="get">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<button type="submit" style="min-width: 38px;" class="btn btn-danger">
						<i class="fa fa-times"></i> <span class="hidden-xs hidden-sm">Cancel</span>
					</button>
				</form>
			</div>
		</td>
		@else
		<td></td>
		@endif
	     </tr>
         @endforeach
	 </tbody>
    </table>
        
      </div>
    </div>
	<div class="row text-center">{!! $trips->render() !!}</div>


  </div>
  

@endsection
