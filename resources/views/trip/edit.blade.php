@extends('app')

@section('content')
  <div class="container">
    <div class="col-md-12" style="margin-bottom: 50px;">

<div class="row">
        <div class="col-md-12">
          <span class="text-brand-brown huge-title">Edit a Trip!</span>
        </div>
      </div>

 	@if (count($errors) > 0)
	  <div class="alert alert-danger">
  		<strong>Whoops!</strong> There were some problems with your input.<br><br>
  		<ul>
  			@foreach ($errors->all() as $error)
  				<li>{{ $error }}</li>
  			@endforeach
  		</ul>
  	  </div>
    	@endif

    </div>

	<form action="/trips/mytrips/saveTrip/{{ $trip->id }}" method="post">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		<div class="row">

			<div class="col-sm-4 col-sm-offset-1" style="margin-bottom: 40px;">

				<div class="row text-center">
					<div class="form-group">
						<h3>From</h3>
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						<!-- onchange="print_state('from_city',this.selectedIndex);"  -->
						<select class="form-control" style="width:100%;" id="from_country" name="from_country">
						</select>
					</div>
				</div>
				<!--
				<div class="row">
					<div class="form-group">
						<select class="form-control" style="width:100%;" name="from_city" id ="from_city"></select>
					</div>
				</div>
				-->
				
				<div class="row">
					<div class="form-group">
						<div class='input-group date' id='datetimepicker1'>
							<input type='text' name="departure" id="departure" value="{{ $trip->dep }}" class="form-control" />
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</div>
				</div>
				
			</div>

			<div class="col-sm-4" style="margin-bottom: 40px;">
				<div class="row text-center">
					<div class="form-group">
						<h3>To</h3>
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						 <!-- onchange="print_state('to_city',this.selectedIndex);"-->
						<select class="form-control" style="width:100%;" id="to_country" name="to_country">
						</select>
					</div>
				</div>
				<!--
				<div class="row">
					<div class="form-group">
						<select class="form-control" style="width:100%;" name="to_city" id ="to_city"></select>
					</div>
				</div>
				-->
				<div class="form-group">
					<div class='input-group date' id='datetimepicker2'>
						<input type='text' name="arrival" id="arrival" value="{{ $trip->arr }}" class="form-control" />
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>
				</div>
				
			</div>
		</div>

		<div class="row">
			<div class="form-group">
				<div class="col-sm-2 col-sm-offset-4">
					<button type="submit" class="btn btn-primary form-control" style="width:100%;">Update</button>
				</div>
			</div>
		</div>

	</form>
  </div>

  
  
  
<script type="text/javascript" src="{{ URL::asset('js/countries.js') }}"></script>
<script language="javascript">print_country_edit("from_country","{{ $trip->from_country }}");</script>
<script language="javascript">print_country_edit("to_country","{{ $trip->to_country }}");</script>
<script type="text/javascript">
	$(function () {
		var date = new Date();
		date.setDate(date.getDate());

		$('#datetimepicker1').datetimepicker({
			format: "YYYY/MM/DD"
		});
		$('#datetimepicker2').datetimepicker({
			format: "YYYY/MM/DD",
			useCurrent: false
		});
		$('#datetimepicker3').datetimepicker({
			format: "YYYY/MM/DD",
			useCurrent: false
		});
		$('#datetimepicker4').datetimepicker({
			format: "YYYY/MM/DD",
			useCurrent: false
		});

		$('#datetimepicker1').data("DateTimePicker").minDate(date);
		$('#datetimepicker2').data("DateTimePicker").minDate(date);

		$("#datetimepicker1").on("dp.change", function (e) {
			$('#datetimepicker2').data("DateTimePicker").minDate(e.date);
		});
		$("#datetimepicker2").on("dp.change", function (e) {
			$('#datetimepicker1').data("DateTimePicker").maxDate(e.date);
		});
		
		$('#datetimepicker3').data("DateTimePicker").minDate(date);
		$('#datetimepicker4').data("DateTimePicker").minDate(date);

		$("#datetimepicker3").on("dp.change", function (e) {
			$('#datetimepicker4').data("DateTimePicker").minDate(e.date);
		});
		$("#datetimepicker4").on("dp.change", function (e) {
			$('#datetimepicker3').data("DateTimePicker").maxDate(e.date);
		});
		
		$('input[type=radio][name=optradio]').change(function(){
			if (this.value == "2"){
				$(".return_date").show();
			} else {
				$(".return_date").hide();
			}
		});
		
		
	});
</script>

<!--
<div class="row text-center">
	<label class="control-label text-center text-brand-blue bold">Required By Time</label>
	<input type="hidden" class="form-control" placeholder="Time" name="dep" value="{{ old('dep') }}">
	<div id="deadline-timepicker"></div>
</div>
-->



@endsection

@section('plugins')
  <link rel="stylesheet" href="{{ URL::asset('css/bootstrap-datetimepicker.min.css') }}"/> 
@endsection

@section('scripts')
  <script src="{{ URL::asset('/js/moment.min.js') }}"></script>
  <script type="text/javascript" charset="UTF-8" src="{{ URL::asset('js/bootstrap-datetimepicker.js') }}"></script>
@endsection

