<div class="col-md-3 col-sm-4">
	{!! Form::open(array('action' => 'ProductsController@filter')) !!}
	
	<div class="product-item-categori">
		<div class="product-type">
			<h2>Product Type</h2>
			<ul>

				
				@foreach($categories as $category)
					<li>
						<span>
							@if (isset($selected_categories))
								@if (in_array($category, $selected_categories))
									{!! Form::checkbox('categories[]', $category, 1, array('onChange' => 'this.form.submit()'))!!} {{ $category}}
								@else
									{!! Form::checkbox('categories[]', $category, 0, array('onChange' => 'this.form.submit()'))!!} {{ $category}}
								@endif

							@else
								{!! Form::checkbox('categories[]', $category, 0, array('onChange' => 'this.form.submit()'))!!} {{ $category}}
							
							@endif
							{{-- <a href="{{action('ProductsController@filter', ['name' => $category])}} "> {{ $category}} </a> --}}
						</span>
					</li>						
				@endforeach
			</ul>
		</div>
	</div>
	<div class="product-item-categori">
		<div class="product-type">
			<h2>Country</h2>
			<ul>
				@foreach($countries as $country)
					<li>
						<span>
							@if (isset($selected_countries))
								@if (in_array($country, $selected_countries))
									{!! Form::checkbox('countries[]', $country, 1, array('onChange' => 'this.form.submit()'))!!} {{ $country}}
								@else
									{!! Form::checkbox('countries[]', $country, 0, array('onChange' => 'this.form.submit()'))!!} {{ $country}}
								@endif

							@else
								{!! Form::checkbox('countries[]', $country, 0, array('onChange' => 'this.form.submit()'))!!} {{ $country}}
							
							@endif
						</span>
					</li>						
				@endforeach
			</ul>
		</div>
	</div>
	<div class="product-item-categori">
		<div class="product-type">
			<h2>Company</h2>
			<ul>
				@foreach($companies as $company)
					<li>
						<span>
							@if (isset($selected_companies))
								@if (in_array($company, $selected_companies))
									{!! Form::checkbox('companies[]', $company, 1, array('onChange' => 'this.form.submit()'))!!} {{ $company}}
								@else
									{!! Form::checkbox('companies[]', $company, 0, array('onChange' => 'this.form.submit()'))!!} {{ $company}}
								@endif

							@else
								{!! Form::checkbox('companies[]', $company, 0, array('onChange' => 'this.form.submit()'))!!} {{ $company}}
							
							@endif
						</span>
					</li>						
				@endforeach
			</ul>
		</div>
	</div>
	{!! Form::close() !!} 

	{!! Form::open(array('action' => 'ProductsController@filter')) !!}
	<div class="price-filter">
		<h2>Filter by price</h2>
		<div id="slider-range"></div>
		<input type="hidden" id="min_price" name="min_price" value="0">
		<input type="hidden" id="max_price"  name="max_price" value="0">
		<button type="submit" class="btn btn-default">Filter</button>
		<p>
		  <label for="amount">Price:</label>
		  <input type="text" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold;">
		</p>
	</div>
	
	{!! Form::close() !!} 
	


</div>
