<div class="footer-bottom">
	<div class="container">
		<div class="row">
		<div class="copyright">
			<ul class="list-inline">
						<li>
							<a href="/contact">
							Contact
							</a>
						</li>
						<li>
							<a href="/about">
							About
							</a>
						</li>
						<li>
							<a href="/faq">
							FAQ										
							</a>
						</li>
						<li>
							<a href="/terms">
								Terms
							</a>
						</li>
			</ul>
		</div>
		</div>
		<div class="row">
			<div class="col-md-6 col-sm-6">
				<div class="copyright">
					&copy; 2016, AnteBox &nbsp;
					&copy; 2015 <a href="http://bootexperts.com/" target="_blank">BootExperts</a>/ All rights reserved.
				</div>

			</div>
			<div class="col-md-6 col-sm-6">
				<div class="footer-social-icon">
					<ul class="list-inline">
						<li><a href="https://facebook.com/AnteBox"><i class="fa fa-facebook"></i></a></li>
						<li><a href="https://twitter.com/anteboxapp"><i class="fa fa-twitter"></i></a></li>
						<li><a href="https://www.instagram.com/antebox"><i class="fa fa-instagram"></i></a></li>
						<li><a href="https://www.youtube.com/channel/UC654J_tnd7isr9o1hVqThaA"><i class="fa fa-youtube"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
        <!-- JS -->
        
 		<!-- jquery-1.11.3.min js
		============================================ -->         
        <script src="/js/frontpage/vendor/jquery-1.11.3.min.js"></script>
        
 		<!-- bootstrap js
		============================================ -->         
        <script src="/js/frontpage/bootstrap.min.js"></script>
		
		<!-- nivo slider js
		============================================ --> 
		<script src="/js/frontpage/jquery.nivo.slider.pack.js"></script>
        
 		<!-- Mean Menu js
		============================================ -->         
        <script src="/js/frontpage/jquery.meanmenu.min.js"></script>
        
   		<!-- owl.carousel.min js
		============================================ -->       
        <script src="/js/frontpage/owl.carousel.min.js"></script>
		
		<!-- jquery price slider js
		============================================ --> 		
		<script src="/js/frontpage/jquery-price-slider.js"></script>
		
		<!-- wow.js
		============================================ -->
        <script src="/js/frontpage/wow.js"></script>		
		<script>
			new WOW().init();
		</script>
        
   		<!-- plugins js
		============================================ -->         
        <script src="/js/frontpage/plugins.js"></script>
		
   		<!-- main js
		============================================ -->           
        <script src="/js/frontpage/main.js"></script>
    </body>
</html>
