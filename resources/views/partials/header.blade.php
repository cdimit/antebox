<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if lt IE 7 ]> <html lang="en" class="ie6">    <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="ie7">    <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9">    <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="no-js" lang="">
    <head>
		<meta charset="UTF-8">
	    <title>AnteBox - A network for local businesses powered by you</title>
	    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	    <meta name="description" content="A new innovative consultancy company that helps globalize businesses that work in a geographically limited area">
	  	<meta name="keywords" content="antebox,antebox revolution,local businesses,local business,travel,travellers,unique,haloumi,halloumi,food,unique food,unique haloumi,unique halloumi,unique fashion,shop,online,startup">
	  	@if(isset($product) && !isset($products))
			<meta property="og:title" content="{{$product->name}} - Antebox" />
			<meta property="og:image" content="{{ URL::asset('img/products/'.$product->pic_url) }}" />
		  	<meta property="og:image:secure_url" content="{{ URL::asset('img/products/'.$product->pic_url) }}" />
		  	<meta property="og:url"   content="https://www.antebox.com/products/{{$product->id}}" />
		  	<meta property="og:description" content="{{ str_limit($product->description, $limit = 150, $end = '...') }}" />
		  	<meta name="twitter:card" content="summary">
			<meta name="twitter:site" content="@anteboxapp">
			<meta name="twitter:creator" content="@anteboxapp">
			<meta name="twitter:title" content="{{$product->name}} - Antebox">
			<meta name="twitter:description" content="{{ str_limit($product->description, $limit = 150, $end = '...') }}">
			<meta name="twitter:image" content="{{ URL::asset('img/products/'.$product->pic_url) }}">
	  	@else
		  	<meta property="og:title" content="AnteBox - A network for local businesses powered by you" />
			<meta property="og:image" content="{{ url(asset('img/app/favicon.jpg')) }}" />
		  	<meta property="og:image:secure_url" content="{{ url(asset('img/app/favicon.jpg')) }}" />
		  	<meta property="og:url"   content="https://www.antebox.com" />
		  	<meta property="og:description" content="A new innovative consultancy company that helps globalize businesses that work in a geographically limited area." />
	  	@endif
	  	<meta property="og:image:type" content="image/png" />
	  	<meta property="og:type"  content="website" />
	  	
	  	

	  	<meta property="fb:admins" content="605108675" />
	  	<meta property="fb:app_id" content="510079345866294" />

	  	<link rel="shortcut icon" href="{{ asset('img/app/favicon.ico') }}">


    	<style>
	      @media (min-width: 768px) {
	        ul.nav li.dropdown:hover > ul.dropdown-menu {
	          display: block;
	        }
      	</style>

	  	<script>
	      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	      ga('create', 'UA-71587454-1', 'auto');
	      ga('send', 'pageview');
	    </script>

	    <!-- Facebook Pixel Code -->
	    <script>
	    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	    document,'script','https://connect.facebook.net/en_US/fbevents.js');

	    fbq('init', '238526746496667');
	    fbq('track', "PageView");</script>
	    <noscript><img height="1" width="1" style="display:none"
	    src="https://www.facebook.com/tr?id=238526746496667&ev=PageView&noscript=1"
	    /></noscript>

        <!-- Favicon
		============================================ -->
		{{-- <link rel="shortcut icon" type="image/x-icon" href="img/favicon.jpg"> --}}
		
		<!-- Fonts
		============================================ -->
		<link href='https://fonts.googleapis.com/css?family=Raleway:400,700,600,500,300,800,900' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Roboto:400,400italic,500,300,300italic,500italic,700' rel='stylesheet' type='text/css'>

 		<!-- CSS  -->
		
		<!-- Bootstrap CSS
		============================================ -->      
        <link rel="stylesheet" href="/css/frontpage/bootstrap.min.css">
        
		<!-- font-awesome.min CSS
		============================================ -->      
        <link rel="stylesheet" href="/css/frontpage/font-awesome.min.css">
		
		<!-- Mean Menu CSS
		============================================ -->      
        <link rel="stylesheet" href="/css/frontpage/meanmenu.min.css">
        
		<!-- owl.carousel CSS
		============================================ -->      
        <link rel="stylesheet" href="/css/frontpage/owl.carousel.css">
        
		<!-- owl.theme CSS
		============================================ -->      
        <link rel="stylesheet" href="/css/frontpage/owl.theme.css">
  	
		<!-- owl.transitions CSS
		============================================ -->      
        <link rel="stylesheet" href="/css/frontpage/owl.transitions.css">
		
		<!-- Price Filter CSS
		============================================ --> 
        <link rel="stylesheet" href="/css/frontpage/jquery-ui.min.css">	

		<!-- nivo-slider css
		============================================ --> 
		<link rel="stylesheet" href="/css/frontpage/nivo-slider.css">
        
 		<!-- animate CSS
		============================================ -->         
        <link rel="stylesheet" href="/css/frontpage/animate.css">
		
		<!-- jquery-ui-slider CSS
		============================================ --> 
		<link rel="stylesheet" href="/css/frontpage/jquery-ui-slider.css">
        
 		<!-- normalize CSS
		============================================ -->        
        <link rel="stylesheet" href="/css/frontpage/normalize.css">
   
        <!-- main CSS
		============================================ -->          
        <link rel="stylesheet" href="/css/frontpage/main.css">
        
        <!-- style CSS
		============================================ -->          
        <link rel="stylesheet" href="/css/frontpage/style.css">
        
        <!-- responsive CSS
		============================================ -->          
        <link rel="stylesheet" href="/css/frontpage/responsive.css">
        
        <script src="/js/frontpage/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body class="home-2">
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
               
        <!-- HEADER AREA -->
        <div class="header-area">
			<div class="header-top-bar">
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-12">
							@if(Auth::guest())
							Welcome visitor
							@else
							Welcome {{ Auth::user()->first_name }}
							@endif
						</div>
						<div class="col-md-8 col-sm-8 col-xs-12">
							<div class="header-top-right">
								<ul class="list-inline">
								@if(Auth::guest())
									<li><a href="{{ url('/login') }}"><i class="fa fa-lock"></i>Login</a></li>
									<li><a href="{{ url('/register') }}"><i class="fa fa-pencil-square-o"></i>Register</a></li>
								@else

								<?php $notify = Auth::user()->notifications()->get()->where('status', '0')->sortByDesc('id');?>
									<li class="dropdown" >
										<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
											<i class="fa fa-bell" aria-hidden="true"></i>
											@if($notify->count()!=0)
											<span class="badge badge-notify">{{ $notify->count()}}</span>
											@endif
											<span class="caret"></span>
										</a>
										@if($notify->count()!=0)
										<ul class="dropdown-menu" aria-labelledby="dropdownMenu1" style="background: #f5f5f5; padding: 0;">
											<table class="table" style="padding: 0; margin-bottom: 0px;">
												@foreach($notify as $n)
												<li>
													<tr class="active">
														<td><a href="/notifications/read/{{$n->id}}">
														@if($n->category=="antebox")
														<img src="{{ URL::asset("img/app/notify.png") }}" width="30" height="30" />
														@elseif($n->category=="msg")
														<img src="{{ URL::asset("img/app/notify-msg.png") }}" width="30" height="30" />
														@elseif($n->category=="paid")
														<img src="{{ URL::asset("img/notifications/paid.png") }}" width="30" height="30" />
														@elseif($n->category=="bid")
														<img src="{{ URL::asset("img/app/notify-bid.png") }}" width="30" height="30" />
														@elseif($n->category=="warning")
														<img src="{{ URL::asset("img/notifications/warning.png") }}" width="30" height="30" />
														@elseif($n->category=="shop")
														<img src="{{ URL::asset("img/notifications/shop.png") }}" width="30" height="30" />
														@elseif($n->category=="deal")
														<img src="{{ URL::asset("img/notifications/deal.png") }}" width="30" height="30" />
														@elseif($n->category=="terms")
														<img src="{{ URL::asset("img/notifications/terms.png") }}" width="30" height="30" />
														@endif
														</a></td>
														<td><a href="/notifications/read/{{$n->id}}"><button type="button" class="btn btn-default btn-block" style=
														"color:#000;">{{$n->msg}}</button></a></td>
														<td><a href="/notifications/remove/{{$n->id}}"><img src="{{ URL::asset("img/app/notify-remove.png") }}" width="30" height="30" /></a></td>
													</tr>  
												</li>

												@endforeach
												@if($notify->count()>1)
												<li>
													<tr class="active">
														<td></td>
														<td>
															<a href="/notifications/removeAll">
																<button type="button" class="btn btn-default btn-block">Clear All</button>
															</a>
														</td>
														<td></td>
													</tr>
												</li>
												@endif
											</table>
										</ul>
										@endif
									</li>

									<li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
											<!-- Welcome, {{ Auth::user()->first_name }} -->
											<span class="fa fa-user"></span>
											<span class="caret"></span>
										</a>
										<ul class="dropdown-menu" role="menu">
											<li><a href="/user/{{ Auth::user()->id }}" style="color:#000;">Profile</a></li>
											<li><a href="/mydeals" style="color:#000;">My Deals</a></li>
						                	<li><a href="{{ url('/myrequests') }}" style="color:#000;">My Requests</a></li>
											<li><a href="/mybids" style="color:#000;">My Bids</a></li>
					                        <li><a href="{{ url('/trips/mytrips') }}" style="color:#000;">My Trips</a></li>
											<li><a href="{{ url('/logout') }}" style="color:#000;">Logout</a></li>
										</ul>
									</li>
								@endif
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="header-bottom">
				<div class="container">
					<div class="row">
						<div class="col-md-2 col-sm-2 col-xs-12">
							<div class="header-logo">
								<a href="/"><img src="/img/frontpage/antebox-logo-opaque.png" alt="logo"></a>
							</div>
						</div>
						<div class="col-md-10 col-sm-10 col-xs-12">
							@if(isset($categories))
							<div class="search-chart-list">
								{!! Form::open(array('url' => 'products/search')) !!}
								<div class="catagori-menu">
									<ul class="list-inline">
										<li><i class="fa fa-search"></i></li>
										<li>
											<select name="category">
												<option value="All">All</option>
												@foreach($categories as $category)
												<option value="{{ $category }}" > {{$category}} </option>
												@endforeach
											</select>
										</li>
									</ul>
								</div>
								<div class="header-search">
									
								<input type="text" name="search_query" placeholder="My Search"/>
								<button type="submit"><i class="fa fa-search"></i></button>
									
								</div>
								{!! Form::close() !!} 

								@endif
								
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>
        <!-- MAIN MENU AREA -->
		<div class="main-menu-area">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="main-menu hidden-xs">

							<nav>
								<ul>
									<li>
										<a href="/">Home</a>
									</li>
									<li>
										<a href="/products">Products</a>
									</li>
									<li>
										<a href="/requests">Requests</a>
									</li>
									<li>
										<a href="/trips">Travellers</a>
									</li>
									<li>
										<a href="/partners">Partners</a>
									</li>
									
								</ul>
							</nav>
						</div>
						<!-- Mobile MENU AREA -->
						<div class="mobile-menu hidden-sm hidden-md hidden-lg">
							@if(!Auth::guest())
							<?php $notify = Auth::user()->notifications()->get()->where('status', '0')->sortByDesc('id');?>
							<div class="row profile-buttons">
								<div class="col-xs-6 text-center open-btn" id="btn-notifications">
									<i class="fa fa-bell"></i> &nbsp; 
									@if($notify->count()!=0)
									Notifications ( {{$notify->count()}} )
									@else
									Notifications 
									@endif
								</div>
								<div class="col-xs-6 text-center open-btn" id="btn-profile">
									<i class="fa fa-user"></i> &nbsp;  Profile
								</div>
							</div>

							<div class="overlay" id="overlay-notifications">
								<div class="close-btn">
									<i class="fa fa-close"></i> &nbsp; Close								
								</div>

								<div class="overlay-content">
									<h3 style="font-weight:bolder;">MY NOTIFICATIONS</h3>

									@if($notify->count()!=0)
									<ul id="ul-notifications">
										@foreach($notify as $n)
										<li class="container">
											<div class="row">
												<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
													@if($n->category == "antebox")
													<i class="fa fa-bullhorn" aria-hidden="true"></i> &nbsp; Antebox 
													@elseif($n->category == "msg")
													<i class="fa fa-comments" aria-hidden="true"></i> &nbsp; Message 
													@elseif($n->category == "paid")
													<i class="fa fa-money" aria-hidden="true"></i> &nbsp; Payment 
													@elseif($n->category == "bid")
													<i class="fa fa-money" aria-hidden="true"></i> &nbsp; Bids 
													@elseif($n->category == "warning")
													<i class="fa fa-money" aria-hidden="true"></i> &nbsp; Warning 
													@elseif($n->category == "shop")
													<i class="fa fa-money" aria-hidden="true"></i> &nbsp; Shop 
													@elseif($n->category == "deal")
													<i class="fa fa-money" aria-hidden="true"></i> &nbsp; Deals 
													@elseif($n->category == "terms")
													<i class="fa fa-money" aria-hidden="true"></i> &nbsp; Terms 
													@endif
													<a href="/notifications/read/{{$n->id}}">
														<p>
															{{ $n->msg}}
														</p>
													</a>
												</div>
												<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
													<a href="/notifications/remove/{{$n->id}}">
														<i class="dismiss fa fa-times-circle" aria-hidden="true"></i>
													</a>
													<a href="/notifications/read/{{$n->id}}">
														<i class="dismiss fa fa-arrow-circle-right" aria-hidden="true"></i>
													</a>
												</div>
											</div>
										</li>
										@endforeach
										@if($notify->count() > 3)
										<li class="container">
											<div class="row">
												<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
													<i class="fa fa-times-circle" aria-hidden="true"></i> &nbsp; Dismiss all
												</div>
												<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
													<a href="/notifications/removeAll">
														<i class="dismiss fa fa-arrow-circle-right" aria-hidden="true"></i>
													</a>
												</div>
											</div>
										</li>
										@endif
										
									</ul>
									@else
									<h4>No notifications...</h4>
									@endif
								</div>
							</div>

							<div class="overlay" id="overlay-profile">
								<div class="close-btn">
									<i class="fa fa-close"></i> &nbsp; Close								
								</div>

								<div class="overlay-content">
									<h3 style="font-weight:bold;">{{ Auth::user()->first_name }}</h3>
									<ul id="profile-links">
										<li>
											<a href="/user/{{ Auth::user()->id }}">
												Profile
											</a>
										</li>
										<li>
											<a href="/mydeals">
												My deals
											</a>
										</li>
										<li>
											<a href="/mybids">
												My bids
											</a>
										</li>
										<li>
											<a href="{{ url('/myrequests') }}">
												My requests
											</a>
										</li>
										<li>
											<a href="{{ url('/trips/mytrips') }}">
												My trips
											</a>
										</li>
										<li>
											<a href="{{ url('/logout') }}">
												Logout
											</a>
										</li>
									</ul>
								</div>
							</div>

							@else

							<div class="row profile-buttons">
								<a href="{{ url('/login') }}">
									<div class="col-xs-6 text-center open-btn">
										<i class="fa fa-user-plus"></i> &nbsp; Register
									</div>
									
								</a>
								<a href="{{ url('/register') }}">
									<div class="col-xs-6 text-center open-btn">
										<i class="fa fa-sign-in"></i> &nbsp;  Login
									</div>
									
								</a>
							</div>
							
							@endif
							<nav>
								<ul>
									<li>
										<a href="/">Home</a>
									</li>
									<li>
										<a href="/products">Products</a>
									</li>
									<li>
										<a href="/requests">Requests</a>
									</li>
									<li>
										<a href="/trips">Travellers</a>
									</li>
									<li>
										<a href="/partners">Partners</a>
									</li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</div>
