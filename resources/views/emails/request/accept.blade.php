<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>AnteBox</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">

<table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
 <tr>
<td align="center" bgcolor="#225680" style="padding: 40px 0 10px 0;">
 <img src="{{ URL::asset('img/email/logo.png') }}"  alt="AnteBox" width="192" height="62" style="display: block; border-style: solid;" /> <!-- Mavro plesio -->
 <font color="#ffffff"><h3>New Delivery Confirmed</h3><h5>Time to buy the products</h5></font>
</td>
 </tr>


 <tr>


<td bgcolor="#f0e1d3" style="padding: 0px 30px 0px 30px;">
<table>
  <tr>
   <td style="padding: 20px 0 0px 0;" >

    <font color="#000000">Hello {{$deal->traveller->first_name}},<br><br>

<p><a href="https://www.antebox.com/user/{{$deal->client->id}}">{{ $deal->client->first_name}}</a> has accepted your bid and has chosen you to complete @if($deal->client->sex=="Male")his @else her @endif <a href="https://www.antebox.com/request/view/{{$deal->bid->request->id}}">request</a>!</p>
<p>You have to visit a <a href="https://www.antebox.com/product/view/{{$deal->bid->request->product->id}}">store</a> soon!</p>
<p>You will be notified when {{$deal->client->first_name}} pays, meaning that we are holding the money securely until you deliver the products.
After that, you will be able to confirm that you have bought the <a href="https://www.antebox.com/product/view/{{$deal->bid->request->product->id}}">products</a>. Remember! Notify the customer by clicking on step 2.</p>
Send a message to {{$deal->client->first_name}} and see the details of the Deal:
<table cellspacing="0" cellpadding="0"> <tr>
  <td align="center" width="300" height="40" bgcolor="#225680" style="-webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; color: #ffffff;">
    <a href="https://www.antebox.com/deal/{{$deal->id}}" style="font-size:16px; font-weight: bold; font-family:sans-serif; text-decoration: none; line-height:40px; width:100%; display:inline-block">

    <span style="color: #ffffff;"> Show the Deal </span>
    </a>
  </td>
  <td align="center" width="300" height="40" bgcolor="green" style="-webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; color: #ffffff;">
    <a href="https://www.antebox.com/deal/{{$deal->id}}" style="font-size:16px; font-weight: bold; font-family:sans-serif; text-decoration: none; line-height:40px; width:100%; display:inline-block">
    <span style="color: #ffffff;">
      Send a Message
    </span>
    </a>
  </td>
  </tr> </table>
<br>
We wish you have safe travel.<br><br>
Thanks,<br>
The Antebox Team.</p></font><hr>
 </td>
</tr>
</table>

<table>
<tr>
<td colspan="3">
<font color="#000000"><p>You might also be interested in:</p></font> <!-- raw 3 cells -->
</td>
</tr>
<tr>
@foreach($products as $product)
  <td width="33%">
<a href="https://www.antebox.com/product/view/{{$product->id}}"><img src="{{ URL::asset('img/products/'.$product->pic_url) }}" alt="{{$product->name}}" width="100%"/></a>
  </td>
@endforeach
</tr>
<tr>
@foreach($products as $product)
  <td width="33%">
<p><font color="#000000" align="center"><u>{{$product->name}}</u></font></p>
  </td>
@endforeach
</tr>
</table>


</td>
 </tr>

<tr>
<td>
 <div class="content center text-center" style='font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;padding:15px;max-width:600px;margin:0 auto;display:block;padding-right:5px;padding-bottom:5px;padding-top:0px;color:#9ca299;font-size:14px;text-align:center;text-shadow:0 1px #ffffff;padding-left:5px'>
      <table cellpadding="10" style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;border-spacing:0;line-height:150%;width:100%;padding: 10px; Margin: auto;' bgcolor="#D0D3CA" >
        <tr style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif'>
          <td align="center" style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif'>
            <table cellpadding="5" style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;border-spacing:0;line-height:150%;width:100%;width: auto;'>
              <tr style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif'>
                <td style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;padding: 5px;'>
                  <a href="https://www.facebook.com/AnteBox" title="Facebook" style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;color:#ff5a5f;text-decoration:none'>
                    <img alt="Facebook" height="42" src="{{ URL::asset('img/email/facebook.png') }}" width="42" style='margin:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;padding:0;max-width:100%;border:0'>
</a>                </td>

                <td style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;padding: 5px;'>
                  <a href="https://twitter.com/anteboxapp" title="Twitter" style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;color:#ff5a5f;text-decoration:none'>
                    <img alt="Twitter" height="42" src="{{ URL::asset('img/email/twitter.png') }}" width="42" style='margin:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;padding:0;max-width:100%;border:0'>
</a>                </td>

                <td style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;padding: 5px;'>
                  <a href="https://www.instagram.com/antebox/" title="Instagram" style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;color:#ff5a5f;text-decoration:none'>
                    <img alt="Instagram" height="42" src="{{ URL::asset('img/email/instagram.png') }}" width="42" style='margin:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;padding:0;max-width:100%;border:0'>
</a>                </td>

              </tr>
            </table>
          </td>
        </tr>
      </table>

  <table class="row" style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;border-spacing:0;line-height:150%;width:100%' bgcolor="#D0D3CA" >
  <tr style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif'>
    <td style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif'></td>
    <td class="container" style='padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;display:block !important;margin:0 auto !important;clear:both !important;max-width:610px !important'>
      <div class="content" style='font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;padding:15px;max-width:600px;margin:0 auto;display:block;padding-right:5px;padding-bottom:5px;padding-top:0px;padding-left:5px;color:#9ca299;font-size:14px;text-align:center;text-shadow:0 1px #ffffff'>
        AnteBox
      </div>
    </td>
    <td style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif'></td>
  </tr>
  <tr style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif'>
    <td style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif'></td>
    <td class="container" style='padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;display:block !important;margin:0 auto !important;clear:both !important;max-width:610px !important'>
      <div class="content" style='font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;padding:15px;max-width:600px;margin:0 auto;display:block;padding-right:5px;padding-bottom:5px;padding-top:0px;padding-left:5px;color:#9ca299;font-size:14px;text-align:center;text-shadow:0 1px #ffffff'>
                  <a class=3D"muted" href="https://www.antebox.com/profile/notifications" style=3D'ma=
rgin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Aria=
l, sans-serif;color:#9ca299;text-decoration:underline'>
          Email preferences
          </a>
      </div>
    </td>
    <td style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif'></td>
  </tr>
</table>
</td>
</tr>

</table>

 


  </div>
</body>
</html>
