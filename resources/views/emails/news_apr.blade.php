<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>AnteBox</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body style="margin: 0; padding: 0;">
<table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
 <tr>
<td align="center" bgcolor="#225680" style="padding: 40px 0 10px 0;">
 <img src="{{ URL::asset('img/email/logo.png') }}"  alt="AnteBox" width="192" height="62" style="display: block;" />
 <font color="#ffffff"><h3>April 2016</h3></font>
</td>
 </tr>

<td >
<tr>
  <td align="left" bgcolor="#D0D3CA" style="padding: 20px 0 0px 0;">
<ul style="list-style-type:none">
  <li><img src="{{ URL::asset('img/email/tick.png') }}" alt="+"/><strong><small><font color="#000000"> Register and make a profile</font></small></strong></li>
  <li><img src="{{ URL::asset('img/email/tick.png') }}" alt="+"/><strong><small><font color="#000000"> Add a trip</font></small></strong></li>
  <li><img src="{{ URL::asset('img/email/untick.png') }}" alt="+"/><strong><small><font color="#000000"> <u>Request a product from 01/06/2016</u></font></small></strong></li>
</ul>  
<br>
  </td>
</tr>
</td>
 <tr>


<td bgcolor="#f0e1d3" style="padding: 20px 30px 40px 30px;">
 <table border="0" cellpadding="0" cellspacing="0" width="100%">



  <tr>
   <td>
   <font color="#000000"><strong><u>22/03/2016 CitySpark Grand Final</u></strong></font>
   </td>
  </tr>
  <tr>
     <td>
    <font color="#000000"><p>Part of the top 10 finalist for the year, attracted the support of the audience with more than 1000 views on that date.</p></font>
  <td width="40%">
    <img src="{{ URL::asset('img/email/CitySparkFinal2016.jpg') }}" alt="CitySpark2016" height="150px" width="200px"/>
  </td>
 </td>
</tr>

  <tr>
   <td style="padding: 20px 0 30px 0;">
 <font color="#000000"><strong><u>4-5/05/2016 Home Delivery Europe 2016</u></strong></font>
   </td>
  </tr>
 <tr>
     <td>
    <font color="#000000"><p>AnteBox will presenting alongside Alibaba and Walmart as part of the innovation zone in Home Delivery Europe 2016!</p></font>
  <td width="30%">
    <img src="{{ URL::asset('img/email/HomeDelivery2016.jpg') }}" alt="HomeDelivery2016" height="150px" width="200px"/>
  </td>
 </td>
</tr>


  <tr>
   <td style="padding: 20px 0 30px 0;">
 <font color="#000000"><strong><u>09/06/2016 Made @ City</u></strong></font>
   </td>
  </tr>
 <tr>
     <td>
    <font color="#000000"><p>Talking about AnteBox as part of City University London greatest projects for 2016 @madeatcity</p></font>
  <td width="30%">
    <img src="{{ URL::asset('img/email/madeAtCity2016.jpg') }}" alt="MadeAtCity2016" height="150px" width="200px"/>
  </td>
 </td>
</tr>

  <tr>
   <td style="padding: 20px 0 30px 0;">
 <font color="#000000"><strong><u>4-15/07/2016 CitySpark incubator</u></strong></font>
   </td>
  </tr>
 <tr>
     <td>
    <font color="#000000"><p>As part of stage 3 of CitySpark, AnteBox will be receiving training and help from CASS Entrepreneurial team and then pitch to investors.</p></font>
  <td  width="30%">
    <img src="{{ URL::asset('img/email/CitySparkInc.jpg') }}" alt="CitySparkIncubator2016" height="150px" width="200px"/>
  </td>
 </td>
</tr>


  <tr>
   <td style="padding: 20px 0 30px 0;">
 <font color="#000000"><strong><u>7-10/11/2016 Web Summit Lisbon</u></strong></font>
   </td>
  </tr>
 <tr>
     <td>
    <font color="#000000"><p>In 5 years, Web Summit has grown from 400 attendees to over 42000 from more than 134 countries. It's been called "the best technology conference on the planet".</p></font>
  <td width="30%">
    <img src="{{ URL::asset('img/email/webSummit2016.png') }}" alt="WebSummit2016" height="150px" width="200px"/>
  </td>
 </td>
</tr>



 </table>
<table>
  <tr>
   <td style="padding: 20px 0 0px 0;" >
    <font color="#000000"><p>Do you have a unique product and you want to increase your store traffic and customer engagement? We are now starting to work with businesses that have unique products. A company working with us will be able to share their story and make their products available to our users from all around the world that can request the products and get them delivered by travellers; people visiting the country the business is operating in can be directed to buy the product from the business's premises or send them to chosen retailers to buy the product.<br>
<br>
Thanks,<br>
The Antebox Team.</p></font>
 </td>
</tr>
</table>

<table>
<tr>
  <td>
<img src="{{ URL::asset('img/email/pal.jpg') }}" alt="antebox" height="100px" width="170px"/>
  <td>
  <td>
<img src="{{ URL::asset('img/email/shu.jpg') }}" alt="antebox" height="100px" width="170px"/>
  <td>
  <td>
<img src="{{ URL::asset('img/email/xal.jpg') }}" alt="antebox" height="100px" width="170px"/>
  <td>
</tr>
</table>


</td>
 </tr>

<tr>
<td>
<div class="content center text-center" style='font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;padding:15px;max-width:600px;margin:0 auto;display:block;padding-right:5px;padding-bottom:5px;padding-top:0px;color:#9ca299;font-size:14px;text-align:center;text-shadow:0 1px #ffffff;padding-left:5px'>
      <table cellpadding="10" style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;border-spacing:0;line-height:150%;width:100%;padding: 10px; Margin: auto;' bgcolor="#D0D3CA" >
        <tr style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif'>
          <td align="center" style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif'>
            <table cellpadding="5" style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;border-spacing:0;line-height:150%;width:100%;width: auto;'>
              <tr style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif'>
                <td style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;padding: 5px;'>
                  <a href="https://www.facebook.com/AnteBox" title="Facebook" style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;color:#ff5a5f;text-decoration:none'>
                    <img alt="Facebook" height="42" src="{{ URL::asset('img/email/facebook.png') }}" width="42" style='margin:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;padding:0;max-width:100%;border:0'>
</a>                </td>

                <td style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;padding: 5px;'>
                  <a href="https://twitter.com/anteboxapp" title="Twitter" style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;color:#ff5a5f;text-decoration:none'>
                    <img alt="Twitter" height="42" src="{{ URL::asset('img/email/twitter.png') }}" width="42" style='margin:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;padding:0;max-width:100%;border:0'>
</a>                </td>

                <td style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;padding: 5px;'>
                  <a href="https://www.instagram.com/antebox/" title="Instagram" style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;color:#ff5a5f;text-decoration:none'>
                    <img alt="Instagram" height="42" src="{{ URL::asset('img/email/instagram.png') }}" width="42" style='margin:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;padding:0;max-width:100%;border:0'>
</a>                </td>

              </tr>
            </table>
          </td>
        </tr>
      </table>

  <table class="row" style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;border-spacing:0;line-height:150%;width:100%' bgcolor="#D0D3CA" >
  <tr style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif'>
    <td style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif'></td>
    <td class="container" style='padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;display:block !important;margin:0 auto !important;clear:both !important;max-width:610px !important'>
      <div class="content" style='font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;padding:15px;max-width:600px;margin:0 auto;display:block;padding-right:5px;padding-bottom:5px;padding-top:0px;padding-left:5px;color:#9ca299;font-size:14px;text-align:center;text-shadow:0 1px #ffffff'>
        AnteBox
      </div>
    </td>
    <td style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif'></td>
  </tr>
  <tr style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif'>
    <td style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif'></td>
    <td class="container" style='padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;display:block !important;margin:0 auto !important;clear:both !important;max-width:610px !important'>
      <div class="content" style='font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;padding:15px;max-width:600px;margin:0 auto;display:block;padding-right:5px;padding-bottom:5px;padding-top:0px;padding-left:5px;color:#9ca299;font-size:14px;text-align:center;text-shadow:0 1px #ffffff'>
                  <a class=3D"muted" href="https://www.antebox.com/profile/notifications" style=3D'ma=
rgin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Aria=
l, sans-serif;color:#9ca299;text-decoration:underline'>
          Email preferences
          </a>
      </div>
    </td>
    <td style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif'></td>
  </tr>
</table>
</td>
</tr>

</table>

  



  </div>
</body>
</html>
