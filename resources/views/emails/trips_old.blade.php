<h1>AnteBox</h1>
<br>
<p>You can now receive special offers when registering your trip at <a href="https://www.antebox.com">AnteBox</a>.</p>
<p>We are working on it, and soon you will also be able to make extra cash by delivering a product from the country or to the country you are visiting.</p>
<p>Do not forget, we are working only with unique products, so get ready to experience a unique trip while visiting special stores by registering it now on <a href="https://www.antebox.com/trips/create">Create a Trip</a></p>
<p>Thank you for creating your profile, travelling made unique again!</p>
<br/>
<p>Do you have your own unique business and want to offer a discount to travellers? Send us an email now at
<a href="mailto:contact@antebox.com">contact@antebox.com</a></p>
<br/>
<p>As new functions are in place, the terms have also been updated. You should review these terms in full on your own at
<a href="https://www.antebox.com/policy">our policy</a>.</p>
<br/>
<p>Thanks,<br/>
The AnteBox Team</p>

<p>
<a href="http://www.antebox.com/"><img src="{{ URL::asset('img/email/logo.png')}}"" width="96" height="31"/></a>
<br/>
<a href="https://www.facebook.com/AnteBox"><img src="{{ URL::asset('img/email/facebook.png')}}" width="32" height="32"/></a>
<a href="https://twitter.com/anteboxapp"><img src="{{ URL::asset('img/email/twitter.png')}}" width="32" height="32"/></a>
<a href="https://www.instagram.com/antebox/"><img src="{{ URL::asset('img/email/instagram.png')}}" width="32" height="32"/></a>
</p>
