<html xmlns="http://www.w3.org/1999/xhtml" style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;background-color:#f7f7f7;font-size:16px;color:#565a5c;line-height:150%'>

  <head style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif'>
 
  
</head>


  <body bgcolor="#EEEEEE" style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;-webkit-text-size-adjust:none;-webkit-font-smoothing:antialiased;height:100%;line-height:150%;font-size:16px;color:#565a5c;background-color:#f7f7f7;width:100% !important'>
    <div class="preheader" style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;height:0;visibility:hidden;opacity:0;color:transparent;width:0;display:none !important'></div>
    <table class="body-wrap" style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;line-height:150%;border-spacing:0;background-color:#f7f7f7;width:100%'>
      <tr style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif'>
        <td style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif'></td>
        <td class="container" style='padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;display:block !important;margin:0 auto !important;clear:both !important;max-width:610px !important'>
          <div class="content" style='font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;padding:15px;max-width:600px;margin:0 auto;display:block;padding-left:5px;padding-right:5px;padding-bottom:5px;padding-top:0px'>
            <table class="head-wrap" style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;line-height:150%;border-spacing:0;margin-bottom:10px;margin-top:10px;width:100%'>
  <tr style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif'>
    <td style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif'></td>
    <td class="container header" style='padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;display:block !important;margin:0 auto !important;clear:both !important;max-width:610px !important'>
      <div class="content" style='font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;padding:15px;max-width:600px;margin:0 auto;display:block;padding-left:5px;padding-right:5px;padding-bottom:5px;padding-top:0px'>
        <table style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;border-spacing:0;line-height:150%;width:100%'>
          <tr style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif'>
            <td style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;text-align: center;'>
<br>
          <a href="http://www.antebox.com" title="AnteBox" style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;color:#ff5a5f;text-decoration:none'>
                <img src="{{ URL::asset('img/email/logo.png')}}" border="0" alt="AnteBox" width="123" height="55" style='margin:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;padding:0;max-width:100%;border:0'>
              </a>
            </td>
          </tr>
        </table>
      </div>
    </td>
    <td style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif'></td>
  </tr>
</table>

            <div class="section " style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif'>

  <div class="p " style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;'>

    Hello {{$first_name}},

</div>


  <div class="p " style='margin:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;padding:0;margin-top:1em;'>

  Thank you for registering and being part of our great community, that was built to support local businesses.

</div>

  <div class="p " style='margin:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;padding:0;margin-top:1em;'>

  Did you know that you can now <a href="https://www.antebox.com/trips">register your trip</a>, <a href="https://www.antebox.com/">request products</a> and <a href="https://www.antebox.com/request">make bids</a> to deliver products to fellow users in the community?

</div>

  <div class="p " style='margin:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;padding:0;margin-top:1em;'>

  With all these new and exciting additions, we have also updated our <a href="https://www.antebox.com/terms">terms and conditions</a>.

</div>


<br>
  <div class="p " style='margin:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;padding:0;margin-top:1em;'>

    Thanks,<br style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif'>The AnteBox Team

</div>


<br style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif'><br style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif'>

          </div>
        </td>
        <td style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif'></td>
      </tr>

      <tr style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif'>
        <td style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif'></td>
        <td class="container no-max-width" style='padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;display:block !important;margin:0 auto !important;clear:both !important;max-width:610px !important'>
          <div class="section footer" style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif'>

  <div class="content center text-center" style='font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;padding:15px;max-width:600px;margin:0 auto;display:block;padding-right:5px;padding-bottom:5px;padding-top:0px;color:#9ca299;font-size:14px;text-align:center;text-shadow:0 1px #ffffff;padding-left:5px'>
      <table cellpadding="10" style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;border-spacing:0;line-height:150%;width:100%;padding: 10px; Margin: auto;'>
        <tr style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif'>
          <td align="center" style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif'>
            <table cellpadding="5" style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;border-spacing:0;line-height:150%;width:100%;width: auto;'>
              <tr style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif'>
                <td style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;padding: 5px;'>
                  <a href="https://www.facebook.com/AnteBox" title="Facebook" style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;color:#ff5a5f;text-decoration:none'>
                    <img alt="Facebook" height="42" src="{{ URL::asset('img/email/facebook.png')}}" width="42" style='margin:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;padding:0;max-width:100%;border:0'>
</a>                </td>

                <td style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;padding: 5px;'>
                  <a href="https://twitter.com/anteboxapp" title="Twitter" style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;color:#ff5a5f;text-decoration:none'>
                    <img alt="Twitter" height="42" src="{{ URL::asset('img/email/twitter.png')}}" width="42" style='margin:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;padding:0;max-width:100%;border:0'>
</a>                </td>

                <td style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;padding: 5px;'>
                  <a href="https://www.instagram.com/antebox/" title="Instagram" style='margin:0;padding:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;color:#ff5a5f;text-decoration:none'>
                    <img alt="Instagram" height="42" src="{{ URL::asset('img/email/instagram.png')}}" width="42" style='margin:0;font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;padding:0;max-width:100%;border:0'>
</a>                </td>

              </tr>
            </table>
          </td>
        </tr>
      </table>

AnteBox
  </div>

</body>
</html>
