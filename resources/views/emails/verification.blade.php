<h1>AnteBox</h1>
<br>
<p>
Hi {{ Auth::user()->first_name }},<br>
Welcome to AnteBox! In order to get started, you need to confirm your email address.
</p>

<table cellspacing="0" cellpadding="0"> <tr>
  <td align="center" width="300" height="40" bgcolor="#225680" style="-webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; color: #ffffff; display: block;">
    <a href="http://www.antebox.com/verify/{{ $user_id }}/{{ $token }}" style="font-size:16px; font-weight: bold; font-family:sans-serif; text-decoration: none; line-height:40px; width:100%; display:inline-block">
    <span style="color: #ffffff;">
      Confirm Email
    </span>
    </a>
  </td>
  </tr> </table>

<br/>

<p>Thanks,<br/>
The AnteBox Team</p>

<p>
<a href="http://www.antebox.com/"><img src="{{ URL::asset('img/email/logo.png')}}"" width="96" height="31"/></a>
<br/>
<a href="https://www.facebook.com/AnteBox"><img src="{{ URL::asset('img/email/facebook.png')}}" width="32" height="32"/></a>
<a href="https://twitter.com/anteboxapp"><img src="{{ URL::asset('img/email/twitter.png')}}" width="32" height="32"/></a>
<a href="https://www.instagram.com/antebox/"><img src="{{ URL::asset('img/email/instagram.png')}}" width="32" height="32"/></a>
</p>
