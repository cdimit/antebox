<h1>AnteBox</h1>
<br>
<p>Thank you for creating your profile.</p>
<p>Browse through the unique selection of products from around the world and request the one you want!</p> 
<p>Travelling? You can also add your trips and get notified if there is a request you can fulffil</p>
<br/>

<p>Thanks,<br/>
The AnteBox Team</p>

<p>
<a href="http://www.antebox.com/"><img src="{{ URL::asset('img/email/logo.png')}}"" width="96" height="31"/></a>
<br/>
<a href="https://www.facebook.com/AnteBox"><img src="{{ URL::asset('img/email/facebook.png')}}" width="32" height="32"/></a>
<a href="https://twitter.com/anteboxapp"><img src="{{ URL::asset('img/email/twitter.png')}}" width="32" height="32"/></a>
<a href="https://www.instagram.com/antebox/"><img src="{{ URL::asset('img/email/instagram.png')}}" width="32" height="32"/></a>
</p>

