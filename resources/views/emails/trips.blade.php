<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>AnteBox</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body style="margin: 0; padding: 0;">
<table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
 <tr>
<td align="center" bgcolor="#f0e1d3" style="padding: 40px 0 10px 0;">
 <img src="{{ URL::asset('img/email/logo.png') }}"  alt="AnteBox" width="192" height="62" style="display: block;" />
</td>
 </tr>
 <tr>
<td bgcolor="#f0e1d3" style="padding: 40px 30px 40px 30px;">
 <table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tr>
   <td align="center">
   <font color="#005aa9"><strong><h2>Great News! Register your trip</h2></strong></font>
   </td>
  </tr>
  <tr>
     <td style="padding: 20px 0 30px 0;">
    <font color="#010282"><p><strong>You can now receive special offers when registering your trip at <a href="https://www.antebox.com">AnteBox</a>.</strong></p></font>
<font color="#010282"><p><strong>We are working on it, and soon you will also be able to make extra cash by delivering a product from the country or to the country you are visiting.</strong></p></font>
<font color="#010282"><p><strong>Do not forget, we are working only with unique products, so get ready to experience a unique trip while visiting special stores by registering it now on <a href="https://www.antebox.com/trips/create">Create a Trip</a></strong></p></font>
<font color="#010282"><p><strong>Thank you for creating your profile, travelling made unique again!</strong></p></font>

   </td>
  </tr>
  <tr>
   <td style="padding: 20px 0 30px 0;">
    <font color="#010282"><p><strong>Do you have your own unique business and want to offer a discount to travellers? Send us an email now at
<a href="mailto:contact@antebox.com">contact@antebox.com</a></strong></p></font>

<font color="#010282"><p><strong>As new functions are in place, the terms have also been updated. You should review these terms in full on your own at
<a href="https://www.antebox.com/policy">our policy</a>.</strong></p></font>
<br/>

   </td>
  </tr>
<tr>
<td>
<font color="#010282"><p><strong>Thanks,<br/>
The AnteBox Team</strong></p></font>
</td>
</tr>
 </table>
<table>
 <tr >
   <td>
    <a href="https://www.facebook.com/AnteBox">
     <img src="{{ URL::asset('img/email/facebook.png') }}" alt="Facebook" width="38" height="38" style="display: block;" border="0" />
    </a>
   </td>
   <td>
    <a href="https://twitter.com/anteboxapp">
     <img  src="{{ URL::asset('img/email/twitter.png') }}" alt="Twitter" width="38" height="38" style="display: block;" border="0" />
    </a>
   </td>
<td>

    <a href="https://www.instagram.com/antebox/">
     <img  src="{{ URL::asset('img/email/instagram.png') }}" alt="Instagram" width="38" height="38" style="display: block;" border="0" />
    </a>
   </td>


  </tr>
</table>
</td>
 </tr>
</table>
</body>

</html>
