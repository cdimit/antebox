@extends('app')

@section('content')
  <div class="container">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-12">
          <span class="text-brand-blue" style="font-size: 78px;">Auctions</span>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4 col-md-offset-8 text-right">
          <a class="text-brand-brown" href="/about/auctions">Read more about auctions <i class="fa fa-chevron-circle-right"></i></a>
        </div>
      </div>
      <div class="row">
        The user nav
      </div>
      <div style="border-top: 10px solid #163850;">
        <div class="row" style="padding-top: 100px;">
        </div>
        @foreach($auctions as $auction)
          @include('auctions.listitem')
          <div class="row" style="padding-top: 100px;">
          </div>
        @endforeach
      </div>
    </div>
    <div class="row" style="padding: 20px;">
    </div>
  </div>
@endsection

@section('plugins')
@endsection

@section('scripts')

@endsection
