@extends('app')

@section('content')
  <div class="container">
    <div class="col-md-12 col-xs-hidden">
      <div class="row">
        <div class="col-md-12">
          <span class="text-brand-blue huge-title">Auctions</span>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4 col-md-offset-8 text-right">
          <a class="text-brand-brown" href="/about/auctions">Read more about auctions <i class="fa fa-chevron-circle-right"></i></a>
        </div>
      </div>
      <div class="row">
        <hr>
      </div>
      <div class="brand-border">
        <div class="row">
          <div class="col-sm-12 text-center">
            <span class="text-brand-blue" style="font-size: 30px; font-weight:bolder;">CREATE YOUR AUCTION</span>
          </div>
        </div>
        <div class="row" style="padding-bottom: 30px;">
            <form role="form-horizontal" method="POST" action="{{ url('/auctions/save')}}">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <input type="hidden" name="user_id" value="{{ Auth::id() }}">
              <input type="hidden" name="meeting_x">
              <input type="hidden" name="meeting_y">
              <input type="hidden" name="custom_parcel_size" value="0">
              <input id="small-parcel-radio" type="radio" class="parcel-size-radio" name="parcel_size" value="Small">
              <input id="medium-parcel-radio" type="radio" class="parcel-size-radio" name="parcel_size" value="Medium">
              <input id="large-parcel-radio" type="radio" class="parcel-size-radio" name="parcel_size" value="Large">
              <input id="custom-parcel-radio" type="radio" class="parcel-size-radio" name="parcel_size" value="Custom">
              <div class="row">
                <!-- first section of the form -->
                @if (count($errors) > 0)
                  <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                      @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                  </div>
                @endif
                <div class="form-group" style="padding-top: 30px;">
                  <div class="col-sm-6 col-sm-offset-3">
                    <label style="text-align:left;" class="control-label text-brand-blue bold">What are you looking for today?</label>
                    <input type="text" class="form-control" name="item_name" value="{{ old('item_name') }}">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-6 col-sm-offset-3" style="padding-top: 30px;">
                    <label style="text-align:left;" class="control-label text-brand-blue bold">Could you provide a description of your item?</label>
                    <label style="text-align:left; padding-left:20px;" class="control-label text-brand-blue">Don't be shy! The more detailed the better.</label>
                    <textarea rows=4 class="form-control" name="description">{{ old('description') }}</textarea>
                  </div>
                </div>
                <div class="form-group text-center">
                  <div class="col-md-12 ">
                    <div class="row" style="padding-top: 50px;">
                      <div class="col-sm-6 text-right">
                        <i style="font-size: 60px;color:green;" class="fa fa-check"></i>
                      </div>
                      <div class="col-sm-6 text-left">
                        <h1 style="display: inline-block" class="text-brand-blue">Great!</h1>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- second section of the form -->
              </div>
              <div class="row">
                <div class="form-group">
                  <div class="col-md-12 text-brand-blue text-center" style="padding-top: 30px;">
                    <p>Now that you've specified what you want, lets look at some logistical details.</p>
                  </div>
                </div>
                <!-- Parcel size selection -->
                <div class="row" style="padding: 30px 0">
                  <div id="small-parcel" class="col-sm-6 col-md-6 col-lg-3 size-select-box">
                    <div class="row text-center parcel-size-heading">
                      Small
                    </div>
                    <div class="row text-center parcel-size-attr">
                      23cm x 20cm x 10cm
                    </div>
                    <div class="row text-center">
                      <img class="small-parcel" src="{{ URL::asset('img/app/line/origin-icon.svg')}}">
                    </div>
                    <div class="row">
                      <p class="text-justify parcel-size-attr">Mobile phones, tablets, microphones, books, bottles</p>
                    </div>
                  </div>

                  <div id="medium-parcel" class="col-sm-6 col-md-6 col-lg-3 size-select-box">
                    <div class="row text-center parcel-size-heading">
                      Medium
                    </div>
                    <div class="row text-center parcel-size-attr">
                      45cm x 40cm x 20cm
                    </div>
                    <div class="row text-center">
                      <img class="medium-parcel" src="{{ URL::asset('img/app/line/origin-icon.svg')}}">
                    </div>
                    <div class="row">
                      <p class="text-justify parcel-size-attr">Laptops, monitors, consoles, souvenirs</p>
                    </div>
                  </div>

                  <div id="large-parcel" class="col-sm-6 col-md-6 col-lg-3 size-select-box">
                    <div class="row text-center parcel-size-heading">
                      Large
                    </div>
                    <div class="row text-center parcel-size-attr">
                      55cm x 45cm x 25cm
                    </div>
                    <div class="row text-center">
                      <img class="large-parcel" src="{{ URL::asset('img/app/line/origin-icon.svg')}}">
                    </div>
                    <div class="row">
                      <p class="text-justify parcel-size-attr">Some stuff</p>
                    </div>
                  </div>

                  <div id="custom-parcel" class="col-sm-6 col-md-6 col-lg-3 size-select-box">
                    <div class="row text-center parcel-size-heading">
                      Custom
                    </div>
                    <div class="row text-center parcel-size-attr">
                      Have it your way!
                    </div>
                    <div class="row text-center">
                      <img class="large-parcel" src="{{ URL::asset('img/app/line/origin-icon.svg')}}">
                    </div>
                    <div class="row text-center parcel-size-attr">
                      You specify the dimensions!
                    </div>
                    <div class="row text-center">
                        <input id="size_h" name="size_h" style="width: 18%;color: #000 !important"/> cm x
                        <input id="size_w" name="size_w" style="width: 18%;color: #000 !important"/> cm x
                        <input id="size_d" name="size_d" style="width: 18%;color: #000 !important"/> cm
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-4">
                    <div class="row text-center">
                      <label class="control-label text-center text-brand-blue bold">Origin</label>
                      <select class="form-control" name="origin" value="{{ old('origin') }}">
                        <option value="ANYWHERE">I don't mind</option>
                        @include('address.countries')
                      </select>
                    </div>
                    <div id="gmap-origin" class="row">

                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="row text-center">
                      <label class="control-label text-center text-brand-blue bold">Destination</label>
                      <select class="form-control" name="destination" value="{{ old('destination') }}">
                        <option value="NONE">Please select country</option>
                        @include('address.countries')
                      </select>
                    </div>

<input type="text" id="search_address" value=""/>
<button onclick="search();">Search</button>

                    <div id="gmap-destination" class="row" style="margin-top: 10px; min-height: 200px;">


                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="row text-center">
                      <label class="control-label text-center text-brand-blue bold">Required By Time</label>
                      <input type="hidden" class="form-control" placeholder="Time" name="deadline" value="{{ old('deadline') }}">
                      <div id="deadline-timepicker"></div>
                    </div>
                  </div>
                </div>
                <div class="form-group text-center text-brand-blue">
                  <div class="col-sm-12" style="padding-top: 50px;">
                    <p>Once your auction has been created, it will be publically visibile on the AnteBox website.</p>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-4 col-sm-offset-4" style="padding-top: 30px;">
                    <button class="btn btn-primary btn-big">PUBLISH</button>
                  </div>
                </div>
              </div>
            </form>
        </div>
      </div>
      <div class="row" style="padding: 20px;">
      </div>
    </div>
  </div>
@endsection

@section('plugins')
  <link rel="stylesheet" href="{{ URL::asset('css/bootstrap-datetimepicker.min.css') }}"/>
@endsection

@section('scripts')
  <script src="{{ URL::asset('/js/moment.min.js') }}"></script>
  <script type="text/javascript" charset="UTF-8" src="{{ URL::asset('js/bootstrap-datetimepicker.js') }}"></script>
  <!--<script src="http://maps.googleapis.com/maps/api/js"></script>-->
<script
src="https://maps.google.com/maps/api/js?libraries=places&sensor=true">
</script>

<script>
var addressField = document.getElementById('search_address');
var geocoder = new google.maps.Geocoder();
function search() {
    geocoder.geocode(
        {'address': addressField.value}, 
        function(results, status) { 
            if (status == google.maps.GeocoderStatus.OK) { 
                var loc = results[0].geometry.location;
                // use loc.lat(), loc.lng()
            } 
            else {
                alert("Not found: " + status); 
            } 
        }
    );
};
</script>


  <script src="{{ URL::asset('js/pages/create-auction.js') }}"></script>
@endsection
