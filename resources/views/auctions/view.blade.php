@extends('app')

@section('content')
  <div class="container">
    <div class="col-sm-12">
      <div class="brand-border">
        <div class="row" style="padding-top: 30px;">
          <div class="col-xs-12 col-sm-6">

            <span class="text-brand-blue auction-title">{{ $auction->item_name }}</span>
            <div class="row">
              <span class="auction-bids">( {{ $auction->bid_count }} bids )</span>
            </div>
          </div>
          <div class="col-xs-12 col-sm-3 col-sm-offset-3 text-left" style="padding-top: 30px;">
            <a href="#"><img class="profile-photo" src="https://scontent-lhr3-1.xx.fbcdn.net/hprofile-xpt1/v/t1.0-1/p50x50/12109116_10153631203344437_7112134705438496255_n.jpg?oh=8503ee92150895b1b980c5b3547acdd2&oe=5703BFC2">  Michalis Strouthos</a>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12">
            @if($auction->ended)
              <span id="ends-in" class="head">This auction has ended!</span>
            @else
              <span class="head">Ends in: </span><span id="ends-in" class="head">{{ $auction->ends_days }} days, {{ $auction->ends_hours }} hours and {{ $auction->ends_minutes}} minutes</span>
            @endif
          </div>
        </div>
        <div class="row" style="padding-top: 30px; padding-bottom: 20px;">
          <div class="col-xs-12 col-sm-6">
            <span>
              {{ $auction->description }}
            </span>
            <div class="row">
              <img class="country-flag" src="{{ URL::asset('/img/flags/flat/64/Cyprus.png')}}">
              <i style="font-size:20px;" class="fa fa-arrow-right"></i>
              <img class="country-flag" src="{{ URL::asset('/img/flags/shiny/64/United-Kingdom.png')}}">
            </div>
            <div class="row">
              <h3>{{ $auction->parcel_size }} Parcel</h3>
              <img class="small-parcel" src="{{ URL::asset('img/app/line/origin-icon.svg') }}">
              <span>23cm x 20cm x 10cm</span>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6">
            @if($auction->ended == false && Auth::id() != $auction->user_id)
              <div class="row">
                <form id="bid-form" role="form">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <input type="hidden" name="auction_id" value="{{ $auction->id }}">
                  <input type="hidden" name="meeting_x" value="{{ $auction->meeting_x }}">
                  <input type="hidden" name="meeting_y" value="{{ $auction->meeting_y }}">
                  <div class="col-xs-12 col-sm-6 bid-box">
                    <div id="bid-errors" class="row">
                    </div>
                    <div class="row form-group">
                      <div class="input-group">
                        <span class="input-group-addon">£</span>
                        <input type="text" name="amount" class="form-control">
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group">
                        <select name="expires" class="form-control">
                          <option value="0">Select Bid Expiry</option>
                          <option value="2">2 hours</option>
                          <option value="4">4 hours</option>
                          <option value="8">8 hours</option>
                          <option value="12">12 hours</option>
                          <option value="24">24 hours</option>
                        </select>
                      </div>
                    </div>
                    <div class="row bid-controls">
                      <div class="col-xs-4">
                        <i id="bid-tip-icon" style="font-size: 36px;" class="fa fa-info-circle text-brand-blue"></i>
                      </div>
                      <div class="col-xs-8">
                        <button class="btn btn-primary form-control">Bid</button>
                      </div>
                    </div>
                  </div>
                  <div id="bid-tip" class="col-xs-12 col-sm-6 tip">
                    <p class="text-justify" style="font-weight: 500;">
                      When bidding always remember that the amount you bid,
                      includes the cost of the product the customer has requested and
                      the fees you require for transferring the product to the customer.
                    </p>
                  </div>
                </form>
              </div>
            @endif
            <div id="auction-bids" class="row" style="padding-top: 20px;">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title text-brand-blue">Bids <span class="badge">{{ $auction->bid_count }}</span><i id="active-bids-icon" class="fa fa-chevron-down float-right"></i></h3>
                </div>
                <div class="panel-body" style="padding: 0;">
                  <ul id="active-bids" class="list-group" style="margin-bottom: 0; display: none;">
<!--                    @foreach($bids as $bid)
                    <li class="list-group-item">
                      <a href="#">{{ App\User::getNameFromId($bid->user_id) }}</a>
                      <div class="float-right">
                        <span class="bid-amount">£ {{ number_format($bid->amount, 2) }}</span>
                        @if(Auth::id() == $auction->user_id)
                          <a class="decline-bid" href="#">Decline</a>
                          <a class="accept-bid" href="#">Accept</a>
                        @endif
                      </div>
                    </li>
 -->                   @endforeach
                  </ul>
                </div>
              </div>

            </div>
          </div>
        </div>
        <div class="row">
          <h3>The item's destination area</h3>
          <div id="destination_area_map" style="min-height: 350px; min-width: 100%;"></div>
        </div>
      </div>
      <div class="row" style="padding: 20px;">
      </div>
    </div>
  </div>
@endsection

@section('plugins')

@endsection

@section('scripts')
<script
src="https://maps.google.com/maps/api/js?v=3.5&sensor=true">
</script>
  <script src="{{ URL::asset('js/pages/auction.js') }}"></script>
  @include('shared.map')
@endsection
