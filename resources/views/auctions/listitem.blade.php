<div class="row" style="position: absolute;width: 100%;">
  <div class="col-xs-8 col-xs-offset-2 preview-head">
    <div class="row">
      <div class="col-xs-1" style="padding-top: 5px;">
        <i class="auction-id fa fa-star-o"></i>
      </div>
      <div class="col-xs-5">
        <div class="row">
          <span class="auction-id">{{ $auction->id }}</span>
        </div>
        <div class="row">
          <i class="fa fa-user user-icon"></i><a class="user-link" href="#">{{ App\User::getNameFromId($auction->user_id) }}</a>
        </div>
        <div class="row auction-head-attr">
          {{ $auction->created_at }}
        </div>
      </div>
      <div class="col-xs-4 col-xs-offset-2">
        <div class="row">
          <a href="#">5 bids</a>
        </div>
        <div class="row">
          {{ $auction->deadline }}
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row preview-content">
  <div class="col-xs-10 col-xs-offset-1 preview-head-margin">
    <div class="row" style="padding-bottom: 50px;">
      <div class="col-xs-3">
        <div class="row">
          <img class="icon-full-div" src="{{ URL::asset('img/app/line/origin-icon.svg')}}">
        </div>
        <div class="row text-center">
          <span class="head">{{ $auction->item_name }}</span>
        </div>
        <div class="row text-center">
          <span class="head-country">From {{ $auction->origin }} <i class="fa fa-flag"></i></span>
        </div>
      </div>
      <div class="col-xs-6">
        <div class="row arrow-clock-aligned">
          <img class="arrow-clock" src="{{ URL::asset('img/app/line/timer.svg')}}">
          <img class="icon-full-div" src="{{ URL::asset('img/app/line/arrow-icon.svg')}}">
        </div>
        <div style="padding-top: 50px;" class="row text-center head">
          <span>3 days left</span>
        </div>
      </div>
      <div class="col-xs-3">
        <div class="row">
          <img class="icon-full-div" src="{{ URL::asset('img/app/line/destination-icon.svg')}}">
        </div>
        <div class="row text-center">
          <span class="head">{{ $auction->destination }}</span>
        </div>
        <div class="row text-center">
          <a class="view-map-link" href="#">View on map</span>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-4 col-xs-offset-4">
        <a href="/auctions/view/{{ $auction->id }}" class="btn btn-primary form-control content-view-control">View Auction</a>
      </div>
    </div>
  </div>
</div>
