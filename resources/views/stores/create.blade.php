@extends('app')

@section('content')
  <div class="container">
    <div class="col-md-12">

	  <div class="row">
        <div class="col-md-10 col-md-offset-2">
          <h1><span class="text-brand-brown">Create a Store!</span></h1>
        </div>
      </div>

 	@if (count($errors) > 0)
	  <div class="alert alert-danger">
  		<strong>Whoops!</strong> There were some problems with your input.<br><br>
  		<ul>
  			@foreach ($errors->all() as $error)
  				<li>{{ $error }}</li>
  			@endforeach
  		</ul>
  	  </div>
    	@endif

    </div>


	<form action="/admin/stores/createStore" method="post" enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		
		<div class="row" style="margin-bottom: 20px;">
			<div class="form-group">
				<label style="text-align: right; font-weight: bold; padding-top: 5px;" class="col-sm-4 control-label text-left">Name (*)</label>
				<div class="col-sm-6">
					<input type="text" class="form-control" name="name" value="{{ old('name') }}">
				</div>
			</div>
		</div>
		
		<div class="row" style="margin-bottom: 20px;">
			<div class="form-group">
				<label style="text-align: right; font-weight: bold; padding-top: 5px;" class="col-sm-4 control-label text-left">Phone</label>
				<div class="col-sm-6">
					<input type="text" class="form-control" name="phone" value="{{ old('phone') }}">
				</div>
			</div>
		</div>
		
		<div class="row" style="margin-bottom: 20px;">
			<div class="form-group">
				<label style="text-align: right; font-weight: bold; padding-top: 5px;" class="col-sm-4 control-label text-left">Address (*)</label>
				<div class="col-sm-6">
					<input type="text" class="form-control" name="address" value="">
				</div>
			</div>
		</div>
		
		<div class="row" style="margin-bottom: 20px;">
			<div class="form-group">
				<label style="text-align: right; font-weight: bold; padding-top: 5px;" class="col-sm-4 control-label text-left">City (*)</label>
				<div class="col-sm-6">
					<input type="text" class="form-control" name="city" value="">
				</div>
			</div>
		</div>
		
		<div class="row" style="margin-bottom: 20px;">
			<div class="form-group">
				<label style="text-align: right; font-weight: bold; padding-top: 5px;" class="col-sm-4 control-label text-left">Zip Code</label>
				<div class="col-sm-6">
					<input type="text" class="form-control" name="zip_code" value="">
				</div>
			</div>
		</div>
		
		<div class="row" style="margin-bottom: 20px;">
			<div class="form-group">
				<label style="text-align: right; font-weight: bold; padding-top: 5px;" class="col-sm-4 control-label text-left">Country (*)</label>
				<div class="col-sm-6">
                                        <select class="form-control" id="country" name="country"></select>
				</div>
			</div>
		</div>
		
		<div class="row" style="margin-bottom: 20px;">
			<div class="form-group">
				<label style="text-align: right; font-weight: bold; padding-top: 5px;" class="col-sm-4 control-label text-left">map_x</label>
				<div class="col-sm-6">
					<input type="text" class="form-control" name="map_x" value="">
				</div>
			</div>
		</div>
		
		<div class="row" style="margin-bottom: 20px;">
			<div class="form-group">
				<label style="text-align: right; font-weight: bold; padding-top: 5px;" class="col-sm-4 control-label text-left">map_y</label>
				<div class="col-sm-6">
					<input type="text" class="form-control" name="map_y" value="">
				</div>
			</div>
		</div>
				
		<div class="row" style="margin-bottom: 20px;">
			<div class="form-group">
				<div class="col-sm-2 col-sm-offset-8">
					<button type="submit" class="btn btn-primary form-control" style="width:100%;">Create</button>
				</div>
			</div>
		</div>

	</form>
  </div>


<script type="text/javascript" src="{{ URL::asset('js/countries.js') }}"></script>
<script language="javascript">print_country("country");</script>

@endsection
