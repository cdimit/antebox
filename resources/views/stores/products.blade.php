@extends('app')


@section('content')
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">

<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>


  <div class="container">

<div class="row-fluid">
    <div class="col-xs-12">



 <table class="table table-striped">
          <thead>
            <tr>
                <th>Name</th>
                <th>Category</th>
                <th>Country</th>
                <th>Options</th>
             </tr>
          </thead>
         <tbody>
         @foreach($store as $pro)
             <tr>
                <td>{{ $pro->name }}</td>
                <td>{{ $pro->category }}</td>
                <td>{{ $pro->company->country }}</td>
                <td><a href="remove/{{$pro->id}}" class="btn btn-danger">Remove</a>
</td>
             </tr>
         @endforeach
         </tbody>
      </table>

<br>
<hr>
        <table class="table table-striped" id="example">
          <thead>
            <tr>
                <th>Name</th>
                <th>Company</th>
		<th>Category</th>
                <th>Country</th>
		<th>Options</th>
             </tr>
          </thead>
         <tbody>
         @foreach($products as $pro)
             <tr>
                <td>{{ $pro->name }}</td>
                <td>{{ $pro->company->name }}</td>
		<td>{{ $pro->category }}</td>
                <td>{{ $pro->company->country }}</td>
		<td><a href="add/{{$pro->id}}" class="btn btn-info">Add</a>
</td>
             </tr>
         @endforeach
         </tbody>
    </table>

      </div>
    </div>

  </div>

<script>
$("#example").dataTable();
</script>



@endsection
