@extends('app')

@section('slideBanner')
<div class="row" style="margin:0; background-color: #fff">
	<div class="col-sm-4 col-sm-offset-4" style="display:none;color: #163850; font-size: 40px;" id="become-a-courier" class="courier">
			<center>
				<a href="#" style="color: #163850;text-decoration: none">Become a courier <i id="closeCourierBanner" class="fa fa-times-circle-o banner-close"></i></a>

			</center>
	</div>
</div>
@endsection

@section('content')
<section>
	<div class="row text-center" style="padding-bottom: 30px;">
		<div class="col-12">
			<h1>"Say hello to your global market"</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12 col-md-12 col-lg-8 col-lg-offset-2">
			<center>
				<div class="auctions-carousel">
					<div class="slick-slide" style="margin: 10px; width: 100px;">
						<div class="thumbnail" style="margin:0;">
				      <img style="height: 110px;width: auto;" src="{{ asset('/img/auction/Halloumi.jpg')}}" alt="">
				      <div class="caption" style="margin: 0">
				        <span style="font-size:20px;font-weight: bold;margin:0;">Halloumi Pittas</span>
				        <p style="margin:0;padding:0;"><a href="#" class="btn btn-primary" role="button">Request Item</a></p>
				      </div>
				    </div>
					</div>
					<div class="slick-slide" style="margin: 10px; width: 100px;">
						<div class="thumbnail" style="margin:0;">
				      <img style="height: 110px;width: auto;" src="{{ asset('/img/auction/anari_dry.jpg')}}" alt="">
				      <div class="caption" style="margin: 0">
				        <span style="font-size:20px;font-weight: bold;margin:0;">Anari Dry</span>
				        <p style="margin:0;padding:0;"><a href="#" class="btn btn-primary" role="button">Request Item</a></p>
				      </div>
				    </div>
					</div>
					<div class="slick-slide" style="margin: 10px; width: 100px;">
						<div class="thumbnail" style="margin:0;">
				      <img style="height: 110px;width: auto;" src="{{ asset('/img/auction/kefalotyri.jpg')}}" alt="">
				      <div class="caption" style="margin: 0">
				        <span style="font-size:20px;font-weight: bold;margin:0;">Kefalotyri Pittas</span>
				        <p style="margin:0;padding:0;"><a href="#" class="btn btn-primary" role="button">Request Item</a></p>
				      </div>
				    </div>
					</div>
					<div class="slick-slide" style="margin: 10px; width: 100px;">
						<div class="thumbnail" style="margin:0;">
				      <img style="height: 110px;width: auto;" src="{{ asset('/img/auction/Halloumi_Lite.jpg')}}" alt="">
				      <div class="caption" style="margin: 0">
				        <span style="font-size:20px;font-weight: bold;margin:0;">Halloumi Lite</span>
				        <p style="margin:0;padding:0;"><a href="#" class="btn btn-primary" role="button">Request Item</a></p>
				      </div>
				    </div>
					</div>
					<div class="slick-slide" style="margin: 10px; width: 100px;">
						<div class="thumbnail" style="margin:0;">
				      <img style="height: 110px;width: auto;" src="{{ asset('/img/auction/Trahanas.jpg')}}" alt="">
				      <div class="caption" style="margin: 0">
				        <span style="font-size:20px;font-weight: bold;margin:0;">Trahanas</span>
				        <p style="margin:0;padding:0;"><a href="#" class="btn btn-primary" role="button">Request Item</a></p>
				      </div>
				    </div>
					</div>
				</div>
			</center>

		</div>
	</div>
</section>
<section>
	<div class="row" style = "padding: 50px 0;">
		<div class="col-12">
			<center>
				<a href="{{ url('auctions/create') }}" class="btn btn-primary">CREATE AN AUCTION</a>
			</center>
		</div>
	</div>
</section>
<section class="branded-background">
	<div class="row" >
		<center>
			<div class="col-12">
				<input class="form-control inline-control" placeholder="Product to search for...."/>
				<input class="form-control inline-control" onfocus="(this.type='date')" placeholder="Date"/>
				<select class="form-control inline-control" value="Source Country">
					<option>From (Country)</option>
					<option>USA</option>
					<option>UK</option>
					<option>France</option>
					<option>Italy</option>
				</select>
				<select class="form-control inline-control">
					<option>To (Country)</option>
					<option>USA</option>
					<option>UK</option>
					<option>France</option>
					<option>Italy</option>
				</select>
				<button class="form-control inline-control">Search</button>
			</div>
		</center>

	</div>
</section>

<script type="text/javascript">
	$('document').ready(function(){
		$('#become-a-courier').slideDown(1500);
		$('#closeCourierBanner').click(function(){
			$('#become-a-courier').slideUp(1000);
		});

		$('.auctions-carousel').slick({
			autoplay: false,
			arrows:false,
			autoplaySpeed: 1500,
			dots: true,
		  infinite: false,
		  speed: 300,
		  slidesToShow: 4,
		  slidesToScroll: 1,
		  responsive: [
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 3,
		        slidesToScroll: 1,
					 	autoplay: false,
		        infinite: true,
		        dots: true
		      }
		    },
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 1
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }
		    // You can unslick at a given breakpoint now by adding:
		    // settings: "unslick"
		    // instead of a settings object
		  ]
		});
	});


</script>
@endsection
