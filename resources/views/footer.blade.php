
<footer class="footer-box">
	<div class="container">
		<div class="row text-center" style="padding-top: 40px;">
		  <img style="max-width: 240px; height: auto;" src="{{ URL::asset('/img/app/logo.png') }}"/>
		</div>

		<div class="row text-center" style="padding-top: 20px; padding-bottom: 20px;">
			<div class="col-lg-1 col-md-1 col-sm-2 col-lg-offset-4 col-md-offset-4 col-sm-offset-2">
				<a class="footer-link" href="/contact">Contact</a>
			</div>
			<div class="col-lg-1 col-md-1 col-sm-2">
				<a class="footer-link" href="/about">About</a>
			</div>
			<div class="col-lg-1 col-md-1 col-sm-2">
				<a class="footer-link" href="/faq">FAQ</a>
			</div>
		<!--<div class="col-lg-1 col-md-1 col-sm-2">
				<a class="footer-link" href="">Blog</a>
			</div>
		-->
			<div class="col-lg-1 col-md-1 col-sm-2">
				<a class="footer-link" href="/terms">Terms</a>
			</div>
		</div>
		<div class="row text-center" style="padding-bottom: 20px;">
			<div class="">
				<a href="https://facebook.com/AnteBox" title="facebook" target="_blank">
					<span class="fa-stack fa-lg">
						<i class="fa fa-circle-thin fa-stack-2x"></i>
						<i class="fa fa-facebook fa-stack-1x"></i>
					</span>
					
				</a>
				<a href="https://twitter.com/anteboxapp" title="twitter" target="_blank">
					<span class="fa-stack fa-lg">
						<i class="fa fa-circle-thin fa-stack-2x"></i>
						<i class="fa fa-twitter fa-stack-1x"></i>
					</span>
				</a>
				<a href="https://www.instagram.com/antebox" title="instagram" target="_blank">
					<span class="fa-stack fa-lg">
						<i class="fa fa-circle-thin fa-stack-2x"></i>
						<i class="fa fa-instagram fa-stack-1x"></i>
					</span>
				</a>
				<a href="https://www.youtube.com/channel/UC654J_tnd7isr9o1hVqThaA" title="youtube" target="_blank">
					<span class="fa-stack fa-lg">
						<i class="fa fa-circle-thin fa-stack-2x"></i>
						<i class="fa fa-youtube fa-stack-1x"></i>
					</span>
				</a>
				<a href="https://www.snapchat.com/add/antebox_app" title="snapchat" target="_blank">
					<span class="fa-stack fa-lg">
						<i class="fa fa-circle-thin fa-stack-2x"></i>
						<i class="fa fa-snapchat fa-stack-1x"></i>
					</span>
				</a>
				<a href="https://www.linkedin.com/company/antebox" title="linked in" target="_blank">
					<span class="fa-stack fa-lg">
						<i class="fa fa-circle-thin fa-stack-2x"></i>
						<i class="fa fa-linkedin fa-stack-1x"></i>
					</span>
				</a>
			</div>
		</div>
		<div class="row text-center" style="color: white; padding-bottom: 20px;">
			<i class="fa fa-copyright" aria-hidden="true"> AnteBox</i>
		</div>
	</div>
</footer>
