<div class="js-cookie-consent cookie-consentalert-block alert-warning" style="text-align: center; vertical-align: middle; padding:10px;" >
    <span class="cookie-consent__message">
        {!! trans('cookieConsent::texts.message') !!}
    </span>

    <button class="js-cookie-consent-agree cookie-consent__agree btn btn-warning">
        {{ trans('cookieConsent::texts.agree') }}
    </button>

</div>
