@extends('app')

@section('content')
  <div class="container">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-12">
          <span class="text-brand-brown" style="font-size: 78px;">Join the Market!</span>
        </div>
      </div>
      <div class="row">
        <div class="col-md-8">
          <span style="font-size:16px;">Create an account today to start finding your favourite items. It's quick, secure and simple to get started</span>
        </div>
        <div class="col-md-4 text-right">
          <a class="text-brand-brown" href="/about">Read more about us <i class="fa fa-chevron-circle-right"></i></a>
        </div>
      </div>
      <div class="row">
        <hr>
      </div>
      <div style="border: 10px solid #163850;">
        <div class="row bs-wizard" style="border-bottom:0;">
            <div id="step-account" class="col-xs-3 bs-wizard-step complete">
              <div class="text-center bs-wizard-stepnum">Account</div>
              <div class="progress"><div class="progress-bar"></div></div>
              <a href="#" class="bs-wizard-dot"></a>
              <div class="bs-wizard-info text-center"></div>
            </div>

            <div id="step-profile" class="col-xs-3 bs-wizard-step complete"><!-- complete -->
              <div class="text-center bs-wizard-stepnum">Profile</div>
              <div class="progress"><div class="progress-bar"></div></div>
              <a href="#" class="bs-wizard-dot"></a>
              <div class="bs-wizard-info text-center"></div>
            </div>

            <div class="col-xs-3 bs-wizard-step active"><!-- complete -->
              <div class="text-center bs-wizard-stepnum">Address</div>
              <div class="progress"><div class="progress-bar"></div></div>
              <a href="#" class="bs-wizard-dot"></a>
              <div class="bs-wizard-info text-center"></div>
            </div>

            <div class="col-xs-3 bs-wizard-step disabled"><!-- active -->
              <div class="text-center bs-wizard-stepnum">Done!</div>
              <div class="progress"><div class="progress-bar"></div></div>
              <a href="#" class="bs-wizard-dot"></a>
              <div class="bs-wizard-info text-center"></div>
            </div>
        </div>
        <div class="row">
          <div class="col-sm-9 col-sm-offset-1" style="padding-top: 30px;">
            <div class="row">
              <div class="col-sm-10">
                <span class="text-brand-brown" style="font-size: 30px; font-weight:bolder;">Add an Address</span>
              </div>
              <div class="col-sm-2">
                <i style="font-size:40px;" class="text-brand-blue fa fa-user"></i>
              </div>
            </div>
            <div class="row" style="padding-top: 40px;padding-bottom: 40px;">
              <div class="col-sm-10 col-sm-offset-1">
                <p class="text-brand-blue">In order to be able to be part of AnteBox you need to provide an address. To begin with, add your home address, then later you can add more.</p>
              </div>
            </div>

            <div class="row">
              <div class="col-sm-11 col-sm-offset-1">
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/profile/done')}}">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">

                  <div class="form-group">
                    <label style="text-align:left;" class="col-md-4 control-label">Address Line 1</label>
                    <div class="col-md-6">
                      <input type="text" class="form-control" name="address_line_1" value="{{ old('address_line_1') }}">
                    </div>
                  </div>

                  <div class="form-group">
                    <label style="text-align:left;" class="col-md-4 control-label">Address Line 2 (Optional)</label>
                    <div class="col-md-6">
                      <input type="text" class="form-control" name="address_line_2" value="{{ old('address_line_2') }}">
                    </div>
                  </div>

                  <div class="form-group">
                    <label style="text-align:left;" class="col-md-4 control-label">Town/City</label>
                    <div class="col-md-6">
                      <input type="text" class="form-control" name="city" value="{{ old('city') }}">
                    </div>
                  </div>

                  <div class="form-group">
                    <label style="text-align:left;" class="col-md-4 control-label">County</label>
                    <div class="col-md-6">
                      <input type="text" class="form-control" name="county" value="{{ old('county') }}">
                    </div>
                  </div>

                  <div class="form-group">
                    <label style="text-align:left;" class="col-md-4 control-label">Postcode</label>
                    <div class="col-md-6">
                      <input type="text" class="form-control" name="postcode" value="{{ old('postcode') }}">
                    </div>
                  </div>

                  <div class="form-group">
                    <label style="text-align:left;" class="col-md-4 control-label">Country</label>
                    <div class="col-md-6">
                      @include('address.countries')
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-md-4 col-md-offset-4" style="padding-top: 30px; padding-bottom: 50px;">
                      <button type="submit" class="btn btn-primary form-control">
                        SUBMIT
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>

          </div>
        </div>

      </div>
      <div class="row" style="padding: 20px;">
      </div>
    </div>
  </div>
@endsection

@section('plugins')
  <link rel="stylesheet" href="{{ URL::asset('css/bzwizard.css') }}"/>
  <link rel="stylesheet" href="{{ URL::asset('css/datepicker.css') }}"/>
@endsection

@section('scripts')
  <script type="text/javascript" charset="UTF-8" src="{{ URL::asset('js/bootstrap-datepicker.js') }}"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      Date.prototype.getFormattedDate = function() {
        var yyyy = this.getFullYear().toString();
        var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
        var dd  = this.getDate().toString();
        return yyyy + '-' + (mm[1]?mm:"0"+mm[0]) + '-' + (dd[1]?dd:"0"+dd[0]); // padding
       };

      $('#dob_calendar').datepicker('format', 'yyyy-mm-dd').on('changeDate', function(ev){
        $('#dob').val(ev.date.getFormattedDate());
      });
    });
  </script>
@endsection
