<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>AnteBox</title>
	<meta property="og:url"           content="http://www.antebox.com" />
	<meta property="og:type"          content="website" />
	<meta property="og:title"         content="AnteBox" />
	<meta property="og:description"   content="A new social platform with a mission to satisfy needs around the world. On the top of these needs, is the access to vaccines and medicine for remote areas. Like our facebook page below or subscribe directly through our website to be updated on our progress!" />
	<meta property="og:image"         content="{{ URL::asset('img/app/share.jpg') }}" />
	<meta name="description" content="A new innovative consultancy company that helps globalize businesses that work in a geographically limited area.">

	<link rel="icon" type="image/png" href="{{ URL::asset('img/app/favicon.png') }}">
	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/landing.css') }}" rel="stylesheet">
	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

	<!-- SweetAlert -->
	<script src="{{ asset('/js/sweetalert.min.js') }}"></script>
	<link href="{{ asset('/css/sweetalert.css') }}" rel="stylesheet">

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-71587454-1', 'auto');
	  ga('send', 'pageview');
	</script>

	<script>
		window.fbAsyncInit = function() {
		  FB.init({
		    appId      : '510079345866294',
		    xfbml      : true,
		    version    : 'v2.5'
		  });
		};

		(function(d, s, id){
		   var js, fjs = d.getElementsByTagName(s)[0];
		   if (d.getElementById(id)) {return;}
		   js = d.createElement(s); js.id = id;
		   js.src = "//connect.facebook.net/en_US/sdk.js";
		   fjs.parentNode.insertBefore(js, fjs);
		 }(document, 'script', 'facebook-jssdk'));
	</script>
<style>
    .btn-primary, .btn-primary:hover, .btn-primary:active, .btn-primary:visited {
	background-color: #D71023 !important;
    }
</style>

</head>
<body class="bg">
	<div id="mycontent">
		<div id="googlemaps" style="height: auto"></div>
		  <div class="container content">
				<div class="row text-center" style="padding-top: 80px;">
					<div class="col-xs-8 col-xs-offset-2">
						<img style="max-width:100%;height:auto;" src="{{ URL::asset('img/app/logo.png') }}"/>
					</div>
				</div>
				<div class="row text-center">
					<div class="col-xs-12" style="padding-top:50px;color: #ffffff">
						<h2>There are millions of businesses around the world with their own story</h2>
						<h2>We bring you products from all around the world</h2>
					</div>
				</div>

				<div class="row" style="padding-top: 10px; text-center">
					<div class="col-xs-12 text-center">
					    <label style="color: #D71023; font-size: 64px; font-weight: bold;">Welcome
					    <img src="{{ URL::asset('img/app/cityspark2.png') }}" width="255" height="64" alt="cityspark.png"/>!</label>
					</div>
				</div>

				<div class="row" style="padding-top: 20px;">
					<div class="col-xs-12 text-center" style="color: #ffffff">
					<h3>Make a new profile now and be one of the first to join!</h3>
					</div>
				</div>

				<div class="row" style="padding-top: 25px;">
					<div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
						<a href="{{ url('/register') }}" class="btn btn-primary">
						    <span class="fa fa-user-plus"></span> Register
						</a>
					</div>
				</div>


<!--				<div class="row" style="padding-top: 10px;">
					<div class="col-xs-12 col-sm-4 col-sm-offset-4">
						<form id="newsletter-subscribe" class="form-horizontal" role="form" method="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<div class="input-group">
								<input type="email" class="form-control" id="email" placeholder="contact@yourdomain.com" name="email">
								<span class="input-group-btn">
								  <input type="submit" class="btn btn-primary" value="Subscribe">
								</span>
						  </div>
						</form>
					</div>
				</div>
-->
				<div class="row" style="padding-top: 60px; padding-bottom: 15px;">
					<div class="col-xs-12 text-center">
						<h2 style="color: #fff;">Follow us now on social media</h2>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-3 col-xs-offset-3 col-sm-1 col-sm-offset-5 text-center">
						<a href="https://facebook.com/AnteBox" target="_blank">
							<img style="max-width: 100%;height: auto;" src="{{ URL::asset('img/social/facebook.png') }}"/>
						</a>
					</div>
					<div class="col-xs-3 col-sm-1 text-center">
						<!-- <i style="font-size: 100px;" class="fa facebook fa-twitter-square"></i> -->
						<a href="https://twitter.com/anteboxapp" target="_blank">
							<img style="max-width: 100%;height: auto;" src="{{ URL::asset('img/social/twitter.png') }}"/>
						</a>
					</div>
				</div>
			</div>
	 </div>
 </div>

	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.5.8/slick.min.js"></script>
	<script type="text/javascript" src="{{ URL::asset('/js/snowstorm.js') }}"></script>

	<script type="text/javascript">
		$( '#newsletter-subscribe' ).on( 'submit', function(e) {
			e.preventDefault();
			$.ajax({
					type: "POST",
					url: '/newsletter/subscribe',
					data: $(this).serialize(),
					success: function(response) {
						swal({
                title: "Share it with your friends!",
                text: "Share the page on facebook and let your friends know about this. Make them part of the revolution!",
                type: "success",
                showCancelButton: true,
                confirmButtonClass: "btn-info",
								confirmButtonColor: "#3b5998",
                confirmButtonText: "Share!",
                cancelButtonText: "No Thanks",
                closeOnConfirm: true
              },
              function(){
								FB.ui({
					        method: 'share',
					        href: window.location.href,
					        description: 'A new social platform with a mission to satisfy needs around the world. On the top of these needs, is the access to vaccines and medicine for remote areas. Like our facebook page below or subscribe directly through our website to be updated on our progress!'
					        }, function(response){});
              });
					},
					error: function(xhr, status, response)
					{
						swal("Oh no!", "The email you have provided is invalid or already subscribed!", "error");
					}
			});
		});
	</script>

	<!-- Include the Google Maps API library - required for embedding maps -->
<script src="http://maps.googleapis.com/maps/api/js"></script>

<script type="text/javascript">
	$(document).ready(function(){

		// positions of ants
		var positions = [[10.616319,-52.5172655],[51.2154614,-1.1177987], [-38.4407555,120.7532461], [58.6000392,166.7166065], [26.7472297,121.2747726]];

		function showGoogleMaps() {
				$('#googlemaps').height($('#mycontent').height());
				var myMapType = new google.maps.StyledMapType([{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#163850"}]},{"featureType":"administrative.country","elementType":"labels.text","stylers":[{"visibility":"off"},{"lightness":-100}]},{"featureType":"water","elementType":"labels.text","stylers":[{"visibility":"off"}]},{"featureType":"administrative.locality","elementType":"labels.text","stylers":[{"visibility":"off"}]},{"featureType":"administrative.province","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"landscape.natural","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#3db033"}]},{"featureType":"administrative","elementType":"labels.text","stylers":[{"visibility":"off"}]},{"featureType":"landscape.man_made","elementType":"labels.text","stylers":[{"visibility":"off"}]}], {name: 'AnteBox Map'});



				var mapOptions = {
						zoom: 2, // initialize zoom level - the max value is 21
						streetViewControl: false, // hide the yellow Street View pegman
						zoomControl: false, // allow users to zoom the Google Map
						draggable: false,
						scrollwheel: false,
						disableDoubleClickZoom: true,
						mapTypeControl: false,
						center: new google.maps.LatLng(10.616319,-52.5172655)
				};

				map = new google.maps.Map(document.getElementById('googlemaps'),
						mapOptions);

				map.mapTypes.set('anteboxmap', myMapType);
				map.setMapTypeId('anteboxmap');

				// Show the default red marker at the location
				for(var position in positions)
				{
					var latLng = new google.maps.LatLng(positions[position][0], positions[position][1]);
					marker = new google.maps.Marker({
							position: latLng,
							map: map,
							draggable: false,
							animation: google.maps.Animation.DROP,
							icon: '{{ URL::asset('img/app/antsmall.png') }}'
					});
				}
		}

		google.maps.event.addDomListener(window, 'load', showGoogleMaps);
	});

</script>
</body>
</html>
