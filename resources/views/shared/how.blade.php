@extends('app')

@section('content')

	<section style="background-color: #d5dbe1; margin-top: -20px; margin-bottom: -200px;">

		<div class="container">
			<div class="row" style="margin-top: 20px;">
				<div class="text-center">
					<h1>HOW IT WORKS</h1>
				</div>
			</div>
			
			<div class="row" style="margin-top: 20px;">
				<div class="center" style="margin: 0 auto;">
					<div class="row embed-responsive embed-responsive-16by9" style="margin-bottom: 20px;">
						<iframe id="how_video" src="https://www.youtube.com/embed/fdsoitu2J0I?rel=0&wmode=opaque&autohide=1&autoplay=1&enablejsapi=1" poster="{{ URL::asset('img/posters/how.png') }}" frameborder="0" allowfullscreen></iframe>
					</div>
				</div>
			</div>
		</div>
			
		<div class="row text-center" style="padding-top: 20px;padding-bottom: 20px;">
			<a href="/register"><div class="col-sm-2 col-sm-offset-3">
				<div style="margin: 10px;border: 3px solid #000; border-radius: 12px; background-color: #fff; min-width: 125px;">
					<div style="margin: 10px;">
						<span style="font-size: 80px;" class="ti-user"></span>
						<hr style="border-top-color: #163850">
						<h3>STEP ONE</h3>
						<span>Register</span>
					</div>
				</div>
			</div></a>
			<div class="col-sm-2">
				<div style="margin: 10px;border: 3px solid #000; border-radius: 12px; background-color: #fff; min-width: 125px;">
					<div style="margin: 10px;">
						<span style="font-size: 80px;" class="ti-pencil-alt"></span>
						<hr style="border-top-color: #163850">
						<h3>STEP TWO</h3>
						<span>Request Item</span>
					</div>
				</div>
			</div>
			<div class="col-sm-2">
				<div style="margin: 10px;border: 3px solid #000; border-radius: 12px; background-color: #fff; min-width: 125px;">
					<div style="margin: 10px;">
						<span style="font-size: 80px;" class="ti-archive"></span>
						<hr style="border-top-color: #163850">
						<h3>STEP THREE</h3>
						<span>Receive bids</span>
					</div>
				</div>
			</div>
		</div>
		<div class="row text-center" style="padding-top: 20px;padding-bottom: 80px;">
			<div class="col-sm-2 col-sm-offset-3">
				<div style="margin: 10px;border: 3px solid #000; border-radius: 12px; background-color: #fff; min-width: 125px;">
					<div style="margin: 10px;">
						<span style="font-size: 80px;" class="ti-check-box"></span>
						<hr style="border-top-color: #163850">
						<h3>STEP FOUR</h3>
						<span>Accept Bid</span>
					</div>
				</div>
			</div>
			<div class="col-sm-2">
				<div style="margin: 10px;border: 3px solid #000; border-radius: 12px; background-color: #fff; min-width: 125px;">
					<div style="margin: 10px;">
						<span style="font-size: 80px;" class="ti-credit-card"></span>
						<hr style="border-top-color: #163850">
						<h3>STEP FIVE</h3>
						<span>Pay</span>
					</div>
				</div>
			</div>
			<div class="col-sm-2">
				<div style="margin: 10px;border: 3px solid #000; border-radius: 12px; background-color: #fff; min-width: 125px;">
					<div style="margin: 10px;">
						<span style="font-size: 80px;" class="ti-package"></span>
						<hr style="border-top-color: #163850">
						<h3>STEP SIX</h3>
						<span>Meet with the traveller</span>
					</div>
				</div>
			</div>
		</div>
	</section>

	
<script>
	$('document').ready(function(){
	
		var flag = false;
		var playing = true;
		$('#how_video').click(function(){
			if (flag) {
				if ( playing == true ) {
					$('#how_video').trigger('pause');
					playing = false;
				} else {
					$('#how_video').trigger('play');
					playing = true;
				}
			} else {
				$('#how_video').prop('muted', false);
				flag = true;
			}
		});
	});
</script>

@endsection
