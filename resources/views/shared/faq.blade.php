@extends('app')

<style>
	.container {
		padding-top: 40px;
	}
	p.question {
		font-weight: bold;
	}
	p.answer {
		margin-bottom: 30px;
	}
</style>

@section('content')

<div class="container">
	<p class="question">Q. What secures the delivery of the product? A traveler may like the product and decide to keep it.</p>
	<p class="answer">A. Once you confirm the successful bid by a traveler, the amount paid is kept securely by AnteBox. This means that if the traveler does not complete the delivery, we will send your money back.</p>

	<p class="question">Q. Can the travelers charge as much as they want for a delivery?</p>
	<p class="answer">A. Travelers are able to make bids on open requests and are not restricted on the amount they will ask. However, the travelers need to consider that several travelers are able to make bids at the same time, and the customer has the right to choose which one is better. Travelers are also encouraged to offer a lower amount as they also receive unique prices and offers for buying the product for the customer but also for their personal purchases.</p>

	<p class="question">Q. Who is considered a traveler?</p>
	<p class="answer">A. Users that register their trips on AnteBox are considered to be travelers and are able to buy a product sold on the platform and deliver it to a customer.</p>

	<p class="question">Q. What is sold on the platform?</p>
	<p class="answer">A. We choose the most unique and local products in the world and partner with businesses selling them.</p>

	<p class="question">Q. Do I have to meet with the traveler to receive the product?</p>
	<p class="answer">A. When making a request, users are asked to add up to three meeting places and the traveler can bid on delivering the product to those locations and one more that he suggests. We do not require you to meet and receive the product, but you need to confirm the delivery in order to release the funds to the traveler. If you would like the traveler to deliver the products in your absence, make sure to accept the delivery at your own risk. Likewise, the traveler should not deliver the product if the customer is absent and did not accept the delivery. There is a chat system within the platform to ensure that users can communicate until a transaction is fully completed.</p>

	<p class="question">Q. Is it dangerous to meet with another person?</p>
	<p class="answer">A. Users are asked to create accounts on AnteBox, that can also be linked to their social media accounts so that users are able to see each other before meeting. The traveler has to buy the products from the local businesses before meeting the customer, that is able to provide feedback for every traveler to the customer. Users are also able to give feedback to each other after a transaction.</p>

	<p class="question">Q. Is the name AnteBox from the ants?</p>
	<p class="answer">A. Yes, ants can go anywhere and get something they want, like our travelers.</p>

	<p class="question">Q. How much will the product and delivery cost in total?</p>
	<p class="answer">A. We determine the price of the product and then travelers make their bids for delivering it. The total cost of the transaction will include a small service fee that will be displayed before the customer agrees the delivery. No additional costs will occur after a deal is made.</p>

	<p class="question">Q. Can I return the product?</p>
	<p class="answer">A. Food products can’t be returned. If for some reason the product does not reach your expectations please let us know by sending an email to contact@antebox.com or by writing it in your feedback. </p>

	<p class="question">Q. How do I know it is the right product?</p>
	<p class="answer">A. Our partners will let you know when your product is collected and in the near future the products will be packed with the AnteBox logo on them.</p>

</div>

@endsection
