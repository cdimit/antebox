<script type="text/javascript">
  $(document).ready(function(){

    // positions of ants
    var positions = [[51.384998, -2.364917]];

    function showGoogleMaps() {

        // var myMapType = new google.maps.StyledMapType([{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#163850"}]},{"featureType":"administrative.country","elementType":"labels.text","stylers":[{"visibility":"off"},{"lightness":-100}]},{"featureType":"water","elementType":"labels.text","stylers":[{"visibility":"off"}]},{"featureType":"administrative.locality","elementType":"labels.text","stylers":[{"visibility":"off"}]},{"featureType":"administrative.province","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"landscape.natural","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#3db033"}]},{"featureType":"administrative","elementType":"labels.text","stylers":[{"visibility":"off"}]},{"featureType":"landscape.man_made","elementType":"labels.text","stylers":[{"visibility":"off"}]}], {name: 'AnteBox Map'});
        var mapOptions = {
            zoom: 16, // initialize zoom level - the max value is 21
            streetViewControl: true, // hide the yellow Street View pegman
            zoomControl: true, // allow users to zoom the Google Map
            draggable: true,
            scrollwheel: false,
            disableDoubleClickZoom: false,
            mapTypeControl: false,
            center: new google.maps.LatLng(51.384998, -2.364917)
        };

        map = new google.maps.Map(document.getElementById('destination_area_map'),
            mapOptions);

        // map.mapTypes.set('anteboxmap', myMapType);
        // map.setMapTypeId('anteboxmap');

        // Show the default red marker at the location
        for(var position in positions)
        {
          var latLng = new google.maps.LatLng(positions[position][0], positions[position][1]);
          marker = new google.maps.Marker({
              position: latLng,
              map: map,
              draggable: false,
              animation: google.maps.Animation.DROP,
              icon: '{{ URL::asset('img/app/antsmall.png') }}'
          });
        }
    }

    google.maps.event.addDomListener(window, 'load', showGoogleMaps);
  });

</script>
