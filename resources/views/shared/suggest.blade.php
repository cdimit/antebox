@extends('app')


    <link rel="stylesheet" href="{{ asset('css/w3.css') }}">
    <link rel="stylesheet" href="{{ asset('css/w3-theme-blue.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
@section('content')


 <div class="container">
<div class="col-md-offset-2">

    <div class="row">
        <div class="form-group">
            <label style="text-align: center; font-weight: bold;" class="control-label text-left">
            <h3><strong>Share your thoughts with us</strong></h3>
            <h4><strong>Suggest Company, Store or Product</strong></h4>
            </labe>
        </div>
    </div>


<div class="w3-row-padding w3-center w3-margin-top">
<div class="w3-third">
  <div class="w3-card-2 w3-padding-top" style="min-height:400px">
  <h4>Company</h4><br>
  <i class="fa fa-building w3-margin-bottom w3-text-theme" style="font-size:120px"></i>
<strong>
  <p>Selling worldwide</p>
  <p>Don't change anything</p>
  <p>Promote your business</p>
  <p>Your unique creations</p>
</strong>
<a href="suggest/company" class="btn btn-success">Continue</a>
  </div>
</div>

<div class="w3-third">
  <div class="w3-card-2 w3-padding-top" style="min-height:400px">
  <h4>Product</h4><br>
  <i class="fa fa-heart w3-margin-bottom w3-text-theme" style="font-size:120px"></i>
<strong>
  <p>Whatever you love</p>
  <p>Something we dont offer</p>
  <p>Smell Paradise</p>
  <p>Dont find shipping?</p>
</strong>
<a href="suggest/product" class="btn btn-success">Continue</a>
  </div>
</div>
</div>


</div>
</div>
@endsection
