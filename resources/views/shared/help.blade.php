@extends('app')

@section('content')
  <div class="container">
    <div class="col-md-12">

    @if (count($errors) > 0)
	<div class="alert alert-danger">
  		<strong>Whoops!</strong> There were some problems with your input.<br><br>
  		<ul>
  			@foreach ($errors->all() as $error)
  				<li>{{ $error }}</li>
  			@endforeach
  		</ul>
  	</div>
    @endif

  @if (session('status'))
    <div class="alert alert-success">
     {{ session('status') }}
    </div>
  @endif

  <form class="form-horizontal" role="form" method="POST" action="{{ url('/contact/send')}}">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="row">
        <div class="form-group">
            <label style="text-align: right; font-weight: bold;" class="col-md-12 control-label text-left">AnteBox welcomes your questions, comments, suggestions, compliments, and complaints as one critical way to continuously improve our services to you.</labe>
        </div>
    </div>
    <br>



@if(Auth::guest())

    <div class="row">
	<div class="form-group">
	    <label style="text-align: right; font-weight: bold;" class="col-md-4 control-label text-left">Name</label>
	    <div class="col-md-6">
		<input type="text" class="form-control" name="name" value="{{ old('name') }}">
	    </div>
	</div>
    </div>
    <br>

    <div class="row">
	<div class="form-group">
	    <label style="text-align: right; font-weight: bold;" class="col-md-4 control-label text-left">E-mail</label>
	    <div class="col-md-6">
		<input type="text" class="form-control" name="email" value="{{ old('email') }}">
	    </div>
	</div>
    </div>
    <br>

@endif

    <div class="row">
	<div class="form-group">
	    <label style="text-align: right; font-weight: bold;" class="col-md-4 control-label text-left">Subject</label>
	    <div class="col-md-6  {{$errors->has('subject') ? 'has-error' : ''}}">
		<input type="text" class="form-control"  name="subject" value="{{ old('subject') }}" >
	    </div>
	</div>
    </div>
    <br>
    <div class="row">
	<div class="form-group">
	    <label style="text-align: right; font-weight: bold;" class="col-md-4 control-label text-left">Message</label>
	    <div class="col-md-6  {{$errors->has('message') ? 'has-error' : ''}} ">
		<textarea class="form-control" rows="4" name="message" style="resize:none;">{{ old('message') }}</textarea>
	    </div>
	</div>
    </div>
    <br>

    <div class="row">
	<div class="form-group">
	    <div class="col-md-2 col-md-offset-8">
		<button style="width:100%" class="btn btn-success">
		    Sent
		</button>
	    </div>
	</div>
    </div>
    <br>

</form>
</div>
</div>
@endsection
