@extends('app')

@section('content')
  <div class="container">
    <div class="col-md-12">

<h1>Privacy Policy</h1>
<h3>Last Updated March 21, 2016</h3>
<br>
<h2>Relationship Determination</h2>

<p><strong>The following constitutes the terms and conditions for using Antebox.com, Antebox App and all the services provided by AnteBox trademark.</strong> In addition, in the text, Antebox can be represented by the words such as "us", "we", "ourselves" and so on.... The users are divided into main three main categories, the "customers" that order products through our platform; the "carriers" that are the users that transfer the products; and the local businesses. The goods ordered through our platform will be called "products". The interface where users will be able to interact will be called "platform". The agreement to buy and deliver a product through the platform will be called the "transaction".</p>

<p>Antebox may terminate these terms and conditions relating to you in any point in time for any reason. Furthermore, changes can be made to the terms and conditions and by continuing to use the platform it means you have accepted them.</p>

<p>By accessing and using this service, you accept and agree to be bound by the terms and provision of this agreement. In addition, when using these particular services, you shall be subject to any posted guidelines or rules applicable to such services. Any participation in this service will constitute acceptance of this agreement. If you do not agree to abide by the above, please do not use this service. We always want to promote fairness and this starts from ourselves and within our organisation. If you find that these terms are unfair or you are not sure about something please contact us at <a href="mailto:contact@antebox.com">contact@antebox.com</a>.</p>

<h2>Registrations</h2>

<p>When registering on Antebox, users need to state valid information and update their profiles when necessary. Users will have the chance to verify their accounts by uploading ID, passport documents or other legitimate documents such as a utility bill to prove an address; this will then be shown on their profile, increasing their level of trust.</p>

<p>Furthermore, when the registration is complete, users have access to use the platform and follow all the terms and conditions as stated by this document.</p>

<p><strong>The terms and conditions will be updated as functionality increases in the site. Every user will be notified with the updated terms of reference.</strong></p>

<br>
</div>
</div>
@endsection
