@extends('app')

@section('content')

<div class="container">


    <div class="row text-center" style="margin-top: 20px;">  
	<h1>About us</h1>
    </div>

    <div class="row text-center" style="margin-bottom: 40px;">
	<img src="{{ URL::asset('img/app/about.png') }}" style="width: 700px; height: 166px;" />
    </div>

    <div class="row" style="margin-top: 20px;">
        <p>
Founded in February 2016 and based in Limassol, Cyprus, AnteBox is an online marketplace for local businesses to share their stories with the world. Our network of partners is comprised of local authentic producers from halloumi cheese in Cyprus to summer truffles in Italy. Customers have the ability to browse through the unique selection of products, request them and subsequently receive delivery bids from travellers. <a href="/how">Learn more here</a>.
        </p>
        <p>
Each one of us has associated a memorable trip or visit with an unforgettable food experience. At Antebox we are determined to make that experience readily available to you by providing an ideal service connecting local businesses to customers through a unique delivery.
        </p>
        <p>
With such a great delivery method, powered by people just like you, these unique products can reach any place in the world within hours. Whether it’s for your own pleasure or to surprise your guests, AnteBox has the right products for you.
        </p>
    
    </div>
    <div class="row" style="margin-top: 20px;">

    	<address><strong>Headquarters</strong><br/>
Omonia Avenue 19, <br/>
Constantinides Building, <br/>
Office 202, 2nd Floor, <br/>
3052 Limassol, Cyprus<br/>
            </address>
Number of local partners: <b>9</b>
    </div>
    <div class="row" style="margin-top: 20px;">
	<address><strong>Offices</strong><br/>
Unruly Hive,<br/> 
42-45 Princelet Street,<br/>
London E1 5LP, <br/>
United Kingdom<br/>
    	</address>
Number of local products: <b>48</b>
    </div>
</div>

@endsection
