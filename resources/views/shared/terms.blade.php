@extends('app')

@section('content')
  <div class="container">
    <div class="col-md-12">

<h1>Terms and Conditions<h1>
<h3>Last Updated July 1, 2016</h3>
<br>
<h2>Relationship Determination</h2>

<p><strong>The following constitutes the terms and conditions for using Antebox.com, Antebox App and all the services provided by AnteBox trademark.</strong> In addition, in the text, Antebox can be represented by the words such as "us", "we", "ourselves" and so on.... The users are divided into main three main categories, the "customers" that order products through our platform; the "carriers" that are the users that transfer the products; and the local businesses. The goods ordered through our platform will be called "products". The interface where users will be able to interact will be called "platform". The agreement to buy and deliver a product through the platform will be called the "transaction".</p>

<p>Antebox may terminate these terms and conditions relating to you in any point in time for any reason. Furthermore, changes can be made to the terms and conditions and by continuing to use the platform it means you have accepted them.</p>

<p>By accessing and using this service, you accept and agree to be bound by the terms and provision of this agreement. In addition, when using these particular services, you shall be subject to any posted guidelines or rules applicable to such services. Any participation in this service will constitute acceptance of this agreement. If you do not agree to abide by the above, please do not use this service. We always want to promote fairness and this starts from ourselves and within our organisation. If you find that these terms are unfair or you are not sure about something please contact us at <a href="mailto:contact@antebox.com">contact@antebox.com</a>.</p>

<h2>Products and transactions</h2>

<p>Antebox emphasizes the fact that we do not own the products and do not employ the carriers. Our only asset is the platform that provides the customer the opportunity to receive the product he wants with the carrier that he selects. Antebox therefore can’t guarantee the quality of the product or service and encourages users to be clear and transparent throughout the transaction process. The feedback system will help enable users to identify good profiles and to promote users to behave in the best way for the participants.</p>

<p>Users are expected to follow all rules and respect other users in all transaction levels, and Antebox can’t be liable for individual’s behaviors. In the event of fraud or conduct misuse, users will be legally prosecuted as abided by law.</p>

<p>When a carrier makes a bid on a customer’s auction to deliver a product on his trip he agrees that until the product is given to the customer, he is the owner of the product and that he is responsible for all the payments and regulations related to it.</p>

<p>When a customer accepts a traveller’s bid, the transaction is final and should not change.</p>


<h2>Registrations</h2>

<p>When registering on Antebox, users need to state valid information and update their profiles when necessary. Users will have the chance to verify their accounts by uploading ID, passport documents or other legitimate documents such as a utility bill to prove an address; a verified sign will then be shown on their profile, increasing their level of trust.</p>

<p>Furthermore, when the registration is complete, users will have access the platform’s services and need to follow all the terms and conditions as stated by this document.</p>

<h2>Carrier</h2>

<p>When the carrier uses Antebox he agrees to all the terms and conditions, and this section is created to further assist their understanding.</p>

<p>When registering the trip, the user agrees that any information added can be visible by other users and can be used by AnteBox. AnteBox will send marketing material for the countries the user is visiting and travelling from, or notifications for any products that need to be delivered, if applicable.<p>

<p>The carrier has the option to browse through auctions and bid on them, or be contacted by another user regarding an auction when the carrier’s trip is registered on the platform. When a bid is accepted the carrier is committed to deliver the product at the agreed location and time. In order to do so, he is able to buy the product with his own funds and have it’s complete ownership until the product is given to the customer. Carriers are discouraged from bidding on a product they can’t guarantee its delivery as it would lead to further actions as stated in Section Cancellation and Refund  Policy.</p>

<p>It is up to the carrier to determine the price he will ask to be paid by the customer for the required product and it’s delivery. The carrier is also responsible for ensuring that all legal requirements for transferring the products are met, including customs, taxes and liquid limitations when boarding amongst others. The carrier also agrees that 4% transaction fees will be deducted from the payment in order for Antebox to pay for additional costs such as payment processing fees.</p>

<p>The carrier is responsible for reporting any requests for illegal products and should not accept delivering any products that are not displayed in the platform.</p>

<p>When buying a product, the traveler should give any receipt or guarantee associated with the purchase to the customer. The traveler should take care of the product being delivered, or else will be liable for any damages occurred.</p>

<p>The traveler should make sure that the product matches the exact description agreed in the auction when purchasing the product or else will be liable to pay any additional costs if the customers rejects it.</p>


<h2>Local Businesses</h2>

<p>Antebox works closely with local businesses that offer unique products to their customers and operate in a limited geographical area. Antebox may contact these businesses directly or local businesses can contact Antebox..</p>

<p>Antebox may charge for advertising space and has the right to terminate any recommended product if Antebox believes there was a considerable change in the offer provided or a business has illegally broken rules of trade.</p>

<p>Antebox may also charge consultation fees depending on the level of service and support the local business needs. We want to build close partnerships and face to face discussions, as a result a lot of time can be spent helping the business that should be accounted for.</p>

<p>Local businesses may also be offered a demo version if we believe it is necessary. The level of support and guidance needed will be determined at the start of the contract and will determine future deliverables. Antebox has the right to charge transaction costs that might occur due to the sale of the local businesses’ products.</p>

<p>Local businesses have the right to terminate their contract with Antebox after six months or otherwise stated. During the collaboration, the business should not reveal any costs or information related to the contract agreement made with Antebox, as each contract will be fitted to the individual local business.</p>

<h2>Auctions / Bids / Payments</h2>

<p>When creating an auction, the customer will have the opportunity to add three possible meeting locations to the auction. The traveller then has the opportunity to bid on delivering the products to these locations and propose a meeting location of his own. Every bid is individual, and consists of the time and date of the delivery, the location and the price.</p>

<p>In order to suggest a change to a certain bid, the customer can contact the traveler that will then have the option to make a new bid for delivering the product.
When a deal is made, the customer’s payment is held by Antebox and is released to the carrier 24hrs after the successful confirmation for the transaction completion. This is done to protect both users and to drive the quality of the service.<p>

<p>Users agree that all payments regarding the transaction are made within Antebox’s platform and that no payment should be made at the meeting point to ensure the safety of the delivery. The traveler should not ask for further payments than the amount agreed in the bid made unless extenuating circumstances occurred and the customer agrees to that. AnteBox is not responsible for agreeing further payments after the bid amount is accepted by both parties, the customer and the carrier.</p>

<p>Antebox is not responsible for making travel arrangements for the users, and the users need to consider their own cost of travelling. Travelling confirmations may be asked to be submitted in case of a disagreement or for any other circumstance Antebox considers necessary.</p>

<p>When making an auction, the user may use promotional codes provided by Antebox or otherwise accept to pay the auction cost charged. The user accepts the fact that an auction may not be satisfied and that the option of renewing the auction will be provided. The user can set the expiry date of the auction, the default level will be two weeks, and should be able to accept offers within that time period. The auction’s expiry date should indicate to the traveler the urgency of the product and a carrier can bid to deliver the product while the auction is live. The actual delivery of the product can be made at any time as long as the user making the auction accepts that.</p>

<h2>Transaction Fees</h2>

<p>AnteBox operates by receiving transaction fees for the products sold via the platform. Through these payments, we want to ensure you have a great experience and you get the products you always wanted. A 12% transaction fee is paid by the customer, and a 4% transaction fee is deducted from the carrier’s payout. This fee covers payment processing fees that are carried by a third party.</p>

<h2>Cancellation and Refund  Policy</h2>

<p>Customers do not have the opportunity to cancel the product after the carrier has confirmed that he has bought the product. Whereas the traveller has the right to cancel the transaction at any point given a valid reason. In this case, the customer will receive a full refund, and the traveller may be banned from using the services if the reason for cancelling the delivery was not sufficiently evidenced.</p>

<p>Users will be able to track live through our platform the location of their product and in this way be warned for delays caused by external factors such as flight delays or cancellations. If a delivery is affected by external factors, Antebox will investigate and provide a fair solution for both users.</p>

<p>It is the traveler’s responsibility to buy the right product requested that matches the exact description given. The customer has the chance to reject the product if the description does not match the product provided. In the unlikely event that the product does not match the description given, the customer should reject the product and send an email to <a href="mailto:contact@antebox.com">contact@antebox.com</a>. Antebox will respond within two days indicating the final decision. If the carrier made the mistake, the carrier will be responsible for the product and for any other costs incurred. The customer in this case will be refunded. An automated feedback post will be posted on the traveler’s profile, indicating that the wrong product was delivered.</p>

<p>If the carrier fails to deliver the product as promised, the carrier will be liable and will not receive the payment, and the customer will be fully refunded. The carrier needs to state the reason and then AnteBox will consider whether or not the carrier will be banned from using the platform. In addition, an automated post that the delivery was cancelled because of the courier will be posted on the courier’s profile.</p>

<p>In case the customer does not show up at the agreed meeting point, the carrier can deny the delivery and an automated note with be posted on the customer’s profile saying that he did not go to the agreed meeting point.</p>

<p>When a traveler misses a flight he will be liable for any additional costs incurred for the product to be delivered. The first step should be to inform the customer in order to avoid any further difficulties. If another flight is booked, the traveler should not assume that the customer will accept any change in the meeting time and should therefore propose a new meeting time and wait for the customer’s approval.</p>

<p>If the customer agrees, the product can be sent in alternative ways, either by another user or a courier company, with the traveler responsible for additional costs. If the customer is not flexible on the delivery time, and due to missing the flight, the transaction is then cancelled and the customer can be refunded. The traveler can then look for other auctions to satisfy with the product he bought or add it to his registered trip in order for other users to see he has it with him.</p>

<p>In all scenarios, the users will have the opportunity to resolve the issue themselves before reporting it to us, as an alternative solution may be satisfactory to both. The communication that will be taken into account is only the one that takes place through our platform, as users are advised to communicate only through our platform.</p>

<p>When a cancellation is made from either user, an automated post will be published on their profile recording the point of the transaction process that was cancelled.</p>

<h2>Communication</h2>

<p>Users will be able to communicate within the platform using the chat system. This will be open from the beginning of the auction up to the point where the transaction ends.</p>

<p>By agreeing to the terms and conditions, the users agree to use the platform as the only means of communication when possible and other types will be used only if necessary as these will not be accepted in case of a disagreement. Other means of communication may be used to assist the meeting of the two parties only.</p>

<p>The user’s personal details, such as telephone number and personal email may be provided to the other user entering the transaction after it has been accepted. Users agree to not use these details again without the other user’s consensus.</p>

<p>Users should not use inappropriate language and if a user is identified doing so may be banned and prosecuted further.</p>


<h2>Privacy Policy</h2>

<p>When a deal is reached with a traveller, the customer will receive a payment request to pay by email through Paypal. This payment request should be completed as soon as possible or a maximum within 24hrs. Failing to do so, the traveller will not receive the confirmation and may not buy the products requested.</p>

<p>AnteBox will not hold any sensitive information, and the payment request will be sent to the email or telephone used by the user when registering.</p>

<p>The traveller will be asked to add payout information; this information will be used to send the funds agreed in the transaction after a service fee of 4% is deducted. The traveller is responsible for providing accurate information. In case that an error occurs, the traveller will be liable and will not receive the funds agreed in the transaction.</p>

<p>If in any circumstance we feel that these terms have been abused, the user’s account will be terminated and further legal action might be taken depending on the breech. In the event that the user wants to terminate his account he can do so by deactivating his account from the user’s profile page. No further action will be taken to prevent the termination except than asking for feedback for the reason of the termination. Enabling us to improve the service provided.</p>
<br>
</div>
</div>
@endsection
