<!doctype html>
<!--[if lt IE 7]><html lang="en" class="no-js ie6"><![endif]-->
<!--[if IE 7]><html lang="en" class="no-js ie7"><![endif]-->
<!--[if IE 8]><html lang="en" class="no-js ie8"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->

<head>
    <meta charset="UTF-8">
    <title>Oleose App Landing Page | Bootstrap Theme</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <link rel="shortcut icon" href="favicon.png">

    <!-- Bootstrap 3.3.2 -->

	<link rel="stylesheet" href="{{ URL::asset('assets/css/bootstrap.min.css') }}">

        <link rel="stylesheet" href="{{ URL::asset('assets/css/animate.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/css/slick.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/css/settings.css') }}">

<!--    <link rel="stylesheet" href="{{ URL::asset('assets/css/styles.css') }}"> -->

        <script type="text/javascript" src="{{ URL::asset('assets/js/modernizr.custom.32033.js"') }}"></script>

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <div class="pre-loader">
        <div class="load-con">
            <img src="assets/img/freeze/logo.png" class="animated fadeInDown" alt="">
            <div class="spinner">
              <div class="bounce1"></div>
              <div class="bounce2"></div>
              <div class="bounce3"></div>
            </div>
        </div>
    </div>

    <header>



        <!--RevSlider-->
        <div class="tp-banner-container">
            <div class="tp-banner" >
                <ul>
                    <!-- SLIDE  -->
                    <li data-transition="fade" data-slotamount="7" data-masterspeed="1500" >
                        <!-- MAIN IMAGE -->
                        <img src="assets/img/transparent.png"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                        <!-- LAYERS -->
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption lfl fadeout hidden-xs"
                            data-x="left"
                            data-y="bottom"
                            data-hoffset="30"
                            data-voffset="-40"
                            data-speed="500"
                            data-start="700"
                            data-easing="Power4.easeOut">
                            <img src="assets/img/freeze/Slides/hand-freeze.png" alt="">
                        </div>


                        <div class="tp-caption large_white_bold sft" data-x="550" data-y="center" data-hoffset="0" data-voffset="-80" data-speed="500" data-start="1200" data-easing="Power4.easeOut">
                            Antebox
                        </div>
                        <div class="tp-caption large_white_light sfb" data-x="550" data-y="center" data-hoffset="0" data-voffset="0" data-speed="1000" data-start="1500" data-easing="Power4.easeOut">
                            Shop Locally
                        </div>

                        <div class="tp-caption sfb hidden-xs" data-x="550" data-y="center" data-hoffset="0" data-voffset="85" data-speed="1000" data-start="1700" data-easing="Power4.easeOut">
                            <a href="#about" class="btn btn-primary inverse btn-lg">LEARN MORE</a>
                        </div>


                    </li>
                    <!-- SLIDE 2 -->
                    <li data-transition="zoomout" data-slotamount="7" data-masterspeed="1000" >
                        <!-- MAIN IMAGE -->
                        <img src="assets/img/transparent.png"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                        <!-- LAYERS -->
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption lfb fadeout hidden-xs"
                            data-x="center"
                            data-y="bottom"
                            data-hoffset="0"
                            data-voffset="0"
                            data-speed="1000"
                            data-start="700"
                            data-easing="Power4.easeOut">
                            <img src="assets/img/freeze/Slides/freeze-slide2.png" alt="">
                        </div>

                        <div class="tp-caption large_white_light sft" data-x="center" data-y="250" data-hoffset="0" data-voffset="0" data-speed="1000" data-start="1400" data-easing="Power4.easeOut">
                          Local products always taste better
                        </div>
                        <div class="tp-caption sfr hidden-xs" data-x="730" data-y="center" data-hoffset="0" data-voffset="85" data-speed="1500" data-start="1900" data-easing="Power4.easeOut">
                            <a href="#screens" class="btn btn-default btn-lg">Find More</a>
                        </div>

                    </li>

                    <!-- SLIDE 3 -->
                    <li data-transition="zoomout" data-slotamount="7" data-masterspeed="1000" >
                        <!-- MAIN IMAGE -->
                        <img src="assets/img/transparent.png"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                        <!-- LAYERS -->
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption customin customout hidden-xs"
                            data-x="right"
                            data-y="center"
                            data-hoffset="0"
                            data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
                        data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-voffset="50"
                            data-speed="1000"
                            data-start="700"
                            data-easing="Power4.easeOut">
                            <img src="assets/img/freeze/Slides/family-freeze.png" alt="">
                        </div>

                        <div class="tp-caption customin customout visible-xs"
                            data-x="center"
                            data-y="center"
                            data-hoffset="0"
                            data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
                        data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-voffset="0"
                            data-speed="1000"
                            data-start="700"
                            data-easing="Power4.easeOut">
                            <img src="assets/img/freeze/Slides/family-freeze.png" alt="">
                        </div>

                        <div class="tp-caption lfb visible-xs" data-x="center" data-y="center" data-hoffset="0" data-voffset="400" data-speed="1000" data-start="1200" data-easing="Power4.easeOut">
                            <a href="#reviews" class="btn btn-primary inverse btn-lg">View Recent Requests</a>
                        </div>


                        <div class="tp-caption mediumlarge_light_white sfl hidden-xs" data-x="left" data-y="center" data-hoffset="0" data-voffset="-50" data-speed="1000" data-start="1000" data-easing="Power4.easeOut">
                           Travelers like Ants,
                        </div>
                        <div class="tp-caption mediumlarge_light_white sft hidden-xs" data-x="left" data-y="center" data-hoffset="0" data-voffset="0" data-speed="1000" data-start="1200" data-easing="Power4.easeOut">
                           Can go Anywhere
                        </div>
                        <div class="tp-caption small_light_white sfb hidden-xs" data-x="left" data-y="center" data-hoffset="0" data-voffset="80" data-speed="1000" data-start="1600" data-easing="Power4.easeOut">
                           <p>Your deliveries are made by travellers like yourself
                             <br>and could reach you even in the same day.
                             <br>You have the ability to make delivery bids on requests
                             <br>and get paid when you deliver the products</p>
                        </div>

                        <div class="tp-caption lfl hidden-xs" data-x="left" data-y="center" data-hoffset="0" data-voffset="160" data-speed="1000" data-start="1800" data-easing="Power4.easeOut">
                            <a href="#reviews" class="btn btn-primary inverse btn-lg">View Recent Requests</a>
                        </div>


                    </li>

                </ul>
            </div>
        </div>


    </header>


    <div class="wrapper">



        <section id="about">
            <div class="container">

                <div class="section-heading scrollpoint sp-effect3">
                    <h1>What is AnteBox?</h1>
                    <div class="divider"></div>
                    <!--<p>Oleose Beautiful App Landing Page</p>-->
                </div>

                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <div class="about-item scrollpoint sp-effect2">
                            <i class="fa fa-globe fa-2x"></i>
                            <h3>Local products</h3>
                            <p>Find unique and traditional products directly from local producers from around the world.</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6" >
                        <div class="about-item scrollpoint sp-effect5">
                            <i class="fa fa-shopping-cart fa-2x"></i>
                            <h3>Request</h3>
                            <p>Whether you are a foody, you miss your country’s products or you want to surprise your guests, we have the right products for you.</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6" >
                        <div class="about-item scrollpoint sp-effect5">
                            <i class="fa fa-plane fa-2x"></i>
                            <h3>Deliver</h3>
                            <p>Shipping is not only complex and expensive, it takes too long! That is why we use travellers, like you, to deliver the requests.</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6" >
                        <div class="about-item scrollpoint sp-effect1">
                            <i class="fa fa-users fa-2x"></i>
                            <h3>Network</h3>
                            <p>Our network is growing and involves producers, retailers, restaurants and hotels.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="features">
            <div class="container">
                <div class="section-heading scrollpoint sp-effect3">
                    <h1>Why Join Us?</h1>
                    <div class="divider"></div>

                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-4 scrollpoint sp-effect1">
                        <div class="media text-center feature">
                            <div class="media-body">
                                <h1 class="media-heading">Clients</h1>
                            </div>
                        </div>
                        <!--
                        <div class="media text-right feature">
                            <a class="pull-right" href="#">
                                <i class="fa fa-search fa-2x"></i>
                            </a>
                            <div class="media-body">
                                <h3 class="media-heading">Discover new flavours</h3>
                                  Whether its for yourself or your guests, our products are offering you unique flavours.
                            </div>
                        </div>
                      -->
                      <div class="media text-right feature">
                          <a class="pull-right" href="#">
                              <i class="fa fa-globe fa-2x"></i>
                          </a>
                          <div class="media-body">
                              <h3 class="media-heading">No Limits</h3>
                               Travelers, like ants, can go anywhere
                          </div>
                      </div>
                      <br>
                        <div class="media text-right feature">
                            <a class="pull-right" href="#">
                                <i class="fa fa-thumbs-up fa-2x"></i>
                            </a>
                            <div class="media-body">
                                <h3 class="media-heading">Personalised Delivery</h3>
                                Receive delivery bids from travellers and select the one you prefer. Contact your traveller through the in-house chat system
                            </div>
                        </div>
                        <div class="media feature text-right">
                            <a class="pull-right" href="#">
                                <i class="fa fa-clock-o fa-2x"></i>
                            </a>
                            <div class="media-body">
                                <h3 class="media-heading">Quick delivery.</h3>
                                Your deliveries are made by travellers like yourself, and could reach you even in the same day.
                            </div>
                        </div>
                        <div class="media text-right feature">
                            <a class="pull-right" href="#">
                                <i class="fa fa-building fa-2x"></i>
                            </a>
                            <div class="media-body">
                                <h3 class="media-heading">Local Products</h3>
                                Local businesses make their products in small batches and take care of every detail
                            </div>
                        </div>
                        <div class="media text-right feature">
                            <a class="pull-right" href="#">
                                <i class="fa fa-home fa-2x"></i>
                            </a>
                            <div class="media-body">
                                <h3 class="media-heading">Home is where you are</h3>
                                Never miss products from your country again!
                            </div>
                        </div>

                    </div>
                    <div class="col-md-4 col-sm-4">
                        <img src="assets/img/freeze/iphone-freeze2.png" class="img-responsive scrollpoint sp-effect5" alt="">
                    </div>
                    <div class="col-md-4 col-sm-4 scrollpoint sp-effect2">
                        <div class="media text-center feature">
                            <div class="media-body">
                                <h1 class="media-heading">Travellers</h1>
                            </div>
                        </div>
                        <div class="media feature">
                            <a class="pull-left" href="#">
                                <i class="fa fa-heart fa-2x"></i>
                            </a>
                            <div class="media-body">
                                <h3 class="media-heading">Visit local shops</h3>
                                Our trips will be so much better if we were able to experience authentic and traditional goods from every country we visit
                            </div>
                        </div>
                        <div class="media feature">
                            <a class="pull-left" href="#">
                                <i class="fa fa-sort-numeric-desc fa-2x"></i>
                            </a>
                            <div class="media-body">
                                <h3 class="media-heading">Get discounts and offers</h3>
                                Our partners are happy to offer you better prices and additional products when you show them you are delivering via AnteBox
                            </div>
                        </div>
                        <div class="media feature">
                            <a class="pull-left" href="#">
                                <i class="fa fa-money fa-2x"></i>
                            </a>
                            <div class="media-body">
                                <h3 class="media-heading">Get paid</h3>
                                You have the ability to make delivery bids on requests and get paid when you deliver the products
                            </div>
                        </div>
                        <div class="media feature">
                            <a class="pull-left" href="#">
                                <i class="fa fa-arrow-up fa-2x"></i>
                            </a>
                            <div class="media-body">
                                <h3 class="media-heading">Support local businesses</h3>
                              These businesses do not have the staff, time or expertise to ship their products abroad.
                            </div>
                        </div>
                        <div class="media feature">
                            <a class="pull-left" href="#">
                                <i class="fa fa-smile-o fa-2x"></i>
                            </a>
                            <div class="media-body">
                                <h3 class="media-heading">Great hospitality.</h3>
                              Our local partners are happy to welcome you with offers, free tasting and tours of their areas.
                            </div>
                        </div>
<!--
                        <div class="media active feature">
                            <a class="pull-left" href="#">
                                <i class="fa fa-plus fa-2x"></i>
                            </a>
                            <div class="media-body">
                                <h3 class="media-heading">And much more!</h3>

                            </div>
                        </div>
                      -->
                    </div>
                </div>
            </div>
        </section>

        <section id="reviews">
            <div class="container">
                <div class="section-heading inverse scrollpoint sp-effect3">
                    <h1>Recent Requests</h1>
                    <div class="divider"></div>
                    <p>Find a request that you can fulfill</p>
                </div>
                <div class="row">
                    <div class="col-md-10 col-md-push-1 scrollpoint sp-effect3">
                        <div class="review-filtering">
                            <div class="review">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="review-person">
                                            <img src="http://api.randomuser.me/portraits/women/94.jpg" alt="" class="img-responsive">
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="review-comment">
                                            <h3>“I want this by next week”</h3>
                                            <p>
                                                <strong>- Halloumi Cheese</strong>
                                                <br>
                                                <br>
                                                Cyprus <i class="fa fa-plane fa-2x"></i> Greece
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="review rollitin">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="review-person">
                                            <img src="http://api.randomuser.me/portraits/men/70.jpg" alt="" class="img-responsive">
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="review-comment">
                                            <h3>“Antebox Is The Best Stable, Fast Platform I Have Ever Experienced”</h3>
                                            <p>
                                                - Theodore Willis
                                                <span>
                                                    <i class="fa fa-star fa-lg"></i>
                                                    <i class="fa fa-star fa-lg"></i>
                                                    <i class="fa fa-star fa-lg"></i>
                                                    <i class="fa fa-star fa-lg"></i>
                                                    <i class="fa fa-star fa-lg"></i>
                                                </span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="review rollitin">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="review-person">
                                            <img src="http://api.randomuser.me/portraits/men/93.jpg" alt="" class="img-responsive">
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="review-comment">
                                            <h3>“Keep It Up Guys Your Work Rules, Cheers :)”</h3>
                                            <p>
                                                - Ricky Grant
                                                <span>
                                                    <i class="fa fa-star fa-lg"></i>
                                                    <i class="fa fa-star fa-lg"></i>
                                                    <i class="fa fa-star fa-lg"></i>
                                                    <i class="fa fa-star fa-lg"></i>
                                                    <i class="fa fa-star-half-o fa-lg"></i>
                                                </span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="screens">
            <div class="container">

                <div class="section-heading scrollpoint sp-effect3">
                    <h1>Products</h1>
                    <div class="divider"></div>
                    <p>See what’s included in the App</p>
                </div>

                <div class="filter scrollpoint sp-effect3">
                    <a href="javascript:void(0)" class="button js-filter-all active">Hot</a>
                    <a href="javascript:void(0)" class="button js-filter-one">New</a>
                    <a href="javascript:void(0)" class="button js-filter-two">On Sale</a>
                    <a href="javascript:void(0)" class="button js-filter-three">Cyprus</a>
                    <a href="javascript:void(0)" class="button js-filter-four">Test</a>
                    <a href="#" class="btn btn-default">All</a>
                </div>
                <div class="slider filtering scrollpoint sp-effect5" >
                    <div class="one">
                        <img src="assets/img/freeze/screens/2.jpg" height="250px" width="250px" alt="">
                        <h4>Profile Page</h4>
                    </div>
                    <div class="two">
                        <img src="assets/img/freeze/screens/44.jpg"  width="250px" height="250px" alt="">
                        <h4>Toggel Menu</h4>
                    </div>
                    <div class="three">
                        <img src="assets/img/freeze/screens/8.jpg"  height="250px" width="250px"  alt="">
                        <h4>Weather Forcast</h4>
                    </div>
                    <div class="one two">
                        <img src="assets/img/freeze/screens/9.jpg" height="250px" width="250px"  alt="">
                        <h4>Sign Up</h4>
                    </div>
                    <div class="one">
                        <img src="assets/img/freeze/screens/11.jpg"  height="250px" width="250px" alt="">
                        <h4>Event Calendar</h4>
                    </div>
                    <div class="two">
                        <img src="assets/img/freeze/screens/45.jpg" width="250px" height="250px" alt="">
                        <h4>Some Options</h4>
                    </div>
                    <div class="three">
                        <img src="assets/img/freeze/screens/47.jpg" width="250px" height="250px" alt="">
                        <h4>Sales Analysis</h4>
                    </div>
                    <div class="four">
                        <img src="assets/img/freeze/screens/9.jpg" width="250px" height="250px" alt="">
                        <h4>Test Testing</h4>
                    </div>
                </div>
            </div>
        </section>

        <section id="demo">
            <div class="container">
                <div class="section-heading scrollpoint sp-effect3">
                    <h1>How it works</h1>
                    <div class="divider"></div>
                    <p></p>
                </div>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 scrollpoint sp-effect2">
                        <div class="video-container" >
                            <iframe src="http://player.vimeo.com/video/70984663"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="getApp">
            <div class="container-fluid">
                <div class="section-heading inverse scrollpoint sp-effect3">
                    <h1>Share your desires with us</h1>
                    <div class="divider"></div>
                    <p>Suggest Company, Store or Product</p>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center hanging-phone scrollpoint sp-effect2 hidden-xs" style="width: 100%;">
                            <img src="assets/img/freeze/freeze-angled2.png" alt="">
                        </div>
                        <div class="platforms">
                            <a href="#" class="btn btn-primary inverse scrollpoint sp-effect1">
                                <i class="fa fa-building fa-3x pull-left"></i>
                                <span>Suggest a</span><br>
                                <b>Company</b>
                            </a>

                                <a href="#" class="btn btn-primary inverse scrollpoint sp-effect2">
                                    <i class="fa fa-heart fa-3x pull-left"></i>
                                    <span>Suggest a</span><br>
                                    <b>Product</b>
                                </a>
                        </div>

                    </div>
                </div>



            </div>
        </section>

        <section id="support" class="doublediagonal">
            <div class="container">
                <div class="section-heading scrollpoint sp-effect3">
                    <h1>Support</h1>
                    <div class="divider"></div>
                    <p>For more info and support, contact us!</p>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-8 col-sm-8 scrollpoint sp-effect1">
                                <form role="form">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Your name">
                                    </div>
                                    <div class="form-group">
                                        <input type="email" class="form-control" placeholder="Your email">
                                    </div>
                                    <div class="form-group">
                                        <textarea cols="30" rows="10" class="form-control" placeholder="Your message"></textarea>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-lg">Submit</button>
                                </form>
                            </div>
                            <div class="col-md-4 col-sm-4 contact-details scrollpoint sp-effect2">
                                <div class="media">
                                    <a class="pull-left" href="#" >
                                        <i class="fa fa-map-marker fa-2x"></i>
                                    </a>
                                    <div class="media-body">
                                        <h4 class="media-heading">Unruly Hive, 42-45 Princelet Street, London E1 5LP, United Kingdom</h4>
                                    </div>
                                </div>
                                <div class="media">
                                    <a class="pull-left" href="#" >
                                        <i class="fa fa-envelope fa-2x"></i>
                                    </a>
                                    <div class="media-body">
                                        <h4 class="media-heading">
                                            <a href="mailto:contact@antebox.com">contact@antebox.com</a>
                                        </h4>
                                    </div>
                                </div>
                                <div class="media">
                                    <a class="pull-left" href="#" >
                                        <i class="fa fa-phone fa-2x"></i>
                                    </a>
                                    <div class="media-body">
                                        <h4 class="media-heading">+44 7761 488380</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

<!--
        <footer>
            <div class="container">
                <a href="#" class="scrollpoint sp-effect3">
                    <img src="assets/img/freeze/logo.png" alt="" class="logo">
                </a>
                <div class="social">
                    <a href="#" class="scrollpoint sp-effect3"><i class="fa fa-twitter fa-lg"></i></a>
                    <a href="#" class="scrollpoint sp-effect3"><i class="fa fa-google-plus fa-lg"></i></a>
                    <a href="#" class="scrollpoint sp-effect3"><i class="fa fa-facebook fa-lg"></i></a>
                </div>
                <div class="rights">
                    <p>Copyright &copy; 2014</p>
                    <p>Template by <a href="http://www.scoopthemes.com" target="_blank">ScoopThemes</a></p>
                </div>
            </div>
        </footer>
-->

    </div>
        <script src="{{ URL::asset('assets/js/jquery-1.11.1.min.js') }}"></script>
        <script src="{{ URL::asset('assets/css/bootstrap.min.css') }}"></script>
        <script src="{{ URL::asset('assets/css/slick.min.css') }}"></script>
        <script src="{{ URL::asset('assets/css/placeholdem.min.js') }}"></script>
        <script src="{{ URL::asset('assets/js/rs-plugin/js/jquery.themepunch.plugins.min.js') }}"></script>
        <script src="{{ URL::asset('assets/js/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>
        <script src="{{ URL::asset('assets/js/waypoints.min.js') }}"></script>
        <script src="{{ URL::asset('assets/js/scripts.js') }}"></script>
    <script>
        $(document).ready(function() {
            appMaster.preLoader();
        });
    </script>
</body>

</html>

