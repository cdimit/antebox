<nav class="w3-sidenav w3-collapse w3-white w3-animate-left" style="z-index:3;width:300px;" id="mySidenav"><br>
  <div class="w3-container w3-row">
    <div class="w3-col s4">
      <img src="/img/user_avatars/{{ $user->pic_url }}" class="w3-circle w3-margin-right" style="width:46px">
    </div>
    <div class="w3-col s8">
      <span>Welcome, <strong>{{$user->first_name}}</strong></span><br>
      <a href="user/{{$user->id}}" class="w3-hover-none w3-hover-text-green w3-show-inline-block"><i class="fa fa-user"></i></a>
      <a href="profile" class="w3-hover-none w3-hover-text-blue w3-show-inline-block"><i class="fa fa-cog"></i></a>
    </div>
  </div>
  <hr>
  <div class="w3-container">
    <h5>Dashboard</h5>
  </div>
  <a href="#" class="w3-padding-16 w3-hide-large w3-dark-grey w3-hover-black" onclick="w3_close()" title="close menu"><i class="fa fa-remove fa-fw"></i>  Close Menu</a>
  <a href="/dashboard" class="w3-padding w3-blue"><i class="fa fa-users fa-fw"></i>  Overview</a>
  <a href="/admin/users" class="w3-padding"><i class="fa fa-users fa-fw"></i>  Users </a>
  <a href="/admin/trips" class="w3-padding"><i class="fa fa-plane fa-fw"></i>  Trips </a>
  <a href="/admin/companies" class="w3-padding"><i class="fa fa-building fa-fw"></i>  Companies </strong></a>
  <a href="/admin/products" class="w3-padding"><i class="fa fa-bicycle fa-fw"></i>  Products</a>
  <a href="/dashboard/suggests" class="w3-padding"><i class="fa fa-heart fa-fw"></i>  Suggests</a>
  <a href="/admin/stores" class="w3-padding"><i class="fa fa-bank fa-fw"></i>  Stores</a>
  <a href="/admin/requests" class="w3-padding"><i class="fa fa-shopping-cart fa-fw"></i>  Requests</a>
  <a href="/admin/deal" class="w3-padding"><i class="fa fa-child fa-fw"></i>  Deals</a><br><br>
</nav>

