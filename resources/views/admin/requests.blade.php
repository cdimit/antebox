@extends('app')


@section('content')
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">

<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>


  <div class="container">

<div class="row-fluid">
    <div class="col-xs-12">
        <table class="table table-striped" id="example">
          <thead>
            <tr>
		<th>ID</th>
		<th>Status</th>
                <th>Product X Qty</th>
                <th>UserID</th>
                <th>Bids</th>
                <th>To</th>
		<th>Options</th>
             </tr>
          </thead>
         <tbody>
         @foreach($requests as $req)
             <tr>
                <td>{{ $req->id }}</td>
		<td>{{ $req->status }}</td>
                <td>{{ $req->product->name }} X {{$req->qty}}</td>
		<td><a href="/user/{{$req->user->id}}">{{ $req->user->first_name }} {{$req->user->last_name}}</a></td>
                <td>{{ $req->bids->count() }}</td>
                <td>{{ $req->places->first()->country }}</td>
		<td>
                <a href="/request/view/{{$req->id}}" class="btn btn-success">View</a>
		</td>
             </tr>
         @endforeach
         </tbody>
    </table>

      </div>
    </div>

  </div>

<script>
$("#example").dataTable();
</script>



@endsection
