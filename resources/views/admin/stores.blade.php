@extends('app')


@section('content')
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">

<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>


  <div class="container">

<div class="row-fluid">
    <div class="col-xs-12">
        <table class="table table-striped" id="example">
          <thead>
            <tr>
                <th>Name</th>
                <th>Country</th>
		<th>City</th>
		<th>Address</th>
		<th>Zip Code</th>
		<th>Phone</th>
		<th>Options</th>
             </tr>
          </thead>
         <tbody>
         @foreach($stores as $st)
             <tr>
                <td>{{ $st->name }}</td>
                <td>{{ $st->country }}</td>
		<td>{{ $st->city }}</td>
		<td>{{ $st->address}}</td>
		<td>{{ $st->zip_code}}</td>
		<td>{{ $st->phone}}</td>
		<td>
		<a href="stores/{{$st->id}}/products" class="btn btn-info">Products</a>
		<a href="stores/{{$st->id}}/edit" class="btn btn-warning">Edit</a>
		</td>
             </tr>
         @endforeach
         </tbody>
    </table>

<p>
<a href="/admin/stores/create" class="btn btn-success">Create Store</a>
</p>


      </div>
    </div>

  </div>

<script>
$("#example").dataTable();
</script>



@endsection
