@extends('app')

@section('content')
  <div class="container">
    <div class="col-md-12">

	<?php $notify = Auth::user()->notifications()->get()->where('status', '0')->sortByDesc('id');?>

<h3>Notifications</h3>
<a href="/admin/notify" class="btn btn-info">Antebox</a>
<a href="/admin/notifymsg" class="btn btn-info">Message</a>
<a href="/admin/notifybid" class="btn btn-info">Bid</a>
<a href="/admin/notifyverify" class="btn btn-info">Verify</a>
<a href="/admin/notifyterms" class="btn btn-info">Terms</a>
<br>

<li class="dropdown" >
<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Notifications ({{ $notify->count()}}) <span class="caret"></span></a>
  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
@if($notify->count()!=0)
<table class="tab">
@foreach($notify as $not)
  <li>
    <tr class="active">
        <td><a href="/notifications/read/{{$not->id}}">
        @if($not->category=="antebox")
        <img src="{{ URL::asset("img/app/notify.png") }}" width="30" height="30" />
        @elseif($not->category=="msg")
        <img src="{{ URL::asset("img/app/notify-msg.png") }}" width="30" height="30" />
        @else
        <img src="{{ URL::asset("img/app/notify-bid.png") }}" width="30" height="30" />
        @endif
        </a></td>
        <td><a href="/notifications/read/{{$not->id}}"><button type="button" class="btn btn-default btn-block">{{$not->msg}}</button></a></td>
        <td><a href="/notifications/remove/{{$not->id}}"><img src="{{ URL::asset("img/app/notify-remove.png") }}" width="30" height="30" /></a></td>
    </tr>
  </li>
@endforeach
<li>
<tr class="active">
<td></td><td>
<a href="/notifications/removeAll"><button type="button" class="btn btn-default btn-block">Clear All</button></a>
</td><td></td>
</tr>
</li>
  </table>
@else
<li>There is no any notifiation</li>
@endif
</ul>

</li>

<br>

<div id="googleMap" style="width:500px;height:380px;"></div>
<input type="text" id="find" value=""><button onclick="fun()">Find</button><br>
Country: <input type="text" id="country" value="" disabled><br>
City: <input type="text" id="city" value="" disabled><br>
Address: <input type="text" id="address" value="" size="60" disabled><br>
<button onclick="edit()" id="edit">Edit</button>
<button onclick="save()" id="save" style="display:none">Save</button>


<br>
<br>
 <form url="/admin/test" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <input type="text" class="form-control" name="keyword" value="">
 <button type="submit" class="btn btn-primary form-control" style="width:100%;">Create</button>
</form>
@foreach($products as $pro)
	{{$pro->name}} <br>
@endforeach

      </div>
    </div>

  </div>
<script
src="https://maps.googleapis.com/maps/api/js">
</script>

<script>
var map;
var myCenter=new google.maps.LatLng(47.978509, 24.162087);

   geocoder = new google.maps.Geocoder();

function codeLatLng(lat, lng) {

    var city="antebox";
    var latlng = new google.maps.LatLng(lat, lng);
    geocoder.geocode({'latLng': latlng}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
      console.log(results)
        if (results[1]) {
         //formatted address
        var values = results[0].formatted_address.split(',');
        if(isNaN(values[values.length-1]))
        document.getElementById("country").value =values[values.length-1];
        else
        document.getElementById("country").value =values[values.length-2];
        document.getElementById("address").value =values;

        //find country name
             for (var i=0; i<results[0].address_components.length; i++) {
            for (var b=0;b<results[0].address_components[i].types.length;b++) {

            //there are different types that might hold a city admin_area_lvl_1 usually does in come cases looking for sublocality type will be more appropriate
                if (results[0].address_components[i].types[b] == "administrative_area_level_1") {
                    //this is the object you are looking for
                    city= results[0].address_components[i];
                    break;
                }
            }
        }

        //city data
        if(values[values.length-1]!=' UK')
                document.getElementById("city").value =city.long_name;
        else
                document.getElementById("city").value =values[values.length-3];

 } else {
          alert("No results found");
        }
      } else {
        alert("Geocoder failed due to: " + status);
        document.getElementById("city").value = "-";
        document.getElementById("country").value = "-";
        document.getElementById("address").value = "-";
      }
    });
  }


function initialize()
{
var mapProp = {
  center:myCenter,
  zoom:3,
  mapTypeId:google.maps.MapTypeId.ROADMAP
  };

  map = new google.maps.Map(document.getElementById("googleMap"),mapProp);

  google.maps.event.addListener(map, 'click', function(event) {
    placeMarker(event.latLng);
  });


}

var marker;

function placeMarker(location) {

 if ( marker ) {
    marker.setPosition(location);
  } else {
  marker = new google.maps.Marker({
      position: location,
      map: map
    });
  }

map.panTo(marker.getPosition());


codeLatLng(location.lat(),location.lng());
}

function fun(){
 find = document.getElementById('find');
            var geocoder =  new google.maps.Geocoder();
    geocoder.geocode( { 'address': find.value}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
                var latlng = new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng());
 placeMarker(latlng);
          } else {
            alert("Something got wrong " + status);
          }
        });
}

function edit(){
 city = document.getElementById('city').disabled = false;
 address = document.getElementById('address').disabled = false;
 document.getElementById('edit').style.display = 'none';
 document.getElementById('save').style.display = 'block';
}

function save(){
 city = document.getElementById('city').disabled = true;
 address = document.getElementById('address').disabled = true;
 document.getElementById('edit').style.display = 'block';
 document.getElementById('save').style.display = 'none';
}


google.maps.event.addDomListener(window, 'load', initialize);
</script>

</div>
</div>
@endsection
