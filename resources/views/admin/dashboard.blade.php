@extends('app')

@section('content')



<?php $notify = Auth::user()->notifications()->get()->where('status', '0')->sortByDesc('id');?>
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <h3>Hello {{ Auth::user()->first_name }}</h3>
        <p><strong>There are <b style="font-size: 20px;">{{ $users->count() }}</b> Users </strong>
		<a href="/admin/users" class="btn btn-success">More</a></p>
        <p><strong>There are <b style="font-size: 20px;">{{ $trips->count() }}</b> Open Trips </strong>
		<a href="/admin/trips" class="btn btn-success">More</a></p>
	<p><strong>There are <b style="font-size: 20px;">{{ $report->count() }}</b> Reports</strong></p>
	<p><strong>There are <b style="font-size: 20px;">{{ $suggest }}</b> Suggests</strong></p>

	<p>
	    <a href="/admin/companies" class="btn btn-default">Companies</a>
	    <a href="/admin/products" class="btn btn-default">Products</a>
	    <a href="/admin/stores" class="btn btn-default">Stores</a>
	    <a href="/admin/requests" class="btn btn-default">Requests</a>
	    <a href="/admin/deal" class="btn btn-default">Deal</a>
	</p>
        <p><strong>Terms and Conditions Last Update <b style="font-size: 20px;">1 July</b></strong>
        <a href="/admin/notifyterms" class="btn btn-danger disabled">Send Notifications (ask IT before use)</a></p>
<br>
	<p><a href="/admin/test" class="btn btn-danger">Test</a></p>

<br>

</div>
</div>
</div>

@endsection
