@extends('app')

@section('content')

<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ asset('css/w3.css') }}">
    <link rel="stylesheet" href="{{ asset('https://fonts.googleapis.com/css?family=Raleway') }}">
    <link rel="stylesheet" href="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css') }}">
<style>
html,body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
</style>
<body class="w3-light-grey">

<?php $user = Auth::user(); ?>

<!-- Top container -->
<div class="w3-container w3-top w3-black w3-hide-large w3-padding" style="z-index:4">
  <button class="w3-btn w3-hide-large w3-padding-2 w3-hover-text-grey" onclick="w3_open();"><i class="fa fa-bars"></i>  Menu</button>
  <span class="w3-right">AnteBox Dashboard</span>
</div>

<!-- Sidenav/menu -->
@include('admin.navbar')

<!-- Overlay effect when opening sidenav on small screens -->
<div class="w3-overlay w3-hide-large w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

<!-- !PAGE CONTENT! -->
<div class="w3-main" style="margin-left:300px;margin-top:43px;">

  <!-- Header -->
  <header class="w3-container" style="padding-top:22px">
    <h5><b><i class="fa fa-dashboard"></i> Suggests</b></h5>
  </header>

<?php $unproduct = $product->where('finished', '0'); ?>
<?php $uncompany = $company->where('finished', '0'); ?>
<?php $fproduct = $product->where('finished', '1'); ?>
<?php $fcompany = $company->where('finished', '1'); ?>

<div class="w3-row-padding w3-margin-bottom">
    <div class="w3-quarter">
      <div class="w3-container w3-red w3-padding-16">
        <div class="w3-left"><i class="fa fa-inbox w3-xxxlarge"></i></div>
        <div class="w3-right">
          <h3>{{$product->count() + $company->count()}}</h3>
        </div>
        <div class="w3-clear"></div>
        <h4>All</h4>
      </div>
    </div>
    <div class="w3-quarter">
      <div class="w3-container w3-blue w3-padding-16">
        <div class="w3-left"><i class="fa fa-heart w3-xxxlarge"></i></div>
        <div class="w3-right">
          <h3>{{$product->count()}}</h3>
        </div>
        <div class="w3-clear"></div>
        <h4>Products</h4>
      </div>
    </div>
    <div class="w3-quarter">
      <div class="w3-container w3-teal w3-padding-16">
        <div class="w3-left"><i class="fa fa-building w3-xxxlarge"></i></div>
        <div class="w3-right">
          <h3>{{$company->count()}}</h3>
        </div>
        <div class="w3-clear"></div>
        <h4>Companies</h4>
      </div>
    </div>
    <div class="w3-quarter">
      <div class="w3-container w3-orange w3-text-white w3-padding-16">
        <div class="w3-left"><i class="fa fa-hourglass w3-xxxlarge"></i></div>
        <div class="w3-right">
          <h3>{{$unproduct->count() + $uncompany->count()}}</h3>
        </div>
        <div class="w3-clear"></div>
        <h4>Unfinished</h4>
      </div>
    </div>
  </div>

<hr>
  <div class="w3-container">
<ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#unfinished"><h5>Unfinished Suggests</h5></a></li>
    <li><a data-toggle="tab" href="#finished"><h5>Finished Suggests</h5></a></li>
 </ul>
<div class="tab-content">
 <div id="unfinished" class="tab-pane fade in active">
    <ul class="w3-ul w3-card-4 w3-white">
@foreach($unproduct as $pro)
      <li class="w3-padding-16">
        <span class="w3-closebtn w3-padding w3-margin-right w3-medium">{{$pro->created_at}} <a href="#p{{$pro->id}}" class="btn btn-success">GO</a></span>
	</a>
        @if($pro->pic_url==null)
        <img class="w3-circle" src="https://alicarnold.files.wordpress.com/2009/11/new-product.jpg?w=150&h=150" style="width:50px;height:50px">
        @else
        <img class="w3-circle" src="{{$pro->pic_url}}" style="width:50px;height:50px">
        @endif
        <span class="w3-xlarge">{{$pro->user_name}} <small>{{$pro->name}}</small></span><br>
      </li>
@endforeach
@foreach($uncompany as $com)
      <li class="w3-padding-16">
        <span class="w3-closebtn w3-padding w3-margin-right w3-medium">{{$com->created_at}} <a href="#c{{$com->id}}" class="btn btn-success">GO</a></span>
	</a>
        <img class="w3-circle" src="http://www.xaviersconsultancy.asia/wp-content/uploads/2015/08/company-icon-300.png" style="width:50px">
        <span class="w3-xlarge">{{$com->name}} <small>{{$com->company}}</small></span><br>
      </li>
@endforeach
    </ul>
  </div>

 <div id="finished" class="tab-pane fade">
    <ul class="w3-ul w3-card-4 w3-white">
@foreach($fproduct as $pro)
      <li class="w3-padding-16">
        <span class="w3-closebtn w3-padding w3-margin-right w3-medium">{{$pro->created_at}} <a href="#p{{$pro->id}}" class="btn btn-success">GO</a></span>
	</a>
        @if($pro->pic_url==null)
        <img class="w3-circle" src="https://alicarnold.files.wordpress.com/2009/11/new-product.jpg?w=150&h=150" style="width:50px;height:50px">
        @else
        <img class="w3-circle" src="{{$pro->pic_url}}" style="width:50px;height:50px">
        @endif
        <span class="w3-xlarge">{{$pro->user_name}} <small>{{$pro->name}}</small></span><br>
      </li>
@endforeach
@foreach($fcompany as $com)
      <li class="w3-padding-16">
        <span class="w3-closebtn w3-padding w3-margin-right w3-medium">{{$com->created_at}} <a href="#c{{$com->id}}" class="btn btn-success">GO</a></span>
	</a>
        <img class="w3-circle" src="http://www.xaviersconsultancy.asia/wp-content/uploads/2015/08/company-icon-300.png" style="width:50px">
        <span class="w3-xlarge">{{$com->name}} <small>{{$com->company}}</small></span><br>
      </li>
@endforeach
    </ul>
  </div>



  <hr>

  <div class="w3-container">
    <h5>Suggests</h5>
@foreach($product->reverse() as $pro)
    <div class="w3-row" id="p{{$pro->id}}"  style="border: 1px solid ;border-radius: 5px;">
      <div class="w3-col m2 text-center">
	@if($pro->pic_url==null)
        <img class="w3-circle" src="https://alicarnold.files.wordpress.com/2009/11/new-product.jpg?w=150&h=150" style="width:96px;height:96px">
	@else
        <img class="w3-circle" src="{{$pro->pic_url}}" style="width:96px;height:96px">
	@endif
      </div>
      <div class="w3-col m10 w3-container">
        @if($pro->user_id==null)
		<h4>{{$pro->user_name}} ( {{$pro->user_email}} ) <span class="w3-opacity w3-medium">{{$pro->created_at}}</span></h4>
	@else
		<h4><a href="/user/{{$pro->user_id}}">{{$pro->user_name}}</a> ( {{$pro->user_email}} ) <span class="w3-opacity w3-medium">{{$pro->created_at}}</span></h4>
	@endif
	<p>Product Name: <strong>{{$pro->name}}</strong></p>
	<p>Product Category: <strong>{{$pro->category}}</strong></p>
	<p>Product Origin: <strong>{{$pro->origin}}</strong></p>
	<hr>
	<p>Producer Name: <strong>{{$pro->producer}}</strong></p>
	<p>Product Website: <strong>{{$pro->website}}</strong></p>
	<p>Product Email: <strong>{{$pro->email}}</strong></p>
	<hr>
        <p>Comment: <div style="white-space:pre-wrap"><strong>{{ $pro->comment }}</strong></div></p>
	@if(!$pro->finished)
        <span class="w3-closebtn w3-padding w3-margin-right w3-medium"><a href="/dashboard/suggest/product/f/{{$pro->id}}" class="btn btn-success">Finished</a></span>
	@else
        <span class="w3-closebtn w3-padding w3-margin-right w3-medium"><a href="/dashboard/suggest/product/u/{{$pro->id}}" class="btn btn-warning">Unfinished</a></span>
	@endif
	<br>
      </div>
    </div>
<br>
@endforeach
@foreach($company->reverse() as $com)
    <div class="w3-row" id="c{{$com->id}}" style="border: 1px solid ;border-radius: 5px;">
      <div class="w3-col m2 text-center">
        <img class="w3-circle" src="http://www.xaviersconsultancy.asia/wp-content/uploads/2015/08/company-icon-300.png" style="width:96px;height:96px">
      </div>
      <div class="w3-col m10 w3-container">
        @if($com->user_id==null)
		<h4>{{$com->name}} ( {{$com->email}} ) <span class="w3-opacity w3-medium">{{$com->created_at}}</span></h4>
	@else
		<h4><a href="/user/{{$com->user_id}}">{{$com->name}}</a> ( {{$com->email}} ) <span class="w3-opacity w3-medium">{{$com->created_at}}</span></h4>
	@endif
	<p>Company Name: <strong>{{$com->company}}</strong></p>
	<p>Company Country: <strong>{{$com->country}}</strong></p>
	<p>Company Address: <strong>{{$com->address}}</strong></p>
	<p>Company Website: <strong>{{$com->website}}</strong></p>
	<p>Suggester Phone: <strong>{{$com->phone}}</strong></p>
	<hr>
	<p>Products: <strong><div style="white-space:pre-wrap">{{ $com->products }}</div></strong></p>
	<p>Why is Unique: <strong><div style="white-space:pre-wrap">{{ $com->unique }}</div></strong></p>
	<p>Comment: <strong><div style="white-space:pre-wrap">{{ $com->comment }}</div></strong></p>
	@if(!$com->finished)
        <span class="w3-closebtn w3-padding w3-margin-right w3-medium"><a href="/dashboard/suggest/company/f/{{$com->id}}" class="btn btn-success">Finished</a></span>
	@else
        <span class="w3-closebtn w3-padding w3-margin-right w3-medium"><a href="/dashboard/suggest/company/u/{{$com->id}}" class="btn btn-warning">Unfinished</a></span>
	@endif
	<br>
      </div>
    </div>
@endforeach
  </div>
  <br>

<script>
// Get the Sidenav
var mySidenav = document.getElementById("mySidenav");

// Get the DIV with overlay effect
var overlayBg = document.getElementById("myOverlay");

// Toggle between showing and hiding the sidenav, and add overlay effect
function w3_open() {
    if (mySidenav.style.display === 'block') {
        mySidenav.style.display = 'none';
        overlayBg.style.display = "none";
    } else {
        mySidenav.style.display = 'block';
        overlayBg.style.display = "block";
    }
}

// Close the sidenav with the close button
function w3_close() {
    mySidenav.style.display = "none";
    overlayBg.style.display = "none";
}
</script>


  <!-- Footer -->
  <footer class="w3-container w3-padding-16 w3-light-grey">
@endsection
  </footer>

  <!-- End page content -->
</div>

