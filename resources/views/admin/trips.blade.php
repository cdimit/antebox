@extends('app')


@section('content')

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.0/Chart.min.js"></script>

  <div class="container">
    <div class="row">
      <div class="col-sm-7">
        <p><strong>There are <b style="font-size: 20px;">{{ $trips->count() }}</b> Total Trips </strong></p>

<div class="dropdown" >
  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
    From Countries ({{$from->count()}})
    <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
@foreach($from as $f)
  <li>
        {{ $f }}
  </li>
@endforeach
  </ul>
</div>


<div class="dropdown" >
  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
    To Countries ({{$to->count()}})
    <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
@foreach($to as $t)
  <li>
        {{ $t }}
  </li>
@endforeach
  </ul>
</div>

<canvas id="myChart" width="100" height="100"></canvas>


      </div>
    </div>
  </div>


<script>
var ctx = document.getElementById("myChart");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
        labels: ["Open", "Cancelled", "Expired", "Completed", "Full", "Almost Full"],
        datasets: [{
            label: '# of Votes',
            data: [{{ $open->count() }}, {{ $cancel->count() }}, {{ $expired->count() }}, {{ $complete->count() }}, {{ $full->count() }}, {{ $almost->count() }}],
            backgroundColor: [
                "#228b22",
                "#8b0000",
                "#ffff00",
                "#1e90ff",
                "#20b2aa",
                "#adff2f"

            ],
            hoverBackgroundColor: [
                "#228b22",
                "#8b0000",
                "#ffff00",
                "#1e90ff",
                "#20b2aa",
                "#adff2f"

            ]
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>



@endsection
