@extends('app')


@section('content')
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">

<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>


  <div class="container">

<div class="row-fluid">
    <div class="col-xs-12">
        <table class="table table-striped" id="example">
          <thead>
            <tr>
		<th>ID</th>
		<th>Picture</th>
                <th>Name</th>
                <th>Company</th>
		<th>Category</th>
                <th>All Visits</th>
                <th>Unique Visits</th>
		<th>Price</th>
		<th style="width:130px">Discount</th>
		<th>Options</th>
             </tr>
          </thead>
         <tbody>
         @foreach($products as $pro)
             <tr>
                <td>{{ $pro->id }}</td>
		<td><img src="/img/products/{{ $pro->pic_url }}" style="width:50px; height:50px; float:left; border-radius:50%; margin-right:25px;"></a></td>
                <td>{{ $pro->name }}</td>
                <td>{{ $pro->company->name }}</td>
		<td>{{ $pro->category }}</td>
                <td>{{ $pro->visit }}</td>
                <td>{{ $pro->visit_uniq }}</td>
		<td>&#8364;{{ $pro->price }}</td>
		<td>
		  @if($pro->discount==null)
		  <form action="/admin/discount/add" method="post" enctype="multipart/form-data">
		    <input type="hidden" name="_token" value="{{ csrf_token() }}">
	    	    <select name="style" class="form-control" style="width:60px">
			<option value="%" >%</option>
			<option value="-" >-</option>
		    </select>
            	    <input type="hidden" name="product" value="{{$pro->id}}">
		    <input type="text" class="form-control" name="value" style="width:70px" placeholder="ex. 30">
		    <button type="submit" class="btn btn-info">Add</button>
		  </form>
		  @else
		    <?php if($pro->discount->style=='%') $discPrice = $pro->price - ($pro->price * ($pro->discount->value / 100)); else $discPrice = $pro->price - $pro->discount->value; ?>
		    Discount Price: &#8364;{{ number_format((float)$discPrice, 2, '.', '')}}<br>
		    Discount: {{$pro->discount->style}}{{$pro->discount->value}}<br>
		    <a href="/admin/discount/remove/{{$pro->id}}" class="btn btn-danger">Remove</a>
		  @endif
		</td>
		<td><a href="product/{{$pro->id}}/stores" class="btn btn-info">Stores</a>
		<a href="/admin/product/edit/{{$pro->id}}" class="btn btn-warning">Edit</a>
		<a href="/product/view/{{$pro->id}}" class="btn btn-success">View</a>
		<a href="/admin/product/remove/{{$pro->id}}" class="btn btn-danger">Remove</a>
		</td>
             </tr>
         @endforeach
         </tbody>
    </table>

                <td><a href="/admin/product/create" class="btn btn-danger">Create Product</a>

      </div>
    </div>

  </div>

<script>
$("#example").dataTable();
</script>



@endsection
