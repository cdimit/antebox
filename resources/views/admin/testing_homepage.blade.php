<!doctype html>
<!--[if lt IE 7]><html lang="en" class="no-js ie6"><![endif]-->
<!--[if IE 7]><html lang="en" class="no-js ie7"><![endif]-->
<!--[if IE 8]><html lang="en" class="no-js ie8"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->

<head>
    <meta charset="UTF-8">
    <title>AnteBox - A network for local businesses powered by you</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="description" content="A new innovative consultancy company that helps globalize businesses that work in a geographically limited area">
  	<meta name="keywords" content="antebox,antebox revolution,local businesses,local business,travel,travellers,unique,haloumi,halloumi,food,unique food,unique haloumi,unique halloumi,unique fashion,shop,online,startup">

  	<meta property="og:title" content="AnteBox - A network for local businesses powered by you" />
  	<meta property="og:type"  content="website" />
  	<meta property="og:url"   content="https://www.antebox.com" />
  	<meta property="og:description" content="A new innovative consultancy company that helps globalize businesses that work in a geographically limited area." />
  	<meta property="og:image" content="{{ url(asset('img/app/favicon.jpg')) }}" />
  	<meta property="og:image:secure_url" content="{{ url(asset('img/app/favicon.jpg')) }}" />
  	<meta property="og:image:type" content="image/png" />
  	<meta property="fb:admins" content="605108675" />
  	<meta property="fb:app_id" content="510079345866294" />

  	<link rel="shortcut icon" href="{{ asset('img/app/favicon.ico') }}">

    <!-- Bootstrap 3.3.2 -->
    <link rel="stylesheet" href="{{ URL::asset('assets/css/bootstrap.min.css') }}">

    <link rel="stylesheet" href="{{ URL::asset('assets/css/animate.css') }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ URL::asset('assets/css/slick.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/js/rs-plugin/css/settings.css') }}">

    <link rel="stylesheet" href="{{ URL::asset('assets/css/styles.css') }}">


    <script type="text/javascript" src="{{ URL::asset('assets/js/modernizr.custom.32033.js') }}"></script>

    <script   src="https://code.jquery.com/jquery-1.12.4.min.js"   integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="   crossorigin="anonymous"></script>

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-71587454-1', 'auto');
      ga('send', 'pageview');
    </script>

    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');

    fbq('init', '238526746496667');
    fbq('track', "PageView");</script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=238526746496667&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->

    <style>
      @media (min-width: 768px) {
        ul.nav li.dropdown:hover > ul.dropdown-menu {
          display: block;
        }
      }
      .price {
        position: absolute;
        top: 30px;
        left: 29px;
        background: #bababa;
        padding: 10px 15px 10px 15px;
      }
    </style>

</head>

<body>

    <div class="pre-loader">
        <div class="load-con">
            <img src="{{ URL::asset('assets/img/freeze/logo.png') }}" class="animated fadeInDown" alt="">
            <div class="spinner">
              <div class="bounce1"></div>
              <div class="bounce2"></div>
              <div class="bounce3"></div>
            </div>
        </div>
    </div>


    <header>


 <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="fa fa-bars fa-lg"></span>
                        </button>
                        <a class="navbar-brand" href="/">
                            <img src="{{ URL::asset('assets/img/logo_blue.png') }}" width="0px" alt="" class="logo">
                        </a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="/requests">Requests</a>
                            </li>
                            <li><a href="/trips">Travellers</a>
                            </li>
                            <li><a href="/products">Products</a>
                            </li>
                            <li ><a href="/partners">Partners</a>
                            </li>
		@if (Auth::guest())
                            <li style="padding-right:20px"><a class="getApp" href="/login">Login</a>
                            </li>
                            <li><a href="/register" class="getApp">Register</a>
                            </li>
		@else

		<?php $notify = Auth::user()->notifications()->get()->where('status', '0')->sortByDesc('id');?>
			<li class="dropdown" >
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
					<i class="fa fa-bell" aria-hidden="true"></i>
					@if($notify->count()!=0)
					<span class="badge badge-notify">{{ $notify->count()}}</span>
					@endif
					<span class="caret"></span>
				</a>
				@if($notify->count()!=0)
				<ul class="dropdown-menu" aria-labelledby="dropdownMenu1" style="background: #f5f5f5; padding: 0;">
					<table class="table" style="padding: 0; margin-bottom: 0px;">
						@foreach($notify as $not)
						<li>
							<tr class="active">
								<td><a href="/notifications/read/{{$not->id}}">
								@if($not->category=="antebox")
								<img src="{{ URL::asset("img/app/notify.png") }}" width="30" height="30" />
								@elseif($not->category=="msg")
								<img src="{{ URL::asset("img/app/notify-msg.png") }}" width="30" height="30" />
								@elseif($not->category=="paid")
								<img src="{{ URL::asset("img/notifications/paid.png") }}" width="30" height="30" />
								@elseif($not->category=="bid")
								<img src="{{ URL::asset("img/app/notify-bid.png") }}" width="30" height="30" />
								@elseif($not->category=="warning")
								<img src="{{ URL::asset("img/notifications/warning.png") }}" width="30" height="30" />
								@elseif($not->category=="shop")
								<img src="{{ URL::asset("img/notifications/shop.png") }}" width="30" height="30" />
								@elseif($not->category=="deal")
								<img src="{{ URL::asset("img/notifications/deal.png") }}" width="30" height="30" />
								@elseif($not->category=="terms")
								<img src="{{ URL::asset("img/notifications/terms.png") }}" width="30" height="30" />
								@endif
								</a></td>
								<td><a href="/notifications/read/{{$not->id}}"><button type="button" class="btn">{{$not->msg}}</button></a></td>
								<td><a href="/notifications/remove/{{$not->id}}"><img src="{{ URL::asset("img/app/notify-remove.png") }}" width="30" height="30" /></a></td>
							</tr>
						</li>

						@endforeach
						@if($notify->count()>1)
						<li>
							<tr class="active">
								<td></td>
								<td>
									<a href="/notifications/removeAll">
										<button type="button" class="btn">Clear All</button>
									</a>
								</td>
								<td></td>
							</tr>
						</li>
						@endif
					</table>
				</ul>
				@endif
			</li>

<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
					<!-- Welcome, {{ Auth::user()->first_name }} -->
					<span class="fa fa-user"></span>
					<span class="caret"></span>
				</a>
				<ul class="dropdown-menu" role="menu">
					@if(Auth::user()->isAdmin())
					<li><a href="/admin" style="color:#336799">Dashboard</a></li>
					@endif
					<li><a href="/user/{{ Auth::user()->id }}" style="color:#336799">Profile</a></li>
					<li><a href="/mydeals" style="color:#336799">My Deals</a></li>
                		        <li><a href="{{ url('/myrequests') }}" style="color:#336799">My Requests</a></li>
					<li><a href="/mybids" style="color:#336799">My Bids</a></li>
		                        <li><a href="{{ url('/trips/mytrips') }}" style="color:#336799">My Trips</a></li>
					<li><a href="{{ url('/logout') }}" style="color:#336799">Logout</a></li>
				</ul>
			</li>

		@endif
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container-->
        </nav>

        <!--RevSlider-->
        <div class="tp-banner-container">
            <div class="tp-banner" >
                <ul>
                    <!-- SLIDE  -->
                    <li data-transition="fade" data-slotamount="7" data-masterspeed="1500" >
                        <!-- MAIN IMAGE -->
                        <img src="{{ URL::asset('assets/img/transparent.png') }}"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                        <!-- LAYERS -->
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption lfl fadeout"
                            data-x="left"
                            data-y="bottom"
                            data-hoffset="30"
                            data-voffset="-40"
                            data-speed="500"
                            data-start="700"
                            data-easing="Power4.easeOut">
                            <img src="{{ URL::asset('assets/img/freeze/Slides/hand-freeze.png') }}" alt="">
                        </div>


                        <div class="tp-caption large_white_bold sft" data-x="550" data-y="center" data-hoffset="0" data-voffset="-80" data-speed="500" data-start="1200" data-easing="Power4.easeOut">
                            Antebox
                        </div>
                        <div class="tp-caption large_white_light sfb" data-x="550" data-y="center" data-hoffset="0" data-voffset="0" data-speed="1000" data-start="1500" data-easing="Power4.easeOut">
                            Shop Locally
                        </div>

                        <div class="tp-caption sfb hidden-xs" data-x="550" data-y="center" data-hoffset="0" data-voffset="85" data-speed="1000" data-start="1700" data-easing="Power4.easeOut">
                            <a href="#about" class="btn btn-primary inverse btn-lg">LEARN MORE</a>
                        </div>


                    </li>
                    <!-- SLIDE 2 -->
                    <li data-transition="zoomout" data-slotamount="7" data-masterspeed="1000" >
                        <!-- MAIN IMAGE -->
                        <img src="{{ URL::asset('assets/img/transparent.png') }}"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                        <!-- LAYERS -->
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption lfb fadeout hidden-xs"
                            data-x="center"
                            data-y="bottom"
                            data-hoffset="0"
                            data-voffset="-50"
                            data-speed="1000"
                            data-start="700"
                            data-easing="Power4.easeOut">
                            <img src="{{ URL::asset('assets/img/freeze/Slides/love.png') }}" alt="">
                        </div>

                        <div class="tp-caption lfb fadeout visible-xs"
                            data-x="center"
                            data-y="bottom"
                            data-hoffset="0"
                            data-voffset="100"
                            data-speed="1000"
                            data-start="700"
                            data-easing="Power4.easeOut">
                            <img src="{{ URL::asset('assets/img/freeze/Slides/love.png') }}" alt="">
                        </div>

                        <div class="tp-caption large_white_light sft" data-x="center" data-y="250" data-hoffset="0" data-voffset="0" data-speed="1000" data-start="1400" data-easing="Power4.easeOut">
                          Local products always taste better
                        </div>
                        <div class="tp-caption sfr hidden-xs" data-x="730" data-y="center" data-hoffset="0" data-voffset="85" data-speed="1500" data-start="1900" data-easing="Power4.easeOut">
                            <a href="#screens" class="btn btn-default btn-lg">Find More</a>
                        </div>

                    </li>

                    <!-- SLIDE 3 -->
                    <li data-transition="zoomout" data-slotamount="7" data-masterspeed="1000" >
                        <!-- MAIN IMAGE -->
                        <img src="{{ URL::asset('assets/img/transparent.png') }}"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                        <!-- LAYERS -->
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption customin customout hidden-xs"
                            data-x="right"
                            data-y="center"
                            data-hoffset="-100"
                            data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
                        data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-voffset="50"
                            data-speed="1000"
                            data-start="700"
                            data-easing="Power4.easeOut">
                            <img src="{{ URL::asset('assets/img/freeze/Slides/antebox.png') }}" height="400px" alt="">
                        </div>

                        <div class="tp-caption customin customout visible-xs"
                            data-x="center"
                            data-y="center"
                            data-hoffset="0"
                            data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
                        data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-voffset="0"
                            data-speed="1000"
                            data-start="700"
                            data-easing="Power4.easeOut">
                            <img src="{{ URL::asset('assets/img/freeze/Slides/antebox.png') }}" alt="">
                        </div>

                        <div class="tp-caption lfb visible-xs" data-x="center" data-y="center" data-hoffset="0" data-voffset="400" data-speed="1000" data-start="1200" data-easing="Power4.easeOut">
                            <a href="#reviews" class="btn btn-primary inverse btn-lg">View Recent Requests</a>
                        </div>


                        <div class="tp-caption mediumlarge_light_white sfl hidden-xs" data-x="left" data-y="center" data-hoffset="0" data-voffset="-50" data-speed="1000" data-start="1000" data-easing="Power4.easeOut">
                           Travelers like Ants,
                        </div>
                        <div class="tp-caption mediumlarge_light_white sft hidden-xs" data-x="left" data-y="center" data-hoffset="0" data-voffset="0" data-speed="1000" data-start="1200" data-easing="Power4.easeOut">
                           Can go Anywhere
                        </div>
                        <div class="tp-caption small_light_white sfb hidden-xs" data-x="left" data-y="center" data-hoffset="0" data-voffset="80" data-speed="1000" data-start="1600" data-easing="Power4.easeOut">
                           <p>Your deliveries are made by travellers like yourself
                             <br>and could reach you even in the same day.
                             <br>You have the ability to make delivery bids on requests
                             <br>and get paid when you deliver the products</p>
                        </div>

                        <div class="tp-caption lfl hidden-xs" data-x="left" data-y="center" data-hoffset="0" data-voffset="160" data-speed="1000" data-start="1800" data-easing="Power4.easeOut">
                            <a href="#reviews" class="btn btn-primary inverse btn-lg">View Recent Requests</a>
                        </div>


                    </li>

                </ul>
            </div>
        </div>


    </header>


    <div class="wrapper">



        <section id="about">
            <div class="container">

                <div class="section-heading scrollpoint sp-effect3">
                    <h1>What is AnteBox?</h1>
                    <div class="divider"></div>
                    <!--<p>Oleose Beautiful App Landing Page</p>-->
                </div>

                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <div class="about-item scrollpoint sp-effect2">
                            <i class="fa fa-globe fa-2x"></i>
                            <h3>Local products</h3>
                            <p>Find unique and traditional products directly from local producers from around the world.</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6" >
                        <div class="about-item scrollpoint sp-effect5">
                            <i class="fa fa-shopping-cart fa-2x"></i>
                            <h3>Request</h3>
                            <p>Whether you are a foody, you miss your country's products or you want to surprise your guests, we have the right products for you.</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6" >
                        <div class="about-item scrollpoint sp-effect5">
                            <i class="fa fa-plane fa-2x"></i>
                            <h3>Deliver</h3>
                            <p>Shipping is not only complex and expensive, it takes too long! That is why we use travellers, like you, to deliver the requests.</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6" >
                        <div class="about-item scrollpoint sp-effect1">
                            <i class="fa fa-users fa-2x"></i>
                            <h3>Network</h3>
                            <p>Our network is growing and involves producers, retailers, restaurants and hotels.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

<section id="features">
            <div class="container">
                <div class="section-heading scrollpoint sp-effect3">
                    <h1>Why Join Us?</h1>
                    <div class="divider"></div>

                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-4 scrollpoint sp-effect1">
                        <div class="media text-center feature">
                            <div class="media-body">
                                <h1 class="media-heading">Clients</h1>
                            </div>
                        </div>
                        <!--
                        <div class="media text-right feature">
                            <a class="pull-right" href="#">
                                <i class="fa fa-search fa-2x"></i>
                            </a>
                            <div class="media-body">
                                <h3 class="media-heading">Discover new flavours</h3>
                                  Whether its for yourself or your guests, our products are offering you unique flavours.
                            </div>
                        </div>
                      -->
                      <div class="media text-right feature">
                          <span class="pull-right">
                              <i class="fa fa-globe fa-2x"></i>
                          </span>
                          <div class="media-body">
                              <h3 class="media-heading">No Limits</h3>
                               Travelers, like ants, can go anywhere
                          </div>
                      </div>
                      <br>
                        <div class="media text-right feature">
                            <span class="pull-right">
                                <i class="fa fa-thumbs-up fa-2x"></i>
                            </span>
                            <div class="media-body">
                                <h3 class="media-heading">Personalised Delivery</h3>
                                Receive delivery bids from travellers and select the one you prefer. Contact your traveller through the in-house chat system
                            </div>
                        </div>
                        <div class="media feature text-right">
                            <span class="pull-right" >
                                <i class="fa fa-clock-o fa-2x"></i>
                            </span>
                            <div class="media-body">
                                <h3 class="media-heading">Quick delivery.</h3>
                                Your deliveries are made by travellers like yourself, and could reach you even in the same day.
                            </div>
                        </div>
                        <div class="media text-right feature">
                            <span class="pull-right" >
                                <i class="fa fa-building fa-2x"></i>
                            </span>
                            <div class="media-body">
                                <h3 class="media-heading">Local Products</h3>
                                Local businesses make their products in small batches and take care of every detail
                            </div>
                        </div>
                        <div class="media text-right feature">
                            <span class="pull-right" >
                                <i class="fa fa-home fa-2x"></i>
                            </span>
                            <div class="media-body">
                                <h3 class="media-heading">Home is where you are</h3>
                                Never miss products from your country again!
                            </div>
                        </div>

                    </div>
                    <div class="col-md-4 col-sm-4">
                        <img src="assets/img/handshake-128fff.png" class="img-responsive scrollpoint sp-effect5" alt="">
                    </div>
                    <div class="col-md-4 col-sm-4 scrollpoint sp-effect2">
                        <div class="media text-center feature">
                            <div class="media-body">
                                <h1 class="media-heading">Travellers</h1>
                            </div>
                        </div>
                        <div class="media feature">
                            <span class="pull-left">
                                <i class="fa fa-heart fa-2x"></i>
                            </span>
                            <div class="media-body">
                                <h3 class="media-heading">Visit local shops</h3>
                                Our trips will be so much better if we were able to experience authentic and traditional goods from every country we visit
                            </div>
                        </div>
                        <div class="media feature">
                            <span class="pull-left">
                                <i class="fa fa-percent fa-2x"></i>
                            </span>
                            <div class="media-body">
                                <h3 class="media-heading">Get discounts and offers</h3>
                                Our partners are happy to offer you better prices and additional products when you show them you are delivering via AnteBox
                            </div>
                        </div>
                        <div class="media feature">
                            <span class="pull-left" >
                                <i class="fa fa-money fa-2x"></i>
                            </span>
                            <div class="media-body">
                                <h3 class="media-heading">Get paid</h3>
                                You have the ability to make delivery bids on requests and get paid when you deliver the products
                            </div>
                        </div>
                        <div class="media feature">
                            <span class="pull-left">
                                <i class="fa fa-arrow-up fa-2x"></i>
                            </span>
                            <div class="media-body">
                                <h3 class="media-heading">Support local businesses</h3>
                              These businesses do not have the staff, time or expertise to ship their products abroad.
                            </div>
                        </div>
                        <div class="media feature">
			    <span class="pull-left">
                                <i class="fa fa-smile-o fa-2x"></i>
                            </span>
                            <div class="media-body">
                                <h3 class="media-heading">Great hospitality.</h3>
                              Our local partners are happy to welcome you with offers, free tasting and tours of their areas.
                            </div>
                        </div>
<!--
                        <div class="media active feature">
                            <a class="pull-left" href="#">
                                <i class="fa fa-plus fa-2x"></i>
                            </a>
                            <div class="media-body">
                                <h3 class="media-heading">And much more!</h3>

                            </div>
                        </div>
                      -->
                    </div>
                </div>
            </div>
        </section>


        <section id="reviews">
            <div class="container">
                <div class="section-heading inverse scrollpoint sp-effect3">
                    <h1>Recent Requests</h1>
                    <div class="divider"></div>
                    <p>Find a request that you can fulfill</p>
                </div>
                <div class="row">
                    <div class="col-md-10 col-md-push-1 scrollpoint sp-effect3">
                        <div class="review-filtering">
			@foreach($request as $req)
                            <div class="review rollitin">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="review-person">
                                            <img src="/img/user_avatars/{{ $req->user->pic_url }}" alt=""  height="108" class="img-circle">
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="review-comment">
                                          <div class="row">
                                              <div class="col-md-9">
                                                <h3>“{{$req->msg}}”</h3>
                                                <p>
                                                    - {{$req->user->first_name}} {{$req->user->last_name}}
                                                    <br><br>
    						 <strong><a href="/product/view/{{$req->product->id}}">{{$req->product->name}}</a></strong> <br>
                                                    <?php $stores =  $req->product->stores->pluck('country')->unique() ?>
                                            @foreach($stores as $st)
                                            <span>{{ $st }}</span>
                                            @endforeach
     <i class="fa fa-plane fa-1x"></i>
    					@foreach($req->places as $place)
                                            @if($place->isClientPlace)
                                            {{ $place->country }}
                                            @endif
                                            @endforeach
    <span></span><a href="/request/view/{{$req->id}}" class="btn btn-success">Make a bid</a>
                                                </p>
                                              </div>
                                              <div class="col-md-3">
                                                <img src="">
                                              </div>
                                          </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
			@endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="screens">
            <div class="container">

                <div class="section-heading scrollpoint sp-effect3">
                    <h1>Products</h1>
                    <div class="divider"></div>
                    <p>See what’s included in the App</p>
                </div>

                <div class="filter scrollpoint sp-effect3">
                    <a href="javascript:void(0)" class="button js-filter-all active">Hot</a>
                    <a href="javascript:void(0)" class="button js-filter-one">New</a>
                    <a href="javascript:void(0)" class="button js-filter-two">On Sale</a>
                    <a href="javascript:void(0)" class="button js-filter-three">Cyprus</a>
                    <a href="javascript:void(0)" class="button js-filter-four">Italy</a>
                    <a href="/products" class="btn btn-default">All</a>
                </div>
                <div class="slider filtering scrollpoint sp-effect5" >
  		   <div class="four two">
			<a href="/product/view/15">
                        <img src="{{ URL::asset('/img/products/15.jpg') }}" height="250px" width="250px" alt=""></a>
                        <span class="price"><i class="fa fa-minus"></i> 25%</span>
                        <h4>Spreadable Beer Jam (Gipsy)</h4>
                    </div>
                    <div class="three">
			<a href="/product/view/1">
                        <img src="{{ URL::asset('/img/products/1.jpg') }}"  height="250px" width="250px" alt=""></a>
                        <span class="price"><i class="fa fa-eur"></i> 7.00</span>
                        <h4>Sousoukos</h4>
                    </div>
                    <div class="one four fill">
			<a href="/product/view/50">
                        <img src="{{ URL::asset('/img/products/50.jpg') }}" height="250px" width="250px"  alt=""></a>
                        <span class="price"><i class="fa fa-eur"></i> 25.00</span>
                        <h4>Panettone with pistachio</h4>
                    </div>
                    <div class="three">
			<a href="/product/view/12">
                        <img src="{{ URL::asset('/img/products/12.jpg') }}" height="250px" width="250px"  alt=""></a>
                        <span class="price"><i class="fa fa-eur"></i> 12.50</span>
                        <h4>Halloumi Cheese</h4>
                    </div>
                    <div class="two four">
			<a href="/product/view/11">
                        <img src="{{ URL::asset('/img/products/11.jpg') }}" height="250px" width="250px"  alt=""></a>
                        <span class="price"><i class="fa fa-minus"></i> 25%</span>
                        <h4>Breaded Baby Mozzarella</h4>
                    </div>
                    <div class="two four">
			<a href="/product/view/5">
                        <img src="{{ URL::asset('/img/products/5.jpg') }}" height="250px" width="250px"  alt=""></a>
                        <span class="price"><i class="fa fa-minus"></i> 25%</span>
                        <h4>Sliced Summer Truffle</h4>
                    </div>
                    <div class="two four">
			<a href="/product/view/7">
                        <img src="{{ URL::asset('/img/products/7.jpg') }}" height="250px" width="250px"  alt=""></a>
                        <span class="price"><i class="fa fa-minus"></i> 25%</span>
                        <h4>Truffle Oil Spray</h4>
                    </div>
                    <div class="one three">
			<a href="/product/view/54">
                        <img src="{{ URL::asset('/img/products/54.jpg') }}" height="250px" width="250px"  alt=""></a>
                        <span class="price"><i class="fa fa-eur"></i> 20.00</span>
                        <h4>Grape Paradise</h4>
                    </div>
                </div>
            </div>
        </section>

        <section id="demo">
            <div class="container">
                <div class="section-heading scrollpoint sp-effect3">
                    <h1>How it works</h1>
                    <div class="divider"></div>
                    <p></p>
                </div>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 scrollpoint sp-effect2">
                        <div class="video-container" >
				<iframe width="560" height="315" src="https://www.youtube.com/embed/fdsoitu2J0I" frameborder="0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="getApp">
            <div class="container-fluid">
                <div class="section-heading inverse scrollpoint sp-effect3">
                    <h1>Share your desires with us</h1>
                    <div class="divider"></div>
                    <p>Suggest Company, Store or Product</p>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center hanging-phone scrollpoint sp-effect2 hidden-xs" style="width: 100%;">
                            <img src="{{ URL::asset('assets/img/freeze/freeze-angled2.png') }}" alt="">
                        </div>
                        <div class="platforms">
                            <a href="https://www.antebox.com/suggest/company" class="btn btn-primary inverse scrollpoint sp-effect1">
                                <i class="fa fa-building fa-3x pull-left"></i>
                                <span>Apply to</span><br>
                                <b>Join</b>
                            </a>

                                <a href="https://www.antebox.com/suggest/product" class="btn btn-primary inverse scrollpoint sp-effect2">
                                    <i class="fa fa-heart fa-3x pull-left"></i>
                                    <span>Suggest a</span><br>
                                    <b>Product</b>
                                </a>
                        </div>

                    </div>
                </div>



            </div>
        </section>

        <section id="support" class="doublediagonal">
            <div class="container">
                <div class="section-heading scrollpoint sp-effect3">
                    <h1>Support</h1>
                    <div class="divider"></div>
                    <p>For more info and support, contact us!</p>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-8 col-sm-8 scrollpoint sp-effect1">
  <form class="form-horizontal" role="form" method="POST" action="{{ url('/support/send')}}">
                                          <input type="hidden" name="_token" value="{{ csrf_token() }}">


                                    <div class="form-group">
                                        <input type="text" class="form-control"  name="name" value="@if(!Auth::guest()) {{Auth::user()->first_name}}@endif" placeholder="Your name" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="email" class="form-control" name="email"  value="@if(!Auth::guest()) {{Auth::user()->email}} @endif" placeholder="Your email" required>
                                    </div>
                                    <div class="form-group">
                                        <textarea cols="30" rows="10" class="form-control" name="message" placeholder="Your message" required></textarea>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-lg">Submit</button>
                                </form>                            </div>
                            <div class="col-md-4 col-sm-4 contact-details scrollpoint sp-effect2">
                                <div class="media">
                                    <a class="pull-left" href="#" >
                                        <i class="fa fa-map-marker fa-2x"></i>
                                    </a>
                                    <div class="media-body">
                                        <h4 class="media-heading">Unruly Hive, 42-45 Princelet Street, London E1 5LP, United Kingdom</h4>
                                    </div>
                                </div>
                                <div class="media">
                                    <a class="pull-left" href="#" >
                                        <i class="fa fa-envelope fa-2x"></i>
                                    </a>
                                    <div class="media-body">
                                        <h4 class="media-heading">
						<a href="mailto:contact@antebox.com">contact@antebox.com</a>
                                        </h4>
                                    </div>
                                </div>
                                <div class="media">
                                    <a class="pull-left" href="#" >
                                        <i class="fa fa-phone fa-2x"></i>
                                    </a>
                                    <div class="media-body">
                                        <h4 class="media-heading">+44 7761 488380</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section style="background-color: #efefef;">
        	<div class="container">
        		<div class="row text-center" style="padding-top: 80px;">
        			<div class="col-md-3">
        				<img src="{{ asset('img/achievements/cityspark.png') }}" style="width: auto; height: 40px;">
        			</div>
        			<div class="col-md-3">
        				<img src="{{ asset('img/achievements/citystarters.png') }}" style="width: auto; height: 40px;">
        			</div>
        			<div class="col-md-3">
        				<img src="{{ asset('img/achievements/madeatcity.png') }}" style="width: auto; height: 40px;">
        			</div>
        			<div class="col-md-3">
        				<img src="{{ asset('img/achievements/thehangout.png') }}" style="width: auto; height: 40px;">
        			</div>
        		</div>
        	</div>
        </section>

        <footer>
            <div class="container">
                <a href="#" class="scrollpoint sp-effect3">
                    <img src="{{ URL::asset('assets/img/freeze/logo.png') }}" alt="" class="logo">
                </a>
                <div class="social">
                    <a href="https://facebook.com/AnteBox" class="scrollpoint sp-effect3"><i class="fa fa-facebook fa-lg"></i></a>
                    <a href="https://twitter.com/anteboxapp" class="scrollpoint sp-effect3"><i class="fa fa-twitter fa-lg"></i></a>
                    <a href="https://www.instagram.com/antebox" class="scrollpoint sp-effect3"><i class="fa fa-instagram fa-lg"></i></a>
                    <a href="https://www.youtube.com/channel/UC654J_tnd7isr9o1hVqThaA" class="scrollpoint sp-effect3"><i class="fa fa-youtube fa-lg"></i></a>
<!--                    <a href="https://www.linkedin.com/company/antebox" class="scrollpoint sp-effect3"><i class="fa fa-linkedin fa-lg"></i></a> -->
                </div>
                <div class="rights">
                    <a href="/contact" class="scrollpoint sp-effect3">Contact</a> |
                    <a href="/about" class="scrollpoint sp-effect3">About</a> |
                    <a href="/faq" class="scrollpoint sp-effect3">FAQ</a> |
                    <a href="/terms" class="scrollpoint sp-effect3">Terms</a>
                    <p style="margin-top:20px;"><i class="fa fa-copyright" aria-hidden="true"> AnteBox</i> &copy; 2016</p>
                </div>
            </div>
        </footer>


    </div>

    <script>
    $( document ).ready(function() {
      $(function() {
        if($(window).width() <= 620) {
          <?php $image_path = URL::asset("assets/img/freeze/logo.png"); ?>
          $('.navbar-brand').html('<img src="{{ $image_path }}" width="0px" alt="" class="logo">');
        }
      });
      $(window).resize(function() {
        if($(window).width() <= 620) {
          <?php $image_path = URL::asset("assets/img/freeze/logo.png"); ?>
          $('.navbar-brand').html('<img src="{{ $image_path }}" width="0px" alt="" class="logo">');
        } else {
          <?php $image_path = URL::asset("assets/img/logo-128fff.png"); ?>
          $('.navbar-brand').html('<img src="{{ $image_path }}" width="0px" alt="" class="logo">');
        }
      });
    });

    </script>

    <script src="{{ URL::asset('assets/js/jquery-1.11.1.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/slick.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/placeholdem.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/rs-plugin/js/jquery.themepunch.plugins.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/waypoints.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/scripts.js') }}"></script>
    <script>
        $(document).ready(function() {
            appMaster.preLoader();
        });
    </script>
</body>

</html>
