@extends('app')


@section('content')
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">

<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>


  <div class="container">

<div class="row-fluid">
    <div class="col-xs-12">
        <p><strong>There are <b style="font-size: 20px;">{{$users->where('verified', '0')->count() }}</b> unverified Users </strong>
	<a href="/admin/notifyverify" class="btn btn-danger disabled">Send Notifications (ask IT before use)</a>
<br>
	<a href="/admin/email/users" class="btn btn-primary">All Users Email List</a>
<br>
	<a href="/admin/email/newsletters" class="btn btn-primary">All Subscribers Users Email List</a>


        <table class="table table-striped" id="example">
          <thead>
            <tr>
                <th>ID</th>
		<th>Picture</th>
                <th>Name</th>
                <th>Email</th>
                <th>Birthday</th>
                <th>Gender</th>
		<th>Verified</th>
             </tr>
          </thead>
         <tbody>
         @foreach($users as $user)
             <tr>

                <td>{{ $user->id }}</td>
		<td><a href="/user/{{$user->id}}">
		<img src="/img/user_avatars/{{ $user->pic_url }}" style="width:50px; height:50px; float:left; border-radius:50%; margin-right:25px;"></a></td>
                <td>{{ $user->first_name }} {{ $user->last_name }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->birthday }}</td>
                <td>{{ $user->sex }}</td>
                <td>{{ $user->verified }}</td>
             </tr>
         @endforeach
         </tbody>
    </table>

      </div>
    </div>

  </div>

<script>
$("#example").dataTable();
</script>


@endsection
