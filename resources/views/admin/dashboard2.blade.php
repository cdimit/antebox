@extends('app')

@section('content')

<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ asset('css/w3.css') }}">
    <link rel="stylesheet" href="{{ asset('https://fonts.googleapis.com/css?family=Raleway') }}">
    <link rel="stylesheet" href="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css') }}">
<style>
html,body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
</style>
<body class="w3-light-grey">

<?php $user = Auth::user(); ?>

<!-- Top container -->
<div class="w3-container w3-top w3-black w3-hide-large w3-padding" style="z-index:4">
  <button class="w3-btn w3-hide-large w3-padding-2 w3-hover-text-grey" onclick="w3_open();"><i class="fa fa-bars"></i>  Menu</button>
  <span class="w3-right">AnteBox Dashboard</span>
</div>

<!-- Sidenav/menu -->
@include('admin.navbar')

<!-- Overlay effect when opening sidenav on small screens -->
<div class="w3-overlay w3-hide-large w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

<!-- !PAGE CONTENT! -->
<div class="w3-main" style="margin-left:300px;margin-top:43px;">

  <!-- Header -->
  <header class="w3-container" style="padding-top:22px">
    <h5><b><i class="fa fa-dashboard"></i> My Dashboard</b></h5>
  </header>

  <div class="w3-row-padding w3-margin-bottom">
  <a href="/admin/users">
    <div class="w3-quarter">
      <div class="w3-container w3-orange w3-text-white w3-padding-16">
        <div class="w3-left"><i class="fa fa-users w3-xxxlarge"></i></div>
        <div class="w3-right">
          <h3>{{$users->count()}}</h3>
        </div>
        <div class="w3-clear"></div>
        <h4>Users</h4>
      </div>
    </div>
  </a>
  <a href="/admin/trips">
    <div class="w3-quarter">
      <div class="w3-container w3-teal w3-padding-16">
        <div class="w3-left"><i class="fa fa-plane w3-xxxlarge"></i></div>
        <div class="w3-right">
          <h3>{{$trips->count()}}</h3>
        </div>
        <div class="w3-clear"></div>
        <h4>Trips</h4>
      </div>
    </div>
  </a>
  <a href="/dashboard/suggests">
    <div class="w3-quarter">
      <div class="w3-container w3-blue w3-padding-16">
        <div class="w3-left"><i class="fa fa-heart w3-xxxlarge"></i></div>
        <div class="w3-right">
          <h3>{{$suggest}}</h3>
        </div>
        <div class="w3-clear"></div>
        <h4>Suggests</h4>
      </div>
    </div>
  </a>
  <a href="#">
    <div class="w3-quarter">
      <div class="w3-container w3-red w3-padding-16">
        <div class="w3-left"><i class="fa fa-flag w3-xxxlarge"></i></div>
        <div class="w3-right">
          <h3>{{$report->count()}}</h3>
        </div>
        <div class="w3-clear"></div>
        <h4>Reports</h4>
      </div>
    </div>
  </a>
  </div>
  <div class="w3-row-padding w3-margin-bottom">
  <a href="/admin/companies">
    <div class="w3-quarter">
      <div class="w3-container w3-green w3-text-white w3-padding-16">
        <div class="w3-left"><i class="fa fa-building w3-xxxlarge"></i></div>
        <div class="w3-right">
          <h3>{{$company}}</h3>
        </div>
        <div class="w3-clear"></div>
        <h4>Companies</h4>
      </div>
    </div>
  </a>
  <a href="/admin/products">
    <div class="w3-quarter">
      <div class="w3-container w3-cyan w3-text-white w3-padding-16">
        <div class="w3-left"><i class="fa fa-bicycle w3-xxxlarge"></i></div>
        <div class="w3-right">
          <h3>{{$products}}</h3>
        </div>
        <div class="w3-clear"></div>
        <h4>Products</h4>
      </div>
    </div>
  </a>
  <a href="/admin/requests">
    <div class="w3-quarter">
      <div class="w3-container w3-indigo w3-padding-16">
        <div class="w3-left"><i class="fa fa-shopping-cart w3-xxxlarge"></i></div>
        <div class="w3-right">
          <h3>{{$requests}}</h3>
        </div>
        <div class="w3-clear"></div>
        <h4>Requests</h4>
      </div>
    </div>
  </a>
  <a href="/admin/deal">
    <div class="w3-quarter">
      <div class="w3-container w3-brown w3-text-white w3-padding-16">
        <div class="w3-left"><i class="fa fa-child w3-xxxlarge"></i></div>
        <div class="w3-right">
          <h3>{{$deals}}</h3>
        </div>
        <div class="w3-clear"></div>
        <h4>Deals</h4>
      </div>
    </div>
  </a>
  </div>

<!--
  <div class="w3-container w3-section">
    <div class="w3-row-padding" style="margin:0 -16px">
      <div class="w3-third">
        <h5>Regions</h5>
        <img src="img_region.jpg" style="width:100%" alt="Google Regional Map">
      </div>
      <div class="w3-twothird">
        <h5>Feeds</h5>
        <table class="w3-table w3-striped w3-white">
          <tr>
            <td><i class="fa fa-user w3-blue w3-padding-tiny"></i></td>
            <td>New record, over 90 views.</td>
            <td><i>10 mins</i></td>
          </tr>
          <tr>
            <td><i class="fa fa-bell w3-red w3-padding-tiny"></i></td>
            <td>Database error.</td>
            <td><i>15 mins</i></td>
          </tr>
          <tr>
            <td><i class="fa fa-users w3-orange w3-text-white w3-padding-tiny"></i></td>
            <td>New record, over 40 users.</td>
            <td><i>17 mins</i></td>
          </tr>
          <tr>
            <td><i class="fa fa-comment w3-red w3-padding-tiny"></i></td>
            <td>New comments.</td>
            <td><i>25 mins</i></td>
          </tr>
          <tr>
            <td><i class="fa fa-bookmark w3-light-blue w3-padding-tiny"></i></td>
            <td>Check transactions.</td>
            <td><i>28 mins</i></td>
          </tr>
          <tr>
            <td><i class="fa fa-laptop w3-red w3-padding-tiny"></i></td>
            <td>CPU overload.</td>
            <td><i>35 mins</i></td>
          </tr>
          <tr>
            <td><i class="fa fa-share-alt w3-green w3-padding-tiny"></i></td>
            <td>New shares.</td>
            <td><i>39 mins</i></td>
          </tr>
        </table>
      </div>
    </div>
  </div>
  <hr>
-->
  <div class="w3-container">
    <h5>General Stats</h5>
    <p>Verified Users</p>
    <?php $ver =  $users->where('verified', '1')->count() / $users->count() * 100; ?>
    <div class="w3-progress-container w3-grey">
      <div id="myBar" class="w3-progressbar w3-green" style="width:{{$ver}}%">
        <div class="w3-center w3-text-white">{{round($ver)}}%</div>
      </div>
    </div>

    <p>Users with Profile Picture</p>
    <?php $pic = ($users->count() - $users->where('pic_url', 'profile.png')->count()) / $users->count() * 100; ?>
    <div class="w3-progress-container w3-grey">
      <div id="myBar" class="w3-progressbar w3-orange" style="width:{{$pic}}%">
        <div class="w3-center w3-text-white">{{round($pic)}}%</div>
      </div>
    </div>

    <p>Facebook Register</p>
    <?php $fb = ($users->count() - $users->where('provider', null)->count()) / $users->count() *100;?>
    <div class="w3-progress-container w3-grey">
      <div id="myBar" class="w3-progressbar w3-blue" style="width:{{$fb}}%">
        <div class="w3-center w3-text-white">{{round($fb)}}%</div>
      </div>
    </div>
  </div>
  <hr>
<!--
  <div class="w3-container">
    <h5>Countries</h5>
    <table class="w3-table w3-striped w3-bordered w3-border w3-hoverable w3-white">
      <tr>
        <td>United States</td>
        <td>65%</td>
      </tr>
      <tr>
        <td>UK</td>
        <td>15.7%</td>
      </tr>
      <tr>
        <td>Russia</td>
        <td>5.6%</td>
      </tr>
      <tr>
        <td>Spain</td>
        <td>2.1%</td>
      </tr>
      <tr>
        <td>India</td>
        <td>1.9%</td>
      </tr>
      <tr>
        <td>France</td>
        <td>1.5%</td>
      </tr>
    </table><br>
    <button class="w3-btn">More Countries  <i class="fa fa-arrow-right"></i></button>
  </div>
  <hr>
-->

  <div class="w3-container">
    <h5>Online Users</h5>
    <ul class="w3-ul w3-card-4 w3-white">
     @foreach($users as $user)
       @if($user->isOnline())
         <li class="w3-padding-16">
	   <a href="/user/{{$user->id}}">
           <img src="/img/user_avatars/{{ $user->pic_url }}" class="w3-left w3-circle w3-margin-right" style="width:35px">
           <span class="w3-xlarge">{{$user->first_name}} {{$user->last_name}}</span><br>
	   </a>
         </li>
       @endif
     @endforeach
    </ul>
  </div>
  <hr>

<!--
  <div class="w3-container">
    <h5>Recent Comments</h5>
    <div class="w3-row">
      <div class="w3-col m2 text-center">
        <img class="w3-circle" src="img_avatar3.png" style="width:96px;height:96px">
      </div>
      <div class="w3-col m10 w3-container">
        <h4>John <span class="w3-opacity w3-medium">Sep 29, 2014, 9:12 PM</span></h4>
        <p>Keep up the GREAT work! I am cheering for you!! Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p><br>
      </div>
    </div>

    <div class="w3-row">
      <div class="w3-col m2 text-center">
        <img class="w3-circle" src="img_avatar1.png" style="width:96px;height:96px">
      </div>
      <div class="w3-col m10 w3-container">
        <h4>Bo <span class="w3-opacity w3-medium">Sep 28, 2014, 10:15 PM</span></h4>
        <p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p><br>
      </div>
    </div>
  </div>
  <br>
  <div class="w3-container w3-dark-grey w3-padding-32">
    <div class="w3-row">
      <div class="w3-container w3-third">
        <h5 class="w3-bottombar w3-border-green">Demographic</h5>
        <p>Language</p>
        <p>Country</p>
        <p>City</p>
      </div>
      <div class="w3-container w3-third">
        <h5 class="w3-bottombar w3-border-red">System</h5>
        <p>Browser</p>
        <p>OS</p>
        <p>More</p>
      </div>
      <div class="w3-container w3-third">
        <h5 class="w3-bottombar w3-border-orange">Target</h5>
        <p>Users</p>
        <p>Active</p>
        <p>Geo</p>
        <p>Interests</p>
      </div>
    </div>
  </div>
-->
<script>
// Get the Sidenav
var mySidenav = document.getElementById("mySidenav");

// Get the DIV with overlay effect
var overlayBg = document.getElementById("myOverlay");

// Toggle between showing and hiding the sidenav, and add overlay effect
function w3_open() {
    if (mySidenav.style.display === 'block') {
        mySidenav.style.display = 'none';
        overlayBg.style.display = "none";
    } else {
        mySidenav.style.display = 'block';
        overlayBg.style.display = "block";
    }
}

// Close the sidenav with the close button
function w3_close() {
    mySidenav.style.display = "none";
    overlayBg.style.display = "none";
}
</script>


  <!-- Footer -->
  <footer class="w3-container w3-padding-16 w3-light-grey">
@endsection
  </footer>

  <!-- End page content -->
</div>

