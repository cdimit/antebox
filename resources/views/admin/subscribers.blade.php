@extends('app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <h3>Hello {{ Auth::user()->getName() }}</h3>
        <p>These are <b style="font-size: 20px;">{{ $subscribers_count }}</b> subscribers on your news mailing list</p>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <ul class="list-group">
          @foreach($subscribers as $subscriber)
            <li class="list-group-item">
              <b>{{ $subscriber->id }}</b>: {{$subscriber->email}}
            </li>
          @endforeach
        </ul>
        {!! $subscribers->render() !!}
      </div>
    </div>
  </div>
@endsection
