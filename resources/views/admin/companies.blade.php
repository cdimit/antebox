@extends('app')


@section('content')
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">

<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>


  <div class="container">

<div class="row-fluid">
    <div class="col-xs-12">
        <table class="table table-striped" id="example">
          <thead>
            <tr>
		<th>ID</th>
                <th>Name</th>
                <th>No of Products</th>
                <th>No of Products Visits</th>
                <th>No of Unique Products Visits</th>
		<th>Options</th>
             </tr>
          </thead>
         <tbody>
         @foreach($companies as $com)
             <tr>
                <td>{{ $com->id }}</td>
                <td>{{ $com->name }}</td>
		<td>{{ $com->products->count() }}</td>
	<?php
$count=0;
$uniqcount=0;
foreach($com->products as $pro){
$count += $pro->visit;
$uniqcount += $pro->visit_uniq;
}
	?>
                <td>{{$count}}</td>
                <td>{{$uniqcount}}</td>
		<td>
		<a href="/admin/company/edit/{{$com->id}}" class="btn btn-warning">Edit</a>
		<a href="/partner/{{$com->nickname}}" class="btn btn-success">View</a>

</td>
             </tr>
         @endforeach
         </tbody>
    </table>

                <td><a href="/admin/company/create" class="btn btn-danger">Create Company</a>

      </div>
    </div>

  </div>

<script>
$("#example").dataTable();
</script>



@endsection
