@extends('app')

	
@section('content')
<style>
.margins {
    	margin: 150px 0px 150px 0px;
}
.card-container {
	display: table;
	background-position: center;
	margin-top: 24px;
	height: 350px;
	width: 100%;
	background-size: cover;
}
.text-container {
	display: table-cell;
	vertical-align: middle;
	color: white;
	text-align: center;
	top: 50%;
}
</style>

<div class="container">
  <div class="row text-center">
    <h1 style="font-size: 72px; margin-top: 40px;">Shop Locally</h1>
    <h4 style="margin-top: 40px;">Request products directly from local shops and get them delivered by travellers just like you</h4>
  </div>

<section>
  <div class="row text-center margins">
      <h1 style="font-size: 48px;">Why we use travellers?</h1>
  </div>

  <div class="row text-center margins">
    <div class="col-md-6">
      <img src="{{ url(asset('img/homepage/globe.png')) }}" style="width: 200px; height: 200px;"/>
    </div>

    <div class="col-md-6">
      	<h2>No limits</h2>
      	<h4>Travelers, like ants, can go anywhere</h4>
    </div>
  </div>
<hr/>
  <div class="row text-center margins">
    <div class="col-md-6">
      
      	<h2>Small Businesses</h2>
      	<h4>Local businesses are usually made of 3-5 employees and don’t have time to send their high quality products abroad. Through AnteBox, travellers visit the stores, buy the products and deliver them to the customers around the world.</h4>
    </div>

    <div class="col-md-6">
      <img src="{{ url(asset('img/steps/homepage/localproducts.png')) }}" style="width: 300px; height: 200px;"/>
    </div>
  </div>
<hr/>
  <div class="row text-center margins">
    <div class="col-md-6">
      <img src="{{ url(asset('img/homepage/personalised_delivery.png')) }}" style="width: 200px; height: 200px;"/>
    </div>

    <div class="col-md-6">
      	<h2>Personalised Delivery</h2>
	<h4>Receive delivery bids from travellers and select the one you prefer. Contact your traveller through the in-house chat system</h4>
    </div>
  </div>
<hr/>
  <div class="row text-center margins">
    <div class="col-md-4">
	<a href="/product/view/11" style="text-decoration: none;">
	<div class="card-container" style="background-image: url(' {{ URL::asset('img/products/11.jpg') }} ');">
		<div class="text-container">
			<h2>Breaded Baby Mozzarella</h2>
			<h4 style="position: absolute; top: 20px; left: 20px;"><img src="{{ url::asset('img/flags/flags_iso/32/it.png') }}"> Italy</h4>
			<div style="position: absolute; bottom: 20px; width: 80px; height: 50px; background: grey;">
				<h4>€ 14.63</h4>
				<h5 style="color: red; margin-top: -8px;"><del>€ 19.50</del></h5>
			</div>
			<div style="position: absolute; top: 32px; right: 15px; width: 60px; height: 50px; background: red;">
				<h4><strong>25% OFF</strong></h4>
			</div>
		</div>
	</div>
	</a>
    </div>
    <div class="col-md-4">
	<a href="/product/view/12" style="text-decoration: none;">
	<div class="card-container" style="background-image: url(' {{ URL::asset('img/products/12.jpg') }} ');">
		<div class="text-container">
			<h2>Halloumi Cheese</h2>
			<h4 style="position: absolute; top: 20px; left: 20px;"><img src="{{ url::asset('img/flags/flags_iso/32/cy.png') }}"> Cyprus</h4>
			<div style="position: absolute; bottom: 20px; width: 80px; height: 40px; background: grey;">
				<h4>€ 12.50</h4>
			</div>
		</div>
	</div>
	</a>
    </div>
    <div class="col-md-4">
	<a href="/product/view/1" style="text-decoration: none;">
	<div class="card-container" style="background-image: url(' {{ URL::asset('img/products/1.jpg') }} ');">
		<div class="text-container">
			<h2>Sousoukos</h2>
			<h4 style="position: absolute; top: 20px; left: 20px;"><img src="{{ url::asset('img/flags/flags_iso/32/cy.png') }}"> Cyprus</h4>
			<div style="position: absolute; bottom: 20px; width: 80px; height: 40px; background: grey;">
				<h4>€ 7.00</h4>
			</div>
		</div>
	</div>
	</a>
    </div>
  </div>

  <div class="row text-center margins">
    <div class="row" style="margin-top: -100px;">
	<div class="col-xs-7">
		<input type="text" class="form-control" name="name" id="search" value=""  placeholder="Search for products..."/>
	</div>
	<div class="col-xs-3">
		<select id="category" class="form-control">
			<option value="">All Categories</option>
			<option class="select-dash" disabled="disabled">--------------</option>
                        
                        
		</select>
	</div>
	<div class="col-xs-2">
		<a class="btn btn-primary" class="form-control" style="width: 100%" id="btnSearch" href="/products/^name^">Search</a>
	</div>
    </div>
  </div>
<hr/>
  <div class="row text-center margins">
    <div class="col-md-6">
 	<h2>Local products always taste better</h2>
	<h4>Local businesses make their products in small batches and take care of every detail</h4>
    </div>

    <div class="col-md-6">
      <img src="{{ url(asset('img/homepage/heart.png')) }}" style="width: 200px; height: 200px;"/>
    </div>
  </div>
<hr/>
  <div class="row text-center margins">
    <div class="col-md-6">
      <img src="{{ url(asset("img/steps/homepage/req3.png")) }}" style="width: 200px; height: 200px;" />
    </div>

    <div class="col-md-6">
      	<h2>Home is where you are</h2>
	<h4>Never miss products from your country again, order now!</h4>
    </div>
  </div>
<hr/>
 <div class="row text-center margins">
    <h2>Local products directly by local producers from around the world</h2>
    <h2>Browse through our partner shops</h2>
  </div>

  <div class="row text-center margins">
    <div class="col-md-6">
      	<h2>Visit local shops</h2>
	<h4>Our trips will be so much better if we were able to experience authentic and traditional goods from every country we visit</h4>
    </div>

    <div class="col-md-6">
      <img src="{{ url(asset("img/steps/homepage/localproducts.png")) }}" style="width: 300px; height: 200px;"/>
    </div>
  </div>
<hr/>
  <div class="row text-center margins">
    <div class="col-md-6">
      <img src="{{ url(asset("img/steps/homepage/request.png")) }}" style="width: 300px; height: 200px;"/>
    </div>

    <div class="col-md-6">
      	<h2>Get discounts and offers</h2>
	<h4>Our partners are happy to offer you better prices and additional products when you show them you are delivering via AnteBox</h4>
    </div>
  </div>
<hr/>
  <div class="row text-center margins">
    <div class="col-md-6">
      	<h2>Get paid</h2>
	<h4>You have the ability to make delivery bids on requests and get paid when you deliver the products</h4>
    </div>

    <div class="col-md-6">
      <img src="{{ url(asset('img/homepage/meeting.png')) }}" style="width: 125px; height: 200px;"/>
    </div>
  </div>
<hr/>
</div>

	<h1 class="text-center" style="margin: 60px 0 60px 0;">What is AnteBox?</h1>
	
	<section style="background: #163850; color: white;">
		<div class="container" style="padding: 60px 0px 60px 0;">
			<div class="row text-center">
				<div class="col-lg-4 col-md-4 col-sm-4">
<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-8  col-lg-offset-0  col-md-offset-0  col-sm-offset-0 col-xs-offset-2">
					<img src="{{ url(asset("img/steps/homepage/localproducts.png")) }}" style="width: 80px; height: 80px;"/>
					<h3 style="font-weight: bold;">Local products</h3>
					<p>Find unique and traditional products
directly from local producers from
around the world. Do you have a
unique product in mind?</p>
				</div>
</div>
</div>
				<div class="col-lg-4 col-md-4 col-sm-4">
<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-8  col-lg-offset-0  col-md-offset-0  col-sm-offset-0 col-xs-offset-2">
					<img src="{{ url(asset("img/steps/homepage/request.png")) }}" style="width: 80px; height: 60px; margin-top: 20px;"/>
					<h3 style="font-weight: bold;">Request</h3>
					<p>Whether you are a foody, you miss
your country’s products or you
want to surprise your guests, we
have the right products for you.
Why request?</p>
				</div>
</div>
</div>
				<div class="col-lg-4 col-md-4 col-sm-4">
<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-8  col-lg-offset-0  col-md-offset-0  col-sm-offset-0 col-xs-offset-2">
					<img src="{{ url(asset("img/steps/homepage/deliver.png")) }}" style="width: 80px; height: 80px;"/>
					<h3 style="font-weight: bold;">Deliver</h3>
					<p>Shipping is not only complex
and expensive, it takes too long!
That is why we use travellers, like
you, to deliver the requests.
Why deliver?</p>
				</div>
</div>
</div>
			</div>
		</div>
	</section>
	
	
	<div class="container" style="padding: 60px 0px 60px 0;">
	
		<div class="row text-center">
			<h2 class="text-center" style="font-weight: bold;">Local products</h2>
		</div>
		<div class="row text-center" style="margin-top: 20px;">
			<div class="col-lg-4 col-md-4 col-sm-4">
<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-8  col-lg-offset-0  col-md-offset-0  col-sm-offset-0 col-xs-offset-2">
				<img src="{{ url(asset("img/steps/homepage/localproducts1.png")) }}" style="width: 80px; height: 80px;"/>
				<h4 style="font-weight: bold;">Reach the world</h4>
				<p>Using travellers as the primary delivery
method your products can reach anyone
around the world.</p>
			</div>
</div>
</div>
			<div class="col-lg-4 col-md-4 col-sm-4">
<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-8  col-lg-offset-0  col-md-offset-0  col-sm-offset-0 col-xs-offset-2">
				<img src="{{ url(asset("img/steps/homepage/localproducts2.png")) }}" style="width: 60px; height: 80px;"/>
				<h4 style="font-weight: bold;">Don’t change anything</h4>
				<p>Travellers come to your shop, buy the
product, and deliver it to the person that
requested it.</p>
			</div>
</div>
</div>
			<div class="col-lg-4 col-md-4 col-sm-4">
<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-8  col-lg-offset-0  col-md-offset-0  col-sm-offset-0 col-xs-offset-2">
				<img src="{{ url(asset("img/steps/homepage/localproducts3.png")) }}" style="width: 110px; height: 50px; margin-top: 30px"/>
				<h4 style="font-weight: bold;">Network</h4>
				<p>Our network is growing and involves
producers, retailers, restaurants and hotels.
You also receieve discounted offers for
creating your web pages and any other
marketing matherial you may need.</p>
			</div>
</div>
</div>
		</div>
		
		<div class="row text-center" style="margin-top: 20px;">
			<a href="#" class="btn btn-primary" style="width: 150px;">Suggest a product</a>
			<a href="#" class="btn btn-primary" style="width: 150px;">Become a partner</a>
		</div>
	</div>
	
	<hr>

	<section>
		<div class="container" style="padding: 8px 0px 36px;">
			<div class="row text-center">
				<h2 style="font-weight: bold;">Request</h2>
			</div>
			
			<div class="row" style="margin-top: 12px">
				<div class="col-lg-5 col-md-5 col-sm-5 col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
					<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-8  col-lg-offset-0  col-md-offset-0  col-sm-offset-0 col-xs-offset-2">
					    <table>
						<tr>
						    <td><img src="{{ url(asset("img/steps/homepage/req1.png")) }}" style="width: 40px; height: 50px; margin-left: 10px"/></td>
						    <td style="padding-left: 20px;">
							<p style="font-weight: bold; margin-top: 6px;">Discover new flavours.</p>
							<p style="margin-top: -8px;">Whether its for yourself or your guests, our products are offering you unique flavours.</p>
					    	    </td>
						</tr>
					    </table>
					</div>
					</div>
					<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-8  col-lg-offset-0  col-md-offset-0  col-sm-offset-0 col-xs-offset-2">
					    <table>
						<tr>
						    <td><img src="{{ url(asset("img/steps/homepage/req2.png")) }}" style="width: 50px; height: 50px;"/></td>
						    <td style="padding-left: 20px;">
							<p style="font-weight: bold;">Know where you are buying from</p>
							<p style="margin-top: -8px;">Every local partner has a dedicated page about their own story.</p>
						    </td>
						</tr>
					    </table>
					</div>
					</div>
					<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-8  col-lg-offset-0  col-md-offset-0  col-sm-offset-0 col-xs-offset-2">
					    <table>
						<tr>
						    <td><img src="{{ url(asset("img/steps/homepage/req3.png")) }}" style="width: 50px; height: 50px;"/></td>
						    <td style="padding-left: 20px;">
							<p style="font-weight: bold;">Never miss products from home</p>
							<p style="margin-top: -8px;">Some products are available or just taste better from your home country.</p>
						    </td>
						</tr>
					    </table>
					</div>
					</div>
				</div>
				<div class="col-lg-5 col-md-5 col-sm-5">
					<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-8  col-lg-offset-0  col-md-offset-0  col-sm-offset-0 col-xs-offset-2">
					    <table>
						<tr>
						    <td><img src="{{ url(asset("img/steps/homepage/req4.png")) }}" style="width: 50px; height: 50px;"/></td>
						    <td style="padding-left: 20px;">
							<p style="font-weight: bold;">Surprise your friends</p>
							<p style="margin-top: -8px;">Do you have friends that are from another country? Treat them their favourite products from their countries!</p>
						    </td>
						</tr>
					    </table>
					</div>
					</div>
					<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-8  col-lg-offset-0  col-md-offset-0  col-sm-offset-0 col-xs-offset-2">
					    <table>
						<tr>
						    <td><img src="{{ url(asset("img/steps/homepage/req5.png")) }}" style="width: 40px; height: 50px;"/></td>
						    <td style="padding-left: 30px;">
							<p style="font-weight: bold;">Quick delivery.</p>
							<p style="margin-top: -8px;">Your deliveries are made by travellers like yourself, and could reach you even in the same day. With constant updates and being able to message your traveller, you know where your delivery is at any point in time.</p>
						    </td>
						</tr>
					    </table>
					</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<hr>

	<section style="padding: 20px 0 20px 0;">
		<div class="container">
			<div class="row text-center">
				<h2>Deliver</h2>
			</div>
			
			<div class="row text-center" style="margin-top: 20px;">
				<div class="col-lg-4 col-md-4">
<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-8  col-lg-offset-0  col-md-offset-0  col-sm-offset-0 col-xs-offset-2">

					<p style="font-weight: bold;">Make the most of your trip.</p>
					<p style="margin-top: -8px;">Visit unique and local shops.</p>
				</div>
</div>
</div>
				<div class="col-lg-4 col-md-4">
<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-8  col-lg-offset-0  col-md-offset-0  col-sm-offset-0 col-xs-offset-2">

					<p style="font-weight: bold;">Support local businesses.</p>
					<p style="margin-top: -8px;">These businesses do not have the staff, time or expertise to ship their products abroad.</p>
				</div>
</div>
</div>
				<div class="col-lg-4 col-md-4">
<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-8  col-lg-offset-0  col-md-offset-0  col-sm-offset-0 col-xs-offset-2">

					<p style="font-weight: bold;">Great hospitality.</p>
					<p style="margin-top: -8px;">Our local partners are happy to welcome you with offers, free tasting and tours of their areas.</p>
				</div>
</div>
</div>
			</div>
			
			<div class="row text-center" style="margin-top: 60px; margin-bottom: 40px;">
				<a href="#" class="btn btn-primary" style="width: 150px;">Add your trip</a>
				<a href="#" class="btn btn-primary" style="width: 150px;">View live requests</a>
			</div>
		</div>
	</section>



@endsection
