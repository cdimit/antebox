@extends('app')

	
@section('content')

	<style>
		.points{
			height: 220px;
		}
	</style>

	<div class="row text-center" >
		<h1 style="  display: inline-block; font-size: 55px; font-weight: 300; margin-bottom: 0; color:#666666">Why sell with us?</h1>
		<div style="position: relative;height: 4px;width: 60px;display: block;text-align: center;margin: 13px auto;background: #128fff;"></div>
		<h3 style="font-size: 25px; color: #999999; font-weight: 300; ">Sample Text Here</h3>
	</div>
	
	<div class="row text-center" style="margin-top: 40px; color:#336799">
		<div class="col-md-4 col-sm-6 col-xs-6 points">
			<i class="fa fa-heart fa-4x" style="solid #128fff;color: #128fff;"></i>
			<h3 style="color: #666666;font-size: 24px">No changes</h3>
			<p style="color:#999999;text-align: center;font-size: 17px;line-height: 25px; ">
				Sell worldwide without having to change your daily operations
			</p>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6 points">
			<i class="fa fa-users fa-4x"  style="solid #128fff;color: #128fff;"></i>
			<h3 style="color: #666666;font-size: 24px">Increase store traffic</h3>
			<p style="color:#999999;text-align: center;font-size: 17px;line-height: 25px; ">
				Travelers will visit your store, buy the product and deliver it to the customer that requested it
			</p>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6 points">
			<i class="fa fa-female fa-4x"  style="solid #128fff;color: #128fff;"></i>
			<h3 style="color: #666666;font-size: 24px">Motivate new customers</h3>
			<p style="color:#999999;text-align: center;font-size: 17px;line-height: 25px; ">
				Offer discounts to travelers, motivating them to buy products for themselves as well
			</p>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6 points">
			<i class="fa fa-commenting fa-4x"  style="solid #128fff;color: #128fff;"></i>
			<h3 style="color: #666666;font-size: 24px">Increase awareness</h3>
			<p style="color:#999999;text-align: center;font-size: 17px;line-height: 25px; ">
				Join a fast growing network of users and businesses
			</p>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6 points">
			<i class="fa fa-globe fa-4x"  style="solid #128fff;color: #128fff;"></i>
			<h3 style="color: #666666;font-size: 24px">Increase availability</h3>
			<p style="color:#999999;text-align: center;font-size: 17px;line-height: 25px; ">
				Now everyone can buy your products without visiting your country
			</p>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6 points">
			<i class="fa fa-bolt fa-4x"  style="solid #128fff;color: #128fff;"></i>
			<h3 style="color: #666666;font-size: 24px">Quick setup</h3>
			<p style="color:#999999;text-align: center;font-size: 17px;line-height: 25px; ">
				Start selling online within minutes (we set you up)
			</p>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6 points">
			<i class="fa fa-gift fa-4x"  style="solid #128fff;color: #128fff;"></i>
			<h3 style="color: #666666;font-size: 24px">Business services</h3>
			<p style="color:#999999;text-align: center;font-size: 17px;line-height: 25px; ">
				Up to 50% discounts for digital and web designing services
			</p>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-6 points">
			<i class="fa fa-check fa-4x"  style="solid #128fff;color: #128fff;"></i>
			<h3 style="color: #666666;font-size: 24px">Free</h3>
			<p style="color:#999999;text-align: center;font-size: 17px;line-height: 25px; ">
				No set-up costs, customers pay a commission per purchase
			</p>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-12 col-md-offset-0 col-sm-offset-3 col-xs-offset-0 points">
			<i class="fa fa-line-chart fa-4x"  style="solid #128fff;color: #128fff;"></i>
			<h3 style="color: #666666;font-size: 24px">Grow your business</h3>
			<p style="color:#999999;text-align: center;font-size: 17px;line-height: 25px; ">
				Receive bulk orders from partner restaurants and retailers
			</p>
		</div>

			<a href="#" class="btn" style="margin-right: 10px;
margin-bottom: 20px;
border-color: #336799;border: 2px solid #336799;
background: none;box-shadow: inset 0 3px 5px rgba(0,0,0,.125);font-size: 14px;
font-weight: 400;outline: 0;border-radius: 3em;padding: 5px 25px;animation-name: fadeInLeft;animation-duration: 1.3s;animation-fill-mode: both;
line-height: 1.42857143;animation-duration: 1.3s;animation-fill-mode: both;
-moz-user-select: none;
text-align: center;
white-space: nowrap;
vertical-align: middle;cursor: pointer;font-family: 'Lato', Arial;text-decoration: none;box-sizing: border-box;">
                                <i class="fa fa-building fa-3x pull-left"></i>
                                <span>Apply to</span><br>
                                <b>Join</b>
                        </a>
	</div>

@endsection
