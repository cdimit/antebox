@extends('app')


@section('content')
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">

<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>


  <div class="container">

<div class="row-fluid">
    <div class="col-xs-12">
        <table class="table table-striped" id="example">
          <thead>
            <tr>
		<th>ID</th>
                <th>Client</th>
                <th>Traveller</th>
                <th>isPaid</th>
                <th>isBought</th>
		<th>isReceived</th>
		<th>isDeliverd</th>
		<th>Price</th>
		<th>Meeteing Date/Time</th>
		<th>Status</th>

             </tr>
          </thead>
         <tbody>
<?php $total =0; $paid=0; $unpaid=0; ?>
         @foreach($deals as $deal)
             <tr>
                <td>{{ $deal->id }}</td>
                <td>{{ $deal->client->first_name }} {{$deal->client->last_name}}</td>
                <td>{{ $deal->traveller->first_name }} {{$deal->traveller->last_name}}</td>
		<td>{{ $deal->pay->paid }}</td>
		<td>{{ $deal->isBuy }}</td>
		<td>{{ $deal->isReceived }}</td>
		<td>{{ $deal->isDeliverd }}</td>
		<td>&#8364;{{ $deal->pay->total }}</td>
		<td>{{ $deal->bid->date }} {{$deal->bid->time}}</td>
		<td>{{ $deal->status }}</td>
		<?php $total += $deal->pay->total;
			if($deal->pay->paid)
				$paid += $deal->pay->total;
			else
				$unpaid += $deal->pay->total;
		?>
             </tr>
         @endforeach
         </tbody>
    </table>

        <p><strong>Total: <b style="font-size: 20px;">&#8364;{{ $total }}</b></strong></p>
        <p><strong>Paid: <b style="font-size: 20px;">&#8364;{{ $paid }}</b></strong></p>
        <p><strong>Unpaid: <b style="font-size: 20px;">&#8364;{{ $unpaid }}</b></strong></p>

      </div>
    </div>

  </div>

<script>
$("#example").dataTable();
</script>



@endsection
