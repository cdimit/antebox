@extends('app')

<style>

</style>

@section('content')
	<link href="{{ asset('/css/bootstrap.vertical-tabs.min.css') }}" rel="stylesheet">
        
	<?php
		$dateTemp = Auth::user()->birthday;
		$createdAt = Auth::user()->created_at;

		$pieces = explode("-","$dateTemp--");
		$createdPieces = explode("-","$createdAt--");

		$monthNames = ["01" => "January",
			   "02" => "February",
			   "03" => "March",
			   "04" => "April",
			   "05" => "May",
			   "06" => "June",
			   "07" => "July",
			   "08" => "August",
			   "09" => "September",
			   "10" => "October",
			   "11" => "November",
			   "12" => "December"];

		$strMonth = $monthNames[$createdPieces[1]];
	?>
         
	<div class="row">
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif

		@if (session('error'))
			<div class="alert alert-danger">
				<strong>{{ session('error') }}</strong>
			</div>
		@endif

		@if (session('status'))
			<div class="alert alert-success">
				<strong>{{ session('status') }}</strong>
			</div>
		@endif
	</div>

  
	
		<div class="container" style="margin-top: 40px;">
			<div class="row" style="min-height:600px;">
				<div class="col-xs-3" style="margin-bottom: 40px;"> <!-- required for floating -->
				  <!-- Nav tabs -->
				  <ul class="nav nav-tabs tabs-left" style="min-width: 150px;">
					<li class="active"><a href="#profile" data-toggle="tab" style="font-weight: bold;">Profile</a></li>
					<li><a href="#avatar" data-toggle="tab" style="font-weight: bold;">Change Picture</a></li>
					<li><a href="#password" data-toggle="tab" style="font-weight: bold;">Change Password</a></li>
					<li><a href="#payout" data-toggle="tab" style="font-weight: bold;">Payout Methods</a></li>
					<li><a href="#notifications" data-toggle="tab" style="font-weight: bold;">Notifications</a></li>
					<li><a href="#verification" data-toggle="tab" style="font-weight: bold;">Verification 
						@if(!Auth::user()->isVerified())
						<span class="text-danger glyphicon glyphicon-exclamation-sign"></span>
						@endif</a></li>
				  </ul>
				</div>

				<div class="col-xs-9">
				  <!-- Tab panes -->
				  <div class="tab-content">
					<div class="tab-pane active" id="profile">
						<form action="/profile/saveProfile" method="post" enctype="multipart/form-data">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<div class="row" style="margin-bottom: 20px">
							<div class="form-group">
								<label style="text-align: left;" class="col-sm-3 control-label">First Name</label>
								<div class="col-sm-9">
								<input type="text" class="form-control" name="first_name" value="{{ Auth::user()->first_name }}">
								</div>
							</div>
							</div>
							<div class="row" style="margin-bottom: 20px">
							<div class="form-group">
								<label style="text-align: left;" class="col-sm-3 control-label">Last Name</label>
								<div class="col-sm-9">
								<input type="text" class="form-control" name="last_name" value="{{ Auth::user()->last_name }}">
								</div>
							</div>
							</div>
							<div class="row" style="margin-bottom: 20px">
							<div class="form-group">
								<label style="text-align: left;" class="col-sm-3 control-label">E-mail</label>
								<div class="col-sm-9">
								<input type="text" class="form-control" name="email" value="{{ Auth::user()->email }}">
								</div>
							</div>
							</div>
							<div class="row" style="margin-bottom: 20px">
							<div class="form-group">
								<label style="text-align: left;" class="col-sm-3 control-label">Telephone</label>
								<div class="col-sm-9">
								<input type="text" class="form-control" name="mobile_phone" value="{{ Auth::user()->mobile_phone }}">
								</div>
							</div>
							</div>
							<div class="row" style="margin-bottom: 20px">
							<div class="form-group">
								<label style="text-align: left;" class="col-sm-3 control-label">I Am</label>
								<div class="col-sm-3">

						<?php $gender = Auth::user()->sex; ?>
								<select name="sex" class="form-control">
									<option value="">Gender</option>
									<option value="Male" <?php if ($gender === 'Male') echo 'selected'; ?> >Male</option>
									<option value="Female" <?php if ($gender === 'Female') echo 'selected'; ?> >Female</option>
											<option value="Other" <?php if ($gender === 'Other') echo 'selected'; ?> >Other</option>
								</select>
								</div>
							</div>
							</div>
							<div class="row" style="margin-bottom: 20px">

							<div class="form-group">
								<label style="text-align: left;" class="col-sm-3 control-label">Birthday</label>
								<div class="col-sm-3">
								<select class="form-control" id="year" name="year"></select>
								</div>
								<div class="col-sm-3">
								<select class="form-control" id="month" name="month"></select>
								</div>
								<div class="col-sm-3">
								<select class="form-control" id="day" name="day"></select>
								</div>
							</div>
							</div>
							<div class="row" style="margin-bottom: 20px">
							<div class="form-group">
								<label style="text-align: left;" class="col-sm-3 control-label text-right">Describe Yourself</label>
								<div class="col-sm-9">
								<textarea class="form-control" rows="4" name="description" style="resize:none;">{{ Auth::user()->description }}</textarea>
								</div>
							</div>
							</div>
							<div class="row" style="margin-bottom: 20px">
							<div class="form-group">
								<div class="col-sm-3 col-sm-offset-6">
									<a href="/user/{{ Auth::user()->id }}" style="width:100%" class="btn btn-info">
										View
									</a>
								</div>
								<div class="col-sm-3">
									<button type="submit" style="width:100%" class="btn btn-success">
										Update
									</button>
								</div>
							</div>
							</div>
						</form>
					</div>
					<div class="tab-pane" id="password">
						<form action="/profile/updatePassword" method="post" enctype="multipart/form-data">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<div class="row" style="margin-bottom: 20px">
							<div class="form-group">
								<label style="text-align: left;" class="col-sm-3 control-label">Old Password</label>
								<div class="col-sm-9">
								<input type="password" class="form-control" name="old_password" value="">
								</div>
							</div>
							</div>
							<div class="row" style="margin-bottom: 20px">
							<div class="form-group">
								<label style="text-align: left;" class="col-sm-3 control-label">New Password</label>
								<div class="col-sm-9">
								<input type="password" class="form-control" name="new_password" value="">
								</div>
							</div>
							</div>
							<div class="row" style="margin-bottom: 20px">
							<div class="form-group">
								<label style="text-align: left;" class="col-sm-3 control-label">Confirm New Password</label>
								<div class="col-sm-9">
								<input type="password" class="form-control" name="new_password_confirmation" value="">
								</div>
							</div>
							</div>
							<div class="row" style="margin-bottom: 20px">
							<div class="form-group">
                                                                <div class="col-sm-3 col-sm-offset-9">
                                                                        <button type="submit" class="btn btn-success form-control" style="width:100%;">Update</button>
                                                                </div>
							</div>
							</div>
						</form>
					</div>
					<div class="tab-pane" id="avatar">
						<form action="/profile/saveProfileImage" method="post" enctype="multipart/form-data">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<div class="row">
								<div class="col-sm-offset-1">
									<div style="margin-top: 10px;">
										<?php $image = 'img/user_avatars/' . Auth::user()->pic_url; ?>
										<img style="max-width: 225px; max-height:225px; width:100%; height:auto;" src="{{ URL::asset($image) }}" />
									</div>
									<button type="submit" id="btnChangePicture" style="margin-top:5px; max-width: 225px;" class="btn btn-success form-control">Update</button><br>
									<label style="max-width: 225px; margin-top:5px;" for="file" class="btn btn-default form-control">
										<span>Change Picture </span>
										<span class="fa fa-camera-retro"></span>
									</label><br>
									<label id="label_file" style="margin-top:10px;"></label>
								</div>
							</div>
							<input type="file" accept="image/*" id="file" name="file" class="hidden">
						</form>
					</div>
					<div class="tab-pane" id="notifications">
						<form action="/profile/updateNotifications" method="get">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							
							<div class="form-group">
								<p>We would like to send you some cool stuff; promotions, coupons and our latest news. By accepting to receive these you will have the chance to follow our steps towards being the greatest online platform offering unique products.
								</p>	
								<p>As we work closely with the community we might also send some surveys and be involved in partner campaigns in order to improve the service we offer to you.
								</p>
							</div>
							<div class="form-group">
								<input id="newsletter" name="newsletter" type="checkbox" value="1" @if(Auth::user()->isNewsletter())checked="checked"@endif  />
								<label style="margin-top: -10px;" for="newsletter"> I Accept</label>
							</div>
							
							<div class="form-group hidden">
								<p>
									Test
								</p>
								<input type="text" placeholder="coming soon" class="form-control">
							</div>
							<div class="form-group">
								<div class="col-sm-3 col-sm-offset-9">
									<button type="submit" class="btn btn-danger form-control" style="width:100%;">Save</button>
								</div>
							</div>
						</form>
					</div>

                                        <div class="tab-pane" id="verification">

<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Email address</h3>
  </div>
  <div class="panel-body">
    @if(Auth::user()->isVerified())
	You have confirmed your email: <strong>{{Auth::user()->email}}</strong>. A confirmed email is important to allow us to securely communicate with you. 
    @else
Please verify your email address by clicking the link in the message we just sent to: {{Auth::user()->email}}
<br>
<br>
Can't find our message? Check your spam folder or <a href="verify">resend the confirmation email.</a>
    @endif
  </div>
</div>
					</div>


                                        <div class="tab-pane" id="payout">

<?php $methods = Auth::user()->payoutMethods;
      $paypal = $methods->where('method', 'paypal')->first();
      $iban = $methods->where('method', 'iban')->first();
?>

                                                <form action="/profile/savePayout" method="post" enctype="multipart/form-data">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="panel panel-default">
  						<div class="panel-heading">
    						<h3 class="panel-title">PayPal</h3>
  						</div>
						<div class="panel-body">
							<div class="form-group">
								<label style="text-align: left; " class="col-sm-3 control-label"><strong>Paypal Account Email</strong></label>
								<div class="col-sm-6">
								<input type="text" class="form-control" name="paypal" @if($paypal) value="{{$paypal->data}}" @endif>
								</div>
							</div>

						</div>
						</div>

						<div class="panel panel-default">
  						<div class="panel-heading">
    						<h3 class="panel-title">IBAN</h3>
  						</div>
						<div class="panel-body">
							<div class="form-group">
								<label style="text-align: left; " class="col-sm-3 control-label"><strong>IBAN Electronic Format</strong></label>
								<div class="col-sm-6">
								<input type="text" class="form-control" name="iban" placeholder="ex: CY17002001280000001200527600" @if($iban) value="{{$iban->data}}" @endif>
								</div>
							</div>

						</div>
						</div>


							<div class="form-group">
								<div class="col-sm-3 col-sm-offset-9">
									<button type="submit" class="btn btn-danger form-control" style="width:100%;">Save</button>
								</div>
							</div>
						</form>

						</div>

                                       	</div>


				</div>

				  </div>
				</div>

				<div class="clearfix"></div>
			</div>
		</div>
	
  
  
<script>

    $('#btnChangePicture').hide();

    var monthNames = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

    $('#year').append($('<option />').val('year').html('Year'));
    for (i = new Date().getFullYear(); i > 1900; i--){
	$('#year').append($('<option />').val(i).html(i));
    }
    $('#year option[ value = {{ $pieces[0] }} ]').attr("selected",true);

    $('#month').append($('<option />').val('month').html('Month'));
    for (i = 1; i < 13; i++){
	$('#month').append($('<option />').val(padDigits(i)).html(monthNames[i-1]));
    }
    $('#month option[ value = {{ $pieces[1] }} ]').attr("selected",true);


    updateNumberOfDays();

    $('#year, #month').on("change", function(){
	updateNumberOfDays();
    });

    function updateNumberOfDays(){
	$('#day').html('');
	month = $('#month').val();
	year = $('#year').val();
	day = daysInMonth(month, year);

	$('#day').append($('<option />').val('day').html('Day'));
	for (i = 1; i < day+1; i++){
	    $('#day').append($('<option />').val(padDigits(i)).html(i));
	}
	$('#day option[ value = {{ $pieces[2] }} ]').attr("selected",true);
    }

    function daysInMonth(month, year){
	return new Date(year, month, 0).getDate();
    }

    function padDigits(number){
	return Array(Math.max(2 - String(number).length + 1, 0)).join(0) + number;
    }

</script>


<!-- script for showing path of image file -->
<script>
    $(function() {
	$("input:file").change(function(){
	    var filePath = $(this).val();
	    var filename = filePath.substring(filePath.lastIndexOf("\\") + 1);

	    if (filename.length > 16){
		var start = filename.substring(0, 6);
		var end = filename.substring(filename.length - 10, filename.length);
		filename = start + "..." + end;
	    }
	    $('#label_file').html(filename);
	    if (filename.length > 0)
		$('#btnChangePicture').show();
	});
    });

</script>

@endsection
