@extends('app')

@section('content')

	<style>
		.vertical-alignment-helper {
			display:table;
			height: 100%;
			width: 100%;
			pointer-events:none;
		}
		.vertical-align-center {
			/* To center vertically */
			display: table-cell;
			vertical-align: middle;
			pointer-events:none;
		}
		.modal-content {
			/* Bootstrap sets the size of the modal in the modal-dialog class, we need to inherit it */
			width:inherit;
			height:inherit;
			/* To center horizontally */
			margin: 0 auto;
			pointer-events:all;
		}

		.white-tooltip + .tooltip > .tooltip-inner {
			border: 1px solid black;
			background-color: #ffffff;
			color: #000000;
			font-size: 14px;
			max-width: 310px;
		}
	</style>

  <div class="container">
    <div class="row">

      <div class="col-lg-3 col-md-4 hide-sm" style="width:33%; float: left;">
		<div style="margin-top: 10px;">
		  <?php $image = 'img/user_avatars/' . $user->pic_url; ?>
          <img style="max-width: 225px; max-height:225px; width:100%; height:auto;" src="{{ URL::asset($image) }}" />
        </div>

		<div style="margin-top: 8px;">
		    <?php if ($user->id == Auth::user()->id){ ?>
			<a href="{{ url('profile') }}" class="btn btn-primary form-control" style="max-width: 225px;">
			    Edit Profile
			</a>
		    <?php } else { ?>
<!--			
			<p  data-toggle="modal"
				data-target="#modal-message"
				id="report_message"
				class="btn btn-primary form-control" style="max-width: 225px; margin-bottom: 8px;">
			    Send a message
			</p>
-->
		        <!-- Button trigger modal -->
			<p  data-toggle="modal"
			    data-target="#modal-flag"
			    id="report_paragraph">
				<i class="fa fa-flag-o fa-2x" title="Report"></i>
			    <a>Report this user</a>
			</p>
			
			<p id="undo_paragraph">
				<i class="fa fa-2x"></i>
			    You have reported this user.
			    <a style="color: red;" id="btn_report_5">Undo?</a>
			</p>
		    <?php } ?>
		</div>

      </div>

      <div class="col-lg-9 col-md-8 col-sm-12" style="width: 67%; float:left;">

        <div class="row">
		<?php
			$createdAt = $user->created_at;

			$createdPieces = explode("-","$createdAt--");

			$monthNames = ["01" => "January",
				   "02" => "February",
				   "03" => "March",
				   "04" => "April",
				   "05" => "May",
				   "06" => "June",
				   "07" => "July",
				   "08" => "August",
				   "09" => "September",
				   "10" => "October",
				   "11" => "November",
				   "12" => "December"];

			$strMonth = $monthNames[$createdPieces[1]];

		?>
          <div class="col-md-12">
            <h1>{{ $user->first_name }} {{ $user->last_name }}</h1>
			<strong>Member since {{ $strMonth }} {{ $createdPieces[0] }}</strong>
          </div>

		  <div class="col-md-12">
            <strong> {{ $user->sex }}</strong>
          </div>

	   <div class="col-md-12">
            <strong> {{ $user->description }}</strong>
          </div>

        </div>
      </div>
    </div>
  </div>

<!-- Modal Message -->
<div class="modal" id="modal-message" tabindex="-3" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
    <div class="vertical-alignment-helper">
	<div class="modal-dialog vertical-align-center">
	    <div class="modal-content">
		<div class="modal-header" style="background-color: #eeeeee; color: #565a5c; font-size: 15px;">
			<button type="button" class="close" data-dismiss="modal" title="Close">
				<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
			</button>
		    <h4 class="modal-title" id="myModalLabel3">Message</h4>
		</div>
		<div class="modal-body">
		    <div class="row space-2" style="margin-bottom: 10px;">
				<div class="col-xs-12 col-middle-alt">
					<input type="text" name="text_subject" id="text_subject" value="" placeholder="Subject" class="form-control" />
				</div>
			</div>
			<div class="row space-2" style="margin-bottom: 10px;">
				<div class="col-xs-12 col-middle-alt">
					<textarea name="text_message" id="text_message" class="form-control" rows="5" style="resize:none;"></textarea>
				</div>
			</div>
		    <div class="row space-2" style="margin-bottom: 10px;">
				<div class="col-xs-3 col-xs-offset-3 col-middle-alt">
					<button id="btn_send" type="submit" class="btn btn-primary form-control">
						Send
					</button>
				</div>
				<div class="col-xs-3 col-middle-alt">
					<button class="btn btn-primary form-control" type="button" class="close" data-dismiss="modal" title="Close">
					Close
					</button>
				</div>
		    </div>
		</div>
	    </div>
	</div>
    </div>
</div>

<!-- Modal Report -->
<div class="modal" id="modal-flag" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="vertical-alignment-helper">
		<div class="modal-dialog vertical-align-center">
			<div class="modal-content">
			    <div class="modal-header" style="background-color: #eeeeee; color: #565a5c; font-size: 15px;">
				<button type="button" class="close" data-dismiss="modal" title="Close">
					<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">Report User</h4>
			    </div>

			    <div class="modal-body">
				<div class="row space-2" style="margin-bottom: 10px;">
				    <div class="col-xs-11 col-middle-alt">
					<a href="#" style="background-color: #fefefe;"
						class="btn btn-default btn-large btn-block"
						id="btn_report_1">
					    This profile shouldn't be on AnteBox
					</a>
				    </div>
				    <div class="col-xs-1 col-middle-alt white-tooltip"
					data-toggle="tooltip" data-placement="left"
					title="This profile may belong to a scammer, criminal offender, or exist for
						other malicious purposes.">
					<i class="fa fa-question-circle fa-2x"></i>
				    </div>
				</div>

				<div class="row space-2" style="margin-bottom: 10px;">
				    <div class="col-xs-11 col-middle-alt">
					<a href="#" style="background-color: #fefefe;"
						class="btn btn-default btn-large btn-block"
						id="btn_report_2">
					    Attempt to share contact information
					</a>
				    </div>
				    <div class="col-xs-1 col-middle-alt white-tooltip"
					data-toggle="tooltip" data-placement="left"
					title="There is an email address, phone number, or website address hidden
						in this profile.">
					<i class="fa fa-question-circle fa-2x"></i>
				    </div>
				</div>

				<div class="row space-2" style="margin-bottom: 10px;">
				    <div class="col-xs-11 col-middle-alt">
					<a href="#" style="background-color: #fefefe;"
						class="btn btn-default btn-large btn-block"
						id="btn_report_3">
					    Inappropriate content or spam
					</a>
				    </div>
				    <div class="col-xs-1 col-middle-alt white-tooltip"
					data-toggle="tooltip" data-placement="left"
					title="The profile photos or description contain violent, graphic,
						promotional, or otherwise offensive content.">
					<i class="fa fa-question-circle fa-2x"></i>
				    </div>
				</div>

                                <div class="row space-2" style="margin-bottom: 10px;">
                                    <div class="col-xs-11 col-middle-alt">
                                	<a href="#" id="otherbtn" style="background-color: #fefefe;"
						class="btn btn-default btn-large btn-block">
                                	    Other
                                        </a>
                                    </div>
				    <div class="col-xs-1 col-middle-alt white-tooltip"
					data-toggle="tooltip" data-placement="left"
					title="None of the above reasons apply.">
					<i class="fa fa-question-circle fa-2x"></i>
				    </div>
                                </div>
			      <div id="otherdiv">


				<div class="row space-2" style="margin-bottom: 10px;">
				    <div class="col-xs-11 col-middle-alt">
					<textarea id="other" class="form-control" style="resize:none;" rows="4"></textarea>
				    </div>
				</div>

				<div class="row space-2" style="margin-bottom: 10px;">
				    <div class="col-xs-3 col-xs-offset-8">
					<button type="submit" id="btn_report_4" class="btn btn-primary form-control">
					    Submit
					</button>
				    </div>
				</div>
			      </div>
			    </div>
			</div>
		</div>
	</div>
</div>

<!-- Modal Thanks -->
<div class="modal" id="modal-thanks" tabindex="-2" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
    <div class="vertical-alignment-helper">
	<div class="modal-dialog vertical-align-center">
	    <div class="modal-content">
		<div class="modal-header" style="background-color: #eeeeee; color: #565a5c; font-size: 15px;">
		    <h4 class="modal-title" id="myModalLabel2">Thank You!</h4>
		</div>

		<div class="modal-body">
		    <div class="row space-2" style="margin-bottom: 10px;">
			<div class="col-xs-12 col-middle-alt">
			    Thanks for taking the time to report this user. Your participation helps keep Antebox a safe and trusted community and it doesn't go unnoticed!
			</div>
		    </div>
		    <div class="row space-2" style="margin-bottom: 10px;">
			<div class="col-xs-4 col-xs-offset-4 col-middle-alt">
			    <button class="btn btn-primary form-control" type="button" class="close" data-dismiss="modal" title="Close">
				Close
			    </button>
			</div>
		    </div>
		</div>
	    </div>
	</div>
    </div>
</div>

<!-- Modal Thanks -->
<div class="modal" id="modal-message-thanks" tabindex="-2" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
    <div class="vertical-alignment-helper">
	<div class="modal-dialog vertical-align-center">
	    <div class="modal-content">
		<div class="modal-header" style="background-color: #eeeeee; color: #565a5c; font-size: 15px;">
		    <h4 class="modal-title" id="myModalLabel3">Thank You!</h4>
		</div>

		<div class="modal-body">
		    <div class="row space-2" style="margin-bottom: 10px;">
			<div class="col-xs-12 col-middle-alt">
			    Your Message has been sent.
			</div>
		    </div>
		    <div class="row space-2" style="margin-bottom: 10px;">
			<div class="col-xs-4 col-xs-offset-4 col-middle-alt">
			    <button id="btn_send" class="btn btn-primary form-control" type="button" class="close" data-dismiss="modal" title="Close">
				Close
			    </button>
			</div>
		    </div>
		</div>
	    </div>
	</div>
    </div>
</div>


<script>
  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();

    $("#otherdiv").hide();
    $("#otherbtn").click(function(){
	$("#otherdiv").show();
    });

    <?php if ( $user->isReportedFrom(Auth::user()->id) ){
echo	'$("#report_paragraph").hide();';
echo	'$("#undo_paragraph").show();';
    } else {
echo	'$("#undo_paragraph").hide();';
echo	'$("#report_paragraph").show();';
    }?>

//ajax
    $("#btn_report_1").click(function(e){
	e.preventDefault();
	$.ajax({type:"get",
	    url: "{{ $user->id }}/report/1",
	    data: { comment: "" },
	    success: function(result){
		$("#modal-flag").modal('hide');
		$("#report_paragraph").hide();
		$("#undo_paragraph").show();
		$("#modal-thanks").modal('show');
	    }
	});
    });

    $("#btn_report_2").click(function(e){
	e.preventDefault();
	$.ajax({type:"get",
	    url: "{{ $user->id }}/report/2",
	    data: { comment: "" },
	    success: function(result){
	        $("#modal-flag").modal('hide');
		$("#report_paragraph").hide();
		$("#undo_paragraph").show();
		$("#modal-thanks").modal('show');
	    }
	});
    });

    $("#btn_report_3").click(function(e){
	e.preventDefault();
	$.ajax({type:"get",
	    url: "{{ $user->id }}/report/3",
	    data: { comment: "" },
	    success: function(result){
		$("#modal-flag").modal('hide');
		$("#report_paragraph").hide();
		$("#undo_paragraph").show();
		$("#modal-thanks").modal('show');
	    }
	});
    });

    $("#btn_report_4").click(function(e){
	e.preventDefault();
	$.ajax({type:"get",
	    url: "{{ $user->id }}/report/4",
	    data: { comment: $("#other").val() },
	    success: function(result){
		$("#modal-flag").modal('hide');
		$("#report_paragraph").hide();
		$("#undo_paragraph").show();
		$("#modal-thanks").modal('show');
	    }
	});
    });

    $("#btn_report_5").click(function(e){
	e.preventDefault();
	$.ajax({type:"get",
	    url: "{{ $user->id }}/report/5",
	    data: { comment: "" },
	    success: function(result){
		$("#undo_paragraph").hide();
		$("#report_paragraph").show();
	    }
	});
    });
	
	$("#btn_send").click(function(e){
	e.preventDefault();
	$.ajax({type:"get",
	    url: "{{ $user->id }}/message",
	    data: { text_subject: $("#text_subject").val(), text_message: $("#text_message").val() },
	    success: function(result){
		$("#modal-message").modal('hide');
		$("#modal-message-thanks").modal('show');
		$("#text_subject").val("");
		$("#text_message").val("");
	    }
	});
    });

  });
</script>



@endsection
