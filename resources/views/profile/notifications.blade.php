@extends('app')

@section('content')
	<link href="{{ asset('/css/bootstrap.vertical-tabs.min.css') }}" rel="stylesheet">
	<style>
		.badge-primary {
			background-color:#337ab7;
		}
	</style>
<div class="container" style="margin-top: 80px;">
 <div class="col-md-8">

        <form action="/profile/updateNotifications" method="get">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">


                                <div class="row">
                                        <div class="panel panel-default">
                                                <div class="panel-heading"><h3 class="panel-title">Email Settings</h3></div>
                                                <div class="panel-body">
            <div class="row">
              <div class="col-sm-4">
                <h3>I want to receive:</h3>
                <p><strong>You can disable these at any time.</strong></p>
              </div>
	  <div class="col-sm-6">
		<div class="col-sm-1">
		  <input @if(Auth::user()->isNewsletter())checked="checked"@endif id="newsletter" name="newsletter" type="checkbox" value="1" />
                  </div><div class="col-sm-10">General promotions, updates and news about Antebox.
              </div></div>
            </div>

                                                </div>
                                        </div>
                                </div>
</div>

  <div class="row">
                        <div class="form-group">
                                <div class="col-sm-2 col-sm-offset-6">
                                        <button type="submit" class="btn btn-danger form-control" style="width:100%;">Save</button>
                                </div>
                        </div>
                </div>

</form>

        </div>

	</div>

@endsection
