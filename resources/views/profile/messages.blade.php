<?php 
	use App\Messages;
	use App\User;
	$all_count = Messages::where('user_id_to', Auth::user()->id)->count();
?>

@extends('app')

@section('content')
	<link href="{{ asset('/css/bootstrap.vertical-tabs.min.css') }}" rel="stylesheet">
	<style>
		.badge-primary {
			background-color:#337ab7;
		}
	</style>
	

	<div class="container" style="margin-top: 80px;">
	
		<div class="row" style="margin-bottom: 40px;">
			<div class="col-xs-2">
				<span class="badge pull-right">10</span>
				<span class="label label-pill label-primary pull-right">2</span>
				<span class="badge badge-primary pull-right">4</span>
			</div>
		</div>
	
		<div class="row" style="min-height:600px;">
			<div class="col-xs-3" style="margin-bottom: 20px;"> <!-- required for floating -->
				<!-- Nav tabs -->
				<ul class="nav nav-tabs tabs-left" style="min-width: 150px;">
					<li class="active"><a href="#all_messages" data-toggle="tab">All Messages <span class="badge badge-primary pull-right">{{ $all_count }}</span></a></li>
					<li><a href="#inbox_messages" data-toggle="tab">Inbox <span class="badge badge-primary pull-right">{{ $all_count }}</span></a></li>
					<li><a href="#unread_messages" data-toggle="tab">Unread</a></li>
					<li><a href="#sent_messages" data-toggle="tab">Sent</a></li>
				</ul>
			</div>

			<div class="col-xs-9">
				<!-- Tab panes -->
				<div class="tab-content">
					<div class="tab-pane active" id="all_messages" style="margin-left: 10px; margin-top: 10px;">
						
						<?php
							$messages = Messages::where('user_id_to', Auth::user()->id)->get();
							$i = 0;
						?>
						<table class="table table-hover">
							<thead>
								<th>From</th><th>Subject</th>
							</thead>
							
							<tbody>
								<?php foreach ($messages as $message){ 
									$i++;
									$from_id = $message['user_id_from'];
									$from = User::find($from_id);
									echo '<tr>';
									echo '<td>'.$from['first_name'].'</td>';
									echo '<td>'.$message['subject'].'</td>';
									echo '</tr>';
									
								}
								?>
							
							</tbody>
						</table>
						
							
							
							
						
					</div>
					<div class="tab-pane" id="inbox_messages">
						<?php
							$messages = Messages::where('user_id_to', Auth::user()->id)->get();
							$i = 0;
						?>
						<table class="table table-hover">
							<thead>
								<th>From</th><th>Subject</th>
							</thead>
							
							<tbody>
								<?php foreach ($messages as $message){ 
									$i++;
									$from_id = $message['user_id_from'];
									$from = User::find($from_id);
									echo '<tr>';
									echo '<td>'.$from['first_name'].'</td>';
									echo '<td>'.$message['subject'].'</td>';
									echo '</tr>';
									
								}
								?>
							
							</tbody>
						</table>
					
					
					
					</div>
					<div class="tab-pane" id="unread_messages">Unread Tab.</div>
					<div class="tab-pane" id="sent_messages">Sent Tab.</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>

	
<?php 
	/*
	echo '<div class="panel-group" id="accordion">';
	$messages = Messages::where('user_id_to', Auth::user()->id)->get();
	$i = 0;
	foreach ($messages as $message){
		$i++;
		$from_id = $message['user_id_from'];
		$from = User::find($from_id);
		echo '<div class="panel panel-default" style="margin-top: -1px;">';
		echo '	<div class="panel-heading" data-toggle="collapse" data-target="#collapse'.$i.'">';
		echo '		<h4 class="panel-title"><span style="width: 200px;"></span><span>'.$message['subject'].'</span></h4>';
		echo '	</div>';
		echo '	<div id="collapse'.$i.'" class="panel-collapse collapse">';
		echo '		<div class="panel-body">';
		echo 			$message['message'];
		echo '		</div>';
		echo '	</div>';
		echo '</div>';
	}
	echo '</div>';
	
	*/
?>
	
<script>
	$(function () {

		var active = true;

		$('#collapse-init').click(function () {
			if (active) {
				active = false;
				$('.panel-collapse').collapse('show');
				$('.panel-title').attr('data-toggle', '');
			} else {
				active = true;
				$('.panel-collapse').collapse('hide');
				$('.panel-title').attr('data-toggle', 'collapse');
			}
		});
		
		$('#accordion').on('show.bs.collapse', function () {
			if (active) $('#accordion .in').collapse('hide');
		});

	});
</script>


@endsection
