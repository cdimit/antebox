$(document).ready(function() {
		var num = {{ $product->price }} * $('#quantity').val();
		var total = num.toFixed(2);
		$('#label_total').html(total);
		
		$('#quantity').change(function(){
			num = {{ $product->price }} * $('#quantity').val();
			total = num.toFixed(2);
			$('#label_total').html(total);
		});
		$('#btn_inc').click(function(){
			var qty = parseInt($('#quantity').val()) + 1;
			num = {{ $product->price }} * qty;
			total = num.toFixed(2);
			$('#label_total').html(total);
		});
		$('#btn_dec').click(function(){
			var qty = parseInt($('#quantity').val()) - 1;
			if ( qty < 1 ){
				$('#quantity').val('2');
				qty = 1;
			} else {
				num = {{ $product->price }} * qty;
				total = num.toFixed(2);
				$('#label_total').html(total);
			}
			
		});
		
		var date = new Date();
		date.setDate(date.getDate());
		
		$('#datetimepicker').datetimepicker({
			format: "YYYY/MM/DD"
		});
		
		$('#datetimepicker').data("DateTimePicker").minDate(date);
		
		(function ($) {
			$('.spinner .btn:first-of-type').on('click', function() {
				$('.spinner input').val( parseInt($('.spinner input').val(), 10) + 1);
			});
			$('.spinner .btn:last-of-type').on('click', function() {
				$('.spinner input').val( parseInt($('.spinner input').val(), 10) - 1);
			});
		})(jQuery);
	});


var map;
var myCenter=new google.maps.LatLng(47.978509, 24.162087);

  geocoder = new google.maps.Geocoder();

function codeLatLng(lat, lng) {

    var city="antebox";
    var latlng = new google.maps.LatLng(lat, lng);
    geocoder.geocode({'latLng': latlng}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
      console.log(results)
        if (results[1]) {
         //formatted address
        var values = results[0].formatted_address.split(',');
        if(isNaN(values[values.length-1]))
        document.getElementById("country").value =values[values.length-1];
        else
        document.getElementById("country").value =values[values.length-2];
        document.getElementById("address").value =values;

        //find country name
             for (var i=0; i<results[0].address_components.length; i++) {
            for (var b=0;b<results[0].address_components[i].types.length;b++) {

            //there are different types that might hold a city admin_area_lvl_1 usually does in come cases looking for sublocality type will be more appropriate
                if (results[0].address_components[i].types[b] == "administrative_area_level_1") {
                    //this is the object you are looking for
                    city= results[0].address_components[i];
                    break;
                }
            }
        }

        //city data
        if(values[values.length-1]!=' UK')
                document.getElementById("city").value =city.long_name;
        else
                document.getElementById("city").value =values[values.length-3];

 } else {
          alert("No results found");
        }
      } else {
        alert("Geocoder failed due to: " + status);
        document.getElementById("city").value = "-";
        document.getElementById("country").value = "-";
        document.getElementById("address").value = "-";
      }
    });
  }


function initialize()
{
var mapProp = {
  center:myCenter,
  zoom:3,
  mapTypeId:google.maps.MapTypeId.ROADMAP
  };

  map = new google.maps.Map(document.getElementById("googleMap"),mapProp);

  google.maps.event.addListener(map, 'click', function(event) {
    placeMarker(event.latLng);
  });
}

var marker;

function placeMarker(location) {

 if ( marker ) {
    marker.setPosition(location);
  } else {
  marker = new google.maps.Marker({
      position: location,
      map: map
    });
  }

map.panTo(marker.getPosition());


codeLatLng(location.lat(),location.lng());
}

function fun(){
 find = document.getElementById('find');
            var geocoder =  new google.maps.Geocoder();
    geocoder.geocode( { 'address': find.value}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            var latlng = new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng());
 	    placeMarker(latlng);
          } else {
            alert("Something got wrong " + status);
          }
        });
}

function editfun(){
 city = document.getElementById('city').disabled = false;
 address = document.getElementById('address').disabled = false;
 document.getElementById('edit').style.display = 'none';
 document.getElementById('save').style.display = 'block';
}

function savefun(){
 city = document.getElementById('city').disabled = true;
 address = document.getElementById('address').disabled = true;
 document.getElementById('edit').style.display = 'block';
 document.getElementById('save').style.display = 'none';
}


google.maps.event.addDomListener(window, 'load', initialize);
