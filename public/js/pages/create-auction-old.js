$('#small-parcel').click(function(){
  $('#small-parcel').addClass('size-select-box-active');
  $('#medium-parcel').removeClass('size-select-box-active');
  $('#large-parcel').removeClass('size-select-box-active');
  $('#custom-parcel').removeClass('size-select-box-active');
  $('#small-parcel-radio').prop("checked", true);
});

$('#medium-parcel').click(function(){
  $('#medium-parcel').addClass('size-select-box-active');
  $('#small-parcel').removeClass('size-select-box-active');
  $('#large-parcel').removeClass('size-select-box-active');
  $('#custom-parcel').removeClass('size-select-box-active');
  $('#medium-parcel-radio').prop("checked", true);
});

$('#large-parcel').click(function(){
  $('#large-parcel').addClass('size-select-box-active');
  $('#small-parcel').removeClass('size-select-box-active');
  $('#medium-parcel').removeClass('size-select-box-active');
  $('#custom-parcel').removeClass('size-select-box-active');
  $('#large-parcel-radio').prop("checked", true);
});

$('#custom-parcel').click(function(){
  $('#custom-parcel').addClass('size-select-box-active');
  $('#medium-parcel').removeClass('size-select-box-active');
  $('#large-parcel').removeClass('size-select-box-active');
  $('#small-parcel').removeClass('size-select-box-active');
  $('#custom-parcel-radio').prop("checked", true);
});

$(document).ready(function(){
  $("#deadline-timepicker").datetimepicker({
    inline: true,
    format: 'YYYY-MM-DD  HH:mm:ss'
  });

  $('form').on('submit', function(e){
    $("input[name='deadline']").val($('#deadline-timepicker').data().date);
  });

  var map;
  var markersArray = [];

  function initMap()
  {
      var latlng = new google.maps.LatLng(51.5287718,-0.2416803);
      var myOptions = {
          zoom: 12,
          center: latlng,
          mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      map = new google.maps.Map(document.getElementById("gmap-destination"), myOptions);

      // add a click event handler to the map object
      google.maps.event.addListener(map, "click", function(event)
      {
          // place a marker
          placeMarker(event.latLng);

          // display the lat/lng in your form's lat/lng fields
          $("input[name='meeting_x']").val(event.latLng.lat());
          $("input[name='meeting_y']").val(event.latLng.lng());
      });
  }

  function placeMarker(location) {
      // first remove all markers if there are any
      deleteOverlays();

      var marker = new google.maps.Marker({
          position: location,
          map: map
      });

      // add marker in markers array
      markersArray.push(marker);

      //map.setCenter(location);
  }

  // Deletes all markers in the array by removing references to them
  function deleteOverlays() {
      if (markersArray) {
          for (i in markersArray) {
              markersArray[i].setMap(null);
          }
      markersArray.length = 0;
      }
  }
  initMap();
});
