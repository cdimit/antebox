$(document).ready(function(){
  $('#bid-tip-icon').mouseover(function(){
    $('#bid-tip').slideDown(1000);
  });

  $('#active-bids-icon').click(function(){
    $(this).toggleClass('fa-chevron-down');
    $(this).toggleClass('fa-chevron-up');
    $('#active-bids').slideToggle(1000);
  });
});

$('#bid-form').submit(function(e){
  e.preventDefault();
  $.ajax({
  type: "POST",
  url: '/auctions/bid',
  data: $(this).serialize(),
  success: function(response) {
    swal(response);
    $('#auction-bids').html(response.data);
  },
  error: function(xhr, status, response)
  {
    $('#bid-errors').html(jQuery.parseJSON(xhr.responseText).errors);
  }
});
});
