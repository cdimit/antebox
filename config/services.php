<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Third Party Services
	|--------------------------------------------------------------------------
	|
	| This file is for storing the credentials for third party services such
	| as Stripe, Mailgun, Mandrill, and others. This file provides a sane
	| default location for this type of information, allowing packages
	| to have a conventional place to find your various credentials.
	|
	*/

	'mailgun' => [
		'domain' => env('MAILGUN_DOMAIN'),
		'secret' => env('MAILGUN_SECRET'),
	],

	'mandrill' => [
		'secret' => '',
	],

	'ses' => [
		'key' => '',
		'secret' => '',
		'region' => 'us-east-1',
	],

	'stripe' => [
		'model'  => 'App\User',
		'secret' => '',
	],

	'facebook' => [
		'client_id' => '482826115237999',
		'client_secret' => 'f90c4e194600f14935d8b1f47968b0c7',
		'redirect' => env('FACEBOOK_REDIRECT')
	],

	'twitter' => [
		'client_id' => 'nTAyChixoXZ1cvCtPdiE1a7EH',
		'client_secret' => 'i2gQujVUWCqkwrNZ8hnyHUXDHuQcyDci6Zl59KA7PG4zZwcckb',
		'redirect' => env('TWITTER_REDIRECT')
	],
];

